<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */

        // App\Providers\DeviceTokenRepositoryServiceProvider::class,

        Laravel\Tinker\TinkerServiceProvider::class,
        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,

        App\Providers\HashidsServiceProvider::class,
        LaravelFCM\FCMServiceProvider::class,

        App\Providers\ActivityRepositoryServiceProvider::class,
        App\Providers\SettingRepositoryServiceProvider::class,
        App\Providers\UserRepositoryServiceProvider::class,
        App\Providers\UserPostRepositoryServiceProvider::class,
        App\Providers\UserSoldPostRepositoryServiceProvider::class,
        App\Providers\BreedRepositoryServiceProvider::class,
        App\Providers\UserFavouritePostRepositoryServiceProvider::class,
        App\Providers\DisciplineRepositoryServiceProvider::class,
        App\Providers\TemperamentRepositoryServiceProvider::class,
        App\Providers\UserRatingRepositoryServiceProvider::class,
        App\Providers\BlogRepositoryServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        LaravelFCM\FCMServiceProvider::class,


        Yajra\DataTables\DataTablesServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        'JWTAuth' => 'Tymon\JWTAuth\Facades\JWTAuth',
        'JWTFactory' => 'Tymon\JWTAuth\Facades\JWTFactory',
        'Image' => Intervention\Image\Facades\Image::class,
        'FCM'      => LaravelFCM\Facades\FCM::class,
        'Stripe' => Cartalyst\Stripe\Laravel\Facades\Stripe::class,
        'Input' => Illuminate\Support\Facades\Input::class,
        
        'DataTables' => Yajra\DataTables\Facades\DataTables::class,

    ],

    'files' => [

        'users' => [
                   'folder_name' => 'users',
                   'public_relative' => 'users/',
                   'full_path' => storage_path('app/public/files/users'),
                   'img_url' => env('IMG_URL').'/storage/app/users/',
                   'url' => env('IMG_URL').'/storage',
                   'width' => 600,
                   'height' => 600,
       ],

        'blogs' => [
            'folder_name' => 'blogs',
            'public_relative' => 'blogs/',
            'full_path' => storage_path('app/public/files/blogs'),
            'img_url' => env('IMG_URL').'/storage/app/blogs/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

       'services' => [

                   'folder_name' => 'services',
                   'public_relative' => 'services/',
                   'full_path' => storage_path('app/public/files/services'),
                   'img_url' => env('IMG_URL').'/storage/app/services/',
                   // 'url' => env('IMG_URL').'/storage',
                   'width' => 600,
                   'height' => 600,
       ],

       'postimages' => [
                   'folder_name' => 'postimages',
                   'public_relative' => 'postimages/',
                   'full_path' => storage_path('app/public/files/postimages'),
                   'img_url' => env('IMG_URL').'/storage/app/postimages/',
                   'url' => env('IMG_URL').'/storage',
                   'width' => 600,
                   'height' => 600,
       ],


        'user_certifications' => [
            'folder_name' => 'user_certifications',
            'public_relative' => 'user_certifications/',
            'full_path' => storage_path('app/public/files/user_certifications'),
            'img_url' => env('IMG_URL').'/storage/app/user_certifications/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'venues' => [
            'folder_name' => 'venues',
            'public_relative' => 'venues/',
            'full_path' => storage_path('app/public/files/venues'),
            'img_url' => env('IMG_URL').'/storage/app/venues/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],



        'coupans' => [
            'folder_name' => 'coupans',
            'public_relative' => 'coupans/',
            'full_path' => storage_path('app/public/files/coupans'),
            'img_url' => env('IMG_URL').'/storage/app/coupans/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'disputeimage' => [
            'folder_name' => 'disputeimage',
            'public_relative' => 'disputeimage/',
            'full_path' => storage_path('app/public/files/disputeimage'),
            'img_url' => env('IMG_URL').'/storage/app/disputeimage/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'venueimage' => [
            'folder_name' => 'venueimage',
            'public_relative' => 'venueimage/',
            'full_path' => storage_path('app/public/files/venueimage'),
            'img_url' => env('IMG_URL').'/storage/app/venueimage/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'events' => [
            'folder_name' => 'events',
            'public_relative' => 'events/',
            'full_path' => storage_path('app/public/files/events'),
            'img_url' => env('IMG_URL').'/storage/app/events/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'groups' => [
            'folder_name' => 'groups',
            'public_relative' => 'groups/',
            'full_path' => storage_path('app/public/files/groups'),
            'img_url' => env('IMG_URL').'/storage/app/groups/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'matches' => [
            'folder_name' => 'matches',
            'public_relative' => 'matches/',
            'full_path' => storage_path('app/public/files/matches'),
            'img_url' => env('IMG_URL').'/storage/app/matches/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'certificates' => [
            'folder_name' => 'certificates',
            'public_relative' => 'certificates/',
            'full_path' => storage_path('app/public/files/certificates'),
            'img_url' => env('IMG_URL').'/storage/app/certificates/',
            'url' => env('IMG_URL').'/storage',
            'width' => 600,
            'height' => 600,
        ],

        'chat' => [
            'folder_name' => 'chat',
            'public_relative' => 'chat/',
            'full_path' => storage_path('app/public/files/chat'),
            'img_url' => env('IMG_URL').'/storage/app/',
            'url' => env('IMG_URL').'/storage',
        ],

    ],


    'hashid' => [
        'encrypt' => env('HASHID_ENCRYPT', true),
        'salt' => env('HASHID_SALT', 'random'),
        'min' => env('HASHID_MIN', 10),
    ],

];
