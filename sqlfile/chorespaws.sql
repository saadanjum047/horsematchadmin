/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.38-MariaDB : Database - chorespaws
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`chorespaws` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `chorespaws`;

/*Table structure for table `accounts` */

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `account_title` varchar(255) DEFAULT NULL,
  `iban_no` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_users_user_id_constraint` (`user_id`),
  CONSTRAINT `accounts_users_user_id_constraint` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `accounts` */

insert  into `accounts`(`id`,`user_id`,`account_title`,`iban_no`,`account_number`,`created_at`,`updated_at`,`deleted_at`) values (1,8,'Jane','4929 9704 3374 2180','4929 9704 3374 2180','2019-03-12 10:18:15','2019-03-12 10:20:54',NULL);

/*Table structure for table `activities` */

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `action` enum('notification','profile_rating','booking_notification','user_verification','user_block','apply','cancelled','accepted','rejected','posted','completed','book','occupied') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `send_to` bigint(20) unsigned NOT NULL,
  `object` enum('notification','booking_notification','profile_rating','user_verification','user_block','appointment','job') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_id` bigint(20) unsigned NOT NULL,
  `actor_id` bigint(20) unsigned NOT NULL,
  `visibility` int(11) NOT NULL DEFAULT '1',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `activities` */

insert  into `activities`(`id`,`user_id`,`action`,`action_id`,`content`,`send_to`,`object`,`notification_id`,`actor_id`,`visibility`,`viewed`,`created_at`,`updated_at`,`deleted_at`) values (1,1,'notification',NULL,NULL,13,'notification',5,1,1,0,'2019-03-18 07:18:01','2019-03-18 07:18:01',NULL),(2,1,'notification',NULL,NULL,14,'notification',5,1,1,0,'2019-03-18 07:18:01','2019-03-18 07:18:01',NULL),(3,1,'notification',NULL,NULL,16,'notification',5,1,1,0,'2019-03-18 07:18:01','2019-03-18 07:18:01',NULL),(4,1,'notification',NULL,NULL,13,'notification',0,1,1,0,'2019-03-18 07:18:51','2019-03-18 07:18:51',NULL),(5,1,'notification',NULL,NULL,14,'notification',0,1,1,0,'2019-03-18 07:18:51','2019-03-18 07:18:51',NULL),(6,1,'notification',NULL,NULL,16,'notification',0,1,1,0,'2019-03-18 07:18:51','2019-03-18 07:18:51',NULL),(7,1,'notification',NULL,NULL,13,'notification',0,1,1,0,'2019-03-19 13:05:40','2019-03-19 13:05:40',NULL),(8,1,'notification',NULL,NULL,14,'notification',0,1,1,0,'2019-03-19 13:05:41','2019-03-19 13:05:41',NULL),(9,1,'notification',NULL,NULL,16,'notification',0,1,1,0,'2019-03-19 13:05:41','2019-03-19 13:05:41',NULL),(10,8,'booking_notification',NULL,NULL,15,'booking_notification',0,8,1,0,'2019-03-20 07:20:49','2019-03-20 07:20:49',NULL),(11,8,'booking_notification',NULL,NULL,15,'booking_notification',0,8,1,0,'2019-03-20 07:27:57','2019-03-20 07:27:57',NULL),(13,8,'booking_notification',NULL,NULL,15,'booking_notification',0,8,1,0,'2019-03-25 06:33:55','2019-03-25 06:33:55',NULL),(14,9,'booking_notification',NULL,NULL,15,'booking_notification',0,9,1,0,'2019-03-29 06:07:34','2019-03-29 06:07:34',NULL),(15,9,'booking_notification',NULL,NULL,15,'booking_notification',0,9,1,0,'2019-03-29 06:48:34','2019-03-29 06:48:34',NULL),(16,9,'booking_notification',NULL,NULL,15,'booking_notification',0,9,1,0,'2019-03-29 08:28:27','2019-03-29 08:28:27',NULL),(17,9,'booking_notification',NULL,NULL,15,'booking_notification',0,9,1,0,'2019-04-02 07:46:29','2019-04-02 07:46:29',NULL),(18,9,'booking_notification',NULL,NULL,15,'booking_notification',0,9,1,0,'2019-04-02 07:58:02','2019-04-02 07:58:02',NULL),(19,9,'booking_notification',NULL,NULL,15,'booking_notification',0,9,1,0,'2019-04-02 08:31:35','2019-04-02 08:31:35',NULL),(20,9,'',NULL,'Your appointment has been cancelled.',15,'',0,9,1,0,'2019-04-02 08:55:22','2019-04-02 08:55:22',NULL),(21,1,'',NULL,'  appointment has been cancelled.',15,'',0,9,1,0,'2019-04-02 08:55:22','2019-04-02 08:55:22',NULL),(22,9,'',NULL,'Your appointment has been cancelled.',15,'',0,9,1,0,'2019-04-02 08:58:58','2019-04-02 08:58:58',NULL),(23,1,'',NULL,'  appointment has been cancelled.',15,'',0,9,1,0,'2019-04-02 08:58:58','2019-04-02 08:58:58',NULL);

/*Table structure for table `admin_queries` */

DROP TABLE IF EXISTS `admin_queries`;

CREATE TABLE `admin_queries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(255) DEFAULT NULL,
  `category` enum('general','dog_sitter','baby_sitter') DEFAULT 'general',
  `query_for` enum('service_provider','customer') DEFAULT 'service_provider',
  `type` enum('general','detailed') DEFAULT 'general',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `admin_queries` */

insert  into `admin_queries`(`id`,`query`,`category`,`query_for`,`type`,`created_at`,`updated_at`,`deleted_at`) values (1,'How many years cleaning experience do you have','general','service_provider','general','2019-03-21 17:49:57','2019-03-21 12:49:57',NULL),(2,'What days of the week are you able to work','general','service_provider','general','2019-03-15 12:52:03','2019-03-15 07:52:03','2019-03-15 07:52:03'),(5,'Do you have any known allergies','general','service_provider','general','2019-03-15 12:52:59','2019-03-15 07:52:59','2019-03-15 07:52:59'),(7,'Do you have any holidays pre booked that may affect your availability  to work','general','service_provider','general','2019-03-15 12:54:59','2019-03-15 07:54:59','2019-03-15 07:54:59'),(10,'Preferred working area','general','service_provider','general','2019-03-21 17:49:57','2019-03-21 12:49:57',NULL),(11,'Do you have any holidays pre booked','general','service_provider','detailed','2019-03-21 17:49:57','2019-03-21 12:49:57',NULL),(12,'Preferred working area','general','service_provider','detailed','2019-03-21 17:49:57','2019-03-21 12:49:57',NULL),(13,'Do you have any holidays pre booked','general','service_provider','general','2019-03-15 12:54:32','2019-03-15 07:54:32','2019-03-15 07:54:32'),(14,'Preferred working area','general','service_provider','general','2019-03-15 12:54:32','2019-03-15 07:54:32','2019-03-15 07:54:32'),(15,'Do you have any holidays pre booked','general','service_provider','general','2019-03-15 12:54:32','2019-03-15 07:54:32','2019-03-15 07:54:32'),(16,'Preferred working area','general','service_provider','general','2019-03-15 12:54:32','2019-03-15 07:54:32','2019-03-15 07:54:32'),(17,'Do you have any holidays pre booked','general','service_provider','general','2019-03-15 12:54:32','2019-03-15 07:54:32','2019-03-15 07:54:32'),(18,'new question','dog_sitter','customer','detailed','2019-03-21 17:49:57','2019-03-21 12:49:57',NULL),(19,'FULL NAME','dog_sitter','customer','detailed','2019-03-21 17:49:57','2019-03-21 12:49:57','2019-03-21 12:49:57');

/*Table structure for table `amount_percentage_slab` */

DROP TABLE IF EXISTS `amount_percentage_slab`;

CREATE TABLE `amount_percentage_slab` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `to` double DEFAULT NULL,
  `from` double DEFAULT NULL,
  `admin_percent` double DEFAULT NULL,
  `currency` enum('US') DEFAULT 'US',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `amount_percentage_slab` */

insert  into `amount_percentage_slab`(`id`,`to`,`from`,`admin_percent`,`currency`,`created_at`,`updated_at`,`deleted_at`) values (3,1000,0.1,1,'US','2019-01-22 14:08:22','2019-01-23 14:18:27',NULL),(7,2000,1001,5,'US','2019-01-23 14:18:48','2019-01-23 14:18:48',NULL);

/*Table structure for table `appointments` */

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(200) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `service_provider_id` int(11) unsigned DEFAULT NULL,
  `service_category_id` int(10) unsigned DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `min_rate_per_hour` double DEFAULT NULL,
  `max_rate_per_hour` double DEFAULT NULL,
  `type` enum('appointment','job') DEFAULT NULL,
  `status` enum('accepted','pending','cancelled','completed','rejected') DEFAULT 'pending',
  `service_provider_type` enum('individual','business') DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `appointment_cancel_time` datetime DEFAULT NULL,
  `time_duration` varchar(150) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `additional_notes` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_appointment_user_id` (`user_id`),
  KEY `book_appointment_service_provider_id` (`service_provider_id`),
  KEY `appointments_service_id` (`service_category_id`),
  CONSTRAINT `appointments_service_id` FOREIGN KEY (`service_category_id`) REFERENCES `sub_service_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `book_appointment_service_provider_id` FOREIGN KEY (`service_provider_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `book_appointment_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `appointments` */

insert  into `appointments`(`id`,`booking_id`,`user_id`,`service_provider_id`,`service_category_id`,`address`,`min_rate_per_hour`,`max_rate_per_hour`,`type`,`status`,`service_provider_type`,`date`,`start_time`,`end_time`,`appointment_cancel_time`,`time_duration`,`latitude`,`longitude`,`additional_notes`,`created_at`,`updated_at`,`deleted_at`) values (7,'APT-7',8,16,2,NULL,NULL,NULL,'appointment','pending',NULL,'2019-03-14','14:00:00','16:00:00',NULL,'2',NULL,NULL,NULL,'2019-03-25 07:33:28','2019-03-26 07:59:22',NULL),(8,'APT-8',8,16,1,NULL,10,15,'job','pending','individual','2019-04-10','14:00:00','15:00:00',NULL,'1',24.862526664644754,67.07211155444384,NULL,'2019-03-26 08:03:13','2019-03-26 08:09:19',NULL),(13,'APT-13',9,16,2,NULL,NULL,NULL,'appointment','pending',NULL,'2019-03-14','14:00:00','16:00:00',NULL,'2',NULL,NULL,NULL,'2019-03-29 06:48:34','2019-04-01 09:30:36',NULL),(16,'APT-16',9,15,2,NULL,NULL,NULL,'appointment','pending',NULL,'2019-03-14','10:00:00','11:00:00',NULL,'1',NULL,NULL,NULL,'2019-03-29 08:28:24','2019-03-29 08:28:26',NULL),(22,'APT-22',9,15,2,NULL,NULL,NULL,'appointment','cancelled',NULL,'2019-03-14','12:00:00','13:00:00','2019-04-03 08:31:35','1',NULL,NULL,NULL,'2019-04-02 08:31:35','2019-04-02 08:58:58',NULL);

/*Table structure for table `book_appointmentss` */

DROP TABLE IF EXISTS `book_appointmentss`;

CREATE TABLE `book_appointmentss` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) DEFAULT NULL,
  `appointment_type` enum('now','later') DEFAULT 'now',
  `user_id` int(11) unsigned DEFAULT NULL,
  `service_provider_id` int(11) unsigned DEFAULT NULL,
  `status` enum('accepted','pending','canceled') DEFAULT 'pending',
  `time_slot_id` int(11) unsigned DEFAULT NULL,
  `time_duration` varchar(150) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `additional_notes` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_appointment_user_id` (`user_id`),
  KEY `book_appointment_service_provider_id` (`service_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `book_appointmentss` */

/*Table structure for table `device_token` */

DROP TABLE IF EXISTS `device_token`;

CREATE TABLE `device_token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `udid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('ios','android') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ios',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_token_user_id` (`user_id`),
  CONSTRAINT `device_token_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `device_token` */

insert  into `device_token`(`id`,`user_id`,`udid`,`token`,`type`,`created_at`,`updated_at`,`deleted_at`) values (2,13,'12356486454654','212312546456546','android','2019-03-11 09:46:39','2019-03-11 09:46:39',NULL),(3,14,'12356486454655','212312546456547','android','2019-03-11 10:14:11','2019-03-11 10:14:11',NULL),(4,16,'12356486454344','212312546456233','android','2019-03-11 10:20:43','2019-03-13 06:52:12',NULL),(5,17,'1D4CB893-6F1C-477A-AFDB-4BE28F8FF0ABC4','e09-wEm8OEU:APA91bFlpXP5fRW7zM1G_9FbGmagUeAl-TVFSdT_Y-tv0eYbDKti_ohLNQ0GtmJZkZIBqbscoMJvlf_cRMy71vYPtyODyAvM3_U_SkM0ACqSfWKDb1awPjD9hYqGlO-B7F7H0MO1UT4d','ios','2019-04-03 10:52:06','2019-04-03 10:52:06',NULL);

/*Table structure for table `favourite_users` */

DROP TABLE IF EXISTS `favourite_users`;

CREATE TABLE `favourite_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `favourite_user` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_favourite_user_id_constraint` (`user_id`),
  KEY `user_favourite_user_favourite_constraint` (`favourite_user`),
  CONSTRAINT `user_favourite_user_favourite_constraint` FOREIGN KEY (`favourite_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_favourite_user_id_constraint` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `favourite_users` */

insert  into `favourite_users`(`id`,`user_id`,`favourite_user`,`created_at`,`updated_at`,`deleted_at`) values (2,9,15,'2019-03-11 12:51:55','2019-03-11 12:51:55',NULL),(3,8,15,'2019-03-12 10:00:43','2019-03-12 10:00:43',NULL);

/*Table structure for table `invoice` */

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) unsigned DEFAULT NULL,
  `invoice_id` varchar(200) DEFAULT NULL,
  `rate_per_hour` double DEFAULT NULL,
  `final_amount` double DEFAULT NULL,
  `total_hours` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_booking_id` (`booking_id`),
  CONSTRAINT `invoice_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `invoice` */

insert  into `invoice`(`id`,`booking_id`,`invoice_id`,`rate_per_hour`,`final_amount`,`total_hours`,`created_at`,`updated_at`,`deleted_at`) values (5,13,'00013',15,30,2,'2019-03-29 06:48:34','2019-03-29 06:48:34',NULL),(7,16,'00016',15,15,1,'2019-03-29 08:28:24','2019-03-29 08:28:24',NULL),(8,7,'00007',10,20,2,'2019-04-01 18:10:50','2019-04-01 18:10:50',NULL),(14,22,'00022',15,15,1,'2019-04-02 08:31:35','2019-04-02 08:31:35',NULL);

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `service_category_id` int(11) unsigned DEFAULT NULL,
  `ser_provider_type` enum('individual','business') DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` enum('pending','accepted','canceled') DEFAULT 'pending',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `min_rate_per_hour` double DEFAULT NULL,
  `max_rate_per_hour` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_user_id` (`user_id`),
  KEY `jobs_service_category_id` (`service_category_id`),
  CONSTRAINT `jobs_service_category_id` FOREIGN KEY (`service_category_id`) REFERENCES `sub_service_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jobs_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `jobs` */

insert  into `jobs`(`id`,`user_id`,`service_category_id`,`ser_provider_type`,`address`,`status`,`latitude`,`longitude`,`min_rate_per_hour`,`max_rate_per_hour`,`date`,`time`,`duration`,`created_at`,`updated_at`,`deleted_at`) values (1,8,1,'individual',NULL,'pending',24.862526664644754,67.07211155444384,10,15,'2019-04-10','14:00:00',NULL,'2019-03-12 10:47:13','2019-03-12 10:47:13',NULL);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notification` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`notification`,`created_at`,`updated_at`) values (10,'test notification','2019-03-18 07:18:00','2019-03-18 07:18:00'),(11,'New Notification','2019-03-18 07:18:50','2019-03-18 07:18:50'),(12,'This is test notification','2019-03-19 13:05:40','2019-03-19 13:05:40'),(13,'Amount of 1.9 have been added to your wallet for service Cleaning paid by Lida Anderson. 5% deducted from payment for admin.','2019-03-20 07:20:49','2019-03-20 07:20:49'),(14,'Amount of 1.9 have been added to your wallet for service Cleaning paid by Lida Anderson. 5% deducted from payment for admin.','2019-03-20 07:27:57','2019-03-20 07:27:57'),(16,'Amount of 1.9 have been added to your wallet for service Baby Sitter paid by Lida Anderson. 5% deducted from payment for admin.','2019-03-25 06:33:55','2019-03-25 06:33:55'),(18,'H. Barrientos Stephen booked appointment for Dog Sitter service.','2019-03-29 06:07:33','2019-03-29 06:07:33'),(19,'H. Barrientos Stephen booked appointment for Dog Sitter service.','2019-03-29 06:48:34','2019-03-29 06:48:34'),(20,'H. Barrientos Stephen booked appointment for Dog Sitter service.','2019-03-29 08:28:26','2019-03-29 08:28:26'),(21,'H. Barrientos Stephen booked appointment for Dog Sitter service.','2019-04-02 07:46:29','2019-04-02 07:46:29'),(22,'H. Barrientos Stephen booked appointment for Dog Sitter service.','2019-04-02 07:58:02','2019-04-02 07:58:02'),(23,'H. Barrientos Stephen booked appointment for Dog Sitter service.','2019-04-02 08:31:35','2019-04-02 08:31:35');

/*Table structure for table `offensive_words` */

DROP TABLE IF EXISTS `offensive_words`;

CREATE TABLE `offensive_words` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `offensive_words` */

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` enum('monthly','yearly','half_yearly') DEFAULT 'monthly',
  `user_type` enum('business','individual') DEFAULT NULL,
  `currency` enum('USD') DEFAULT 'USD',
  `price` float DEFAULT '0',
  `appstore_p_id` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `packages` */

insert  into `packages`(`id`,`name`,`type`,`user_type`,`currency`,`price`,`appstore_p_id`,`created_at`,`updated_at`,`deleted_at`) values (1,'Package 1','half_yearly','business','USD',20,'joZR6JaqDsTUHNRh1FT6','2019-03-08 11:33:50','2019-03-15 10:30:06','2019-03-15 10:30:06'),(2,'Enterprise Package','yearly',NULL,'USD',2000,'joZR6JaqDsTUHNRh1FT6','2019-03-15 10:23:11','2019-03-26 11:56:32','2019-03-26 11:56:32'),(3,'Popular Package','half_yearly',NULL,'USD',1000,'joZR6JaqDsTUHNRh1FT6','2019-03-15 10:23:46','2019-03-26 11:56:19','2019-03-26 11:56:19');

/*Table structure for table `payment_settings` */

DROP TABLE IF EXISTS `payment_settings`;

CREATE TABLE `payment_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `type` enum('minimum_payout','admin_percent') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `payment_settings` */

insert  into `payment_settings`(`id`,`amount`,`type`,`created_at`,`updated_at`,`deleted_at`) values (2,5,'admin_percent','2019-03-13 17:19:25','2019-04-03 08:17:45',NULL),(3,5,'minimum_payout','2019-03-13 17:19:28','2019-04-03 08:17:45',NULL);

/*Table structure for table `payout_requests` */

DROP TABLE IF EXISTS `payout_requests`;

CREATE TABLE `payout_requests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `currency` enum('USD') DEFAULT 'USD',
  `status` enum('rejected','pending','complete') DEFAULT 'pending',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payout_requests_user_id` (`user_id`),
  CONSTRAINT `payout_requests_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `payout_requests` */

insert  into `payout_requests`(`id`,`user_id`,`amount`,`currency`,`status`,`created_at`,`updated_at`,`deleted_at`) values (2,15,'1','USD','complete','2019-03-21 06:53:02','2019-03-21 08:32:10',NULL),(3,15,'1','USD','complete','2019-03-25 12:10:04','2019-03-25 12:10:47',NULL),(4,15,'1','USD','complete','2019-03-26 06:59:29','2019-03-26 07:05:19',NULL),(5,8,'1','USD','pending','2019-03-27 08:28:22','2019-03-27 08:28:22',NULL);

/*Table structure for table `preferred_location` */

DROP TABLE IF EXISTS `preferred_location`;

CREATE TABLE `preferred_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `postal_code` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `preferred_location_user_id` (`user_id`),
  CONSTRAINT `preferred_location_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `preferred_location` */

insert  into `preferred_location`(`id`,`user_id`,`latitude`,`longitude`,`postal_code`,`created_at`,`updated_at`,`deleted_at`) values (3,13,24.8607,67.0011,'74600','2019-03-11 09:46:39','2019-03-11 09:46:39',NULL),(4,13,17.385,78.4867,'17000','2019-03-11 09:46:39','2019-03-11 09:46:39',NULL),(5,14,24.8607,67.0011,'74600','2019-03-11 10:14:10','2019-03-11 10:14:10',NULL),(6,14,17.385,78.4867,'17000','2019-03-11 10:14:10','2019-03-11 10:14:10',NULL),(7,15,24.8607,67.0011,'74600','2019-03-11 10:20:42','2019-03-11 10:20:42',NULL),(8,15,17.385,78.4867,'17000','2019-03-11 10:20:43','2019-03-11 10:20:43',NULL),(9,16,24.8607,67.0011,'74600','2019-03-13 06:52:12','2019-03-13 06:52:12',NULL),(10,16,17.385,78.4867,'17000','2019-03-13 06:52:12','2019-03-13 06:52:12',NULL);

/*Table structure for table `promocodes` */

DROP TABLE IF EXISTS `promocodes`;

CREATE TABLE `promocodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promocode` varchar(10) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `redeem_type` enum('single','multiple') DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `promocodes` */

insert  into `promocodes`(`id`,`promocode`,`discount`,`redeem_type`,`status`,`created_at`,`updated_at`,`deleted_at`) values (22,'cmXf32',2.00,'single',1,'2019-03-22 07:13:07','2019-03-22 07:13:07',NULL),(23,'WYKUGk',3.00,'single',1,'2019-03-22 07:17:40','2019-03-22 07:26:49',NULL),(24,'6G5EXl',4.00,'single',0,'2019-03-22 07:27:12','2019-03-22 07:27:12',NULL);

/*Table structure for table `queries_answers` */

DROP TABLE IF EXISTS `queries_answers`;

CREATE TABLE `queries_answers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `query_id` int(11) unsigned DEFAULT NULL,
  `query` varchar(255) DEFAULT NULL,
  `detailed_answer` text,
  `general_answer` enum('yes','no') DEFAULT NULL,
  `type` enum('general','detailed') DEFAULT 'general',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_providers_answers_provider_id` (`user_id`),
  KEY `queries_answers_query_id` (`query_id`),
  CONSTRAINT `queries_answers_query_id` FOREIGN KEY (`query_id`) REFERENCES `admin_queries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_providers_answers_provider_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `queries_answers` */

insert  into `queries_answers`(`id`,`user_id`,`query_id`,`query`,`detailed_answer`,`general_answer`,`type`,`created_at`,`updated_at`,`deleted_at`) values (2,15,NULL,'How many years cleaning experience do you have ','1 year',NULL,'general','2019-03-12 17:49:55','2019-03-12 17:49:55',NULL),(3,15,NULL,'What days of the week are you able to work','test',NULL,'general','2019-03-12 17:58:39','2019-03-12 17:58:39',NULL),(4,16,NULL,'How many years cleaning experience do you have','2 years',NULL,'general','2019-03-13 06:52:11','2019-03-13 06:52:11',NULL),(5,16,NULL,'What days of the week are you able to work','monday, tuesday',NULL,'general','2019-03-13 06:52:12','2019-03-13 06:52:12',NULL),(6,8,1,'How many years cleaning experience do you have','3',NULL,'detailed','2019-03-25 06:51:39','2019-03-25 06:51:39',NULL),(7,8,11,'Do you have any holidays pre booked',NULL,'yes','general','2019-03-25 06:51:39','2019-03-25 06:51:39',NULL);

/*Table structure for table `ratings` */

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `reciever` int(11) unsigned DEFAULT NULL,
  `rating` varchar(30) DEFAULT NULL,
  `appointment_id` int(11) unsigned DEFAULT NULL,
  `feedback_note` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ratings_user_id` (`user_id`),
  KEY `ratings_appointment_id` (`appointment_id`),
  CONSTRAINT `ratings_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ratings_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ratings` */

insert  into `ratings`(`id`,`user_id`,`reciever`,`rating`,`appointment_id`,`feedback_note`,`created_at`,`updated_at`,`deleted_at`) values (1,8,15,'4.5',22,'Satisfied with work.','2019-03-11 17:52:52','0000-00-00 00:00:00',NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

/*Table structure for table `service_categories` */

DROP TABLE IF EXISTS `service_categories`;

CREATE TABLE `service_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `service_categories` */

insert  into `service_categories`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'Home Services','2019-03-07 11:31:15','2019-03-20 13:23:17',NULL),(2,'Pet Service','2019-03-07 11:39:22','2019-03-08 13:37:08','2019-03-08 13:37:08'),(5,'Pet Services','2019-03-08 13:41:08','2019-03-08 13:41:23','2019-03-08 13:41:23'),(6,'Pet Services','2019-03-08 13:41:55','2019-03-08 13:49:04','2019-03-08 13:49:04'),(7,'Test','2019-03-08 13:48:54','2019-03-15 07:55:25','2019-03-15 07:55:25'),(8,'Test','2019-03-08 13:49:04','2019-03-08 13:50:00','2019-03-08 13:50:00'),(9,'ok','2019-03-08 13:55:03','2019-03-08 13:55:20','2019-03-08 13:55:20'),(10,'Pet Service','2019-03-15 07:55:25','2019-03-20 13:23:17',NULL),(11,'Car services','2019-03-20 13:23:17','2019-03-20 13:23:17',NULL);

/*Table structure for table `service_provider_categories` */

DROP TABLE IF EXISTS `service_provider_categories`;

CREATE TABLE `service_provider_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `price_per_hour` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_provider_categories_user_id` (`user_id`),
  KEY `service_provider_categories_cat_id` (`category_id`),
  CONSTRAINT `service_provider_categories_cat_id` FOREIGN KEY (`category_id`) REFERENCES `sub_service_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_provider_categories_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `service_provider_categories` */

insert  into `service_provider_categories`(`id`,`category_id`,`user_id`,`price_per_hour`,`created_at`,`updated_at`,`deleted_at`) values (1,2,13,NULL,'2019-03-11 09:46:39','2019-03-11 09:46:39',NULL),(2,2,14,NULL,'2019-03-11 10:14:10','2019-03-11 10:14:10',NULL),(3,3,14,NULL,'2019-03-11 10:14:11','2019-03-11 10:14:11',NULL),(4,2,15,NULL,'2019-03-11 10:20:43','2019-03-11 10:20:43',NULL),(5,3,15,NULL,'2019-03-11 10:20:43','2019-03-11 10:20:43',NULL),(6,2,16,NULL,'2019-03-13 06:52:12','2019-03-13 06:52:12',NULL),(7,3,16,NULL,'2019-03-13 06:52:12','2019-03-13 06:52:12',NULL);

/*Table structure for table `service_provider_reference` */

DROP TABLE IF EXISTS `service_provider_reference`;

CREATE TABLE `service_provider_reference` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `position` varchar(150) DEFAULT NULL,
  `contact_number` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_provider_reference_user_id` (`user_id`),
  CONSTRAINT `service_provider_reference_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `service_provider_reference` */

insert  into `service_provider_reference`(`id`,`user_id`,`email`,`position`,`contact_number`,`created_at`,`updated_at`,`deleted_at`) values (3,13,'patrickjewell@mailinator.com','Teacher','03334444444','2019-03-11 09:46:39','2019-03-11 09:46:39',NULL),(4,13,'tammyterrel@mailinator.com','Bank officer','03334444444','2019-03-11 09:46:39','2019-03-11 09:46:39',NULL),(5,14,'patrickjewell@mailinator.com','Teacher','03334444444','2019-03-11 10:14:10','2019-03-11 10:14:10',NULL),(6,14,'tammyterrel@mailinator.com','Bank officer','03334444444','2019-03-11 10:14:10','2019-03-11 10:14:10',NULL),(7,15,'patrickjewell@mailinator.com','Teacher','033356683756','2019-03-11 10:20:43','2019-03-11 10:20:43',NULL),(8,15,'tammyterrel@mailinator.com','Bank officer','033356683756','2019-03-11 10:20:43','2019-03-11 10:20:43',NULL),(9,16,'patrickjewell@mailinator.com','Teacher','033356683756','2019-03-13 06:52:12','2019-03-13 06:52:12',NULL),(10,16,'tammyterrel@mailinator.com','Bank officer','033356683756','2019-03-13 06:52:12','2019-03-13 06:52:12',NULL);

/*Table structure for table `service_provider_schedule` */

DROP TABLE IF EXISTS `service_provider_schedule`;

CREATE TABLE `service_provider_schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_provider_id` int(11) unsigned DEFAULT NULL,
  `day` enum('monday','tuesday','wednesday','thursday','friday','saturday','sunday') DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `status` enum('on','off') DEFAULT 'on',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `time_slots_service_provider_id` (`service_provider_id`),
  CONSTRAINT `time_slots_service_provider_id` FOREIGN KEY (`service_provider_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `service_provider_schedule` */

insert  into `service_provider_schedule`(`id`,`service_provider_id`,`day`,`starttime`,`endtime`,`status`,`created_at`,`updated_at`,`deleted_at`) values (4,15,'tuesday','12:00:00','16:00:00','on','2019-03-12 08:46:10','2019-03-12 08:47:35',NULL),(5,15,'thursday','10:00:00','16:00:00','on','2019-03-14 12:13:47','2019-03-14 12:13:47',NULL),(6,16,'thursday','10:00:00','16:00:00','on','2019-03-26 12:36:56','2019-03-26 12:36:56',NULL),(7,16,'wednesday','10:00:00','16:00:00','on','2019-03-26 13:08:49','2019-03-26 13:08:49',NULL);

/*Table structure for table `sub_service_categories` */

DROP TABLE IF EXISTS `sub_service_categories`;

CREATE TABLE `sub_service_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `service_keyword` enum('baby_sitter','dog_sitter','other') DEFAULT 'other',
  `price_per_hour` double DEFAULT NULL,
  `currency` enum('USD') DEFAULT 'USD',
  `description` text,
  `image` varchar(50) DEFAULT NULL,
  `is_default` tinyint(3) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_service_categories_category_id` (`category_id`),
  CONSTRAINT `sub_service_categories_category_id` FOREIGN KEY (`category_id`) REFERENCES `service_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sub_service_categories` */

insert  into `sub_service_categories`(`id`,`category_id`,`service_name`,`service_keyword`,`price_per_hour`,`currency`,`description`,`image`,`is_default`,`created_at`,`updated_at`,`deleted_at`) values (1,1,'Baby Sitter','baby_sitter',2,'USD','Room, Hall Cleaning',NULL,1,'2019-03-11 14:35:23','2019-03-20 10:44:10',NULL),(2,2,'Dog Sitter','dog_sitter',15,'USD','plumbing',NULL,1,'2019-03-11 15:07:13','2019-03-11 15:07:13',NULL),(3,1,'Gardening','other',25,'USD','Gardening',NULL,0,'2019-03-11 15:09:01','2019-03-11 15:09:01',NULL),(4,2,'Laundry Service','other',10,'USD','Laundry Service','iZmTybaZD2XofeXvIKe8xfJell0c5KomblolUrKS.jpeg',0,'2019-03-12 10:44:16','2019-03-20 12:11:26','2019-03-20 12:11:26'),(5,1,'Dish Washing','other',12,'USD','Dish Washing','4XHP3bu0dZByXh85vI2cxTrK10ypJKpxUmVVDDLC.jpeg',0,'2019-03-20 11:13:46','2019-03-20 12:10:44','2019-03-20 12:10:44'),(6,1,'Dish Washing','other',13,'USD','Dish Washing','aPmGSoFHGXwgFXb7NrUF9ukrb9Nord86WVuZzUcd.jpeg',0,'2019-03-20 12:12:27','2019-03-20 12:12:57','2019-03-20 12:12:57');

/*Table structure for table `subscriptions` */

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) unsigned NOT NULL,
  `package_id` int(11) unsigned NOT NULL,
  `price` float DEFAULT '0',
  `transaction_id` varchar(255) NOT NULL DEFAULT '0',
  `productId` varchar(255) DEFAULT NULL,
  `transactionDate` varchar(255) DEFAULT NULL,
  `transactionReceipt` text,
  `payment_type` enum('braintree','inApp','no-payment','stripe') DEFAULT 'braintree',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `subscriptions` */

/*Table structure for table `supports` */

DROP TABLE IF EXISTS `supports`;

CREATE TABLE `supports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(255) DEFAULT NULL,
  `type` enum('about','terms','privacy') DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `supports` */

insert  into `supports`(`id`,`query`,`type`,`description`,`created_at`,`updated_at`,`deleted_at`) values (1,'Who We Are','about','About Us content goes here','2019-01-22 00:26:47','2019-03-25 12:53:56',NULL),(2,'Our Vision','about','Chores and Paws','2019-01-22 00:27:13','2019-03-25 12:53:56',NULL),(3,'Terms and Conditions','terms','Terms and Conditions','2019-01-22 00:27:41','2019-03-25 12:54:19',NULL),(4,'Privacy Policy','privacy','Privacy Policy','2019-01-22 00:28:21','2019-03-25 12:54:41',NULL);

/*Table structure for table `user_package` */

DROP TABLE IF EXISTS `user_package`;

CREATE TABLE `user_package` (
  `int` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `package_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`int`),
  KEY `user_package_user_id` (`user_id`),
  KEY `user_package_package_id` (`package_id`),
  CONSTRAINT `user_package_package_id` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_package_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_package` */

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_role_user_id_constraint` (`user_id`),
  KEY `roles_user_role_id_constraint` (`role_id`),
  CONSTRAINT `roles_user_role_id_constraint` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_user_id_constraint` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_roles` */

/*Table structure for table `user_social_accounts` */

DROP TABLE IF EXISTS `user_social_accounts`;

CREATE TABLE `user_social_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `social_network` enum('facebook','google','twitter') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'facebook',
  `social_network_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_social_accounts_user_id_foreign` (`user_id`),
  CONSTRAINT `user_social_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_social_accounts` */

insert  into `user_social_accounts`(`id`,`user_id`,`social_network`,`social_network_id`,`created_at`,`updated_at`,`deleted_at`) values (1,8,'facebook','162221808455568654645645','2019-03-08 07:40:19','2019-03-08 07:40:19',NULL),(2,14,'facebook','162221808455568654645644','2019-03-11 10:14:11','2019-03-11 10:14:11',NULL),(3,15,'facebook','162221808455568654645344','2019-03-11 10:20:43','2019-03-11 10:20:43',NULL);

/*Table structure for table `user_wallet` */

DROP TABLE IF EXISTS `user_wallet`;

CREATE TABLE `user_wallet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `amount` varchar(50) DEFAULT '0',
  `currency` enum('USD','PKR') DEFAULT 'USD',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_wallet_user_id` (`user_id`),
  CONSTRAINT `user_wallet_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `user_wallet` */

insert  into `user_wallet`(`id`,`user_id`,`amount`,`currency`,`created_at`,`updated_at`,`deleted_at`) values (1,8,'496','USD','2019-03-13 06:52:11','2019-03-14 10:21:23',NULL),(2,16,'0','USD','2019-03-13 06:52:11','2019-03-13 06:52:11',NULL),(3,15,'12.200000000000001','USD','2019-03-13 06:52:11','2019-03-26 07:05:19',NULL),(4,9,'515','USD','2019-03-13 06:52:11','2019-04-02 08:58:58',NULL),(5,17,'0','USD','2019-04-03 10:52:06','2019-04-03 10:52:06',NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `address` text,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `access_token` text,
  `profile_image` varchar(50) DEFAULT NULL,
  `profile_image_head` varchar(50) DEFAULT NULL,
  `profile_image_shoulder` varchar(50) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `cv_image` varchar(50) DEFAULT NULL,
  `identity_card_image` varchar(50) DEFAULT NULL,
  `user_type` enum('paneluser','customer','service_provider') DEFAULT NULL,
  `signup_via` enum('email','facebook','twitter','google') DEFAULT 'email',
  `service_provider_type` enum('individual','business') DEFAULT NULL,
  `notification_status` enum('on','off') DEFAULT 'on',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `show_username` enum('on','off') DEFAULT 'on',
  `login_at` datetime DEFAULT NULL,
  `recover_password_key` text,
  `recover_attempt_at` datetime DEFAULT NULL,
  `is_block` tinyint(3) DEFAULT '0',
  `verified` tinyint(3) DEFAULT '0',
  `is_available` enum('on','off') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`username`,`address`,`email`,`password`,`access_token`,`profile_image`,`profile_image_head`,`profile_image_shoulder`,`phone_number`,`cv_image`,`identity_card_image`,`user_type`,`signup_via`,`service_provider_type`,`notification_status`,`latitude`,`longitude`,`show_username`,`login_at`,`recover_password_key`,`recover_attempt_at`,`is_block`,`verified`,`is_available`,`created_at`,`updated_at`,`deleted_at`) values (1,'Chores and Paws Admin','chorespawsadmin',NULL,NULL,'chorespaws@mailinator.com','$2y$10$LV1V1zYtE1cfXGu4kmFm5O18Z7oH0B/L.QBjWGLKfHZqTHsTmf98K','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvY2hvcmVzcGF3cy9wdWJsaWMvYXBpL2xvZ2luIiwiaWF0IjoxNTU3MzkxODA3LCJleHAiOjE1NjAwMTk4MDcsIm5iZiI6MTU1NzM5MTgwNywianRpIjoiZjVpdkJ6alRSR0RXa2dMdCJ9.FXij1fbUHI0HhqDzQ-0eJLO4lIFNsAXKHx_kJjdnzVw',NULL,NULL,NULL,NULL,NULL,NULL,'paneluser','email',NULL,'on',NULL,NULL,'on','2019-05-09 08:50:07','NncRGOsgRfAfjizcLBIOdQ2l2YzBx9ZEPJWszKEmLai5EW22lgn9Lf3AnUGW','2019-03-07 11:15:45',0,0,NULL,'2019-01-17 05:03:37','2019-05-09 08:50:07',NULL),(8,'Lida','Anderson','lidaanderson',NULL,'lidaanderson@mailinator.com','$2y$10$KJB.Qt5kQPdcgYroxGlZFe15FEdPLbHhaUFGnFJqE00yFqtLhK2eW','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjgsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvY2hvcmVzcGF3cy9wdWJsaWMvYXBpL2xvZ2luIiwiaWF0IjoxNTUzNTg3MzY4LCJleHAiOjE1NTYyMTUzNjgsIm5iZiI6MTU1MzU4NzM2OCwianRpIjoidERjMjhoeEhHNzJ3a2JvcCJ9.NmZCR-dLMOzXAG4Wwtbxgo-7JjCoAX6lbSWc8NfyLO4','KQ0avM6srBGqlIYydUPjKzsah4pFUPAjSkgPheth.jpeg',NULL,NULL,NULL,NULL,NULL,'customer','facebook',NULL,'on',24.8607,67.0011,'on','2019-03-26 08:02:48',NULL,NULL,1,0,NULL,'2019-03-08 07:40:19','2019-03-26 11:42:18',NULL),(9,'H. Barrientos','Stephen','hbarrientosstephen',NULL,'alexhopkins@mailinator.com','$2y$10$KJB.Qt5kQPdcgYroxGlZFe15FEdPLbHhaUFGnFJqE00yFqtLhK2eW','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjksImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvY2hvcmVzcGF3cy9wdWJsaWMvYXBpL2xvZ2luIiwiaWF0IjoxNTU0MTk0MDEwLCJleHAiOjE1NTY4MjIwMTAsIm5iZiI6MTU1NDE5NDAxMCwianRpIjoiWkFEaWFINFFkSHF3bmdDNSJ9.JDDElrvDe8lHq89dKT3id7u_Gk1iIkA_AsRFEjae8jE','6xYBxxxFiUjjURnRyIGL3aS0V8vnWkwsqhdVmZid.jpeg',NULL,NULL,'5376122131',NULL,NULL,'customer','email',NULL,'on',24.8607,67.0011,'on','2019-04-02 08:33:30','gPvnDOx3m2JlU8clcPy4cdPyJQ3xGXgyk0VmtoXA4CNZiOZh9wY82F7OFsaa','2019-03-28 11:07:17',0,0,NULL,'2019-03-08 10:55:19','2019-04-02 08:33:30',NULL),(10,'Kyle','N. Durgin','kylendurgin',NULL,'kylendurgin@mailinator.com','$2y$10$WoTvnYjyDqfFjQDUELcr5eNDJiYE8xvJgeGVu.cG4gwxYeGAit0c2','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9yZWdpc3RlciIsImlhdCI6MTU1MjA0Mzc5MCwiZXhwIjoxNTU0NjcxNzkwLCJuYmYiOjE1NTIwNDM3OTAsImp0aSI6IlZPTzhLNFdvMlA3RmFxQ1QifQ.TSikoHy6aNmbsQ2tI4bcmvQugUi9WyU26Mg1X7tiKkI',NULL,NULL,NULL,NULL,NULL,NULL,'customer','email',NULL,'on',24.8607,67.0011,'on','2019-03-08 11:16:30',NULL,NULL,0,0,NULL,'2019-03-08 11:16:30','2019-03-26 11:43:20',NULL),(11,'Robert','Bernard','robertbernard',NULL,'robertbernard@mailinator.com','$2y$10$EG6TdUKNpF.6QmQaU/jRpO1QCynlUzajdmfUvb9Cpjf5mBwRLp/x.','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjExLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9yZWdpc3RlciIsImlhdCI6MTU1MjA0MzkwNSwiZXhwIjoxNTU0NjcxOTA1LCJuYmYiOjE1NTIwNDM5MDUsImp0aSI6IjdSMDliZ29IYklxQUZweUoifQ.RkvI70G0hnbldSTi94bBh-GArZXPu5jjTG_kCexYziM','k6X2zoHGZTBivficUwXJcpPXrvh1Ito6oQeZNHFm.jpeg',NULL,NULL,NULL,NULL,NULL,'customer','email',NULL,'on',24.8607,67.0011,'on','2019-03-08 11:18:25',NULL,NULL,0,0,NULL,'2019-03-08 11:18:25','2019-03-26 06:55:50',NULL),(13,'Steven','J. Hoag','stevenjhoag',NULL,'jhoagsteven@mailinator.com','$2y$10$zF9egchygpVH3cMoizPx7.LSpfoFJXDT0N7vYM6ecHXoQcMq9i8l.','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9yZWdpc3Rlci1zZXJ2aWNlLXByb3ZpZGVyIiwiaWF0IjoxNTUyMjk3NTk5LCJleHAiOjE1NTQ5MjU1OTksIm5iZiI6MTU1MjI5NzU5OSwianRpIjoiaUR6ZWZXSzB6VERSSFVZViJ9.iPYpEGqXdkR4qL-ztNPWZqO7s0-k6j-2W1TyuL652T8','13D39ewkUVbEhiq4398ETu5JejZ0OxEoWM3WQJAQ.jpeg','LyPFRyrMJkjgqx0i41Bvryskn8HrbHCpbK5Y7FZG.png','7zZcuycb8QnyAwexK5M7WfLnUs4j61K0JMyoijP6.jpeg',NULL,'DKhJgAvU8LOR0H8i9zZEtmW0qdiFG66dp8UBe8DE.jpeg','50TeDetxOVxYuqZlZeVmnY6eLJT1cfQ5z00G7tQA.jpeg','service_provider','email','individual','on',24.8607,67.0011,'on','2019-03-11 09:46:39',NULL,NULL,0,1,NULL,'2019-03-11 09:46:39','2019-03-18 06:34:17',NULL),(14,'Katherine','Ray','katherineray',NULL,'raykatherine@mailinator.com','$2y$10$MP2cN6i6WsDK18y0Jjtuy.sAtAuaUlWzqtfIAOPGfUZvQug7rvKuK','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE0LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9yZWdpc3Rlci1zZXJ2aWNlLXByb3ZpZGVyIiwiaWF0IjoxNTUyMjk5NDIxLCJleHAiOjE1NTQ5Mjc0MjEsIm5iZiI6MTU1MjI5OTQyMSwianRpIjoicm9RVDJmdzhkN2RFUHY5OSJ9.19xRUtZN-8iUvp3dhgPvzp_VSiC4zL7A0Ih7H95D7nI','2jPpU0pb4i8EYqVF1tLTnhFqFXjWQJHMPWpfo2hS.jpeg','OVlEkfxw7dUtj2HHsDDKlQZ5eSRD57XBLfDCkz51.png','a8P2B3umaXpXsX6IPKavhL2hQIYuIC1rchEmTUGb.jpeg',NULL,'FSB9O3kyrgdT3EQVwL8yC4SSSd6SWD3J1CoKIUaS.jpeg','vWqGZn8tHAkZDbRI4YLb5RrwW0ZNdII2tnEQ1G8a.jpeg','service_provider','facebook','individual','on',24.8607,67.0011,'on','2019-03-11 10:17:00',NULL,NULL,0,0,NULL,'2019-03-11 10:14:10','2019-03-11 10:17:01',NULL),(15,'Celeste','Strong','celestestrong',NULL,'strongceleste@mailinator.com','$2y$10$X01PBgQOqE9QTEuUCYPL3.EQ.JIe/FBvma2Zz7y05TDLfLCxjNEsq','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9sb2dpbiIsImlhdCI6MTU1MzUxNTc2NSwiZXhwIjoxNTU2MTQzNzY1LCJuYmYiOjE1NTM1MTU3NjUsImp0aSI6Ing5R2ZXS0FhVk1XbjdjbGUifQ.ptHncEp9SIlJLkH3HL3P0v9pNBTkgvUHUta8HriitWs','kgGeTLXQP5XQKowEFLG3YEerE8Bc80ObNV7An6xJ.jpeg','x0HXVgMdkpi44E3A8bXBvcxbnOFZ9Lz7DSPj1ipF.png','LnUfJS2prM7rPpjz9W7Vw7JDkttKTc9VVYtMqIpB.jpeg',NULL,'YchYLmwGXAIr9nA73lDXoToKdpxhwcMjNc5lI8kV.jpeg','7BbGqcA1k7XQbwP7NZVkOpskZLOVjvlkLpp3C8XT.jpeg','service_provider','facebook','individual','on',24.8607,67.0011,'on','2019-03-25 12:09:25',NULL,NULL,1,0,NULL,'2019-03-11 10:20:42','2019-03-26 11:20:05',NULL),(16,'Ryan','Mitchell','ryanmitchell',NULL,'mitchellryan@mailinator.com','$2y$10$grZv.kihGzORSXIzB1taye9fcOKuNqfJmAVxmk33dx6Cglx7zti5m','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9sb2dpbiIsImlhdCI6MTU1Mzg1NzM2NCwiZXhwIjoxNTU2NDg1MzY0LCJuYmYiOjE1NTM4NTczNjQsImp0aSI6InBwS2x5TGdkblB1Y0UyaUYifQ.DDP_udO0lMkb7k3yEgnX35Bza8_tM042mlPSG_zD81Y','l3JVW4LiqOJxSUS0QqqTUiPnb5QjM94763hSemkQ.jpeg','wW5eS9JJoXGEey9bpdhyxVuR3JULRJDB7jTR0Jrx.jpeg','powVC540Lt6gZkh0PXZiAiXqrXNNn6NrgloWLWGR.png',NULL,'hpbvnnKzeoWzWbXvKKMVOpd1yYBzZV9KXSIu8WB9.jpeg','enVEu88uS7mSrru6JYWyEpvR7fS6LtLwpZPO7e4h.jpeg','service_provider','email','individual','on',24.8607,67.0011,'on','2019-03-29 11:02:44','RTRRrN0e28GRGuJEfREuaQ4fjagXyPaWdCYnUqSCHEPNEXhPyZ2xf8aTa8pP','2019-03-27 13:11:31',0,0,NULL,'2019-03-13 06:52:11','2019-03-29 11:02:44',NULL),(17,'Rebecca','R. Reams','rebeccarreams',NULL,'rebeccareams@mailinator.com','$2y$10$eGlTBftMqeDm5HLn72wECeuX8Ib4AbKrOa0jEEgd1o/Nhj9vY5r1i','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE3LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2Nob3Jlc3Bhd3MvcHVibGljL2FwaS9yZWdpc3RlciIsImlhdCI6MTU1NDI4ODcyNiwiZXhwIjoxNTU2OTE2NzI2LCJuYmYiOjE1NTQyODg3MjYsImp0aSI6ImZEUXZkelRlbVJtOEFuMG0ifQ.AHg1VeMxZazbRE55i9LKQo3M2JbZYSgJlwCZXtwtDg4',NULL,NULL,NULL,NULL,NULL,NULL,'customer','email',NULL,'on',24.8607,67.0011,'on','2019-04-03 10:52:06',NULL,NULL,0,0,NULL,'2019-04-03 10:52:06','2019-04-03 10:52:06',NULL);

/*Table structure for table `wallet_transactions` */

DROP TABLE IF EXISTS `wallet_transactions`;

CREATE TABLE `wallet_transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wallet_id` int(11) unsigned NOT NULL,
  `amount` double DEFAULT '0',
  `type` enum('deposit','withdrawl') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wallet_transaction_is` (`wallet_id`),
  CONSTRAINT `wallet_transaction_is` FOREIGN KEY (`wallet_id`) REFERENCES `user_wallet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

/*Data for the table `wallet_transactions` */

insert  into `wallet_transactions`(`id`,`wallet_id`,`amount`,`type`,`created_at`,`updated_at`,`deleted_at`) values (21,3,19,'deposit','2019-03-14 07:56:28','2019-03-14 07:56:28',NULL),(23,3,19,'deposit','2019-03-14 07:58:40','2019-03-14 07:58:40',NULL),(29,1,20,'withdrawl','2019-03-14 08:12:28','2019-03-14 08:12:28',NULL),(30,3,19,'deposit','2019-03-14 08:12:28','2019-03-14 08:12:28',NULL),(49,1,4,'withdrawl','2019-03-14 10:12:04','2019-03-14 10:12:04',NULL),(50,3,1.9,'deposit','2019-03-14 10:12:04','2019-03-14 10:12:04',NULL),(51,1,4,'withdrawl','2019-03-14 10:13:23','2019-03-14 10:13:23',NULL),(52,3,1.9,'deposit','2019-03-14 10:13:24','2019-03-14 10:13:24',NULL),(53,1,4,'withdrawl','2019-03-14 10:18:37','2019-03-14 10:18:37',NULL),(54,3,1.9,'deposit','2019-03-14 10:18:37','2019-03-14 10:18:37',NULL),(55,1,4,'withdrawl','2019-03-14 10:21:23','2019-03-14 10:21:23',NULL),(56,3,1.9,'deposit','2019-03-14 10:21:23','2019-03-14 10:21:23',NULL),(58,3,1.9,'deposit','2019-03-14 10:32:15','2019-03-14 10:32:15',NULL),(60,3,1.9,'deposit','2019-03-14 10:34:11','2019-03-14 10:34:11',NULL),(61,3,1.9,'deposit','2019-03-14 10:37:08','2019-03-14 10:37:08',NULL),(64,3,1.9,'deposit','2019-03-14 10:45:12','2019-03-14 10:45:12',NULL),(65,3,1.9,'deposit','2019-03-20 07:20:49','2019-03-20 07:20:49',NULL),(66,3,1.9,'deposit','2019-03-20 07:27:57','2019-03-20 07:27:57',NULL),(68,3,1,'withdrawl','2019-03-21 08:32:10','2019-03-21 08:32:10',NULL),(71,3,1.9,'deposit','2019-03-25 06:33:55','2019-03-25 06:33:55',NULL),(72,3,1,'withdrawl','2019-03-25 12:10:47','2019-03-25 12:10:47',NULL),(73,3,1,'withdrawl','2019-03-26 07:05:19','2019-03-26 07:05:19',NULL),(78,4,30,'withdrawl','2019-03-29 06:07:33','2019-03-29 06:07:33',NULL),(79,4,30,'withdrawl','2019-03-29 06:48:34','2019-03-29 06:48:34',NULL),(81,4,30,'deposit','2019-03-29 12:09:38','2019-03-29 12:09:38',NULL),(89,4,30,'deposit','2019-04-01 09:30:36','2019-04-01 09:30:36',NULL),(92,4,15,'withdrawl','2019-04-02 07:46:28','2019-04-02 07:46:28',NULL),(96,4,15,'withdrawl','2019-04-02 07:58:02','2019-04-02 07:58:02',NULL),(98,4,15,'withdrawl','2019-04-02 08:31:35','2019-04-02 08:31:35',NULL),(99,4,15,'deposit','2019-04-02 08:55:22','2019-04-02 08:55:22',NULL),(100,4,15,'deposit','2019-04-02 08:58:58','2019-04-02 08:58:58',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
