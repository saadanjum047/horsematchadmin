<?php 

namespace App\Traits;

Trait paymentTrait {
    public function token() {
        $token = \Braintree_ClientToken::generate();
        return $token;
    }

    public function transaction($nonce){

        try {

            $result = \Braintree_Transaction::sale([
                'amount' => '10.00',
                'paymentMethodNonce' => $nonce,
                'options' => [
                    'submitForSettlement' => True
                ]
            ]);

            if ($result->success) 
                return true;

            return false;
        } catch (Exception $e) {
            dd($e);
        }

    }
}

class PaymentGateway
{
	use paymentTrait;
}