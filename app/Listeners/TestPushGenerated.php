<?php

namespace App\Listeners;

use App\Events\TestPush;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

use App\Data\Models\DeviceToken;
use App\Data\Models\User;
use App\Data\Models\Activity;
use App\Data\Models\UserPayment;
use App\Data\Models\UserEvent;
use App\Data\Models\UserTask;
use App\Helpers\Helper;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM,App;


class TestPushGenerated implements ShouldQueue{

    public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(TestPush $event){

        $user_id = $event->user_id;
        $view = $event->view;
        $type = $event->type;

        //dd($user_id, $view, $type);


        $currentDate = @date("Y-m-d");

        $admin_detail = User::where('user_type', 'admin')->first();
        $admin_id = isset($admin_detail->id) ? $admin_detail->id : 0;
        $admin_id = Helper::hashid_encode($admin_id);

        $title = "Test Push";
        //$title = isset($record->title) ? $record->title : "";

        $fcm_title = "Test Push";
        $fcm_message = "This is actipass test push";
        $fcm_datetime = @date("Y-m-d H:i:s");

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        //$click_url = url('/myaccount/myevents');
        //$app_icon = url('/public/images/vp-Icon.png');
        $notification_count = 1;

        $notificationBuilder = new PayloadNotificationBuilder($fcm_title);
        $notificationBuilder->setTitle($fcm_title)
            ->setSound('default')
            //->setClickAction($click_url)
            //->setIcon($app_icon)
            ->setBadge(1)
            ->setBody($fcm_message);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
            'view' => $view,
            'view_id' => Helper::hashid_encode($user_id),
            'title' => $fcm_title,
            'message' => $fcm_message,
            'body' => $fcm_message,
            'created_at' => $fcm_datetime,
            'type'=>$type,
            //'custom_url'=>$click_url,
            //'click_action'=>$click_url,
            //'icon'=>$app_icon,
            'notification_count'=>$notification_count
        ]]);
        $dataBuilder->setData(['custom' => [
            'view' => $view,
            'view_id' => Helper::hashid_encode($user_id),
            'title' => $fcm_title,
            'message' => $fcm_message,
            'body' => $fcm_message,
            'created_at' => $fcm_datetime,
            'type'=>$type,
            //'custom_url'=>$click_url,
            //'click_action'=>$click_url,
            //'icon'=>$app_icon,
            'notification_count'=>$notification_count
        ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();


        //$tokenIds = [$student_id,$teacher_id];
        //$tokens = DeviceToken::join("users","users.id","=","device_token.user_id")->where("users.notification",1)->whereIn("user_id",$tokenIds)->get(['token']);
        $tokens = DeviceToken::join("users","users.id","=","user_devices.user_id")->where("user_id",$user_id)->get(['token', 'type']);
        $device_tokens = count($tokens);
        foreach ($tokens as $value) {
            //$device_tokens[] = $value->token;

            //dd($value);
            //validation
            if ($value->type == "ios") {
                $downstreamResponse = FCM::sendTo($value->token, $option, $notification, $data);

                \Log::debug($downstreamResponse->numberSuccess());
                \Log::debug($option);
                \Log::debug($data);
                \Log::debug($value->type);
                \Log::debug($user_id);

            } else {
                $downstreamResponse = FCM::sendTo($value->token, $option, null, $data);


                \Log::debug($downstreamResponse->numberSuccess());
                \Log::debug($option);
                \Log::debug($data);
                \Log::debug($value->type);
                \Log::debug($user_id);

            }

            //dd($downstreamResponse);

        }
        //dd(count($device_tokens));
        if(count($tokens) > 0){

            //dd('oka');
            //$downstreamResponse = FCM::sendTo($device_tokens, $option, $notification, $data);
            //$success = $downstreamResponse->numberSuccess();

//            $activity = new Activity();
//            $activity->from_id = $admin_detail->id;
//            $activity->to_id  = $record->user_id;
//            $activity->module  =  'task';
//            $activity->action = 'reminder';
//            $activity->title = $fcm_title;
//            $activity->message = $fcm_message;
//            $activity->object_id = $record_id;
//            $activity->created_date = $currentDate;
//            $activity->save();


            return true;
        }else{
            return false;
        }

    }

    public function queue($queue, $command, $data) {
//      $queueName = 'tree_user_registration_'.config('app.queue_registration_fix');
//      $queue->pushOn($queueName, $command, $data);
    }
}
