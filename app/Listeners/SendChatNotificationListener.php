<?php
namespace App\Listeners;

use App\Events\SendChatNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

use App\Data\Models\DeviceToken;
use App\Data\Models\User;
use App\Data\Models\Activity;
use App\Data\Models\UserPayment;
use App\Data\Models\UserEvent;
use App\Data\Models\UserTask;
use App\Helpers\Helper;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Image, Storage;
use FCM,App;
use App\Data\Models\UserDevice;

class SendChatNotificationListener implements ShouldQueue
{

     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */

    public function handle(SendChatNotification $notification)
    {


	    $event = $notification->event;
        $event = (object) $event;

        $success = false;
        $user=User::where(['chat_id'=>$event->chat_id])->get(['id','username'])->first();

        $postTitle = $event->title;
        $postBody = $event->body;

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high');

        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound('default')
                            ->setBody($postBody);
                         //   ->setClickAction('MY_KEY_NOTIFICATION');

        $dataBuilder = new PayloadDataBuilder();
        $user=User::where('chat_id',$event->chat_id)->first();
        $dataBuilder->addData(['data' => [
                                    'view_id' => $event->view_id,
                                    'title'=>$postTitle,
                                    'body'=>$postBody,
                                    'type' => 'chat',
                                    'id' => $event->id,
                                    'user_image' => $event->user_image
                    ]]);
        // $dataBuilder->setData(['data' => [
        //       'view_id' => $event->user_id,
        //       'title'=>$postTitle,
        //       'body'=>$postBody,
        //       'type' => 'chat',

        //             ]]);


        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $deviceToken = User::join("user_devices","user_devices.user_id","=","users.id")
            ->where('users.chat_id', $event->chat_id)->where('user_devices.status',1)
//            ->where('users.is_notification',1)
            ->get(['user_devices.token','users.id', 'user_devices.status']);
        //dd($deviceToken);
     //   $deviceToken=UserDevice::where(['user_id'=>$event->buyer_id,'status'=>1])->get();

        $token = array();
        if(count($deviceToken) > 0){
            foreach ($deviceToken as $value) {
                if($value->token != NULL){
                    if($value->status == 1){
                        $token[] = $value->token;
                    }
                }

            }


                if(count($token) > 0){
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                $success = $downstreamResponse->numberSuccess();
                \Log::debug($token);
                \Log::debug($option);
                \Log::debug($notification);
                \Log::debug($data);
                \Log::debug($success);
                return true;
            }

        }
        if($success){
           return true;
        }
        return true;
    }


public function queue($queue, $command, $data) {
       $queueName = 'chores_paws_event_end_reminder_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
