<?php
namespace App\Listeners;

use App\Events\SendPostNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

use App\Data\Models\DeviceToken;
use App\Data\Models\User;
use App\Data\Models\Activity;
use App\Data\Models\UserPayment;
use App\Data\Models\UserEvent;
use App\Data\Models\UserTask;
use App\Helpers\Helper;
use App\Data\Models\UserFollower;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM,App;
use App\Data\Models\UserDevice;
use App\Data\Models\Breed;

class SendPostNotificationListener implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */

    public function handle(SendPostNotification $notification)
    {

	    $event = $notification->event;
	    $event->user_id=Helper::hashid_decode($event->user_id);
        $event->breed_id=Helper::hashid_decode($event->breed_id);

        $breed= Breed::where(['id'=>$event->breed_id])->get(['id','title'])->first();

        $success = false;
        $postTitle = "A new horse ".Helper::handle_string($breed['title'])." has been posted";
        $postBody = $event->address." that you might be interested in";

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound('default')
                            ->setBody($postBody);
                         //   ->setClickAction('MY_KEY_NOTIFICATION');

        $dataBuilder = new PayloadDataBuilder();
        $payloadarray = [
            'view_id'=>$event->id,
            'title'=>$postTitle,
            'body'=>$postBody,
            'type' => 'addhorse',
            ];
        // $dataBuilder->addData(['custom' => [
        //                             'view_id' => $event->user_id,
        //                             'title'=>$postTitle,
        //                             'body'=>$postBody,
        //                             'type' => 'notification',

        //             ]]);
        // $dataBuilder->setData(['custom' => [

        //       'view_id' => $event->user_id,
        //       'title'=>$postTitle,
        //       'body'=>$postBody,
        //       'type' => 'notification',
        //             ]]);
        $dataBuilder->addData(['data'=>json_encode($payloadarray)]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();

//        $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//            ->where('users.id', $event->user_id )
//            ->get(['device_token.token','users.id', 'users.notification_status']);
//        $user = User::where('id',$event->user_id)->where('is_notification',1)->first();
        $deviceToken = UserFollower::where('user_followers.user_id',$event->user_id)
            ->join("user_devices","user_devices.user_id","=","user_followers.follower_id")
            ->join("users","user_followers.user_id","=","users.id")
//            ->where('user_devices.status',1)
//            ->where('user_followers.user_id','=',$event->user_id)
//            ->where('user_id','!=',$event->user_id)
//            ->where('users.is_notification',1)
            ->get();

        \Log::debug($deviceToken);
//        $deviceToken=UserDevice::where(['status'=>1])->where('user_id','!=',$event->user_id)->get()->toArray();

        $token = array();
        if(count($deviceToken) > 0){
            foreach ($deviceToken as $value) {
                if($value['token'] != NULL){
                    if($value['status'] == 1){
                        $token[] = $value['token'];
                    }
                }


            }

           // $deviceToken=array();
//
//            $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//                ->where('users.id', $event->user_id )->groupBy('device_token.user_id')
//                ->get(['device_token.token','users.id', 'users.notification_status']);


//
//            $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//                ->where('users.id', $event->user_id )->groupBy('device_token.user_id')
//                ->get(['device_token.user_id']);

           // dd($deviceToken);


            if(count($deviceToken) > 0){
                foreach ($deviceToken as $val) {
                 // if($value->notification_status == 1) {
           // $input['id'] = isset($event->id) ? hashid_decode($event->id) : "";


                        $activity = new Activity();
                        $activity->user_id = $val['user_id'];
                        $activity->action = 'post';
                        $activity->object = "post";
                        $activity->title = $postTitle;
                        $activity->content = $postBody;
                    //    $activity->notification_id = hashid_decode($event->id);
                        $activity->action_id = Helper::hashid_decode($event->id);
                        $activity->send_to = $val['user_id'];
                        $activity->actor_id = $event->user_id;
                        $activity->save();

                 //   }



                }
           }



                if(count($token) > 0){
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $success = $downstreamResponse->numberSuccess();
                \Log::debug($token);
                \Log::debug($option);
                \Log::debug($notification);
                \Log::debug($data);
                \Log::debug($success);
                return true;
            }

        }
        if($success){
           return true;
        }
        return true;
    }


public function queue($queue, $command, $data) {
       $queueName = 'chores_paws_event_end_reminder_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
