<?php
namespace App\Listeners;

use App\Events\SendPostUploadNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

use App\Data\Models\DeviceToken;
use App\Data\Models\User;
use App\Data\Models\Activity;
use App\Data\Models\UserPayment;
use App\Data\Models\UserEvent;
use App\Data\Models\UserTask;
use App\Helpers\Helper;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM,App;
use App\Data\Models\UserDevice;


class SendPostUploadNotificationListener implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */

    public function handle(SendPostUploadNotification $notification)
    {

	    $event = $notification->event;
        $event->user_id=Helper::hashid_decode($event->user_id);

        $success = false;
        $postTitle = "Your post has been uploaded!";
        $postBody = "View Details";
       // $post_id= Helper::hashid_decode($event->id);

        //\Log::debug('postId'.$post_id);


        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound('default')
                            ->setBody($postBody);
                         //   ->setClickAction('MY_KEY_NOTIFICATION');

        $dataBuilder = new PayloadDataBuilder();
        $payloadarray = [
            'view_id' => $event->id,
            'title' => $postTitle,
            'body' => $postBody,
            'type' => 'notification'
            ];
        // $dataBuilder->addData(['custom' => [
        //                             'view_id' => $event->id,
        //                             'title'=>$postTitle,
        //                             'body'=>$postBody,
        //                             'type' => 'notification',

        //             ]]);
        // $dataBuilder->setData(['custom' => [

        //       'view_id' => $event->id,
        //       'title'=>$postTitle,
        //       'body'=>$postBody,
        //       'type' => 'notification',
        //             ]]);

        $dataBuilder->addData(['data'=>json_encode($payloadarray)]);
        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();


        $deviceToken=UserDevice::where(['status'=>1])->where('user_id','=',$event->user_id)->get();

        $token = array();
        if(count($deviceToken) > 0){
            foreach ($deviceToken as $value) {
                if($value->token != NULL){
                    if($value->status == 1){
                        $token[] = $value->token;
                    }
                }

            }


                if(count($token) > 0){
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $success = $downstreamResponse->numberSuccess();
                \Log::debug($token);
                \Log::debug($option);
                \Log::debug($notification);
                \Log::debug($data);
                \Log::debug($success);
                return true;
            }

        }
        if($success){
           return true;
        }
        return true;
    }


public function queue($queue, $command, $data) {
       $queueName = 'chores_paws_event_end_reminder_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
