<?php
namespace App\Listeners;

use App\Events\SendRatingNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

use App\Data\Models\DeviceToken;
use App\Data\Models\User;
use App\Data\Models\Activity;
use App\Data\Models\UserPayment;
use App\Data\Models\UserEvent;
use App\Data\Models\UserTask;
use App\Helpers\Helper;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM,App;
use App\Data\Models\UserDevice;


class SendRatingNotificationListener implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */

    public function handle(SendRatingNotification $notification)
    {


	    $event = $notification->event;

        $success = false;
        $user=User::where(['id'=>$event->user_id])->get(['id','username'])->first();

        $postTitle = "You have been rated";
        $postBody = $user['username']." has rated you with ".$event->rating." stars";

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound('default')
                            ->setBody($postBody);
                         //   ->setClickAction('MY_KEY_NOTIFICATION');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['data' => [
                                    'view_id' => $event->user_id,
                                    'title'=>$postTitle,
                                    'body'=>$postBody,
                                    'type' => 'rating',
                    ]]);
        // $dataBuilder->setData(['custom' => [

        //       'view_id' => $event->user_id,
        //       'title'=>$postTitle,
        //       'body'=>$postBody,
        //       'type' => 'notification',
        //             ]]);



        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();

//        $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//            ->where('users.id', $event->user_id )
//            ->get(['device_token.token','users.id', 'users.notification_status']);
        $activity = new Activity();
        $activity->user_id = $event->buyer_id;
        $activity->action = 'rating';
        $activity->object = "rating";
        $activity->title = $postTitle;
        $activity->content = $postBody;
        //    $activity->notification_id = hashid_decode($event->id);
        $activity->action_id = Helper::hashid_decode($event->id);
        $activity->send_to = $event->buyer_id;
        $activity->actor_id = $event->user_id;
        $activity->save();
        $deviceToken = User::join("user_devices","user_devices.user_id","=","users.id")
            ->where('users.id', $event->buyer_id )->where('user_devices.status',1)
            ->where('users.is_notification',1)
            ->get(['user_devices.token','users.id', 'user_devices.status']);

//        $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//            ->where('users.is_notification',1)
//            ->where('users.id', $event->buyer_id )
//            ->get(['device_token.token','users.id', 'users.notification_status']);
//        $deviceToken=UserDevice::where(['user_id'=>$event->buyer_id,'status'=>1])->get();

        $token = array();
        if(count($deviceToken) > 0){
            foreach ($deviceToken as $value) {
                if($value->token != NULL){
                    if($value->status == 1){
                        $token[] = $value->token;
                    }
                }


            }

            $deviceToken=array();
//
//            $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//                ->where('users.id', $event->user_id )->groupBy('device_token.user_id')
//                ->get(['device_token.token','users.id', 'users.notification_status']);


//
//            $deviceToken = User::join("device_token","device_token.user_id","=","users.id")
//                ->where('users.id', $event->user_id )->groupBy('device_token.user_id')
//                ->get(['device_token.user_id']);

           // dd($deviceToken);


           // if(count($deviceToken) > 0){
             //   foreach ($deviceToken as $value) {
//                    if($value->notification_status == 1) {
           // $input['id'] = isset($event->id) ? hashid_decode($event->id) : "";


               //     }

                //}
  //          }



                if(count($token) > 0){
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $success = $downstreamResponse->numberSuccess();
                \Log::debug($token);
                \Log::debug($option);
                \Log::debug($notification);
                \Log::debug($data);
                \Log::debug($success);
                return true;
            }

        }
        if($success){
           return true;
        }
        return true;
    }


public function queue($queue, $command, $data) {
       $queueName = 'chores_paws_event_end_reminder_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
