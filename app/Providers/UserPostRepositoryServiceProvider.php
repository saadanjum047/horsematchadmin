<?php

namespace App\Providers;

use App\Data\Models\UserPost;
use App\Data\Repositories\UserPostRepository;
use Illuminate\Support\ServiceProvider;

//UserPostRepositoryServiceProvider
class UserPostRepositoryServiceProvider extends ServiceProvider{

    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot(){
    //
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register(){
        $this->app->bind('UserPostRepository', function () {
        return new UserPostRepository( new UserPost);
        });
    }

}//UserPostRepositoryServiceProvider class ends here