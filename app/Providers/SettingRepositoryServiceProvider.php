<?php

namespace App\Providers;

use App\User;
use App\Data\Repositories\SettingRepository;
use Illuminate\Support\ServiceProvider;
use App\Data\Models\Setting;

//SettingRepositoryServiceProvider
class SettingRepositoryServiceProvider extends ServiceProvider {
    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot(){//
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register(){
        $this->app->bind('SettingRepository', function () {
        return new SettingRepository( new Setting );
        });
    }

}//SettingRepositoryServiceProvider class ends here