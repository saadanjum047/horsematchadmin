<?php

namespace App\Providers;

use App\Data\Models\Blog;
use App\Data\Repositories\BlogRepository;
use Illuminate\Support\ServiceProvider;

//BlogRepositoryServiceProvider
class BlogRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('BlogRepository', function () {
            return new BlogRepository( new Blog);
        });
    }

}//BlogRepositoryServiceProvider class ends here