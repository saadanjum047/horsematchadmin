<?php

namespace App\Providers;

use App\Data\Models\Temperament;
use App\Data\Repositories\TemperamentRepository;
use Illuminate\Support\ServiceProvider;

//TemperamentRepositoryServiceProvider
class TemperamentRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('TemperamentRepository', function () {
            return new TemperamentRepository( new Temperament);
        });
    }

}//TemperamentRepositoryServiceProvider class ends here