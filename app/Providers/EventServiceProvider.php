<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\TestPush' => [
            'App\Listeners\TestPushGenerated',
        ],
        'App\Events\SendRatingNotification' => [
            'App\Listeners\SendRatingNotificationListener',
        ],
        'App\Events\SendPostNotification' => [
            'App\Listeners\SendPostNotificationListener',
        ]
        ,
        'App\Events\SendPostUploadNotification' => [
            'App\Listeners\SendPostUploadNotificationListener',
        ],
        'App\Events\SendChatNotification' => [
            'App\Listeners\SendChatNotificationListener',
        ]
    ];


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
