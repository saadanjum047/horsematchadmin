<?php

namespace App\Providers;

use App\Data\Models\Discipline;
use App\Data\Repositories\DisciplineRepository;
use Illuminate\Support\ServiceProvider;

//DisciplineRepositoryServiceProvider
class DisciplineRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('DisciplineRepository', function () {
            return new DisciplineRepository( new Discipline);
        });
    }

}//DisciplineRepositoryServiceProvider class ends here