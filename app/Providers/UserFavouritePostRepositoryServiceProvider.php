<?php

namespace App\Providers;

use App\Data\Models\UserFavouritePost;
use App\Data\Repositories\UserFavouritePostRepository;
use Illuminate\Support\ServiceProvider;

//UserFavouritePostRepositoryServiceProvider
class UserFavouritePostRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('UserFavouritePostRepository', function () {
            return new UserFavouritePostRepository( new UserFavouritePost);
        });
    }

}//UserFavouritePostRepositoryServiceProvider class ends here
