<?php

namespace App\Providers;

use App\Data\Models\UserSoldPost;
use App\Data\Repositories\UserSoldPostRepository;
use Illuminate\Support\ServiceProvider;

//UserSoldPostRepositoryServiceProvider
class UserSoldPostRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('UserSoldPostRepository', function () {
            return new UserSoldPostRepository( new UserSoldPost);
        });
    }

}//UserSoldPostRepositoryServiceProvider class ends here