<?php

namespace App\Providers;

use stdClass;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;

@define('BRAINTREE_ENV', 'sandbox');
@define('BRAINTREE_MERCHANT_ID', '5r5k3smdry2n2py5');
@define('BRAINTREE_PUBLIC_KEY', 'rjycjg5tbvv32x9h');
@define('BRAINTREE_PRIVATE_KEY', 'c39f6b9bffb0e1b2b13f4194a7e67173');

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        \Braintree_Configuration::environment(BRAINTREE_ENV);
        \Braintree_Configuration::merchantId(BRAINTREE_MERCHANT_ID);
        \Braintree_Configuration::publicKey(BRAINTREE_PUBLIC_KEY);
        \Braintree_Configuration::privateKey(BRAINTREE_PRIVATE_KEY);

        // Queue::after(function (JobProcessed  $event) {

        //     DB::table('logger')->insert([
        //         'data' => json_encode($event),
        //     ]);
        //     Log::info(json_encode($event));        
        // });

        // Queue::after(function (JobProcessed $event) {

        //     Log::info('[QUEUE COMPLETE]', $event->job->getName());
 
        //  });
 
        // Queue::after(function ($connection, $job, $data) {
        //     $all = new stdClass();
        //     $all->connection = $connection;
        //     $all->job = $job;
        //     $all->data = $data;
        //     DB::table('logger')->insert([
        //         'data' => json_encode($all),
        //     ]);
        //     Log::info(json_encode($all));
        // });

        
            // Log::info(json_encode($all));

            // Log::info(json_encode($all));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
