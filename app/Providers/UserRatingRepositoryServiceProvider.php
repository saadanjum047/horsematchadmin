<?php

namespace App\Providers;

use App\Data\Models\UserRating;
use App\Data\Repositories\UserRatingRepository;
use Illuminate\Support\ServiceProvider;

//UserRatingRepositoryServiceProvider
class UserRatingRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('UserRatingRepository', function () {
            return new UserRatingRepository( new UserRating);
        });
    }

}//UserRatingRepositoryServiceProvider class ends here
