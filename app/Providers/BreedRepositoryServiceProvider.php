<?php

namespace App\Providers;

use App\Data\Models\Breed;
use App\Data\Repositories\BreedRepository;
use Illuminate\Support\ServiceProvider;

//BreedRepositoryServiceProvider
class BreedRepositoryServiceProvider extends ServiceProvider{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind('BreedRepository', function () {
            return new BreedRepository( new Breed);
        });
    }

}//BreedRepositoryServiceProvider class ends here