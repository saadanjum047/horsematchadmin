<?php

namespace App\Jobs;

use App\Data\Models\User;
use App\Helpers\Helper;
use checkmobi\CheckMobiRest;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SendNotification implements ShouldQueue
{
    // 
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $title;
    protected $description;
    protected $promotion;
    protected $index;
    protected $user;
    public function __construct($title,$description , $promotion , $user , $index)
    {
        //
        $this->title = $title;
        $this->description = $description;
        $this->promotion = $promotion;
        $this->user = $user;
        $this->index = $index;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Fire event queue
        $api = new CheckMobiRest("9AE029A2-7EF6-4F0D-8621-3D7C3627D551");
        
            // $api->SendSMS(array("from" => "HorseMatch", "to" => '+923147637613', "text" => $this->description));
            Log::info('Queue Fired' );
            Log::info($this->user->phone );
            // dd($this->user->phone);
            // if($this->index < 1){

            // $api->SendSMS(array("from" => "HorseMatch", "to" => $this->user->phone, "text" => $this->description));
            
                // Helper::sendEmail($user->email,"Promotion Sent By Horsematch Admin",$this->description);
            // Log::info('Queue Fired');
            
            // $response = $client->SendSMS([
                //     "to" => "your_number_here", 
                //     "text" => "Hello world !",
            //     "platform" => "ios"
            // ]);
            // $api->SendSMS(array("from" => "HorseMatch", "to" => $user->phone, "text" => $this->description));
            // Helper::sendEmail($user->email,"Promotion Sent By Horsematch Admin",$this->description);
        // }
         
        
        $this->promotion->sent_users = $this->index + 1;
        $this->promotion->save();
    }

    
}
