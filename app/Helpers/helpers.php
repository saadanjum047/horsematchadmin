<?php

if (! function_exists('hashid_encode')) {
    function hashid_encode($id)
    {
        if (config('app.hashid.encrypt') == false) {
            return $id;
        }
        return app('Hashids')
            ->encode($id);
    }
}

if (! function_exists('hashid_decode')) {
    function hashid_decode($str)
    {
        if (config('app.hashid.encrypt') == false) {
            return $str;
        }
        $decode = app('Hashids')->decode($str);
        return (int) reset($decode);
    }
}

if (! function_exists('makePayment')) {
    function makePayment($data = [])
    {
        require_once 'stripe/init.php'; 
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here https://dashboard.stripe.com/account/apikeys
        // \Stripe\Stripe::setApiKey("sk_test_xJ3xuQd5eOck8UKb7IMk17Zp"); //Live Key pk_test_giXvBDf6FIUIEoHEKwpD3eKr 

        // Pk Ali Account pk_test_giXvBDf6FIUIEoHEKwpD3eKr 
        // Sk Ali Account sk_test_xJ3xuQd5eOck8UKb7IMk17Zp
        \Stripe\Stripe::setApiKey("sk_test_xJ3xuQd5eOck8UKb7IMk17Zp");

        
        // Get the credit card details submitted by the form
        $token = $data['stripeToken'];
        
        // Create the charge on Stripe's servers - this will charge the user's card
        try {
          $charge = \Stripe\Charge::create(array(
            "amount" => $data['amount'], // amount in cents, again
            "currency" => $data['currency'],
            "source" => $token,
            "description" => $data['description']
            ));
        } catch(\Stripe\Error\Card $e) {
          // The card has been declined
        }
    }
}


