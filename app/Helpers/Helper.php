<?php

namespace App\Helpers;

use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
//use Intervention\Image\ImageManagerStatic as Image;
//use Illuminate\Support\Facades\App;
//use Illuminate\Support\Facades\Config;
//use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Crypt;
use Hashids\Hashids;
use checkmobi\CheckMobiRest;


class Helper {

    //random_number
    public static  function random_number($length = 6){
        $min = pow(10, $length - 1) ;
        $max = pow(10, $length) - 1;
        return mt_rand($min, $max);
    }//random_number

    public static function ipinfo(){
        $ip = Self::getIp();

        $ch = curl_init();
        // 110.37.223.78
        curl_setopt($ch, CURLOPT_URL, 'http://ipinfo.io/'.$ip);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        curl_close($ch);

        $data = json_decode($result);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $data;
        } else {
            return false;
        }
    }    

    public static function googleTimeEstimate($lat1,$long1,$lat2,$long2){
 
        $co_ordinates = $lat1.','.$long1.'&destinations='.$lat2.','.$long2;
        $para = $co_ordinates.'&key='.config('app.google.key');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$para);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result);
        if (json_last_error() == JSON_ERROR_NONE) {
            return $data;
        } else {
            return false;
        }
    }

    public static function getGeoIpAddress(){
        $geoIpAccount = config('app.geo_ip_account');
        $client = new Client('' . $geoIpAccount['user_id'] . '', '' . $geoIpAccount['license_key'] . '');
        $ip = self::getIp();

        try {
            $record = session('geo_ip_location');
            if (empty($record)) {
                $record = $client->city($ip);
            }
            if ($record) {
                session(['geo_ip_location' => $record]);
                return (object)[
                    'city' => $record->city->name,
                    'state' => $record->subdivisions[0]->name,
                    'country' => $record->country->name,
                    'continent' => $record->continent->name,
                ];
            }
        } catch (\Exception $ex) {
        }
    }

    /**
     * @param $request
     * @param array $data
     * @return mixed
     */
    public static function addParamsToRequest($request, array $data){
        $environment = App::environment();
        $client_secret = Config::get('constants.passport.dev.client_secret');
        $client_id = Config::get('constants.passport.dev.client_id');

        if ($environment == 'local') {
            $client_secret = Config::get('constants.passport.local.client_secret');
            $client_id = Config::get('constants.passport.local.client_id');
        }
        $request->request->add(['username' => $data['email'], 'client_id' => $client_id, 'client_secret' => $client_secret, 'grant_type' => 'password']);
        return $request;
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit){

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "M") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public static function uploadImage(array $data){
        $path = public_path().'/users/'.$data['user_id'].'/images';

        if(!File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $image = $data['image'];
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = $path .'/'. $filename;
        Image::make($image->getRealPath())->orientate()->save($path);

        return '/users/'.$data['user_id'].'/images/'.$filename;
    }


    public static function generateDateRange(Carbon $start_date, Carbon $end_date){

        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            if($date->isWeekday())
            {
                $dates[]     = $date->format('Y-m-d');
            }
            //$date->format('Y-m-d');
        }

        return $dates;
    }

    public static function getIp() {
        
        $ip = request()->server('HTTP_X_REAL_IP');
        if (empty($ip)) {
            $ip = request()->server('HTTP_X_FORWARDED_FOR');
            if (empty($ip)) {
                $ip = request()->ip();
            }
        }
        return $ip;
    }

    //base64_to_jpeg
    public function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb");
        $data = explode(',', $base64_string);
        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);
        return $output_file;
    }//base64_to_jpeg method ends here

    //generateRandomString
    public function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }//generateRandomString method ends here

    //timeAgo
    public function timeAgo($time_ago){

        $cur_time 	= time();
        $time_elapsed 	= $cur_time - $time_ago;
        $seconds 	= $time_elapsed ;
        $minutes 	= round($time_elapsed / 60 );
        $hours 		= round($time_elapsed / 3600);
        $days 		= round($time_elapsed / 86400 );
        $weeks 		= round($time_elapsed / 604800);
        $months 	= round($time_elapsed / 2600640 );
        $years 		= round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "$seconds seconds ago";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hours ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }//timeAgo method ends here

    //getaddress
    public  function getaddress($lat,$lng){
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
        $json = @file_get_contents($url);
        $data=json_decode($json);
        $status = $data->status;
        if($status=="OK")
            return $data->results[0]->formatted_address;
        else
            return false;
    }//getaddress method ends here

    //findDistance
    public function findDistance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }

    }//findDistance method ends here

    //apiResponse
    public function apiResponse($response){
        //json headers
        @header('Cache-Control: no-cache, must-revalidate');
        @header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        @header('Content-type: application/json');
        //json output
        echo @json_encode($response, JSON_PRETTY_PRINT);
        exit();
    }//apiResponsemethod ends hre

    //jsonResponse
    public function jsonResponse($response, $code){
        //json headers
        @header('Cache-Control: no-cache, must-revalidate');
        @header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        @header('Content-type: application/json');
        //status code
        header('Status: '.$code);
        //json output
        echo @json_encode($response, JSON_PRETTY_PRINT);
        exit();
    }//jsonResponse method ends hre

    //pagination
    public  static function pagination_backup($obj){

        $data['pagination'] = [];
        $data['pagination']['total'] = $obj->total();
        $data['pagination']['current'] = $obj->currentPage();
        $data['pagination']['first'] = 1;
        $data['pagination']['last'] = $obj->lastPage();
        if($obj->hasMorePages()) {
            if ( $obj->currentPage() == 1) {
                $data['pagination']['previous'] = 1;
            } else {
                $data['pagination']['previous'] = $obj->currentPage()-1;
            }
            $data['pagination']['next'] = $obj->currentPage()+1;
        } else {
            $data['pagination']['previous'] = $obj->currentPage()-1;

            $data['pagination']['next'] =  -1;
        }
        if ($obj->lastPage() > 1) {
            $data['pagination']['pages'] = range(1,$obj->lastPage());
        } else {
            $data['pagination']['pages'] = [1];
        }

        return $data;
    }//pagination

    //pagination
    public  static function pagination($obj){

        $data = [];
        $data['total'] = $obj->total();
        $data['current'] = $obj->currentPage();
        $data['first'] = 1;
        $data['last'] = $obj->lastPage();
        if($obj->hasMorePages()) {
            if ( $obj->currentPage() == 1) {
                $data['previous'] = 1;
            } else {
                $data['previous'] = $obj->currentPage()-1;
            }
            $data['next'] = $obj->currentPage()+1;
        } else {
            $data['previous'] = $obj->currentPage()-1;

            $data['next'] =  -1;
        }
        if ($obj->lastPage() > 1) {
            $data['pages'] = range(1,$obj->lastPage());
        } else {
            $data['pages'] = [1];
        }

        return $data;
    }//pagination

    //strip_tags_app_content
    public  static function strip_tags_app_content($html){
        return strip_tags($html,'<div><span><ul><ol><li><br><hr><b><i><h1><h2><h3><h4><blockquote><sup><sub><a><u><font><p><table><tbody><tr><td><th><tfoot><img><iframe>');
    }

    //shortDescription
    public static function shortDescription($input){

        if (strlen($input) > 0) {
            $string = substr($input, 0, 25).'...';
        } else {
            $string = "N/A";
        }

        return $string;
    }


    public static function handle_string($string){
        $breaks = array("\r", "\n", "/", "\t", "&amp;", "amp;", "nbsp;", "#39;", "quot;", "&");
        $response = str_ireplace($breaks, "", $string);
        return $response;
    }

    /**
     * @param  string
     * @return encrypted string
     */
    public static function customEncrypt($string){
           
            $output = Crypt::encryptString($string);
            return $output;
       
    }

    /**
     * @param  string
     * @return decrypted string
     */
    public static function customDecrypt($string){
            $output = Crypt::decryptString($string);
            return $output;
        
    }

    public static function hashid_encode($id){
        if (config('app.hashid.encrypt') == false) {
            return $id;
        }
        return app('Hashids')
            ->encode($id);
    }

    public static function hashid_decode($str) {
        if (config('app.hashid.encrypt') == false) {
            return $str;
        }
        $decode = app('Hashids')->decode($str);
        return (int) reset($decode);
    }

    //getLatLong
    public function getLatLong($address){

        $google_key = "AIzaSyClS0RUrk3JDJ6nCksNC_2hd2SNQYpGa9A";
        $geocodeFromAddr= file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.$google_key.'&sensor=false');
        $output = json_decode($geocodeFromAddr);

        //Get latitude and longitute from json data
        $data['latitude']  = isset($output->results[0]->geometry->location->lat) ? $output->results[0]->geometry->location->lat : "";
        $data['longitude'] = isset($output->results[0]->geometry->location->lng) ? $output->results[0]->geometry->location->lng : "";

        return $data;

    }//getLatLong method ends here

    //latlong_curl
    public function latlong_curl($address){

        $googleKey = "AIzaSyALl80_UG3mhY1jLREaHSqHWn6RQSHbcV0";

        $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?key='.$googleKey.'&sensor=false&address=' . urlencode($address);

        $post_data = "";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url );
        curl_setopt($ch, CURLOPT_POST, 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

    //if (curl_errno($ch)) {
    //    print curl_error($ch);
    //}
        curl_close($ch);

        $output = json_decode($result);
    //Get latitude and longitute from json data
        $data['latitude']  = $output->results[0]->geometry->location->lat;
        $data['longitude'] = $output->results[0]->geometry->location->lng;

        return $data;

    }//latlong_curl method ends here

    public static function sendPush($deviceToken, $type, $message){

        // API access key from Google FCM App Console
        //define( 'API_ACCESS_KEY', 'AAAAwFWu_7g:APA91bEu6MtTGVJaBxRbLRJ-h3atTWcs3RgHSJVEQe-F5kw7Yk5wObhfHEKb8hB3gGKk_Y3bbnDB4G-efDxlo7GwF1QpV45E9FgAxUus7_9wmYvEC1gsiMNylxl-S8knTU2voDLeCzez' );

        define( 'API_ACCESS_KEY', 'AAAAVuPz2JM:APA91bGyEBR67m5R7qF8SL5WF9UBHQZBHXPPlYER1ZVCQVJw8WapRn_0TSTRtM94NI9_4vZ2MCUMfggdwNTFhGb-Dis4n7PSJf4nQwszpPmdhb2juf8459epE0gZWRNxp4Yt3IGnkYmM1RJuGywqRq73poelGgftLg
AIzaSyANOXL7Lk2dwaV3dyUOsZiOuxJNZ9bw7XI' );


        // generated via the cordova phonegap-plugin-push using "senderID" (found in FCM App Console)
        // this was generated from my phone and outputted via a console.log() in the function that calls the plugin
        // my phone, using my FCM senderID, to generate the following registrationId
        // $singleID = $deviceToken;

        // prep the bundle
        // to see all the options for FCM to/notification payload:
        // https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support


        // 'vibrate' available in GCM, but not in FCM
        $fcmMsg = array(
            'body' => $message,
            'message' => $message,
            'title' => $type,
            //'badge' => '1',
            'badge' => '0',
            'sound' => "default",
            'color' => "#203E78"
        );


        $fcmData = array(
            'body' => $message,
            'message' => $message,
            'title' => $type,
            //'badge' => '1',
            'badge' => '0',
            'sound' => "default",
            'color' => "#203E78"
        );
        // I haven't figured 'color' out yet.
        // On one phone 'color' was the background color behind the actual app icon.  (ie Samsung Galaxy S5)
        // On another phone, it was the color of the app icon. (ie: LG K20 Plush)

        // 'to' => $singleID ;  // expecting a single ID
        // 'registration_ids' => $registrationIDs ;  // expects an array of ids
        // 'priority' => 'high' ; // options are normal and high, if not set, defaults to high.
        $fcmFields = array(
            'to' => $deviceToken,
            'priority' => 'high',
            'sound' => 'default',
            'newOrder' => isset($newOrder) ? $newOrder : "0",
            'notification' => $fcmMsg,
            "data" => $fcmData
        );

        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        //dd($result);
        return $result;
        curl_close( $ch );

    }

    //better_strip_tags
    public static function better_strip_tags( $str, $allowable_tags = '', $strip_attrs = false, $preserve_comments = false, callable $callback = null ) {
        $allowable_tags = array_map( 'strtolower', array_filter( // lowercase
            preg_split( '/(?:>|^)\\s*(?:<|$)/', $allowable_tags, -1, PREG_SPLIT_NO_EMPTY ), // get tag names
            function( $tag ) { return preg_match( '/^[a-z][a-z0-9_]*$/i', $tag ); } // filter broken
        ) );
        $comments_and_stuff = preg_split( '/(<!--.*?(?:-->|$))/', $str, -1, PREG_SPLIT_DELIM_CAPTURE );
        foreach ( $comments_and_stuff as $i => $comment_or_stuff ) {
            if ( $i % 2 ) { // html comment
                if ( !( $preserve_comments && preg_match( '/<!--.*?-->/', $comment_or_stuff ) ) ) {
                    $comments_and_stuff[$i] = '';
                }
            } else { // stuff between comments
                $tags_and_text = preg_split( "/(<(?:[^>\"']++|\"[^\"]*+(?:\"|$)|'[^']*+(?:'|$))*(?:>|$))/", $comment_or_stuff, -1, PREG_SPLIT_DELIM_CAPTURE );
                foreach ( $tags_and_text as $j => $tag_or_text ) {
                    $is_broken = false;
                    $is_allowable = true;
                    $result = $tag_or_text;
                    if ( $j % 2 ) { // tag
                        if ( preg_match( "%^(</?)([a-z][a-z0-9_]*)\\b(?:[^>\"'/]++|/+?|\"[^\"]*\"|'[^']*')*?(/?>)%i", $tag_or_text, $matches ) ) {
                            $tag = strtolower( $matches[2] );
                            if ( in_array( $tag, $allowable_tags ) ) {
                                if ( $strip_attrs ) {
                                    $opening = $matches[1];
                                    $closing = ( $opening === '</' ) ? '>' : $closing;
                                    $result = $opening . $tag . $closing;
                                }
                            } else {
                                $is_allowable = false;
                                $result = '';
                            }
                        } else {
                            $is_broken = true;
                            $result = '';
                        }
                    } else { // text
                        $tag = false;
                    }
                    if ( !$is_broken && isset( $callback ) ) {
                        // allow result modification
                        call_user_func_array( $callback, array( &$result, $tag_or_text, $tag, $is_allowable ) );
                    }
                    $tags_and_text[$j] = $result;
                }
                $comments_and_stuff[$i] = implode( '', $tags_and_text );
            }
        }
        $str = implode( '', $comments_and_stuff );
        return $str;
    }//better_strip_tags method ends here

    public static function hide_and_seek($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'secure for purpose';
        $secret_iv = 'secret for purpose';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    //makePayment
    public static function makePayment($data = []){

        $info = array();

        $error1 = "";
        $error2 = "";
        $error3 = "";
        $error4 = "";
        $error5 = "";
        $error6 = "";
        $error7 = "";
        $error8 = "";

        require_once 'stripe/init.php';
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here https://dashboard.stripe.com/account/apikeys
        // \Stripe\Stripe::setApiKey("sk_test_xJ3xuQd5eOck8UKb7IMk17Zp"); //Live Key pk_test_giXvBDf6FIUIEoHEKwpD3eKr

        // Pk Ali Account pk_test_giXvBDf6FIUIEoHEKwpD3eKr
        // Sk Ali Account sk_test_xJ3xuQd5eOck8UKb7IMk17Zp
        //\Stripe\Stripe::setApiKey("sk_test_xJ3xuQd5eOck8UKb7IMk17Zp");
        \Stripe\Stripe::setApiKey("sk_test_gEkVsL4NRdkuGiGFM47oJuBB");


        // Get the credit card details submitted by the form
        $token = $data['stripeToken'];

        // Create the charge on Stripe's servers - this will charge the user's card
        try {

            $charge = \Stripe\Charge::create(array(
                "amount" => $data['amount'], // amount in cents, again
                "currency" => $data['currency'],
                "source" => $token,
                "description" => $data['description']
            ));

            $info['success'] = $charge;

        } catch(Stripe_CardError $e) {
            $error1 = $e->getMessage();
            $info['error'] = $error1;
            $info['success'] = array();
        } catch (Stripe_InvalidRequestError $e) {
            $error2 = $e->getMessage();
            $info['error'] = $error2;
            $info['success'] = array();
        } catch (InvalidRequest $e) {
            $error3 = $e->getMessage();
            $info['error'] = $error3;
            $info['success'] = array();
        } catch (Stripe_AuthenticationError $e) {
            // Authentication with Stripe's API failed
            $error4 = $e->getMessage();
            $info['error'] = $error4;
            $info['success'] = array();
        } catch (Stripe_ApiConnectionError $e) {
            // Network communication with Stripe failed
            $error5 = $e->getMessage();
            $info['error'] = $error5;
            $info['success'] = array();
        } catch (Stripe_Error $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $error6 = $e->getMessage();
            $info['error'] = $error6;
            $info['success'] = array();
        } catch (\Stripe\Error\Base $e) {
            // Code to do something with the $e exception object when an error occurs
            $error7 = $e->getMessage();
            $info['error'] = $error7;
            $info['success'] = array();
        } catch (Exception $e) {
            $error8 = $e->getMessage();
            $info['error'] = $error8;
            $info['success'] = array();
        }

        return $info;

    }//makePayment method ends here

    //sendEmail
    public static function sendEmail($to,$subject,$text){

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: HorseMatch <admin@horsematch.com>' . "\r\n";
        // $headers .= 'Cc: apptestwork@gmail.com' . "\r\n";

        $message = "
                    <html>
                        <head>
                            <title>$subject</title>
                        </head>
                        <body>
                            <p>$text</p>
                            <p>Regards</p>
                            <p>Support Team</p>
                        </body>
                    </html>
                        ";

        $send =  mail($to,$subject,$message,$headers);
        return true;

    }//sendEmail

    //checkmobi_sms
    public static function checkmobi_sms($username, $user_phone, $code){

        $info = array();


        //$api = new CheckMobiRest("E3AA38E2-97A1-496F-A03F-3DE9E812BDBC");
        //$api = new CheckMobiRest("2C3507A5-302A-41E2-AB62-44C69C1EE966");
        $api = new CheckMobiRest("9AE029A2-7EF6-4F0D-8621-3D7C3627D551");

        $account_detail = $api->GetAccountDetails();

        if (isset($account_detail['response']['credit_balance']) && $account_detail['response']['credit_balance'] > 0.1) {

            //dd($account_detail);

            $breaks = array("\r", "\n", "/", "\t", "&amp;", "amp;", "nbsp;", "#39;", "quot;", "&", "(", ")", "");
            //$user_phone = str_ireplace($breaks, "", $user_phone);

            //$user_phone = str_replace(' ', '', $user_phone);

            //$user_phone = ltrim($user_phone, '0');
            //$user_phone = substr_replace($user_phone, '+92', 0, 0);
            

            $number_detail = $api->CheckNumber(array("number" => $user_phone));
            //dd($number_detail);
            $valid_number = isset($number_detail['response']['e164_format']) ? $number_detail['response']['e164_format'] : "";
            //dd($valid_number);
            if (isset($valid_number) && $valid_number != "") {
                $text_message = "Hi $username, \n\n$code is your 4-digits confirmation code for Log In to  \n\n HorseMatch Application. ";
                $response = $api->SendSMS(array("from" => "HorseMatch", "to" => $user_phone, "text" => $text_message));
                //dd($response);
                $info['status'] = "1";
                $info['message'] = "sms is sent";
            } else {
                $info['status'] = "0";
                $info['message'] = "sms is not sent";
            }


        } else {
            $info['status'] = "0";
            $info['message'] = "sms is not sent";
        }//sms working ends here

        //dd($info);

        return $info;

    }//checkmobi_sms method ends here
    
        public static function checkmobi_sms_forgot($username, $user_phone, $code){

        $info = array();


        //$api = new CheckMobiRest("E3AA38E2-97A1-496F-A03F-3DE9E812BDBC");
        //$api = new CheckMobiRest("2C3507A5-302A-41E2-AB62-44C69C1EE966");
        $api = new CheckMobiRest("9AE029A2-7EF6-4F0D-8621-3D7C3627D551");

        $account_detail = $api->GetAccountDetails();

        if (isset($account_detail['response']['credit_balance']) && $account_detail['response']['credit_balance'] > 0.1) {

            //dd($account_detail);

            $breaks = array("\r", "\n", "/", "\t", "&amp;", "amp;", "nbsp;", "#39;", "quot;", "&", "(", ")", "");
            //$user_phone = str_ireplace($breaks, "", $user_phone);

            //$user_phone = str_replace(' ', '', $user_phone);

            //$user_phone = ltrim($user_phone, '0');
            //$user_phone = substr_replace($user_phone, '+92', 0, 0);


            $number_detail = $api->CheckNumber(array("number" => $user_phone));
            //dd($number_detail);
            $valid_number = isset($number_detail['response']['e164_format']) ? $number_detail['response']['e164_format'] : "";
            //dd($valid_number);
            if (isset($valid_number) && $valid_number != "") {
                $text_message = "Hi $username, \n\n$code is your temporary password for Log In to  \n\n HorseMatch Application. ";
                $response = $api->SendSMS(array("from" => "HorseMatch", "to" => $user_phone, "text" => $text_message));
                //dd($response);
                $info['status'] = "1";
                $info['message'] = "sms is sent";
            } else {
                $info['status'] = "0";
                $info['message'] = "sms is not sent";
            }


        } else {
            $info['status'] = "0";
            $info['message'] = "sms is not sent";
        }//sms working ends here

        //dd($info);

        return $info;

    }//checkmobi_sms method ends here


}
