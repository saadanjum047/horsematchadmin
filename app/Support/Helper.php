<?php

namespace App\Support;
use Request;
use Illuminate\Http\Request as ServerRequest;
use DB;


class Helper {

	const VIDEO_CREATED = 1201;
	const VIDEO_COMMENT = 1202;
	const VIDEO_LIKED   = 1203;
	const PHOTO_CREATED = 1301;
	const PHOTO_COMMENT = 1302;
    const PHOTO_LIKED   = 1303;
	const FOLLOWER_REQUEST   = 1401;
    const USER_ACHIEVEMENT   = 1501;

	public static function appSettings() {
		$isSecure = config('app.is_secure');
		$appSettings = config('app.settings');
		$endPoints = $appSettings['auth']['endpoint'];
		$tokens = $appSettings['auth']['token'];
		unset($endPoints['token']);
		$selectiveSettings = [
				'baseUrl' => url('/'),
            	'apiUrl' => $endPoints['api'].'/api',
            	'auth' => [
            		'endpoint' => $endPoints,
            		'token'	=> $tokens
            	],
            	'program_start_date' => '2016-10-01',
                'facebook_app_id'=>config('app.facebook_app_id'),
        ];

		$settings = array_merge($selectiveSettings,$appSettings);
		return $settings;
	}

	

    public static function getFileUrl($url){
        $currentFileSystem = config('filesystems.default');
        if ($currentFileSystem == 'local' || $currentFileSystem == 'public') {
            return \URL::to('storage/'.$url);
        } else if($currentFileSystem == 'rackspace'){
            if (config('app.is_secure')) {
                $storageUrl = config('filesystems.disks.rackspace.secure_url').$url;
            } else {
                $storageUrl = config('filesystems.disks.rackspace.public_url').$url;
            }
            return $storageUrl;
        } else {
            $storageUrl = \Storage::url($url);
            $storageUrl = str_replace('%40','@',$storageUrl);
            return $storageUrl;
        }
    }


    public static function makePayment($data = []){

       $info = array();


       require_once 'stripe/init.php';
       // Set your secret key: remember to change this to your live secret key in production
       // See your keys here https://dashboard.stripe.com/account/apikeys
       // \Stripe\Stripe::setApiKey("sk_test_xJ3xuQd5eOck8UKb7IMk17Zp"); //Live Key pk_test_giXvBDf6FIUIEoHEKwpD3eKr

       // Pk Ali Account pk_test_giXvBDf6FIUIEoHEKwpD3eKr
       // Sk Ali Account sk_test_xJ3xuQd5eOck8UKb7IMk17Zp
       \Stripe\Stripe::setApiKey("sk_test_qQxQScTKCjNIm0977x1xQoV7");
       //\Stripe\Stripe::setApiKey("sk_test_MH10cNtARRY2YshFN3KlH7yK");


       // Get the credit card details submitted by the form
       $token = $data['stripeToken'];

       // Create the charge on Stripe's servers - this will charge the user's card
       try {

           $charge = \Stripe\Charge::create(array(
               "amount" => $data['amount'], // amount in cents, again
               "currency" => $data['currency'],
               "source" => $token,
               "description" => $data['description']
           ));

           //dd($charge->balance_transaction);
           //dd($charge->balance_transaction);

           $info['success'] = $charge;

       } catch(Stripe_CardError $e) {
           $error1 = $e->getMessage();
           $info['error'] = $error1;
           $info['success'] = array();
       } catch (Stripe_InvalidRequestError $e) {
           $error2 = $e->getMessage();
           $info['error'] = $error2;
           $info['success'] = array();
       } catch (InvalidRequest $e) {
           $error3 = $e->getMessage();
           $info['error'] = $error3;
           $info['success'] = array();
       } catch (Stripe_AuthenticationError $e) {
           // Authentication with Stripe's API failed
           $error4 = $e->getMessage();
           $info['error'] = $error4;
           $info['success'] = array();
       } catch (Stripe_ApiConnectionError $e) {
           // Network communication with Stripe failed
           $error5 = $e->getMessage();
           $info['error'] = $error5;
           $info['success'] = array();
       } catch (Stripe_Error $e) {
           // Display a very generic error to the user, and maybe send
           // yourself an email
           $error6 = $e->getMessage();
           $info['error'] = $error6;
           $info['success'] = array();
       } catch (\Stripe\Error\Base $e) {
           // Code to do something with the $e exception object when an error occurs
           $error7 = $e->getMessage();
           $info['error'] = $error7;
           $info['success'] = array();
       } catch (Exception $e) {
           $error8 = $e->getMessage();
           $info['error'] = $error8;
           $info['success'] = array();
       }

       return $info;

   }//makePayment method ends here

   public static function sendPush($deviceToken, $type, $message){
      
       // API access key from Google FCM App Console
       //define( 'API_ACCESS_KEY', 'AAAAwFWu_7g:APA91bEu6MtTGVJaBxRbLRJ-h3atTWcs3RgHSJVEQe-F5kw7Yk5wObhfHEKb8hB3gGKk_Y3bbnDB4G-efDxlo7GwF1QpV45E9FgAxUus7_9wmYvEC1gsiMNylxl-S8knTU2voDLeCzez' );

       define( 'API_ACCESS_KEY', env('FCM_SERVER_KEY') );


       // generated via the cordova phonegap-plugin-push using "senderID" (found in FCM App Console)
       // this was generated from my phone and outputted via a console.log() in the function that calls the plugin
       // my phone, using my FCM senderID, to generate the following registrationId
       // $singleID = $deviceToken;

       // prep the bundle
       // to see all the options for FCM to/notification payload:
       // https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support


       // 'vibrate' available in GCM, but not in FCM
       $fcmMsg = array(
           'body' => $message,
           'message' => $message,
           'title' => $type,
           //'badge' => '1',
           'badge' => '0',
           'sound' => "default",
           'color' => "#203E78"
       );


       $fcmData = array(
           'body' => $message,
           'message' => $message,
           'title' => $type,
           //'badge' => '1',
           'badge' => '0',
           'sound' => "default",
           'color' => "#203E78"
       );
       // I haven't figured 'color' out yet.
       // On one phone 'color' was the background color behind the actual app icon.  (ie Samsung Galaxy S5)
       // On another phone, it was the color of the app icon. (ie: LG K20 Plush)

       // 'to' => $singleID ;  // expecting a single ID
       // 'registration_ids' => $registrationIDs ;  // expects an array of ids
       // 'priority' => 'high' ; // options are normal and high, if not set, defaults to high.
       $fcmFields = array(
           'to' => $deviceToken,
           'priority' => 'high',
           'sound' => 'default',
           'newOrder' => isset($newOrder) ? $newOrder : "0",
           'notification' => $fcmMsg,
           "data" => $fcmData
       );

       $headers = array(
           'Authorization: key=' . API_ACCESS_KEY,
           'Content-Type: application/json'
       );

       $ch = curl_init();
       curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
       curl_setopt( $ch,CURLOPT_POST, true );
       curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
       curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
       curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
       curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
       $result = curl_exec($ch );
       //dd($result);
       return $result;
       curl_close( $ch );

   }
}
