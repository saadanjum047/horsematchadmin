<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Session;


use Closure;

class WebValidateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user = Session::get('usersession');

        if( empty($user) )
            return redirect('/login');

        return $next($request);
    }
}
