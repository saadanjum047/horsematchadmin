<?php

//namespace App\Http\Middleware;
namespace App\Http\Middleware;

use Closure;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Log;

class Admin {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        $value = $request->session()->get('isAdmin');

        if($value=='yes'){
            return $next($request);


        }else{
            return redirect('/admin');

        }
       // dd($request);
        //return $next($request);
    }
}
