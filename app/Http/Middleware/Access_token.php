<?php

//namespace App\Http\Middleware;
namespace App\Http\Middleware;

use Closure;


use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
//use App\User;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Log;

use App\Data\Models\User;

class Access_token {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        try{

            $token = JWTAuth::getToken();

            if($token){
                $claims = JWTAuth::decode($token);
                // dd($claims->get('sub'));
                if($claims instanceof Payload && $claims->get('sub')) {
                    $request['user_id'] = $claims->get('sub');
                    //exist
                    $exist = User::where('id', $request['user_id'])->first();
                    //validation
                    if ($exist != NULL) {
                        if ($exist->user_type == "admin") {
                            $request['admin_loggedin'] = true;
                        }
                        return $next($request);
                    } else {
                        $code = 410;
                        //$output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
                        $output = ['error' => ['code' => $code, 'messages' => ["Invalid user"] ]];
                        return response()->json($output, $code);
                    }
                } else {
                    $code = 401;
                    //$output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
                    $output = ['error' => ['code' => $code, 'messages' => ["Invalid token"] ]];
                    return response()->json($output, $code);
                }
            }else{
                $code = 401;
                //$output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
                $output = ['error' => ['code' => $code, 'messages' => ["Please enter token!"] ]];
                return response()->json($output, $code);
            }

        } catch( TooManyRequestsHttpException $e ) {
            $code = 401;
            //$output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
            $output = ['error' => ['code' => $code, 'messages' => ["Too many attempts!"] ]];
            return response()->json($output, $code);
        } catch(\Exception $e) {
            $code = 410;
            //$output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
            $output = ['error' => ['code' => $code, 'messages' => ["Invalid token"] ]];
            return response()->json($output, $code);
        }

    }


}//Access_token class ends here
