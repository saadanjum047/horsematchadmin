<?php

//namespace App\Http\Middleware;
namespace App\Http\Middleware;

use Closure;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Log,App;

class Referee_access {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        $value = $request->session()->get('isReferee');
        $session_id = $request->session()->get('session_id');
        $detail = App::make('UserRepository')->findById($session_id, true, true);

        if ($detail != NULL) {
            if ($detail->is_subscribe == 0 && $value == 'yes') {
                return redirect('/referee-selectplans');
                //header('Location:referee-selectplans');
            } else if ($detail->is_subscribe == 1 && $value == 'yes') {
                return $next($request);
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }

    }

}//Organization_access
