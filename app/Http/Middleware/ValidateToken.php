<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;


use Closure;

class ValidateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        //return $next($request);
        try{
            $token = JWTAuth::getToken();       
           
            if(!$token)
            {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Token Required.']]];                
                return response()->json($output, $code);
            }

            else if($token){

                $claims = JWTAuth::decode($token);                
                if($claims instanceof Payload && $claims->get('sub')) {                          
                    $request['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                    
                }
            }
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
        {               
                 //$code = 400;
                $code = $e->getStatusCode();
                 $output = ['error' => ['code' => $code, 'messages' => ['Token has expired']]];
                 return response()->json($output, $code);
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
        {               
                 //$code = 400;
                $code = $e->getStatusCode();
                 $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                 return response()->json($output, $code);
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e)
        {               
                 //$code = 400;
                $code = $e->getStatusCode();
                 $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                 return response()->json($output, $code);
        }        
        return $next($request);
    }
}
