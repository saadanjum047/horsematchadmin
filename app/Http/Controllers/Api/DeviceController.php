<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Events\Event;
use Validator, App;
use Tymon\JWTAuth\Payload;
use JWTAuth;
use Image,Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Data\Repositories\DisciplineRepository;
use Illuminate\Support\Facades\Cache;

use App\Data\Models\User;
use App\Data\Models\Breed;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserDevice;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use GuzzleHttp\Handler\CurlFactory;
//Helpers
use App\Helpers\Helper;
use App\Events\SendRatingNotification;

//DisciplineController
class DeviceController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(){
    }

    public function loginDeviceToken(Request $Request)
    {

        $input = $Request->all();

        // devices::where(['udid'=>$input['udid']])->delete();

        $devices = UserDevice::where(['user_id' => $input['user_id'], 'type' => $input['type']])->get()->first();

        if ($devices == null) {
            $device_id = UserDevice::insertGetId(['user_id' => $input['user_id'], 'token' => $input['token'], 'udid' => $input['udid'], 'type' => $input['type']]);
        } else {
            $device_id = UserDevice::where(['user_id' => $input['user_id']])->update(['token' => $input['token'], 'udid' => $input['udid'], 'type' => $input['type']]);
        }

        if ($device_id > 0) {

            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Success'], 'data' => @json_decode("{}")]];
            return response()->json($output, $code);
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Device token Invalid!'],'data' => @json_decode("{}")]];
            return response()->json($output, $code);
        }


    }

    public function deleteToken(Request $request)
    {
        $input = $request->all();

        $rep = UserDevice::where([['user_id', '=', $input['user_id']], ['udid', '=', $input['udid']]])->forceDelete();

        if($rep){
            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Successfull Remove!'], 'data' => []]];
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Already Remove!']]];
        }

        return response()->json($output, $code);



    }

    public function getNotification(Request $request)
    {
        $input = $request->all();
        $notification = Notification::where(['user_id' => $input['user_id']])->orderBy('id', 'DESC')->get()->toArray();
        dd($notification);
        $data = array();
        foreach ($notification as $nkey => $nval) {

            if ($nval['type'] == 'booking') {

            }
        }


        if(!empty($data)){
            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['data found yet!'], 'data' => $data]];
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Data not found!']]];
        }

        return response()->json($output, $code);
    }

    public function unReadCount(Request $request){
        $input=$request->all();
        $data= Notification::where(['user_id'=> $input['user_id'],'isRead'=>0])->orderBy('id', 'DESC')->get()->toArray();
        $unRead= count($data);

        if($unRead>0){
            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['data found yet!'], 'data' => array('unRead'=>$unRead)]];
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Data not found!']]];
        }

        return response()->json($output, $code);

    }

    public function readNotifications(Request $request){
        $input=$request->all();

        $data= Notification::where(['user_id'=> $input['user_id'],'isRead'=>0])->update(['isRead'=>1]);
        if($data){
            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Successfully Read'], 'data' => @json_decode("{}") ]];
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Data not found!']]];
        }

        return response()->json($output, $code);


    }

    public function deleteNotification(Request $request){
        $input=$request->all();
        $deleteNOti=  Notification::where(['id'=>$input['notification_id']])->delete();

        if($deleteNOti){
            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Successfully delete!'], 'data' => @json_decode("{}") ]];
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Data not found!']]];
        }

        return response()->json($output, $code);

    }

    public function deleteAllNotification(Request $request){
        $input=$request->all();
        $deleteNOti=  Notification::where(['user_id'=>$input['user_id']])->delete();

        if($deleteNOti){
            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Successfully delete!'], 'data' => @json_decode("{}") ]];
        } else {
            $code = 404;
            $output = ['response' => ['code' => $code, 'status' => 0, 'messages' => ['Data not found!']]];
        }

        return response()->json($output, $code);

    }

    public function testNotification(Request $request)
    {
        $input = $request->all();

        event(new SendRatingNotification($input));

        dd('2');
        //$user_ip = getenv('REMOTE_ADDR');
        //$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
        //  dd($geo['geoplugin_timezone']);
        // dd($request->ip());
        $title = "Test Title";
        $body = "Test body";
        $message = 'Test Message';

        // $notific = Notification::insertGetId(['type' => 'test', 'title' => $title, 'desc' => $message]);
        $dataArray = array("id" => 1, "message" => $message);
        $data = $this->send_notification($input["device_token"], $title, $body, $dataArray);
        dd($data);

    }

    public function send_notification($token, $title, $body, $dataArray)
    {

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60 * 20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setTitle($title)
            ->setSound(1)
            //  ->setClickAction('MY_KEY_NOTIFICATION')
            ->setBody($body);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($dataArray);
        $dataBuilder->setData($dataArray);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        // $token = "eL11osxaKhI:APA91bH1VkG-Usu7jzEPkm-uujmh7EtHQ1gTNQqpA_g250nLpmKPk5H5fmaNhNaPd6Sb_7wGRnFyWKW3MWho1TM3uYV9_ji7CIgcaV94e_Tqs7Ntbz8JqoZopcQYNMf13EBo-rHPkwGu";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        return $downstreamResponse;

    }


}//DisciplineController class ends here
