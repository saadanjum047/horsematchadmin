<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use Validator, App;
use JWTAuth;
use Log;
use App\Data\Repositories\SettingRepository;

use App\Data\Models\Setting;
use Image, Storage;
use Carbon\Carbon;

class SettingController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(SettingRepository $setting) {
        $this->_repository = $setting;
    }


    public function index(Request $request){

        $input = $request->only('content_type');

        try {

            $rules = [
                'content_type'         =>  'required|in:tc,pp,us'
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            } else {

                $content = Setting::where("type",$input['content_type'])->first();

                if ($content != NULL) {

                    $response = handle_string($content->content);

                    $code = 200;
                    $message = "Content found";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $response]];

                } else {
                    $code = 401;
                    $message = "No content is found";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }//input validation ends here

        } catch(\Exception $e){
            $code = 401;
           $message = "Opps something went wrong";
            //$message  = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//index method ends here

    public function country_list(Request $request){

        
        $input = $request->only('keyword', 'per_page', 'page');
        $input['page'] = isset($input['page']) ? $input['page'] : "";
        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;

        try {

            $record = $this->_repository->country_list($input);

            if ($record != NULL) {

                $code = 200;
                $message = "Record found";
                $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $record['pagination']]];

            } else {
                $code = 401;
                $message = "No record is found";
                $output = ['error' => ['code' => $code, 'messages' => [$message]]];
            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            //$message  = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//country_list method ends here

    public function city_list(Request $request){

        $input = $request->only('keyword', 'country_id', 'per_page');

        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;

        try {

            $record = $this->_repository->city_list($input);

            if ($record != NULL) {

                $code = 200;
                $message = "Record found";
                $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $record['pagination']]];

            } else {
                $code = 401;
                $message = "No record is found";
                $output = ['error' => ['code' => $code, 'messages' => [$message]]];
            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            //$message  = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//city_list method ends here

    /* ---------- Content ----------------- */
    public function app_content(Request $request){

        try{

            $input = $request->only('content_type');

            $rules = [
                'content_type'         =>  'required|in:tc,pp,us,forum'
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            } else {

                $content = Setting::where("type",$input['content_type'])->select('content')->first();

                //validation
                if ($content != NULL) {

                    //simple handling
                    $simple = strip_tags($content->content);
                    $breaks = array("\r", "\n", "/", "\t", "&amp;", "amp;", "nbsp;", "#39;", "quot;", "&");
                    $simple = str_ireplace($breaks, "", $simple);
                    //simple handling ends here

                    $record['content'] = $content->content;
                    $record['simple_content'] = $simple;
                } else {
                    $record['content'] = "";
                    $record['simple_content'] = "";
                }

                if ($record) {
                    $code = 200;
                    $output = [
                        'response' => [
                            'code' => $code,
                            'data' => $record,
                        ]
                    ];
                }else{
                    $code = 409;
                    $output = ['error'=>['code'=>$code,'messages'=>['An error occured while getting content.']]];
                }
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//app_content method ends here

    //update_content
    public function update_content(Request $request) {

        $input = $request->only( 'id', 'content');
        $error_log = array();

        try {

            $rules = [
                'id' => 'required|exists:settings,id',
                'content' => 'required'
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                //dd($input);

                $id = $input['id'];
                $record = Setting::where('id', $id)->first();

                $new_array['content'] = isset($input['content']) ? $input['content'] : $record->content;
                $update = Setting::where('id', $id)->update($new_array);

                $clear_cache = Cache::flush();

                $code = 200;
                $message = "Record is updated";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];
            }

        }catch(\Exception $e){
            $code = 401;

            $message = "Opps something went wrong";
            //$message = $e->getFile().' '.$e->getFile().' '.$e->getMessage();

            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }
        return response()->json($output, $code);
    }//update_content method ends here

    /* ---------- Filters ----------------- */
    public function app_filters(Request $request){

        try{

            $record = $this->_repository->app_filters();

            $code = 200;
            $output = [
                'response' => [
                    'code' => $code,
                    'data' => $record,
                ]
            ];

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//app_filters method ends here


    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.certificates.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.certificates.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.certificates.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.collage.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }

        }
        return true;
    }

}//SettingController class ends here