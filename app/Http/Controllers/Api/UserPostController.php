<?php

namespace App\Http\Controllers\Api;

use App\Events\SendRatingNotification;
use App\Http\Controllers\Controller;

use Validator, App;
use Tymon\JWTAuth\Payload;
use JWTAuth;
use Image,Storage;
use Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Data\Repositories\UserPostRepository;
use Illuminate\Support\Facades\Cache;

use App\Data\Models\User;
use App\Data\Models\UserPost;
use App\Data\Models\ReportedPost;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserNotinterestPost;
//Helpers
use App\Helpers\Helper;
use App\Data\Models\UserPostDiscipline;
use App\Events\SendPostNotification;
use App\Events\SendPostUploadNotification;


//UserPostController
class UserPostController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(UserPostRepository $userPostRepository){
        $this->_repository = $userPostRepository;
    }

    //add
    public function add(Request $request){

        try {

            $input = $request->only('user_id', 'title', 'headline', 'breed_id', 'size', 'temperament', 'discipline_id', 'color_id', 'address', 'latitude', 'longitude', 'price', 'gender_id', 'input_links', 'input_images');

//            $input['breed_id'] = isset($input['breed_id']) ? Helper::hashid_decode($input['breed_id']) : "0";
//            if(isset($input['breed_id'])){
//                $breed_id = [];
//                foreach ($input['breed_id'] as $key=>$value){
//                    $breed_id[]=($value);
//                }
//                $input['breed_id'] = implode(',',$breed_id);
//            }
            if(isset($input['breed_id'])){
                $breed_id= [];
                foreach ($input['breed_id'] as $key=>$value){
//                    $discipline_id[]=Helper::hashid_decode($value);
                    $breed_id[]=($value);
                }

                $input['breed_id']= implode(',',$breed_id);
            }
            $input['size'] = isset($input['size']) ? $input['size'] : "";
            $input['temperament'] = isset($input['temperament']) ? $input['temperament'] : "";
            $input['is_lease'] = $request->is_lease;


            if(isset($input['discipline_id'])){
                $discipline_id= [];
                foreach ($input['discipline_id'] as $key=>$value){
//                    $discipline_id[]=Helper::hashid_decode($value);
                    $discipline_id[]=($value);
                }

                $input['discipline_id']= implode(',',$discipline_id);
            }

            //$input['discipline_id'] = isset($input['discipline_id']) ? Helper::hashid_decode($input['discipline_id']) : "0";
            $input['color_id'] = isset($input['color_id']) ? Helper::hashid_decode($input['color_id']) : "0";
            $input['gender_id'] = isset($input['gender_id']) ? Helper::hashid_decode($input['gender_id']) : "0";


            $timestamp = @date("Y-m-d H:i:s");
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'America/Los_Angeles');
            $dateTime = $date->setTimezone('UTC');
            $currentdate = @date("Y-m-d");

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'title' => 'required|max:255',
                'headline'=> 'required',
                'address'=> 'required',
                'latitude'=> 'required',
                'longitude'=> 'required',
//                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20240',
//                'sport_id'=> 'required',
//                'city_id'=> 'required',
            ];


            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                if (isset($input['input_links']) ) {
                    $input_links = $input['input_links'];
                } else {
                    $input_links = array();
                }
                unset($input['input_links']);

                if (isset($input['input_images']) ) {
                    $input_images = $input['input_images'];
                } else {
                    $input_images = array();
                }
                unset($input['input_images']);

                //dd($input_images, $input);


                $input['created_date'] = $currentdate;
                $input['created_at'] = $dateTime;


                $action = $this->_repository->create($input);

                if ($action) {

                    $last_id = Helper::hashid_decode($action->id);
                    if(isset($discipline_id)){
                        foreach ($discipline_id as $value){

                            UserPostDiscipline::insert(['post_id'=>$last_id,'discipline_id'=>$value]);
                        }

                    }



                    //links handling
                    if (isset($input_links)) {
                        foreach($input_links as $inputl) {
                            $category_array = array();
                            $category_array['user_id'] = $input['user_id'];
                            $category_array['post_id'] = $last_id;
                            $category_array['link'] = $inputl;
                            $category_array['created_date'] = $currentdate;
                            $category_array['created_at'] = $dateTime;
                            $action_added = UserPostLink::insertGetId($category_array);
                        }
                    }//links handling ends here

                    //images handling
                    if (isset($input_images)) {
                        foreach($input_images as $inputimg) {
                            $image_array = array();
                            $file_name = $inputimg->store(config('app.files.postimages.folder_name'));
                            $image_array['image']=$inputimg->hashName();
                            $this->gallery_crop($inputimg);
                            unset($inputimg);
                            $image_array['user_id'] = $input['user_id'];
                            $image_array['post_id'] = $last_id;
                            $image_array['created_date'] = $currentdate;
                            $image_array['created_at'] = $dateTime;
                            $action_added = UserPostImage::insertGetId($image_array);
                        }

                    }///images handling ends here
                    //  $user_preference = UserPreference::where('user_id', $input['user_id'])->first();
                    //$input_preference = unserialize($user_preference->input);
                    event(new SendPostNotification($action));
                    event(new SendPostUploadNotification($action));

                    $clear_cache =  Cache::flush();
                    $user = User::where('id',$request->user_id)->first();
                    $user->postsremaining = $user->postsremaining - 1;
                    $user->save();
                    $code = 200;
                    $message = "Post is added";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $action]];
                } else {
                    $code = 401;
                    $message = "Post could not be added";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }


            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//add method ends here
    //view
    public function view(Request $request){

        try {

            $input = $request->only('user_id','id');
            $input['id'] = isset($input['id'])? Helper::hashid_decode($input['id']):"";

            $rules = [
                'id' => 'required|exists:user_posts,id',
                //'user_id' => 'required|exists:users,id,visibility,1,user_type,organization',
                'user_id' => 'required|exists:users,id',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $article = $this->_repository->findById($input['id'], false, true);
                                $is_reported = ReportedPost::where('post_id',Helper::hashid_decode($article->id))->where('user_id',$request->user_id)->first();
                if($is_reported){
                    $article->is_reported = 1;
                }
                else{
                    $article->is_reported = 0;
                }

                if ($article == null) {
                    $code = 200;
                    $message = " Record Not Found !";
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    // $record = $this->article_model($article->id);
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $article]];
                }

            }

        } catch (\Exception $e){
            $code = 401;
            $message = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            Log::debug($message);
            $message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }
        return response()->json($output, $code);

        //
    }//view method ends here
    //my_all
    public function my_all(Request $request){

        try {

            $input = $request->only('user_id', 'is_sold');

            $rules = ['user_id' => 'required|exists:users,id,visibility,1'];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $record = $this->_repository->my_all($input);
                //dd($record);

                if ($record == null || count($record) == 0) {
                    $code = 401;
                    $message = "Venue Records Not Found !";
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $record['pagination']]];
                }

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $custom_message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$custom_message]]];
        }

        return response()->json($output, $code);

    }//my_all method ends here
    //all
    public function all(Request $request){

        try {

            $input = $request->only('user_id', 'admin_loggedin', 'keyword', 'perpage',  'status', 'device_type', 'is_sold');

            if (isset($input['admin_loggedin']) == true ) {
                $rules = [
                    'user_id' => 'required|exists:users,id,visibility,1',
                ];
            } else {
                $rules = [
                    'user_id' => 'required|exists:users,id,visibility,1',
                ];
            }

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                //device handling
                if (isset($input['device_type']) && $input['device_type'] == "web") {
                    $this->_repository->setPaginateLinks();
                }
                $perPage = isset($input['perpage']) && $input['perpage'] != "" ? $input['perpage'] : 10;
                $articles = $this->_repository->findByAll(true, $perPage, $input, true, true, false);


                if ($articles == null || count($articles) == 0) {
                    $code = 401;
                    $message = "Venue Records Not Found !";
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $articles['data'], 'pagination' => $articles['pagination']]];
                }
//

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $custom_message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$custom_message]]];
        }

        return response()->json($output, $code);

    }//all method ends here

    public function searchByHorseName(Request $request){

        try {

            $input = $request->only('user_id', 'admin_loggedin', 'keyword', 'perpage',  'status', 'device_type', 'is_sold');

            if (isset($input['admin_loggedin']) == true ) {
                $rules = [
                    'user_id' => 'required|exists:users,id,visibility,1',
                ];
            } else {
                $rules = [
                    'user_id' => 'required|exists:users,id,visibility,1',
                ];
            }

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                //device handling
                if (isset($input['device_type']) && $input['device_type'] == "web") {
                    $this->_repository->setPaginateLinks();
                }
                $perPage = isset($input['perpage']) && $input['perpage'] != "" ? $input['perpage'] : 10;
                $articles = $this->_repository->searchByHorseName(true, $perPage, $input, true, true, false);


                if ($articles == null || count($articles) == 0) {
                    $code = 401;
                    $message = "Venue Records Not Found !";
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $articles['data'], 'pagination' => $articles['pagination']]];
                }
//

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $custom_message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$custom_message]]];
        }

        return response()->json($output, $code);

    }//all method ends here
    //admin_post_accept
    public function admin_post_accept(Request $request) {

        $input = $request->only('user_id', 'id');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";
        $error_log = array();

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'id' => 'required|exists:user_posts,id',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $update['status'] = 'approved';
                $action = UserPost::where('id', $input['id'])->update($update);
                $clear_cache = Cache::flush();

                $code = 200;
                $message = "Record is deleted";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//admin_post_accept method ends here
    //admin_post_reject
    public function admin_post_reject(Request $request) {

        $input = $request->only('user_id', 'id');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";
        $error_log = array();

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'id' => 'required|exists:user_posts,id',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $update['status'] = 'rejected';
                $action = UserPost::where('id', $input['id'])->update($update);
                $clear_cache = Cache::flush();

                $code = 200;
                $message = "Record is deleted";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//admin_post_reject method ends here

    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.venues.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.venues.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.venues.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.collage.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }

        }
        return true;
    }
    //gallery_crop
    public function gallery_crop($file, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {
        ini_set('memory_limit','500M');
        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            $store = Storage::put(config('app.files.postimages.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
            $store = Storage::put(config('app.files.postimages.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
            $store = Storage::put(config('app.files.postimages.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        }
        return true;
    }//gallery_crop

    public function notInterested(Request $request)
    {
        $input = $request->all();
        $input['post_id']=Helper::hashid_decode($input['post_id']);

        $var = UserNotinterestPost::where(['user_id' => $input['user_id'], 'post_id' => $input['post_id']])->get()->first();

        if ($var == null) {

            $fav = UserNotinterestPost::insertGetId($input);

            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Successfully Not intrested'], 'data' => @json_decode("{}")]];
            return response()->json($output, $code);

        } else {

            // UserNotinterestPost::where(['user_id' => $input['user_id'], 'post_id' => $input['post_id']])->delete();

            $code = 200;
            $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Remove from Notinterested'], 'data' => @json_decode("{}")]];
            return response()->json($output, $code);

        }

    }

    public function deletePost(Request $request)
    {
        $input = $request->all();
        $input['post_id']=Helper::hashid_decode($input['post_id']);


        UserPost::where(['user_id' => $input['user_id'], 'id' => $input['post_id']])->forceDelete();

        $code = 200;
        $output = ['response' => ['code' => $code, 'status' => 1, 'messages' => ['Remove from post'], 'data' => @json_decode("{}")]];
        return response()->json($output, $code);

    }

    public function editPost(Request $request){
        try {

            $input = $request->only('id','user_id', 'title', 'headline', 'breed_id', 'size', 'temperament', 'discipline_id', 'color_id', 'address', 'latitude', 'longitude', 'price', 'gender_id', 'input_links', 'input_images');

            $input['id'] = Helper::hashid_decode($input['id']);
            // $input['id'] = Helper::hashid_decode($request->post_id);
//            $input['breed_id'] = isset($input['breed_id']) ? Helper::hashid_decode($input['breed_id']) : "0";
            $input['size'] = isset($input['size']) ? $input['size'] : "";
            $input['temperament'] = isset($input['temperament']) ? $input['temperament'] : "";
            $input['is_lease'] = $request->is_lease;

            if(isset($input['breed_id'])){
                $breed_id= [];
                foreach ($input['breed_id'] as $key=>$value){
//                    $discipline_id[]=Helper::hashid_decode($value);
                    $breed_id[]=($value);
                }

                $input['breed_id']= implode(',',$breed_id);
            }
            if(isset($input['discipline_id'])){
                $discipline_id= [];
                foreach ($input['discipline_id'] as $key=>$value){
                    $discipline_id[]=($value);
                }

                $input['discipline_id']= implode(',',$discipline_id);
            }



            //$input['discipline_id'] = isset($input['discipline_id']) ? Helper::hashid_decode($input['discipline_id']) : "0";
            $input['color_id'] = isset($input['color_id']) ? Helper::hashid_decode($input['color_id']) : "0";
            $input['gender_id'] = isset($input['gender_id']) ? Helper::hashid_decode($input['gender_id']) : "0";


            $timestamp = @date("Y-m-d H:i:s");
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'America/Los_Angeles');
            $dateTime = $date->setTimezone('UTC');
            $currentdate = @date("Y-m-d");

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'title' => 'required|max:255',
                'headline'=> 'required',
                'address'=> 'required',
                'latitude'=> 'required',
                'longitude'=> 'required',
//                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20240',
//                'sport_id'=> 'required',
//                'city_id'=> 'required',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                if (isset($input['input_links']) ) {
                    $input_links = $input['input_links'];
                } else {
                    $input_links = array();
                }
                unset($input['input_links']);
                UserPostImage::where('post_id',Helper::hashid_decode($request->id))->forceDelete();
                if (isset($input['input_images']) ) {
                    $input_images = $input['input_images'];
                } else {
                    $input_images = array();
                }
                unset($input['input_images']);

                //dd($input_images, $input);


                $input['created_date'] = $currentdate;
                // $input['created_at'] = $dateTime;


                $action = $this->_repository->update($input);
                \Log::debug("done till here");
                if ($action) {

                    $last_id = Helper::hashid_decode($action->id);
                    if(isset($discipline_id)){
                        UserPostDiscipline::where('post_id',Helper::hashid_decode($request['id']))->forceDelete();
                        foreach ($discipline_id as $value){

                            UserPostDiscipline::insert(['post_id'=>$last_id,'discipline_id'=>$value]);
                        }

                    }

                    //links handling
                    if (isset($input_links)) {
                        foreach($input_links as $inputl) {
                            $category_array = array();
                            $category_array['user_id'] = $input['user_id'];
                            $category_array['post_id'] = $last_id;
                            $category_array['link'] = $inputl;
                            $category_array['created_date'] = $currentdate;
                            $category_array['created_at'] = $dateTime;
                            $action_added = UserPostLink::insertGetId($category_array);
                        }
                    }//links handling ends here

                    //images handling
                    if (isset($input_images)) {
                        \Log::debug($input_images);
//                        \Log::debug($input_images);
                        foreach($input_images as $inputimg) {
                            $image_array = array();
                            $file_name = $inputimg->store(config('app.files.postimages.folder_name'));
                            $image_array['image']=$inputimg->hashName();
                            $this->gallery_crop($inputimg);
                            unset($inputimg);
                            $image_array['user_id'] = $input['user_id'];
                            $image_array['post_id'] = $last_id;
                            $image_array['created_date'] = $currentdate;
                            $image_array['created_at'] = $dateTime;
                            $action_added = UserPostImage::insertGetId($image_array);
                        }
//
                    }///images handling ends here
//                      $user_preference = UserPreference::where('user_id', $input['user_id'])->first();
//                    $input_preference = unserialize($user_preference->input);
                    // event(new SendPostNotification($action));
                    // event(new SendPostUploadNotification($action));


                    $clear_cache =  Cache::flush();
//                    $user = User::where('id',$request->user_id)->first();
//                    $user->postsremaining = $user->postsremaining - 1;
//                    $user->save();
                    $code = 200;
                    $message = "Post is added";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $action]];
                } else {
                    $code = 401;
                    $message = "Post could not be added";

                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }


            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);
    }


}//UserPostController class ends here
