<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator, JWTAuth, DB;

use App\Data\Models\User;
use App\Data\Models\Breed;
use App\Data\Models\Discipline;
use App\Data\Models\Temperament;
use App\Data\Models\UserPost;
use App\Data\Models\UserSoldPost;
use App\Data\Models\UserFavouritePost;

use App\Helpers\Helper;

//AdminServiceController
class AdminServiceController extends Controller{

    const PER_PAGE = 10;

    //admin_users
    public function admin_users(Request $request) {

        $info = User::
            select('id', 'username', 'email', 'phone')
            ->where('user_type', 'user')
            ->orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "username" => Helper::handle_string($info->username),
                    "email" => Helper::handle_string($info->email),
                    "phone" => Helper::handle_string($info->phone),
                    "total_ads" => "0",
                    "total_sold" => "0",
                    "total_rating" => "0",
                    "total_reported" => "0",
                    "action" => " <a class='fa fa-info-circle customBtnStyle marginRight'  title='User Detail' href='horsematch/userdetail?id=$record_id'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_users method ends here

    //admin_pending_ads
    public function admin_pending_ads(Request $request) {

        $info = UserPost::
            where('status', 'pending')
            ->orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "headline" => Helper::handle_string($info->headline),
                    "total_sold" => "0",
                    "total_favourite" => "0",
                    "total_report" => "0",
                    // "action" => " <a class='fa fa-info-circle customBtnStyle marginRight'  title='User Detail' href='horsematch/userdetail?id=$record_id'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                    "action" => "<a class='fa fa-info-circle customBtnStyle marginRight'  title='Ads Detail' href='horsematch/adsdetail?id=$record_id'></a> <a class='fa fa-check marginRight customBtnStyle accept_record row_" . $record_id . "' style='color:#bd2130;' onclick='acceptRecord(`$record_id`)' href='javascript:void(0)'></a> <a class='fa fa-times marginRight customBtnStyle reject_record row_" . $record_id . "' style='color:#bd2130;' onclick='rejectRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_pending_ads method ends here

    //admin_approved_ads
    public function admin_approved_ads(Request $request) {

        $info = UserPost::
            where('status', 'approved')
            ->orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "headline" => Helper::handle_string($info->headline),
                    "total_sold" => "0",
                    "total_favourite" => "0",
                    "total_report" => "0",
                    // "action" => " <a class='fa fa-info-circle customBtnStyle marginRight'  title='User Detail' href='horsematch/userdetail?id=$record_id'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                    "action" => "<a class='fa fa-info-circle customBtnStyle marginRight'  title='Ads Detail' href='horsematch/adsdetail?id=$record_id'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_approved_ads method ends here

    //admin_rejected_ads
    public function admin_rejected_ads(Request $request) {

        $info = UserPost::
            where('status', 'rejected')
            ->orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "headline" => Helper::handle_string($info->headline),
                    "total_sold" => "0",
                    "total_favourite" => "0",
                    "total_report" => "0",
                    // "action" => " <a class='fa fa-info-circle customBtnStyle marginRight'  title='User Detail' href='horsematch/userdetail?id=$record_id'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                    "action" => "<a class='fa fa-info-circle customBtnStyle marginRight'  title='Ads Detail' href='horsematch/adsdetail?id=$record_id'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_rejected_ads method ends here

    //admin_breeds
    public function admin_breeds(Request $request) {

        $info = Breed::
        orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "action" => " <a class='ml-2 marginRight customBtnStyle fa fa-edit edit_record row_" . $record_id . "' onclick='editRecord(`$record_id`)' href='javascript:void(0)' style='color:#007bff;'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_breeds method ends here

    //admin_disciplines
    public function admin_disciplines(Request $request) {

        $info = Discipline::
        orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "action" => " <a class='ml-2 marginRight customBtnStyle fa fa-edit edit_record row_" . $record_id . "' onclick='editRecord(`$record_id`)' href='javascript:void(0)' style='color:#007bff;'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_disciplines method ends here

    //admin_temperaments
    public function admin_temperaments(Request $request) {

        $info = Temperament::
        orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "action" => " <a class='ml-2 marginRight customBtnStyle fa fa-edit edit_record row_" . $record_id . "' onclick='editRecord(`$record_id`)' href='javascript:void(0)' style='color:#007bff;'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_temperaments method ends here

    //admin_user_horses
    public function admin_user_horses(Request $request) {

        $input = $request->all();
        $input['user_id'] = isset($input['user_id']) ? Helper::hashid_decode($input['user_id']) : "";
        //dd($input);

        $info = UserPost::
        where('user_id', $input['user_id'])
        ->orderBy('id', 'desc')
            ->get()
            ->map(function ($info){

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "headline" => Helper::handle_string($info->headline),
                    //"action" => " <a class='ml-2 marginRight customBtnStyle fa fa-edit edit_record row_" . $record_id . "' onclick='editRecord(`$record_id`)' href='javascript:void(0)' style='color:#007bff;'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_user_horses method ends here

    //admin_user_sold
    public function admin_user_sold(Request $request) {

        $input = $request->all();
        $input['user_id'] = isset($input['user_id']) ? Helper::hashid_decode($input['user_id']) : "";
        //dd($input['user_id']);

        $info = UserSoldPost::
                join('user_posts', 'user_posts.id', '=', 'user_sold_posts.post_id')
                ->where('user_posts.user_id', $input['user_id'])
                ->select('user_sold_posts.id', 'user_posts.title', 'user_posts.headline')
            ->orderBy('user_sold_posts.id', 'desc')
            ->get()
            ->map(function ($info){

                //dd($info);

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "headline" => Helper::handle_string($info->headline),
                    //"action" => " <a class='ml-2 marginRight customBtnStyle fa fa-edit edit_record row_" . $record_id . "' onclick='editRecord(`$record_id`)' href='javascript:void(0)' style='color:#007bff;'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_user_sold method ends here

    //admin_user_favourite
    public function admin_user_favourite(Request $request) {

        $input = $request->all();
        $input['user_id'] = isset($input['user_id']) ? Helper::hashid_decode($input['user_id']) : "";

        $info = UserFavouritePost::
        join('user_posts', 'user_posts.id', '=', 'user_favourite_posts.post_id')
            ->where('user_posts.user_id', $input['user_id'])
            ->select('user_favourite_posts.id', 'user_posts.title', 'user_posts.headline')
            ->orderBy('user_favourite_posts.id', 'desc')
            ->get()
            ->map(function ($info){

                //dd($info);

                $record_id = Helper::hashid_encode($info->id);

                return [
                    "id" => Helper::hashid_encode($info->id),
                    "title" => Helper::handle_string($info->title),
                    "headline" => Helper::handle_string($info->headline),
                    //"action" => " <a class='ml-2 marginRight customBtnStyle fa fa-edit edit_record row_" . $record_id . "' onclick='editRecord(`$record_id`)' href='javascript:void(0)' style='color:#007bff;'></a> <a class='fa fa-trash marginRight customBtnStyle delete_record row_" . $record_id . "' style='color:#bd2130;' onclick='deleteRecord(`$record_id`)' href='javascript:void(0)'></a>"
                ];

            });

        $count = count($request);

        $output['status'] = 1;
        $output['count'] = $count;
        $output['data'] = $info;

        return response()->json($output);

    }//admin_user_favourite method ends here

    //recent_posts
    public function recent_posts(Request $request){

        $query = "";
        $input = $request->all();
        $response = array();
        $code = 0;
        $info = array();

        try{

            //$query = Order::select()->get()->count()

            $query =  DB::select( DB::raw(" SELECT 
            COUNT(*) AS VALUE, DATE(created_at) AS currentDate
            FROM `user_posts`
            GROUP BY currentDate ORDER BY created_at ") );
            $count = count($query);
            //validation
            if ($count > 0) {

                //loop through array
                foreach($query as $r) {

                    //array
                    $info[] = array(
                        "date" => isset($r->currentDate) ? $r->currentDate : "",
                        "value" => isset($r->VALUE) ? $r->VALUE : 0
                    );

                }//loop ends here

                //$code = 200;
                $response['status'] = 1;
                $response['message'] = "posts list";
                $response['data'] =  $info;
            } else {
                //$code = 200;
                $response['status'] = 0;
                $response['message'] = "No post found yet";
            }


        } catch(\Exception $e) {
            //$code = 404;
            $response['status'] = 0;
            $response['message'] = $e->getMessage();
        }

        //json
        //return response()->json($response, $code);
        return response()->json($response);

    } //recent_posts method ends here

}//AdminServiceController class ends here