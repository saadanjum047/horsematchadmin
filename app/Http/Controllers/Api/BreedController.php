<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Validator, App;
use Tymon\JWTAuth\Payload;
use JWTAuth;
use Image,Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Data\Repositories\BreedRepository;
use Illuminate\Support\Facades\Cache;

use App\Data\Models\User;
use App\Data\Models\Breed;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserEventMatch;
use App\Data\Models\UserSoldPost;

//Helpers
use App\Helpers\Helper;


//BreedController
class BreedController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(BreedRepository $breedRepository){
        $this->_repository = $breedRepository;
    }

    //add
    public function add(Request $request) {


        $input = $request->only('user_id','title');

        try {

            $rules = [
                'user_id' => 'required|exists:users,id,user_type,admin',
                'title' => 'required|unique:breeds,title',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                unset($input['user_id']);

                //$input['created_date'] = @date("Y-m-d");
                $record = $this->_repository->create($input);

                if ($record == NULL) {
                    $code = 404;
                    $output = ['error' => ['code' => $code, 'messages' => ['Record could not be added']]];
                } else {
                    $code = 200;
                    $message = "Record is added";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record]];
                }

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//add method ends here

    //all
    public function all(Request $request){

        $input = $request->only('user_id', 'pagination','type','per_page', 'start_date', 'end_date', 'keyword', 'user_type', 'owner_id', 'created_at');
        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;

        try {

            $pagination = true;
            if(isset($input['pagination']) && $input['pagination']) {
                $pagination = $input['pagination'];
            }

            $per_page = self::PER_PAGE;
            if(isset($input['per_page']) && $input['per_page']) {
                $per_page = $input['per_page'];
            }

            $record = $this->_repository->findByAll($pagination, $per_page, $input,false,true,true);

            if ($record != NULL) {
                $code = 200;
                $message = "Record found";
                $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $record['pagination']]];
            } else {
                $code = 401;
                $message = "No record is found";
                $output = ['error' => ['code' => $code, 'messages' => [$message]]];
            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            //$message  = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//all method ends here

    public function view(Request $request) {

        $input = $request->only('user_id','id');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'id' => 'required|exists:breeds,id',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                //dd($input);
                $id = $input['id'];
                $record = $this->_repository->findById($id);
                if ($record == NULL) {
                    $code = 404;
                    $message = "Record not found";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                } else {
                    $code = 200;
                    $message = "Record found";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record]];
                }
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }
        return response()->json($output, $code);
    }//view method ends here

    public function update(Request $request) {

        $input = $request->only('user_id','id','title');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'id' => 'required|exists:breeds,id',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $id = $input['id'];
                $record = Breed::where('id', $id)->first();

                $new_array['title'] = isset($input['title']) ? $input['title'] : "";

                $update = Breed::where('id', $id)->update($new_array);
                $clear_cache = Cache::flush();

                $info = $this->_repository->findById($id);

                $code = 200;
                $message = "Record is updated";
                $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $info]];

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//update method ends here

    public function delete(Request $request) {

        $input = $request->only('user_id', 'id');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";
        $error_log = array();

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'id' => 'required|exists:breeds,id',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $action = Breed::where('id', $input['id'])->forceDelete();
                $clear_cache = Cache::flush();

                $code = 200;
                $message = "Record is deleted";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//delete method ends here

}//BreedController class ends here