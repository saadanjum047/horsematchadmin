<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Validator, App;
use Tymon\JWTAuth\Payload;
use JWTAuth;
use Image,Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Data\Repositories\UserSoldPostRepository;
use Illuminate\Support\Facades\Cache;

use App\Data\Models\User;
use App\Data\Models\UserPost;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserSoldPost;

//Helpers
use App\Helpers\Helper;


//UserSoldPostController
class UserSoldPostController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(UserSoldPostRepository $userSoldPostRepository){
        $this->_repository = $userSoldPostRepository;
    }

    //manage
    public function manage(Request $request) {

        $input = $request->only('user_id','post_id', 'buyer_id', 'action');
        $input['post_id'] = isset($input['post_id']) ? Helper::hashid_decode($input['post_id']) : "";
        $input['buyer_id'] = isset($input['buyer_id']) ? Helper::hashid_decode($input['buyer_id']) : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'post_id' => 'required|exists:user_posts,id',
                'action' => 'required|in:add,remove'
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $record = $this->_repository->manage($input);
                //record  validation
                if ($record['status'] == 1) {
                    $code = 200;
                    $message = $record['message'];
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data']]];
                } else {
                    $code = 401;
                    $message = $record['message'];
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//manage method ends here

    //all
    public function all(Request $request){

        $custom_error = "";
        $input = $request->only('user_id', 'page', 'pagination','type','per_page', 'start_date', 'end_date', 'keyword', 'user_type', 'owner_id', 'created_at');
        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;
        $input['page'] = isset($input['page']) ? $input['page'] : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                //dd($input);
                $record = $this->_repository->all($input);

                if ($record != NULL) {
                    $code = 200;
                    $message = "Record found";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $record['pagination']]];
                } else {
                    $code = 401;
                    $message = "No record is found";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//sold_list method ends here

    public function bought(Request $request){

        $custom_error = "";
        $input = $request->only('user_id', 'page', 'pagination','type','per_page', 'start_date', 'end_date', 'keyword', 'user_type', 'owner_id', 'created_at');
        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;
        $input['page'] = isset($input['page']) ? $input['page'] : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                //dd($input);
                $record = $this->_repository->bought($input);

                if ($record != NULL) {
                    $code = 200;
                    $message = "Record found";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $record['pagination']]];
                } else {
                    $code = 401;
                    $message = "No record is found";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//sold_list method ends here

}//UserSoldPostController class ends here
