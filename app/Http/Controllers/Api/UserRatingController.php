<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Validator, App;
use Tymon\JWTAuth\Payload;
use JWTAuth;
use Image,Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Data\Repositories\UserRatingRepository;
use Illuminate\Support\Facades\Cache;

use App\Data\Models\User;
use App\Data\Models\UserPost;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserSoldPost;
use App\Events\SendRatingNotification;
//Helpers
use App\Helpers\Helper;

//UserRatingController
class UserRatingController extends Controller{

    const PER_PAGE = 10;
    private $_repository;


    public function __construct(UserRatingRepository $userRatingRepository){
        $this->_repository = $userRatingRepository;
    }


    //manage
    public function manage(Request $request) {


        $input = $request->only('user_id', 'rating', 'review','buyer_id', 'action');
        $input['review'] = isset($input['review']) ? $input['review'] : "";
        $input['buyer_id'] = isset($input['buyer_id']) ? Helper::hashid_decode($input['buyer_id']) : "";
        //$input['action'] = "add";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'buyer_id' => 'required|exists:users,id',
                'rating' => 'required',
//                'action' => 'required|in:add,remove'
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                $record = $this->_repository->create($input);

                if ($record) {
                    event(new SendRatingNotification($record));

                    $code = 200;
                    $message = "You have rated";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record]];
                } else {
                    $code = 401;
                    $message = "not rated";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//manage method ends here

    //all
    public function all(Request $request){

        $custom_error = "";
        $input = $request->only('user_id', 'page', 'pagination','type','per_page', 'start_date', 'end_date', 'keyword', 'user_type', 'owner_id', 'created_at');
        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;
        $input['page'] = isset($input['page']) ? $input['page'] : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                //dd($input);
                $record = $this->_repository->all($input);

                if ($record != NULL) {
                    $code = 200;
                    $message = "Record found";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data']]];
                } else {
                    $code = 401;
                    $message = "No record is found";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//all method ends here

    //other_rating
    public function other_rating(Request $request){

        $custom_error = "";
        $input = $request->only('user_id', 'other_id', 'page', 'pagination','type','per_page', 'start_date', 'end_date', 'keyword', 'user_type', 'owner_id', 'created_at');
        $input['other_id'] = isset( $input['other_id']) ?  Helper::hashid_decode($input['other_id']) : "";
        $input['per_page'] = isset( $input['per_page']) ?  $input['per_page'] : 10;
        $input['page'] = isset($input['page']) ? $input['page'] : "";

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'other_id' => 'required|exists:users,id',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                //dd($input);
                $record = $this->_repository->other_rating($input);

                if ($record != NULL) {
                    $code = 200;
                    $message = "Record found";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data']]];
                } else {
                    $code = 401;
                    $message = "No record is found";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }

        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//other_rating method ends here

}//UserRatingController class ends here
