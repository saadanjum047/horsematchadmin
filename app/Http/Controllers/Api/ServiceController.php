<?php

namespace App\Http\Controllers\Api;

//use
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator, DB, App;
use Illuminate\Support\Facades\Input;

//models
use App\Data\Models\Country;
use App\Data\Models\City;

//ServiceController
class ServiceController extends Controller{

    const PER_PAGE = 10;

}//ServiceController class ends here