<?php

namespace App\Http\Controllers\Api;

use App\Data\Models\UserPostDiscipline;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Events\SendPostNotification;
use App\Events\SendPostUploadNotification;
use App\Data\Models\ReportedPost;
use App\Http\Controllers\Controller;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\Log;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadDataBuilder;
use App\Data\Models\Content;
use LaravelFCM\Message\PayloadNotificationBuilder;

//use
use Validator, App;
use JWTAuth;
use Illuminate\Http\Request;
use App\Data\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Image, Storage;
use Carbon\Carbon;
use DateTime;
use App\Events\SendChatNotification;
//models
use App\Data\Models\User;
use App\Data\Models\Packages;
use App\Data\Models\Subscription;
use App\Data\Models\Transaction;
use App\Data\Models\Activity;
use App\Data\Models\UserPost;
use App\Data\Models\UserDevice;
use App\Data\Models\UserFollower;

//Helpers
use App\Helpers\Helper;

//event handling
use Illuminate\Support\Facades\Event;
//use App\Events\TaskReminder;
use App\Events\TestPush;
//use App\Events\AlimUser;
//use App\Events\NormalUser;
//use App\Events\AdminMessage;
use App\Data\Models\UserRating;


class UserController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(UserRepository $user){
        $this->_repository = $user;
    }

    //register
    public function register(Request $request) {

        $timestamp = @date("Y-m-d H:i:s");
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'America/Los_Angeles');
        $dateTime = $date->setTimezone('UTC');
        $currentdate = @date("Y-m-d");

        $input = $request->only('username', 'email', 'password', 'phone','chat_id');
        $input['user_type'] = 'user';

        $input['image'] = $request->file('image');
        $input = array_filter($input);

        try{

            $rules['username'] =  'required';
            $rules['email'] =  'required|email|unique:users,email';
            $rules['password'] =  'required';
            $rules['phone'] =  'required|unique:users,phone';
            $rules['image'] =  'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:20240';

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            } else {

                //dd($input);
                $input['org_password'] = Helper::hide_and_seek('encrypt', $input['password']);

                if(isset($input['image'])){
                    $file_name = $input['image']->store(config('app.files.users.folder_name'));
                    $user_image =$input['image']->hashName();
                    $this->crop($input['image']);
                } else {
                    $user_image = "";
                }

                $input['image'] = $user_image;
                $input['signup_via'] = 'email';
                $input['created_date'] = $currentdate;
                $input['created_at'] = $dateTime;

                $userExist = $this->_repository->model->where('users.email', '=', $input['email'])->first();
                if ($userExist == NULL) {

                    $user = $this->_repository->register($input);
                    $user_id = Helper::hashid_decode($user->id);

                    //session if device type web
                    if (isset($input['device_type']) && $input['device_type'] == "web") {
                        $session = Session::put('is_front', "1");

                        $session = Session::put('session_type',$user->user_type);
                        $session = Session::put('session_email',$input['email']);
                        $session = Session::put('session_id',$user_id);
                        $session = Session::put('session_user_id',$user->id);
                        $session = Session::put('session_access_token',$user->access_token);

                    }//session if device type web

                    $clear_cache = Cache::flush();

                    $user = $this->_repository->findById($user_id);

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                    $response = Helper::checkmobi_sms($input['username'], $input['phone'], $user->verify_code);

                    $code = 200;
                    $message = "You are registered successfully";
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];
                    $output['response']['data'] = $user;
                    $output['response']['verify_code'] = $user->verify_code;
                    $output['response']['sms_status'] = $response;


                } else {
                    $code = 401;
                    $output = ['error' => ['code' => $code, 'messages' => ['User already registered with this email address.']]];
                }

            }//input validation ends here

        } catch (\Exception $e) {
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//register method ends here

    //login
    public function login(Request $request) {

        $array = array();
        $error_log = array();
        $action = "";

        $input = $request->only( 'email', 'password', 'device_type');

        try{
            $rules['email'] =  'required';
            $rules['password'] =  'required';

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            } else {

                $user = $this->_repository->login($input);

                if ($user) {

                    $user_id = Helper::hashid_decode($user->id);

                    if (isset($input['device_type']) && $input['device_type'] == "web") {
                        $session = Session::put('is_front',"1");
                        $session = Session::put('isAdmin', "yes");
                        $session = Session::put('session_type',$user->user_type);
                        $session = Session::put('session_email',$input['email']);
                        $session = Session::put('session_id',$user_id);
                        $session = Session::put('session_user_id',$user->id);
                        $session = Session::put('session_access_token',$user->access_token);
                    }

                    $code = 200;
                    $message = "Login Successfully";
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                    $output['response']['data'] = $user;

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'The provided credentials are incorrect!' ] ] ];
                }

            }//input validation ends here

        } catch (\Exception $e) {
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//login method ends here

    //verify_code
    public function verify_code(Request $request) {

        $array = array();
        $error_log = array();
        $action = "";

        $input = $request->only('verify_code');

        try{

            $rules['verify_code'] =  'required';

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            } else {

                $user = $this->_repository->verify_code($input);

                if ($user) {

                    $user_id = Helper::hashid_decode($user->id);

                    if (isset($input['device_type']) && $input['device_type'] == "web") {
                        //dd($user);
                        $session = Session::put('is_front',"1");
                        $session = Session::put('isAdmin', "yes");
                        $session = Session::put('session_type',$user->user_type);
                        $session = Session::put('session_email',$input['email']);
                        $session = Session::put('session_id',$user_id);
                        $session = Session::put('session_user_id',$user->id);
                        $session = Session::put('session_access_token',$user->access_token);
                    }

                    $code = 200;
                    $message = "You are verified successfully";
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                    $output['response']['data'] = $user;

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'The provided code does not match with our record! Try again' ] ] ];
                }

            }//input validation ends here

        } catch (\Exception $e) {
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//verify_code method ends here

    //forgotpassword
    public function forgotpassword(Request $request) {

        $array = array();
        $error_log = array();
        $action = "";

        $input = $request->only( 'email');

        try{

            $rules['email'] =  'required|email|exists:users,email';

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            } else {

                //dd($input);

                $exist = User::where('user_type', '!=', 'admin')->where('email', $input['email'])->first();
                //validation
                if ($exist != NULL) {
                    //dd($exist);
                    $key = Helper::hide_and_seek('decrypt', $exist->org_password);

                    $message = "You are receiving this email as you request for password. <br> Your password is - $key  ";
                    $sendEmail = Helper::sendEmail($input['email'], "Forget Password?", $message);

                    $code = 200;
                    $message = "Email is sent";
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ], "data" => $key ] ];
                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Email could not be sent' ] ] ];
                }//validation ends here

            }//input validation ends here

        } catch (\Exception $e) {
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//forgotpassword method ends here
    
    
    //forgotpassword
    public function forgotpasswordnew(Request $request) {

        $array = array();
        $error_log = array();
        $action = "";

        $input = $request->only('phone');

        try{

            // $rules['phone'] =  'required|exists:users.phone';

            // $validator = Validator::make($input, $rules);

                //dd($input);
                $exist = User::where('user_type','user')->where('phone',$input['phone'])->first();
                //validation
                if ($exist != NULL) {
                    //dd($exist);
                    \Log::debug($exist->phone);
                    $key = Helper::hide_and_seek('decrypt', $exist->org_password);
                    User::where('id',$exist->id)->update([
                        'is_password_changed'=>1
                    ]);
//                    $exist->is_password_changed = 1;


//                    $message = "You are receiving this email as you request for password. <br> Your password is - $key  ";
                    $sendEmail = Helper::checkmobi_sms_forgot($exist->username, $exist->phone,$key);
                    $code = 200;
                    $message = "Message is sent";
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ], "data" => $key ] ];
                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Message could not be sent' ] ] ];
                }//validation ends here

            //input validation ends here

        } catch (\Exception $e) {
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug("OOPS Something went wrong");
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//forgotpassword method ends here


    //admin_user_view
    public function admin_user_view(Request $request) {

        $input = $request->only('id');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";
        $error_log = array();

        try {

            $rules = [
                'id' => 'required|exists:users,id',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {
                $id = $input['id'];

                $user = $this->_repository->findById($id, false, true);

                if ($user == NULL) {
                    $code = 404;
                    $output = ['error' => ['code' => $code, 'messages' => ['User not found']]];
                } else {

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                    $code = 200;
                    $output = ['response' => ['code' => $code, 'data' => $user]];

                }
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//admin_user_view method ends here

    //admin_user_update
    public function admin_user_update(Request $request) {

        $input = $request->only('id', 'username');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";

        try {

            $rules = [
                'id' => 'required|exists:users,id,visibility,1',

            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $record = User::where('id', $input['id'])->first();

                $input['username'] = isset($input['username']) ?  $input['username'] : $record->username;

                $user = $this->_repository->update($input,true);
                unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                $code = 200;
                $output = ['response' => ['code' => $code, 'data' => $user]];

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//admin_user_update method ends here

    //admin_user_delete
    public function admin_user_delete(Request $request) {

        $input = $request->only('user_id', 'id');
        $input['id'] = isset($input['id']) ? Helper::hashid_decode($input['id']) : "";
        $error_log = array();

        try {

            $rules = [
                'user_id' => 'required|exists:users,id',
                'id' => 'required|exists:users,id',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $action = User::where('id', $input['id'])->forceDelete();
                $clear_cache = Cache::flush();

                $code = 200;
                $message = "Record is deleted";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];

            }//input validation ends here

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//admin_user_delete method ends here

    //view
    public function view(Request $request) {


        $input = $request->only('user_id','chat_id');
        $error_log = array();

        try {

           // dd($input);

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {



                $id = $input['user_id'];
                if(isset($input['chat_id'])){
                //    $user = $this->_repository->findByAttribute($id, false, true);
                    $user = $this->_repository->findByAttribute('chat_id', $input['chat_id'], false, true, false);

                    $user->rating=UserRating::where(['buyer_id'=>$user->id])->avg('rating');
                    $user->is_follow = UserFollower::where('user_id',$user->id)->where('follower_id',$request->user_id)->count();
                    $user->id=Helper::hashid_encode($user->id);

                } else {
                    $user = $this->_repository->findById($id, false, true);
//                    $user->rating=UserRating::where(['buyer_id'=>$user->id])->avg('rating');

                }


                if ($user == NULL) {
                    $code = 404;
                    $output = ['error' => ['code' => $code, 'messages' => ['User not found']]];
                } else {

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                    $code = 200;
                    $output = ['response' => ['code' => $code, 'data' => $user]];

                }
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//view method ends here

    //userdetail
    public function userdetail(Request $request) {

        $input = $request->only('user_id', 'input_id');
        $input['input_id'] = isset($input['input_id']) ? hashid_decode($input['input_id']) : "";
        $error_log = array();

        try {

            //dd($input);

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'input_id' => 'required|exists:users,id,visibility,1',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {
                $id = $input['input_id'];
                $user = $this->_repository->findById($id, false, true);

                // Session::set('usersession', '1');

                if ($user == NULL) {
                    $code = 404;
                    $output = ['error' => ['code' => $code, 'messages' => ['User not found']]];
                } else {

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                    $code = 200;
                    $output = ['response' => ['code' => $code, 'data' => $user]];

                }
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//userdetail method ends here

    //resetPassword
    public function resetPassword(Request $request) {

        $input = $request->only('user_id', 'old_password', 'new_password');
        $error_log = array();

        try {

            // dd($input);

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'old_password' => 'required',
                'new_password' => 'required',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                $user_detail = User::where('id', $input['user_id'])->first();

                if (Hash::check($input['old_password'], $user_detail->password)) {

                    $new_array['id'] = $input['user_id'];
                    $new_array['org_password'] = Helper::hide_and_seek('encrypt', $input['new_password']);
                    $new_array['password'] = Hash::make($input['new_password']);
                    $user_detail->is_password_changed = 0;
                    User::where('id',$user_detail->id)->update(
                        [
                            'is_password_changed'=>0
                        ]
                    );
                    $user = $this->_repository->update($new_array,true);

                    unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                    $code = 200;
                    $output = ['response' => ['code' => $code, 'messages' => ['Password is updated'], 'data' => $user]];

                } else {
                    $code = 401;
                    $output = ['error' => ['code' => $code, 'messages' => ['Your entered old password is incorrect!']]];
                }



            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//resetPassword method ends here

    //update
    public function update(Request $request) {
        $is_notification = $request->is_notification;
        $input = $request->only('user_id','chat_id', 'username', 'dob','gender','about_me', 'years_riding', 'skill_level', 'discipline', 'breed','phone','is_notification');
        $input['discipline'] = isset($input['discipline']) ? Helper::hashid_decode($input['discipline']) : "0";
        $input['breed'] = isset($input['breed']) ? Helper::hashid_decode($input['breed']) : "0";

        $input['image'] = $request->file('image');
        $input = array_filter($input);


        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:20240',
                'phone' =>  'sometimes|unique:users,phone'
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                $record = User::where('id', $input['user_id'])->first();
                if(isset($input['phone'])){
                    \Log::debug($input['phone']);
                    if($record['phone']===$input['phone']){
                        \Log::debug($input['phone']);
                        \Log::debug('done');
                                    $output = ['error' => [ 'code' => 406, 'messages' => [ 'Phone Number Already Exists' ] ] ];
                                            return response()->json($output, 406);
                    }
                }

                if(isset($input['image'])){
                    $file_name = $input['image']->store(config('app.files.venues.folder_name'));
                    $user_image =$input['image']->hashName();
                    $this->crop($input['image']);
                } else {
                    $user_image = $record->image;
                }

                $input['image'] = $user_image;
                $input['username'] = isset($input['username']) ?  $input['username'] : $record->username;
                $input['dob'] = isset($input['dob']) ?  $input['dob'] : $record->dob;
                $input['gender'] = isset($input['gender']) ?  $input['gender'] : $record->gender;
                $input['about_me'] = isset($input['about_me']) ?  $input['about_me'] : $record->about_me;
                $input['years_riding'] = isset($input['years_riding']) ?  $input['years_riding'] : $record->years_riding;
                $input['skill_level'] = isset($input['skill_level']) ?  $input['skill_level'] : $record->skill_level;
                $input['discipline'] = isset($input['discipline']) ?  $input['discipline'] : $record->discipline;
                $input['breed'] = isset($input['breed']) ?  $input['breed'] : $record->breed;
                // $input['is_notification'] = isset($input['is_notification']) ? $input['is_notification'] : $record->is_notification;
                // $input['is_notification'] = isset($input['is_notification']) ? $input['is_notification'] : $record->is_notification;
                if(isset($is_notification)){
                    $input['is_notification'] = $is_notification;
                }

                $input['id'] = $input['user_id'];
                unset($input['user_id']);
                $user = $this->_repository->update($input,true);
                unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                $code = 200;
                $output = ['response' => ['code' => $code, 'data' => $user]];

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//update method ends here

    //setting_update
    public function setting_update(Request $request) {

        $currentdate = @date("Y-m-d");

        $input = $request->only('user_id', 'is_notification');


        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'is_notification' => 'required|in:1,0',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                $record = User::where('id', $input['user_id'])->first();

                $input['is_notification'] = isset($input['is_notification']) ?  $input['is_notification'] : $record->is_notification;

                //dd($input);

                $input['id'] = $input['user_id'];
                unset($input['user_id']);
                $user = $this->_repository->update($input,true);
                unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                $cache = Cache::flush();


                $code = 200;
                $output = ['response' => ['code' => $code, 'data' => $user]];

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//setting_update method ends here

    //resend_code
    public function resend_code(Request $request) {

        $currentdate = @date("Y-m-d");

        $input = $request->only('user_id');


        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $record = User::where('id', $input['user_id'])->first();

                //verify_code
                $code = Helper::random_number(4);
                $exist = User::where('verify_code', $code)->first();
                if ($exist != NULL) {
                    $input['verify_code'] = Helper::random_number(4);
                } else {
                    $input['verify_code'] = $code;
                }//verify_code ends here

                $input['verify_status'] = '0';


                $input['id'] = $input['user_id'];
                unset($input['user_id']);
                $user = $this->_repository->update($input,true);
                //unset($user->password, $user->firebase_id, $user->news_letter, $user->notification, $user->activation_code, $user->visibility, $user->login_at, $user->recover_password_key, $user->recover_attempt_at, $user->account_visibility);

                $response = Helper::checkmobi_sms($record->username, $record->phone, $input['verify_code']);
                $message = "Welcome to HorseMatch. <br> Your verification code : <b>$code</b>  ";
                $sendEmail = Helper::sendEmail($record->email, "Welcome to HorseMatch", $message);
                \Log::info($sendEmail);

                $cache = Cache::flush();

                $code = 200;
                $output = ['response' => ['code' => $code, 'data' => $user, 'sms_status' => $response]];

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//resend_code method ends here

    //all
    public function all(Request $request) {

        $input = $request->only('user_id', 'perpage', 'pagination', 'loggedin_id', 'per_page', 'keyword', 'user_type', 'owner_id', 'device_type', 'match_id', 'sport_id');

        $input['sport_id'] = isset($input['sport_id']) ? hashid_decode($input['sport_id']) : "";
        $input['match_id'] = isset($input['match_id']) ? hashid_decode($input['match_id']) : "";

        //dd($input);
        $input['owner_id'] = isset($input['owner_id']) ? hashid_decode($input['owner_id']) : "";
        $input['loggedin_id'] = isset($input['loggedin_id']) ? hashid_decode($input['loggedin_id']) : "";
        $error_log = array();

        try {

            if (isset($input['device_type']) && $input['device_type'] == "web") {
                $this->_repository->setPaginateLinks();
            }

            $pagination = true;
            if(isset($input['pagination']) && $input['pagination'] == "false") {
                $pagination = false;
            }

            $perPage = isset($input['perpage']) && $input['perpage'] != "" ? $input['perpage'] : 10;
            //dd($perPage);

            if (isset($input['device_type']) && $input['device_type'] == "web") {
                $record = $this->_repository->findByAll($pagination, $perPage, $input, true, true, true);
            } else {
                $record = $this->_repository->findByAll($pagination, $perPage, $input, false, true, true);
            }

            if ($record != NULL) {

                $code = 200;
                $message = "Record found";
                $pagination = isset($record['pagination']) ? $record['pagination'] : "";
                $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $record['data'], 'pagination' => $pagination]];

            } else {
                $code = 401;
                $message = "No record is found";
                $output = ['error' => ['code' => $code, 'messages' => [$message]]];
            }

        }catch(\Exception $e){
            $code = 401;

            $message = "Opps something went wrong";
            //$message = $e->getFile().' '.$e->getFile().' '.$e->getMessage();

            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }
        return response()->json($output, $code);
    }//all method ends here

    //manage_device
    public function manage_device(Request $request) {

        $input = $request->only('user_id', 'udid','token','type');
        $error_log = array();

        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'udid' => 'required',
                'token' => 'required',
                'type' => 'required',
            ];

            $validator = Validator::make($input, $rules);


            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $code = 200;
                $message = "";

                //validation
                if(isset($input['udid']) && $input['udid'] != "" && isset($input['token']) && $input['token'] != "" && isset($input['type']) && $input['type'] != ""){
                    $manage_device = $this->_repository->manage_device($input);
                    $message = isset( $manage_device['message']) ?  $manage_device['message'] : "";
                    $code = 200;
                    $output = ['response' => ['code' => $code,  'messages' => [$message]]];
                } else {
                    $code = 401;
                    $output = ['error' => ['code' => $code, 'messages' => ['Device could not be managed']]];
                }

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//manage_device method ends here

    //remove_device
    public function remove_device(Request $request) {

        $input = $request->only('user_id', 'udid','token','type');
        $error_log = array();

        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'udid' => 'required',
            ];

            $validator = Validator::make($input, $rules);


            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $code = 200;
                $message = "";


                $remove_device = $this->_repository->remove_device($input);
                if (isset($remove_device['status']) && $remove_device['status'] == "1") {
                    $message = isset($remove_device['message']) ?  $remove_device['message'] : "";
                    $code = 200;
                    $output = ['response' => ['code' => $code,  'messages' => [$message]]];
                } else {
                    $code = 401;
                    $message = isset($remove_device['message']) ?  $remove_device['message'] : "";
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//remove_device method ends here

    //update
    public function block(Request $request) {

        $input = $request->only('user_id', 'id', 'is_block');

        // if (isset($input['id']) && $input['id'] ) {
        //     $input['id'] = hashid_decode($input['id']);
        // }
        // dd( (int) $input['id'] );
        try {
            $rules = [
                'user_id' => 'required|exists:users,id,user_type,admin',
                'id' => 'required|exists:users,id',
                'is_block' => 'required|in:1,0',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {
                unset($input['user_id']);
                $user = $this->_repository->update($input,true);

                $code = 200;
                $output = ['response' => ['code' => $code, 'data' => $user]];
            }

        }catch(\Exception $e){
            $code = 401;
            $message = $e->getFile().' '.$e->getFile().' '.$e->getMessage();
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }
        return response()->json($output, $code);
    }//update method ends here

    public function changePassword(Request $request){

        $input = $request->only( 'user_id', 'old_password','new_password', 'confirm_new_password');

        $rules = [
            'old_password'    => 'required',
            'new_password'         => 'required',
            'confirm_new_password'         => 'required',
        ];

        $messages = [
        ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => $validator->messages()->all()];
        } else {

            if( $input['new_password'] !== $input['confirm_new_password'] ){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ "New Password and Confirm New Password must be same. " ] ] ];
            } else {
                $output = $this->_repository->changePassword($input);

                if( gettype($output) == "object" && $output != NULL ){
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Password Changed successfully." ] ] ];
                    $output['response']['data'] = $output;
                } else if( isset($output['error']) && $output['error'] != "" ){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ $output['error'] ] ] ];
                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ "An error occurred while changing password." ] ] ];
                }
            }

        }
        return response()->json($output);
    }

    /* -------- Contact Us -------------- */

    //contact_us
    public function contact_us(Request $request) {

        $input = $request->only('user_id', 'message');
        $error_log = array();
        $user_message = array();

        $dateTime = @date("Y-m-d H:i:s");
        $currentdate = @date("Y-m-d");

        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
                'message' => 'required',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $id = $input['user_id'];
                $user = $this->_repository->findById($id);

                $user_message['user_id'] = $input['user_id'];
                $user_message['subject'] = "contactus";
                $user_message['message'] = $input['message'];
                $user_message['created_date'] = $currentdate;
                $user_message['created_at'] = $dateTime;
                $action = UserMessage::insertGetId($user_message);

                $message = $input['message'];

                //$user->email
                $email = "apptestwork@gmail.com";

                $sendEmail = sendEmail($email, "Contact Us", $message);

                $code = 200;
                $message = "Message is sent";
                $output = ['response' => ['code' => $code,'messages' => [$message]]];

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//contact_us method ends here

    //manage_preference
    public function manage_preference(Request $request) {

        $input = $request->only('user_id', 'breeds','not_interested', 'disciplines', 'genders', 'colors', 'size_start', 'size_end', 'temp_start', 'temp_end', 'price_start', 'price_end','location','lat','long','miles_start','miles_end','is_lease');

        try {

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1'
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $record = $this->_repository->manage_preference($input);

                if ($record['status'] == 1) {
                    $code = 200;
                    $message = $record['message'];
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];
                } else {
                    $code = 401;
                    $message = $record['message'];
                    $output = ['error' => ['code' => $code, 'messages' => [$message]]];
                }

            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//manage_preference method ends here

    //get_preference
    public function get_preference(Request $request) {

        $input = $request->only('user_id');
        $error_log = array();

        try {

            // dd($input);

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $user = $this->_repository->get_preference($input);

                // Session::set('usersession', '1');

                if ($user == NULL) {
                    $code = 404;
                    $output = ['error' => ['code' => $code, 'messages' => ['User not found']]];
                } else {

                    $code = 200;
                    $output = ['response' => ['code' => $code, 'data' => $user]];

                }
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
        }

        return response()->json($output, $code);

    }//get_preference method ends here

    //test_push
    public function test_push(Request $request) {

        $custom_error = "";
        $input = $request->only('user_id', 'view', 'type', 'password');

        $view = isset($input['view']) ? $input['view'] : "test";
        $type = isset($input['type']) ? $input['type'] : "push";

        $error_log = array();

        try {



            $rules = [
                'user_id' => 'required|exists:users,id',
            ];
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {
                $id = $input['user_id'];
                Event::fire(new TestPush($id, $view, $type));
                $code = 200;
                $message = "Push is sent";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];
            }

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//test_push method ends here

    public function chatNotification(Request $request) {

         $input = $request->only('chat_id','user_id','title','body','id','user_image','view_id');
     try {

            event(new SendChatNotification($input));
                $code = 200;
                $message = "Push is sent";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];

        }catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//test_push method ends here

    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.collage.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }

        }
        return true;
    }
    
        public function getPackages(){
        $packages = Packages::get();
        return response()->json(['success'=>true,'packages'=>$packages],200);
    }

    public function getSubscriptions(){
        $subscriptions = Subscription::get();
        return response()->json(['success'=>true,'subscription_packages'=>$subscriptions],200);
    }

    public function transactions(Request $request){
        $transactions = Transaction::where('user_id',$request->user_id)->with(['packages','subscriptions'])->orderBy('id','desc')->get();
        return response()->json(['success'=>true,'transactions'=>$transactions],200);
    }

    public function chargePayment(Request $request){
        $user = User::where('id',$request->user_id)->first();
         $customer = Stripe::customers()->create([
     'email' => $user->email,
 ]);
        // $token = Stripe::tokens()->create([
        //     'card' => [
        //         'number'    => '4242424242424242',
        //         'exp_month' => 10,
        //         'cvc'       => 314,
        //         'exp_year'  => 2021,
        //     ],
        // ]);
 $card = Stripe::cards()->create($customer['id'], $request['token']);
// dd($token);

//  dd($card);
         $charge = Stripe::charges()->create([
             'customer' => $card['customer'],
             'currency' => 'USD',
             'amount'   => $request->amount,
         ]);
        $user = User::where('id',$request->user_id)->first();
        if($request->type==='subscription'){
            $daycount = Subscription::where('id',$request->subscription_id)->first();
            
            $count=$daycount['horses_count'];
            $user=User::find($request->user_id);
            $user->postsremaining = $user['postsremaining']+$count;
            $user->save();
        }
        else if($request->type==='boost'){
                        $daycount = Packages::where('id',$request->package_id)->first();
            $user_post=UserPost::find(Helper::hashid_decode($request->post_id));
            $user_post['is_boosted']=1;
            $user_post->save();
        }
        $user=User::where('id',$request->user_id)->first();
        $transaction = new Transaction();
        $transaction->user_id = $request->user_id;
        if($request->type==='subscription'){
            $transaction->type='subscription';
            $transaction->subscription_id = $request->subscription_id;
        }
        if($request->type==='boost'){
            $transaction->package_id = $request->package_id;
            $package = Packages::where('id',$request->package_id)->first();
            $days = $package['numberofdays'];
            $transaction->type='boost';
            $date = (Carbon::now()->addDays($days));
            $user_post = UserPost::find(Helper::hashid_decode($request->post_id));
            $user_post['is_boosted_expiry'] = $date;
            $user_post->save();
            // $user_post = UserPost::where('id',$request->post_id)->update(['is_boosted',1]);
        }
        $transaction->amount = $request->amount;
        $transaction->save();

        return response()->json(['success'=>true,'message'=>'Card Charged Successfully']);
    }
    
    public function boostpostcron(){
        $user_posts = UserPost::whereDate('is_boosted_expiry','<=', Carbon::today())->where('is_boosted',1)->distinct('user_id')->get()->toArray();
        // $user_devices = UserDevice::whereIn('user_id',$user_posts)->get();
        foreach($user_posts as $user_post){
        $token = UserDevice::whereIn('user_id',$user_post)->select('token')->first();
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Boost Expired');
        $notificationBuilder->setBody('Boost Expired')
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['data' => [
                'title'=>"Your Boosted Post Has Been Expired",
                'body'=>"Your Boosted Post Has Been Expired",
                'type' => 'boostexpired',
                'view_id' => Helper::hashid_encode($user_post['id']),
                    ]]);
                    
                // $dataBuilder->addData(['type' => 'boostexpired']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

// You must change it to get your tokens

        $downstreamResponse = FCM::sendTo($token['token'], $option, $notification, $data);
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

// return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

// return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();

// return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

// return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
                        $activity = new Activity();
                        $activity->user_id = $user_post['user_id'];
                        $activity->action = 'post';
                        $activity->action_id = $user_post['id'];
                        $activity->object = "postExpiry";
                        $activity->title = "Boost Expired";
                        $activity->content = "Your Boosted Post Has Been Expired";
                        //   $activity->notification_id = hashid_decode($event->id);
                        $activity->send_to = $user_post['user_id'];
                        $activity->actor_id = $user_post['user_id'];
                        $activity->save();
        }
 
        $user_posts = UserPost::whereDate('is_boosted_expiry','<=', Carbon::today())->update([
            'is_boosted'=>0
            ]);
    }

    public function followUser(Request $request){
        $user_follow = UserFollower::where('user_id',Helper::hashid_decode($request->follower_id))->where('follower_id',$request->user_id)->first();
        \Log::debug($user_follow);
        if($user_follow){
            $user_follow->delete();
            return response()->json(['success'=>true,'message'=>'User Unfollowed Successfully']);
        }
        else{
            $user_follow = new UserFollower();
            $user_follow->user_id = Helper::hashid_decode($request->follower_id);
            $user_follow->follower_id = $request->user_id;
            $user_follow->save();
            \Log::debug($user_follow);
            return response()->json(['success'=>true,'message'=>'User Followed Successfully']);
        }
    }

    public function reportpost(Request $request){
        $user_post = new ReportedPost();
        $user_post->user_id = ($request->user_id);
        $user_post->post_id = Helper::hashid_decode($request->post_id);
        $user_post->reported_message = $request->reported_message;
        $user_post->save();
        return response()->json(['success'=>true,'message'=>'Your Report Has Been Submitted Successfully']);

//        $user_post = UserPost::where('id',Helper::hashid_decode($request->post_id))->first();
//        $user_post->is_reported = 1;
//        $user_post->reported_message = $request->reported_message;
//        $user_post->save();
//        return response()->json(['success'=>true,'message'=>'Your Report Has Been Submitted Successfully']);
    }
    
        public function getContent($type){
        $content = Content::where('type',$type)->first();
        return response()->json(['success'=>true,'content'=>$content]);
    }

    
}

