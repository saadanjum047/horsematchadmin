<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Validator, App;

use App\Data\Repositories\ActivityRepository;

use App\Data\Models\Activity;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cache;


class ActivityController extends Controller{

    const PER_PAGE = 10;
    private $_repository;

    public function __construct(ActivityRepository $activity){
        $this->_repository = $activity;
    }

    // admin_activities
    public function admin_activities_backup(Request $request){

        try {

            $input = $request->only('user_id', 'pagination', 'perpage','keyword', 'device_type');
            //dd($input['status']);

            $rules = [
                // 'user_id' => 'required|exists:users,id,visibility,1',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $activecount = Activity::where('user_id', $input['user_id'])->where('viewed', '0')->count();


                $articles = $this->_repository->admin_activities($input);

                if ($articles == null || count($articles) == 0) {
                    $code = 200;
                    $message = "User Skill Records Not Found !";
                    $output = ['response' => ['code' => $code, 'activecount' => $activecount, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    $output = ['response' => ['code' => $code, 'activecount' => $activecount, 'messages' => [$message], 'data' => $articles]];
                }

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//admin_activities method ends here

    // admin_activities
    public function admin_activities(Request $request){

        try {

            $input = $request->only('user_id', 'admin_loggedin', 'own_id', 'is_other','keyword', 'perpage',  'status', 'device_type','city_id','sport_id','zip_code', 'viewed', 'is_admin');

            //$input['viewed'] = '0';
            $input['is_admin'] = '1';

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $activecount = Activity::where('user_id', $input['user_id'])->where('viewed', '0')->count();


                //device handling
                if (isset($input['device_type']) && $input['device_type'] == "web") {
                    $this->_repository->setPaginateLinks();
                }

                $perPage = isset($input['perpage']) && $input['perpage'] != "" ? $input['perpage'] : 10;
                $articles = $this->_repository->findByAll(true, $perPage, $input, true, true, false);


                if ($articles == null || count($articles) == 0) {
                    $code = 401;
                    $message = "Venue Records Not Found !";
                    $output = ['response' => ['code' => $code, 'activecount' => $activecount, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    $output = ['response' => ['code' => $code, 'activecount' => $activecount, 'messages' => [$message], 'data' => $articles['data'], 'pagination' => $articles['pagination']]];
                }

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//admin_activities method ends here

    // all
    public function all(Request $request){

        try {

            $input = $request->only('user_id', 'admin_loggedin', 'own_id', 'is_other','keyword', 'perpage',  'status', 'device_type','city_id','sport_id','zip_code', 'viewed', 'is_admin');

            $input['viewed'] = '0';
            $input['is_admin'] = '0';

            $rules = [
                'user_id' => 'required|exists:users,id,visibility,1',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {


                //device handling
                if (isset($input['device_type']) && $input['device_type'] == "web") {
                    $this->_repository->setPaginateLinks();
                }

                $perPage = isset($input['perpage']) && $input['perpage'] != "" ? $input['perpage'] : 10;
                $articles = $this->_repository->findByAll(true, $perPage, $input, true, true, false);



                if ($articles == null || count($articles) == 0) {
                    $code = 401;
                    $message = "Venue Records Not Found !";
                    $output = ['response' => ['code' => $code, 'messages' => [$message]]];

                } else {
                    $code = 200;
                    $message = "Request Completed Successfully ";
                    $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $articles['data'], 'pagination' => $articles['pagination']]];
                }

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//all method ends here

    // admin_activity_read
    public function admin_activity_read(Request $request){

        try {

            $input = $request->only('user_id', 'id');
           $input['id'] = isset($input['id']) ? hashid_decode($input['id']) : "";

            $rules = [
                // 'user_id' => 'required|exists:users,id,visibility,1',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            } else {

                $array['viewed'] = '1';
                $action = Activity::where('id', $input['id'])->update($array);

                $cache =  Cache::flush();

                $code = 200;
                $message = "User Skill Records Not Found !";
                $output = ['response' => ['code' => $code, 'messages' => [$message]]];

            }//input validation ends here

        } catch (\Exception $e){
            $code = 401;
            $message=$e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($message);
            $message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//admin_activity_read method ends here

}
