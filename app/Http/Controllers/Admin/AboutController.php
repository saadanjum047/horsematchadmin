<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Data\Models\Setting;
use Redirect;
use Input;
use Validator;
use Illuminate\Validaion\ValidaionException;

class AboutController extends Controller {


	public function index( Request $request ) 
	{
		$title = 'About Us';
		$about = Setting::where('id',3)->first();

		return view("admin.pages.about.about",
			[
				'title' => $title,
				'about' => $about,
			]
		);
	}

	public function update_about( Request $request ) 
	{
		
		$input = $request->input();
		
		$policy = Setting::find(3);
		
		$policy->content = $input['description'];
		
		$policy->save();
		
		return redirect('about-us');
	}

}