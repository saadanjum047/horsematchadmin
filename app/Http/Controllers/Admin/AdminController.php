<?php

namespace App\Http\Controllers\Admin;

use JWTAuth;
use stdClass;
use Carbon\Carbon;
use Validator, App;
use App\Helpers\Helper;
use App\Data\Models\Blog;
use App\Data\Models\User;
use App\Data\Models\Setting;
use checkmobi\CheckMobiRest;
use Illuminate\Http\Request;
use App\Data\Models\Packages;
use App\Data\Models\UserPost;
use App\Jobs\SendNotification;
use App\Data\Models\Promotions;
use App\Jobs\SendNotifications;
use App\Data\Models\Transaction;
use App\Data\Models\Subscription;
use App\Data\Models\UserSoldPost;


use Illuminate\Support\Facades\DB;
use App\Data\Models\CustomerSupport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Data\Repositories\UserRepository;

class AdminController extends Controller{

    //index


    public function index(Request $request)
    {
        try{
            //title

            $query = 'SELECT COUNT(*) count FROM `users`';
            $results = DB::select( DB::raw($query) ) ;

            $title = "Dashboard";

            $recent_members = User::where('user_type' , 'user')->count();

         //   $CustomerSupport = CustomerSupport::count();

            $UserPost = UserPost::count();


            $userdataset = DB::select("SELECT count(id) as total,MONTH(created_at) as month FROM `users` GROUP BY MONTH(created_at)");

            $adsdataset = DB::select("SELECT count(id) as total, MONTH(created_at) as month FROM `user_posts` GROUP BY MONTH(created_at)");

            $amountdataset = DB::select("SELECT sum(user_posts.price) as total, MONTH(user_posts.created_at) as month FROM `user_sold_posts` INNER JOIN `user_posts` ON user_posts.id=user_sold_posts.post_id GROUP BY MONTH(created_at)");
            
            $soldhorses = DB::select("SELECT count(id) as total,MONTH(created_at) as month FROM `user_sold_posts` GROUP BY MONTH(created_at)");
            
            
            $sellinghorsescount = DB::select("SELECT count(id) as sum FROM `user_sold_posts`");
            
            $_soldHorses = UserSoldPost::all();
            // ->whereNotIn('user_posts.id' , $soldHorses->pluck('post_id')->toarray() )
            
            // $soldhorsescount = DB::select("SELECT count(id) as sum FROM `user_posts`") ;

            $_soldhorsescount = UserPost::whereNotIn('id' , $_soldHorses->pluck('post_id')->toarray() )->count();
            
            $sum = new stdClass();
            $sum->sum = $_soldhorsescount; 
            // dd($sum);
            $soldhorsescount[] = $sum;
            
            // dd($soldhorsescount);


            $sellinghorses = DB::select("SELECT count(id) as total,MONTH(created_at) as month FROM `user_posts` GROUP BY MONTH(created_at)");


            $sum = UserPost::join('user_sold_posts','user_sold_posts.post_id','=','user_posts.id')->sum('user_posts.price');
            
            $transactions_dataset = DB::select("SELECT sum(amount) as total,MONTH(created_at) as month FROM `transactions` GROUP BY MONTH(created_at)");

            $sum = Transaction::sum('amount');
            // dd($sum , $transactions_dataset);



            $sellinghorsescount = $sellinghorsescount[0]->sum;
            $soldhorsescount = $soldhorsescount[0]->sum;
            $sellinghorses_arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($sellinghorses as $item){
                $sellinghorses_arr[$item->month - 1] = intval($item->total);
            }
            $soldhorses_arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($soldhorses as $item) {
                $soldhorses_arr[$item->month - 1] = intval($item->total);
            }
            $amount_arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($amountdataset as $item){
                $amount_arr[$item->month - 1] = intval($item->total);
            }
            $ads_arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($adsdataset as $item){
                $ads_arr[$item->month - 1] = intval($item->total);
            }
            $user_arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($userdataset as $item) {
                $user_arr[$item->month - 1] = intval($item->total);
                
            }

            $transactions_arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($transactions_dataset as $item) {
                $transactions_arr[$item->month - 1] = intval($item->total);
            }
            // dd($transactions_arr);
            //view
            return view("admin.index",
                [
                    'title'             => $title,
                    'recent_members'    => $recent_members,
                  //  'CustomerSupport'   => $CustomerSupport,
                    'UserPost'          => $UserPost,
                    'sum'               => $sum,
                    'transactions_arr'  => $transactions_arr,

                    'results'           => $results,
                    'user_arr'           => $user_arr,
                    'ads_arr'           => $ads_arr,
                    'amount_arr'        => $amount_arr,
                    'soldhorses'       =>$soldhorses_arr,
                    'sellinghorses'    =>$sellinghorses_arr,
                    'soldhorsescount'   =>$soldhorsescount,
                    'sellinghorsescount' =>$sellinghorsescount
                ]
            );

        }
        catch(\Exception $e) {
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            echo '<center>Something went wrong</center>';
        }

    } //index method ends here

    //login
    public function login(Request $request){
        //title
        $this->data['title'] = "Admin Login";
        //view
        return view("admin.auth.signin", $this->data);
    } //login method ends here

    //doLogin
    public function doLogin(Request $request){
        try{

            $input = $request->only('email', 'password');
            $useremail = trim(strip_tags($input['email']));
            $userpassword = trim(strip_tags($input['password']));

            $record = User::where('email', $input['email'])->where('user_type', 'admin')->first();
            if ($record != NULL) {

                //account handling
                if (Hash::check($input['password'], $record->password)) {

                    $user = User::where('email', $input['email'])->where('user_type', 'admin')->first();
                    $user_data['access_token'] = JWTAuth::fromUser($user);
                    $action = User::where('id', $user->id)->update($user_data);

                    $request->session()->put('isAdmin', 'yes');
                    $request->session()->put('user_type', 'admin');
                    $request->session()->put('user_email', $useremail);
                    $request->session()->put('user_name',$user->username);

                    return redirect('dashboard');
                } else {
                    $request->session()->put('message','Invalid Email Or Password');
                    return redirect()->back()->with('error','Invalid Email Or Password');
                }//account handling ends here
            } else {
                $request->session()->put('message','Invalid Email Or Password');
                return redirect()->back()->with('error','Invalid Email Or Password');
            }


        } catch(\Exception $e) {
           $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            echo '<center>Something went wrong</center>';
        }

    }//doLogin method ends here

    public function promotions(){
        $promotions = Promotions::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.promotions',[
            'promotions'=>$promotions
        ]);
    }

    public function promotionsNew(){
        $promotions = Promotions::orderBy('created_at', 'desc')->get();
        return view('admin.promotions-new',[
            'promotions'=>$promotions
        ]);
    }


    public function sendPromotion(Request $request){
//        $users = User::get_users();
//        $api = new CheckMobiRest("9AE029A2-7EF6-4F0D-8621-3D7C3627D551");
//        foreach ($users as $user){
//            $api->SendSMS(array("from" => "HorseMatch", "to" => $user->phone, "text" => $request->description));
//            Helper::sendEmail($user->email,$request->title,$request->description);
//        }
        $promotion = new Promotions();
        $promotion->promotion_text = $request->description;
        $promotion->number_of_users = User::where('user_type' , 'user')->count();
        $promotion->save();

        $users = User::latest()->where('user_type' , 'user')->get();

        foreach ($users as $k => $user){
            SendNotification::dispatch("Promotion Sent By Horsematch Admin",$request->description , $promotion , $user , $k );
        }
        session()->put('message','Promotions Sent Successfully');
        return redirect()->back();
    }

    public function getPackages(){
        $packages= Packages::paginate(5);
        return view("admin.packages",
            [
                'title' => 'Packages Management',
                'packages' => $packages
            ]
        );
    }
    public function getPackagesNew(){
        $packages= Packages::all();
        return view("admin.packages-new",
            [
                'title' => 'Packages Management',
                'packages' => $packages
            ]
        );
    }

    public function addNewPackage(Request $request){
        $package = new Packages();
        $package->name = $request['packagename'];
        $package->price = $request['price'];
        $package->numberofdays = $request['numberofdays'];
        $package->amount = $request['price'];
        $package->save();
        return response()->json(['success'=>true]);
    }

    public function editPackage(Request $request){
        Packages::where('id',$request['id'])->update([
            'name'=>$request['packagename'],
            'price'=>$request['price'],
            'numberofdays'=>$request['numberofdays'],
            'amount'=>$request['amount']
        ]);
        return response()->json(['success'=>true]);
    }

    public function deletepackage(Request $request){
        Packages::where('id',$request['id'])->delete();
        return response()->json(['success'=>true]);
    }

    public function getSubscriptions(){
        $subscriptions= Subscription::paginate(5);
        return view("admin.subscription",
            [
                'title' => 'Subscription Management',
                'subscriptions' => $subscriptions
            ]
        );
    }
    public function getSubscriptionsNew(){
        $subscriptions= Subscription::all();
        return view("admin.subscription-new",
            [
                'title' => 'Subscription Management',
                'subscriptions' => $subscriptions
            ]
        );
    }

    public function addnewSubscription(Request $request){
        $subscription = new Subscription();
        $subscription->name = $request->packagename;
        $subscription->duration = $request->numberofdays;
        $subscription->horses_count = $request->numberofhorses;
        $subscription->amount = $request->price;
        $subscription->save();
        return response()->json(['success'=>true]);
    }
    public function deletesubscription(Request $request){
        Subscription::where('id',$request['id'])->delete();
        return response()->json(['success'=>true]);
    }

    public function editsubscription(Request $request){
        Subscription::where('id',$request->id)->update([
            'name'=>$request['packagename'],
            'duration'=>$request['numberofdays'],
            'horses_count'=>$request['numberofhorses'],
            'amount'=>$request['price']
        ]);
        return response()->json(['success'=>true]);
    }
    public function getTransactions(){
        $boosthistory = Transaction::where('type','boost')->with(['user','packages','subscriptions'])->orderBy('created_at','DESC')->paginate(5);
        $packagehistory = Transaction::where('type','subscription')->with(['user','packages','subscriptions'])->orderBy('created_at','DESC')->paginate(5);
        $total_amount = Transaction::sum('amount');
        return view("admin.paymenthistory",
            [
                'title' => 'Payment History',
                'boosthistory' => $boosthistory,
                'total_amount' => $total_amount,
                'packagehistory' => $packagehistory
            ]
        );

    }

    public function getTransactionsNew(){
        $boosthistory = Transaction::where('type','boost')->with(['user','packages','subscriptions'])->orderBy('created_at','DESC')->get();
        $packagehistory = Transaction::where('type','subscription')->with(['user','packages','subscriptions'])->orderBy('created_at','DESC')->get();
        $total_amount = Transaction::sum('amount');
        return view("admin.paymenthistory-new",
            [
                'title' => 'Payment History',
                'boosthistory' => $boosthistory,
                'total_amount' => $total_amount,
                'packagehistory' => $packagehistory
            ]
        );

    }

    public function deleteuseradmin(Request $request){
        User::where('id',(int)$request->id)->forceDelete();
        return response()->json(['success'=>true]);
    }

    public function deleteblogadmin(Request $request){
        Blog::where('id',(int)$request->id)->forceDelete();
        return response()->json(['success'=>true]);
    }
    
        public function changepasswordadmin(Request $request){
        User::where('email','admin@horsematch.com')->update([
           'password'=>bcrypt($request->password)
        ]);
        return response()->json(['success'=>true]);
    }
}
