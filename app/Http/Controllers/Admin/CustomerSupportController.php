<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Data\Models\CustomerSupport;
use Redirect;
use Input;
use Validator;
use Illuminate\Validaion\ValidaionException;

class CustomerSupportController extends Controller 
{

	public function index(Request $request) 
	{
		$title = 'Customer Support - Messages';
		$messages = CustomerSupport::get_messages();
		
		return view("admin.pages.customer_support.index",
			[
				'title' 	=> $title,
				'messages' 	=> $messages
			]
		);
	}


	public function view (Request $request , $id ) 
	{
		$title = 'View Message';
		
		$CustomerSupport = CustomerSupport::find($id);

		$content = 'Hello ' . $CustomerSupport->username . ",<br /><br />";

		$footer = '<br /><br />' . 'Thanks, <br /> Regards, <br />Horse Match';

		$message = $content . $footer;

		if ($CustomerSupport) {
			return view("admin.pages.customer_support.view",
				[
					'title' 			=> $title,
					'CustomerSupport' 	=> $CustomerSupport,
					'message' 			=> $message,
					'id' 				=> $id
				]
			);
		}
		else {
			return redirect('blogs');
		}
	}

	public function delete (Request $request , $id )
	{
		
		$CustomerSupport = CustomerSupport::find($id);
		
		$CustomerSupport->delete();
		
		return redirect('customer-support');
	}


	public function send( Request $request )
	{
		
		$input = $request->input();
		
		$id = $input['contact_id'];
		
		$description = $input['description'];
		
		$CustomerSupport = CustomerSupport::find($id);
		
		$email = $CustomerSupport->email;
		
		$name = $CustomerSupport->username;

		// Assigning Sender Name and Email
		$to_name = $name;
		
		$to_email = $email;
		
	    $title = 'You have new message from Horse match';
	    
	    $content = $description;

	    \Mail::send('emails.mail', ['name' => $name, 'email' => $email, 'title' => $title, 'content' => $content], function ($message) use ($title,$CustomerSupport) {

            $message->to($CustomerSupport->email)->subject('You have new message from Horse match');
        });

        
        $CustomerSupport->replied = $description;
        
        $CustomerSupport->sent = 1;
        
        $CustomerSupport->save();

	    return redirect('customer-support');
	}
}