<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Data\Models\PackageManagement;
use Redirect;
use Input;
use Validator;
use Illuminate\Validaion\ValidaionException;

class PackageManagementController extends Controller {


	public function index( Request $request ) 
	{
		$title = 'Package Management';

		$packages = PackageManagement::get_packages();

		return view("admin.pages.package_management.index",
			[
				'title' 	=> $title,
				'packages' 	=> $packages,
			]
		);
	}

	public function update_about( Request $request ) 
	{
		$input = $request->input();
		
		$policy = Setting::find(3);
		
		$policy->content = $input['description'];
		
		$policy->save();
		
		return redirect('about-us');
	}

	public function delete( Request $request, $package_id ) 
	{
		$package = PackageManagement::find($package_id);
		
		$package->delete();
		
		return redirect('package-management');
	}

	public function add_package( Request $request ) 
	{
		$title = 'Add Package';

		return view("admin.pages.package_management.add",
			[
				'title' 	=> $title
			]
		);
	}


	public function view_package( Request $request,$package_id)
	{
		echo 'view package of '.  $package_id;
	}

	public function update_package( Request $request,$package_id)
	{
		echo $package_id;
	}

	public function store_package( Request $request )
	{
		$input = $request->input();

		$criteria = '';

		$package_type = $input['package_type'];

		if ( $package_type == 'Subscription' ) {
			$criteria = 'quantity';
		}
		else if ( $package_type == 'Featured Post' ) {
			$criteria = 'day';
		}


		$packageManagement = new PackageManagement;
		
		$packageManagement->name = $input['name_of_package'];

		$packageManagement->price = $input['price'];

		$packageManagement->package_type = $input['package_type'];

		$packageManagement->quantity = $input['quantity'];

		$packageManagement->criteria = $criteria;

		$packageManagement->save();
		
		return redirect('package-management');

	}

	public function check_package( Request $request )
	{
		
		$input = $request->input();
		
		$name = $input['name_of_package'];
		
		$package_type = $input['package_type'];

		if ( $package_type == 'Featured Post' ) {
			
			$count = PackageManagement::where('package_type','=',$package_type)->count();

			if ( $count == 3 ) {
				echo '1';
			}
			else {
				echo '2';
			}
		}
	}
	
}