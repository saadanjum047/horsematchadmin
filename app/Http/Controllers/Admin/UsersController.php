<?php

namespace App\Http\Controllers\Admin;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Data\Models\Blog;
use App\Data\Models\User;
use App\Data\Models\Content;
use Illuminate\Http\Request;
use App\Data\Models\Activity;
use App\Data\Models\UserPost;
use App\Jobs\SendNotification;
use App\Data\Models\Discipline;
use App\Data\Models\Promotions;
use App\Data\Models\UserRating;
use App\Jobs\SendNotifications;
use App\Data\Models\UserSoldPost;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class UsersController extends Controller
{

	public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            if($request->searchparam){
                $title = 'Users Detail';
                $soldarray = [];
                $sellingarray = [];
                $users = DB::table('users')
                    ->where('user_type','!=','admin')
                    ->where('username', 'like', '%' . $request->searchparam . '%')
                    ->orWhere('email', 'like', '%' . $request->searchparam . '%')
                    ->orderBy('users.id','DESC')
                    ->paginate(10);
                foreach ($users as $user){
                    $sold = DB::table('user_sold_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $sold = array_push($soldarray,$sold);
                }
                foreach ($users as $user){
                    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $selling = array_push($sellingarray,$selling);
                }
                return view("admin.users",
                    [
                        'users' => $users,
                        'title' => $title,
                        'soldarray' => $soldarray,
                        'sellingarray' => $sellingarray,
                        'searchparam' => $request->searchparam
                    ]
                );
            }
        }
		$title = 'Users Detail';
		$soldarray = [];
		$sellingarray = [];
		$users = User::get_users();
		foreach ($users as $user){
		    $sold = DB::table('user_sold_posts')
                    ->where('user_id','=',$user->id)
                    ->select('id')
                    ->count();
            $sold = array_push($soldarray,$sold);
        }
		foreach ($users as $user){
		    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
		    $selling = array_push($sellingarray,$selling);
        }
		return view("admin.users",
			[
				'users' => $users,
				'title' => $title,
                'soldarray' => $soldarray,
                'sellingarray' => $sellingarray,
                'searchparam' => ''
			]
		);
	}

	public function users_details( Request $request, $id ) {
		$title = 'Details';
		$user = User::get_user($id);

		return view("admin.userdetail",
			[
				'title' => $title,
				'user' => $user
			]
		);
	}
	public function changeStatus (Request  $request){
	    $user_id = $request->userid;
	    $user = User::where('id',$user_id)->first();
        $user_block = $user->is_block===0?1:0;
	    User::where('id',$user_id)->update(['is_block'=>$user_block]);
        return response()->json(['update'=>'User Status Updated Successfully']);
    }

    public function sellingHorses(Request  $request){
        $title = "Selling Horses";
        $data = DB::table('user_posts as up')
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->select('up.reported_message as reported_message','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
            ->whereMonth('up.created_at', Carbon::now()->month)
            ->paginate(10);
        return view("admin.sellinghorses",
            [
                'data' => $data,
                'title' => $title
            ]
        );
    }
    
    
        public function sellinghorsesnew(Request  $request){
        $title = "Selling Horses";
        $data = DB::table('user_posts as up')
            ->groupBy('up.id')
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->join('user_post_images as uip','uip.post_id','=','up.id')
            ->orderBy('up.created_at','desc')
            ->select('u.image as userimages','up.created_at','u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
            ->whereMonth('up.created_at', Carbon::now()->month)
            ->paginate(5);
        return view("admin.sellinghorsesnew",
            [
                'data' => $data,
                'title' => $title
            ]
        );
    }
    public function sellinghorsesnewNew(Request  $request){
        $title = "Selling Horses";
        // $data = DB::table('user_posts as up')
        //     ->groupBy('up.id')
        //     ->leftJoin('users as u','u.id','=','up.user_id')
        //     ->leftJoin('breeds as b','b.id','=','up.breed_id')
        //     ->leftJoin('genders as g','g.id','=','up.gender_id')
        //     ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
        //     ->join('user_post_images as uip','uip.post_id','=','up.id')
        //     ->orderBy('up.created_at','desc')
        //     ->select('u.image as userimages','up.created_at','u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
        //     ->whereMonth('up.created_at', Carbon::now()->month)
        //     ->paginate(5);
        return view("admin.sellinghorsesnew-new",
            [
                // 'data' => $data,
                'title' => $title
            ]
        );
    }
    
    public function sellinghorsesAjax(Request  $request){

        $soldHorses = UserSoldPost::all();

        $all_horses = UserPost::
        groupBy('id')
        ->whereNotIn('user_posts.id' , $soldHorses->pluck('post_id')->toarray() )
        ->leftJoin('users as u','u.id','=','user_posts.user_id')
        ->leftJoin('breeds as b','b.id','=','user_posts.breed_id')
        ->leftJoin('genders as g','g.id','=','user_posts.gender_id')
        ->leftJoin('disciplines as d','d.id','=','user_posts.discipline_id')
        ->leftJoin('user_post_images as uip','uip.post_id','=','user_posts.id')

        // sorting
        ->when(request('sort'), function ($q) {
            $q->orderBy(request('sort') , request('order'));
        })
        // searching
        ->when(request('search'), function ($q) {
            $q->where('user_posts.title', 'like', '%' . request('search') . '%');
            $q->orWhere('u.username', 'like', '%' . request('search') . '%');
            $q->orWhere('g.title', 'like', '%' . request('search') . '%');
            $q->orWhere('user_posts.price', 'like', '%' . request('search') . '%');
            $q->orWhere('b.title', 'like', '%' . request('search') . '%');
            $q->orWhere('user_posts.size', 'like', '%' . request('search') . '%');
            $q->orWhere('user_posts.temperament', 'like', '%' . request('search') . '%'); 
        })

        ->select('u.image as userimages','user_posts.created_at','u.phone as phone','user_posts.address as address','uip.image as image','user_posts.id as id','b.title as breedtitle','user_posts.temperament as temperament','user_posts.title as horsetitle','user_posts.size as size','user_posts.price as price','g.title as gender','d.title as discipline','u.username as username','user_posts.created_date as ad_posted')
        ->orderBy('created_at' , 'desc')
        ->get();


        // dd($all_horses[0]);

        $horses = $all_horses->slice($request->offset , $request->limit)->values();

        foreach($horses as $horse){

            $horse->username = ucwords(mb_convert_encoding($horse->username , 'UTF-8', 'UTF-8'));

            
            if(!$horse->horsetitle){
                $horse->horsetitle = 'N/A';

            }else{
                $horse->horsetitle = str_replace('"', '', $horse->horsetitle);
            }

            if(!$horse->breed){
                $horse->breedtitle = 'N/A';
            }
            if(!$horse->gender){
                $horse->gender = 'N/A';
            }
            
            if($horse->userimages){
                $horse->image = '<span class="avatar w-40">
                <img src="storage/app/users/'.$horse->userimages.'" alt="..."></span>';
            }else{
                $horse->image = '<span class="avatar w-40">
                <img src="http://dev-cmolds.com/horsematch/public/adminassets/pngfind.com-avatar-png-icon-3819326.png" alt="..."></span>';
            }

            
            $fun = "window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]={$horse->horsetitle}&amp;p[url]=".route('facebook-share' , $horse->id )."','sharer','toolbar=0,status=0,width=550,height=300');";

            $share_link =  route('facebook-share' , $horse->id );


            $horse->action = '
            
            <a href="javascript:;" class="text-primary " onClick="'. $fun .'"><i class="fab fa-facebook tool-tip" data-toggle="tooltip" title="facebook share" ></i></a>

            <a href="javascript:;" data-clipboard-text="'.$share_link.'" class="share-link1" ><i id="'.$share_link.'" class="fa fa-paperclip text-success tool-tip" title="copy link" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Copy Link</span></a>

            <a href="javascript:;" onclick="viewsellinghorses('.$horse->id.')" data-id="active" class="viewBtn viewDetailsBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye tool-tip" title="view" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">View</span></a>

            <a href="javascript:;" onclick="deletehorse('.$horse->id.')" data-id="active" class="deleteBtn confirmDel" data-toggle="modal" ><i class="fa fa-trash-o tool-tip" title="delete" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Delete</span></a>

            
            <a href="javascript:;" onclick="showHorseImages('.$horse->id.')" data-id="active" class="text-warning" data-toggle="modal" ><i class="fa fa-images tool-tip" title="show images" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Delete</span></a>
            ';
        }

        


            return response()->json([ 
                'rows' => $horses , 
                'total' => $all_horses->count(), 
                'totalNotFiltered' => $all_horses->count(),
                ]);
    }


    public function getHorsesImagesAjax($post_id){

        $horse_images = DB::table('user_post_images as image')->where('image.post_id' , $post_id)->select('*')->get()->pluck('image')->toArray();

        return response()->json(['images' => $horse_images ]);
    }


    public function sellinghorsesAjaxEloquent(Request  $request){

        $all_horses = UserPost::with('breed' , 'user' , 'gender' , 'discipline' , 'images')
        
        ->when(request('search'), function ($q) {
            $q->where('title', 'like', '%' . request('search') . '%');
            $q->orWhere('price', 'like', '%' . request('search') . '%');
            $q->orWhere('size', 'like', '%' . request('search') . '%');
            $q->orWhere('temperament', 'like', '%' . request('search') . '%');
            
            $q->orWhereHas('gender', function($q){
                $q->where('title', 'like', '%' . request('search') . '%');
            });
        
            $q->orWhereHas('breed', function($q){
                $q->where('title', 'like', '%' . request('search') . '%');
            });
        
            $q->orWhereHas('user', function($q){
                $q->where('username', 'like', '%' . request('search') . '%');
            });
            // $q->orWhere('gender.title', 'like', '%' . request('search') . '%');
        })
        ->get();
        
        // dd($horses , $horses[0]->gender);
        
        foreach($all_horses as $horse){
        
            if($horse->title){
                $horse->horsetitle = str_replace('"', '', $horse->title);                
                
                // $horse->horsetitle = $horse->title;
            }else{
                $horse->horsetitle = 'N/A';
            }
        
            if($horse->breed){
                // dd($horse->breed);
                $horse->breedtitle = $horse->breed->title;
            }else{
                $horse->breedtitle = 'N/A';
            }
            
            if($horse->gender){
                $title = $horse->gender->title;
                unset($horse->gender);
                $horse->gender = $title;
            }else{
        
                $horse->gender = 'N/A';
            }
            
            if($horse->discipline){
                // $horse->discipline = $horse->discipline->title;
            }else{
                $horse->discipline = 'N/A';
            }
            if($horse->user){
                $horse->username = ucwords($horse->user->username);
                $horse->phone = ucwords($horse->user->phone);
            }
        
            if($horse->userimages){
                $horse->image = '<span class="avatar w-40">
                <img src="storage/app/users/'.$horse->userimages.'" alt="..."></span>';
            }else{
                $horse->image = '<span class="avatar w-40">
                <img src="http://dev-cmolds.com/horsematch/public/adminassets/pngfind.com-avatar-png-icon-3819326.png" alt="..."></span>';
            }
        
            
            $fun = "window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]={$horse->horsetitle}&amp;p[url]=".route('facebook-share' , $horse->id )."','sharer','toolbar=0,status=0,width=550,height=300');";
        
            $share_link =  route('facebook-share' , $horse->id );
        
            $horse->action = '
            
            <a href="javascript:;" class="text-primary" onClick="'. $fun .'"><i class="fab fa-facebook" data-toggle="tooltip" title="facebook share" ></i></a>
        
            <a href="javascript:;" data-clipboard-text="'.$share_link.'" class="share-link1" ><i id="'.$share_link.'" class="fa fa-paperclip text-success" title="copy link" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Copy Link</span></a>
        
            <a href="javascript:;" onclick="viewsellinghorses('.$horse->id.')" data-id="active" class="viewBtn viewDetailsBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="view" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">View</span></a>
        
            <a href="javascript:;" onclick="deletehorse('.$horse->id.')" data-id="active" class="deleteBtn confirmDel" data-toggle="modal" ><i class="fa fa-trash-o" title="delete" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Delete</span></a>
            ';
        }
        
        if(request('sort')){
            if(request('order') == 'asc' ){
                $all_horses = $all_horses->sortBy(request('sort'));
            }else{
                $all_horses = $all_horses->sortByDesc(request('sort'));
            }
        }
        
        $horses = $all_horses->slice($request->offset , $request->limit)->values();
        // $horses = $all_horses->slice( 10 , 10)->values();
        
        
        
            return response()->json([ 
                'rows' => $horses , 
                'total' => $all_horses->count(), 
                'totalNotFiltered' => $all_horses->count(),
                ]);
        
        }
        

    public function sellinghorsesAjaxOld(Request  $request){

        $data = DB::table('user_posts as up')
            ->groupBy('up.id')
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->join('user_post_images as uip','uip.post_id','=','up.id')

            ->when(request('search'), function ($q) {
                $q->where('up.title', 'like', '%' . request('search') . '%');
                $q->where('up.id', '=' , 'id' );
                // $q->orWhere('u.username', 'like', '%' . request('search') . '%');
            })
            ->when(request('sort') && request('sort') != 'action' , function ($q) {
                return $q->orderBy( request('sort') ,  request('order'));
            })

            ->orderBy('up.created_at','desc')
            ->select('u.image as userimages','up.created_at','u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
            ->whereMonth('up.created_at', Carbon::now()->month)

            ->get();

            // dd($data);
            $horses = $data->slice($request->offset , $request->limit)->values();

            foreach($horses as $horse){
                $horse->username = ucwords($horse->username);    
                $horse->horsetitle = ucwords($horse->horsetitle);

                if(!$horse->breedtitle){
                    $horse->breedtitle = 'N/A';
                }

                if($horse->userimages){
                    $horse->image = '<span class="avatar w-40">
                    <img src="storage/app/users/'.$horse->userimages.'" alt="..."></span>';
                }else{
                    $horse->image = '<span class="avatar w-40">
                    <img src="http://dev-cmolds.com/horsematch/public/adminassets/pngfind.com-avatar-png-icon-3819326.png" alt="..."></span>';
                }


                // $horse->action = "<a href='javascript:;' onclick='edit1(".json_encode($horse , true).")' data-id='active' class='viewBtn viewUserBtn' data-toggle='modal' data-target='#viewUserModal'><i class='fa fa-eye' title='view' data-toggle='tooltip' aria-hidden='true'></i><span class='sr-only'>View</span></a>";

                $fun = "window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]={$horse->horsetitle}&amp;p[url]=".route('facebook-share' , $horse->id )."','sharer','toolbar=0,status=0,width=550,height=300');";

                $share_link =  route('facebook-share' , $horse->id );

                $horse->action = '
                
                <a href="javascript:;" class="text-primary" onClick="'. $fun .'"><i class="fab fa-facebook" data-toggle="tooltip" title="facebook share" ></i></a>

                <a href="javascript:;" data-clipboard-text="'.$share_link.'" class="share-link1" ><i id="'.$share_link.'" class="fa fa-paperclip text-success" title="copy link" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Copy Link</span></a>

                <a href="javascript:;" onclick="viewsellinghorses('.$horse->id.')" data-id="active" class="viewBtn viewDetailsBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="view" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">View</span></a>

                <a href="javascript:;" onclick="deletehorse('.$horse->id.')" data-id="active" class="deleteBtn confirmDel" data-toggle="modal" ><i class="fa fa-trash-o" title="delete" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Delete</span></a>
                ';
                
                // <a href="javascript:;" id="'.$share_link.'" class="share-link" ><i id="'.$share_link.'" class="fa fa-paperclip" title="copy link" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Copy Link</span></a>




            }

            return response()->json([ 
                'rows' => $horses , 
                'total' => $data->count(), 
                'totalNotFiltered' => $data->count(),
                ]);

    }

    public function sellinghorse($id){
        $data = DB::table('user_posts as up')
            ->where('up.id','=',$id)
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->join('user_post_images as uip','uip.post_id','=','up.id')
            ->select('u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username')
            ->first();

            if(!$data->breedtitle){
                $data->breedtitle = 'N/A';
            }

            return response()->json(['success'=>true,'data'=>$data]);
    }
    
    
        public function reportedadview($id){
        $data = DB::table('user_posts as up')
            ->where('up.id','=',$id)
            ->join('reported_posts as rp','rp.post_id','=','up.id')
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->join('user_post_images as uip','uip.post_id','=','up.id')
            ->select('rp.reported_message as reported_message','u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','rp.created_at as ad_posted')
            ->first();
            return response()->json(['success'=>true,'data'=>$data]);
    }


    public function soldHorses(Request $request){
	    $title = "Sold Horses";
	    $data = DB::table('user_sold_posts as usp')
                ->join('user_posts as up','up.id','=','usp.post_id')
                ->join('users as u','u.id','=','usp.user_id')
                ->join('breeds as b','b.id','=','up.breed_id')
                ->join('genders as g','g.id','=','up.gender_id')
                ->join('disciplines as d','d.id','=','up.discipline_id')
                ->select('up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
                ->whereMonth('up.created_at',Carbon::now()->month)
                ->paginate(10);
	    return view("admin.soldhorses",
            [
                'data'=>$data,
                'title'=>$title
            ]
        );
    }
    
    
    //Sunaina Changes
    	public function usersone(Request $request)
    {
        if ($request->isMethod('post')) {
            if($request->searchparam){
                $title = 'Users Detail';
                $soldarray = [];
                $sellingarray = [];
                $users = DB::table('users')
                    ->leftJoin('disciplines','disciplines.id','=','users.discipline')
                    ->where('user_type','!=','admin')
                    ->where('username', 'like', '%' . $request->searchparam . '%')
                    ->orWhere('email', 'like', '%' . $request->searchparam . '%')
                    ->orderBy('users.id','DESC')
                    ->paginate(5);
                foreach ($users as $user){
                    $sold = DB::table('user_sold_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $sold = array_push($soldarray,$sold);
                }
                foreach ($users as $user){
                    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $selling = array_push($sellingarray,$selling);
                }
                dd($users);
                foreach($users as $user){
                    $sum = UserRating::where('user_id',$user->id)->sum();
                    dd($sum);
                }
                return view("admin.users",
                    [
                        'users' => $users,
                        'title' => $title,
                        'soldarray' => $soldarray,
                        'sellingarray' => $sellingarray,
                        'searchparam' => $request->searchparam
                    ]
                );
            }
        }
		$title = 'Users Detail';
		$soldarray = [];
		$sellingarray = [];
        $users = DB::table('users')
            ->select('users.*','disciplines.title')
            ->leftJoin('disciplines','disciplines.id','=','users.discipline')
            ->where('user_type','!=','admin')
            ->orderBy('users.id','DESC')
            // ->get();
            // ->get();
            ->paginate(5);
		foreach ($users as $user){
		    $sold = DB::table('user_sold_posts')
                    ->where('user_id','=',$user->id)
                    ->select('id')
                    ->count();
            $sold = array_push($soldarray,$sold);
        }
		foreach ($users as $user){
		    $ratings = UserRating::where('user_id',$user->id)->sum('rating');
            $count = UserRating::where('user_id',$user->id)->count();
            if($count>0) {
                $user->rating = $ratings / $count;
            }
            else{
                $user->rating = 0;
            }
        }
		foreach ($users as $user){
		    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
		    $selling = array_push($sellingarray,$selling);
        }
		// return view("admin.users1",
		return view("admin.users1-new",
			[
				'users' => $users,
				'title' => $title,
                'soldarray' => $soldarray,
                'sellingarray' => $sellingarray,
                'searchparam' => ''
			]
		);
	}
	
    public function usersAjax(Request $request)
    {   
        // dd($request->all());

        $title = 'Users Detail';
		$soldarray = [];
		$sellingarray = [];

        $all_users = DB::table('users')
            // 'user_ratings.*'
            ->select('users.*','disciplines.title as discipline_title' )
            ->join('disciplines','disciplines.id','=','users.discipline')
            // ->leftJoin('user_ratings','user_ratings.user_id','=','users.id')
            // ->select('users.*','user_ratings.*')
            ->when(request('search'), function ($q) {
                $q->where('username', 'like', '%' . request('search') . '%');
                $q->orWhere('email', 'like', '%' . request('search') . '%');
            })
            ->when(request('sort') , function ($q) {
                return $q->orderBy( request('sort') ,  request('order'));
            })
            ->when(!request('sort'), function ($q) {
                $q->orderBy('users.id','DESC');
            })
            ->get();
            // ->paginate(15);

        $all_users = $all_users->where('user_type', '=', 'user');

        // if(request('sort') && request('sort') == 'ratings' && request('order') == 'asc') {
        //     $all_users = $all_users->sortByDesc('rating');
        // }
        
        // if(request('sort') && request('sort') == 'ratings' && request('order') == 'desc') {
        //     $all_users = $all_users->sortBy('rating');
        // }
        
        
        $users = $all_users->slice($request->offset , $request->limit)->values();
        
        // dd($users->first());

        // dd($users);

		foreach ($users as $user){
		    $ratings = UserRating::where('user_id',$user->id)->sum('rating');
            $count = UserRating::where('user_id',$user->id)->count();
            if($count>0) {
                $user->rating = $ratings / $count;
            }
            else{
                $user->rating = 0;
            }
        }

        foreach ($users as $user){
		    $sold = DB::table('user_sold_posts')
                    ->where('user_id','=',$user->id)
                    ->select('id')
                    ->count();
            $sold = array_push($soldarray,$sold);
        }


		foreach ($users as $user){
		    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
		    $selling = array_push($sellingarray,$selling);
        }
        

        foreach($users as $user){
            
            // dd($user);

            $user->username = ucwords($user->username);
            // $user->action = '<button class="btn btn-success" >Click</button>';

            if($user->is_block=="0"){
                $user->is_block = '<a href="javascript:;" onclick="blockunblock('.$user->id.')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmstatusmodal" class="activeBtn mtb-10">
                Active</a>';
            }else{
                $user->is_block = '<a href="javascript:;" onclick="blockunblock('.$user->id.')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmstatusmodal" class="activeBtn mtb-10" style="background: #F63737;color:white;">Blocked</a>';

            }

            $user->ratings = '';
            for($i = 0; $i < 5; $i++){
                if((int)$user->rating===0){
                    $user->ratings .= '<span class="fa fa-star"></span>';
                }
                elseif((int)$user->rating>$i){
                    $user->ratings .= '<span class="fa fa-star checked"></span>';
                }
                else{
                    $user->ratings .= '<span class="fa fa-star"></span>';
                }
            }
            
            // $user->rating = $user->ratings;
            
            if($user->image){
                $user->profile_image =  '<img class="profile-image-dt" src="storage/app/users/'.$user->image.'" alt="..." />';
            }else{
                $user->profile_image = '<img class="profile-image-dt" src="http://dev-cmolds.com/horsematch/public/adminassets/pngfind.com-avatar-png-icon-3819326.png" />';
            }

            if(!$user->years_riding){
                $user->years_riding = 'N/A';
            }
            if(!$user->discipline_title){
                $user->discipline_title = 'N/A';
            }
            if(!$user->skill_level){
                $user->skill_level = 'N/A';
            }

            // object->toArray();
            // $fun = 'edit("'.$user->username.'","'.$user->phone.'","'.$user->email.'","'.$user->years_riding.'","'.$user->skill_level.'","'.$user->image.'","'.(int)$user->rating.'","'.substr($user->title,0,-2).'");';
            
            $user->action = "<a href='javascript:;' onclick='edit1(".json_encode($user , true).")' data-id='active' class='viewBtn viewUserBtn' data-toggle='modal' data-target='#viewUserModal'><i class='fa fa-eye tool-tip' title='View' data-toggle='tooltip' aria-hidden='true'></i><span class='sr-only'>View</span></a>";

            

            
        }
        return response()->json([ 
                'rows' => $users , 
                'total' => $all_users->count(), 
                'totalNotFiltered' => DB::table('users')->select('*')->get()->count(),
                ]);
	}
	
    public function emailcollateral(Request $request)
    {
        if ($request->isMethod('post')) {
            if($request->searchparam){
                $title = 'Users Detail';
                $soldarray = [];
                $sellingarray = [];
                $users = DB::table('users')
                    ->where('user_type','!=','admin')
                    ->where('username', 'like', '%' . $request->searchparam . '%')
                    ->orWhere('email', 'like', '%' . $request->searchparam . '%')
                    ->orderBy('users.id','DESC')
                    ->paginate(10);
                foreach ($users as $user){
                    $sold = DB::table('user_sold_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $sold = array_push($soldarray,$sold);
                }
                foreach ($users as $user){
                    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $selling = array_push($sellingarray,$selling);
                }
                return view("admin.users",
                    [
                        'users' => $users,
                        'title' => $title,
                        'soldarray' => $soldarray,
                        'sellingarray' => $sellingarray,
                        'searchparam' => $request->searchparam
                    ]
                );
            }
        }
		$title = 'Email Collateral';
		$soldarray = [];
		$sellingarray = [];
		$users = User::get_users();
		foreach ($users as $user){
		    $sold = DB::table('user_sold_posts')
                    ->where('user_id','=',$user->id)
                    ->select('id')
                    ->count();
            $sold = array_push($soldarray,$sold);
        }
		foreach ($users as $user){
		    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
		    $selling = array_push($sellingarray,$selling);
        }
		return view("admin.emailcolletaral",
			[
				'users' => $users,
				'title' => $title,
                'soldarray' => $soldarray,
                'sellingarray' => $sellingarray,
                'searchparam' => ''
			]
		);
	}
	
		    	public function teammanagement(Request $request)
    {
        if ($request->isMethod('post')) {
            if($request->searchparam){
                $title = 'Users Detail';
                $soldarray = [];
                $sellingarray = [];
                $users = DB::table('users')
                    ->where('user_type','!=','admin')
                    ->where('username', 'like', '%' . $request->searchparam . '%')
                    ->orWhere('email', 'like', '%' . $request->searchparam . '%')
                    ->orderBy('users.id','DESC')
                    ->paginate(10);
                foreach ($users as $user){
                    $sold = DB::table('user_sold_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $sold = array_push($soldarray,$sold);
                }
                foreach ($users as $user){
                    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
                    $selling = array_push($sellingarray,$selling);
                }
                return view("admin.users",
                    [
                        'users' => $users,
                        'title' => $title,
                        'soldarray' => $soldarray,
                        'sellingarray' => $sellingarray,
                        'searchparam' => $request->searchparam
                    ]
                );
            }
        }
		$title = 'Team Management';
		$soldarray = [];
		$sellingarray = [];
		$users = User::get_users();
		foreach ($users as $user){
		    $sold = DB::table('user_sold_posts')
                    ->where('user_id','=',$user->id)
                    ->select('id')
                    ->count();
            $sold = array_push($soldarray,$sold);
        }
		foreach ($users as $user){
		    $selling = DB::table('user_posts')
                        ->where('user_id','=',$user->id)
                        ->select('id')
                        ->count();
		    $selling = array_push($sellingarray,$selling);
        }
		return view("admin.teammanagement",
			[
				'users' => $users,
				'title' => $title,
                'soldarray' => $soldarray,
                'sellingarray' => $sellingarray,
                'searchparam' => ''
			]
		);
	}

	public function deleteuserpost(Request $request){
	    UserPost::where('id',$request->id)->forceDelete();
	    Activity::where('action_id',$request->id)->forceDelete();
	    return response()->json(['success'=>true]);
    }

    public function addBlogPost(Request $request){
	    $blog = new Blog();
	    $blog->title = $request['title'];
	    $blog->description = $request['addmessage'];
	    if($request->hasFile('image')) {
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $timestamp = strtotime(now());
            $destinationPath = storage_path("app/blogs/");
            $image->move($destinationPath, $timestamp . "." . $extension);
            $blog->image = $timestamp . "." . $extension;
        }
        $blog->created_date = Carbon::today();
        $blog->save();
        return Redirect::back()->with('message','Operation Successful !');
    }
    public function editBlogPost(Request $request){
        $blog = Blog::findOrFail($request->input('id'));
        $blog->title = $request['title'];
        $blog->description = $request['editmessage'];
        $image = $request->file('image');
        if (isset($image)){
            $extension = $image->getClientOriginalExtension();
            $timestamp = strtotime(now());
            $destinationPath = storage_path("app/blogs/");
            $image->move($destinationPath, $timestamp . "." . $extension);
            $blog->image = $timestamp . "." . $extension;
        }
        $blog->save();
        return Redirect::back()->with('message','Operation Successful !');
    }

    public function resendpromotion(Request $request){
	    $promotions = Promotions::where('id',$request->id)->first();
        $promotion = new Promotions();
        $promotion->promotion_text = $promotions->promotion_text;
        $promotion->number_of_users = User::where('user_type' , 'user')->count();
        $promotion->save();

        $users = User::latest()->where('user_type' , 'user')->get();
        foreach ($users as $k => $user){
           SendNotification::dispatch("Promotion Sent By Horsematch Admin", $promotion->promotion_text , $promotion , $user , $k );
        }

        // SendNotifications::dispatch("Promotion Sent By Horsematch Admin",$promotion->promotion_text , $promotion );
        session()->put('message','Promotions Resent Successfully');
        return redirect()->back();
    }


    public function blockunblock(Request $request){
	    $user = User::where('id',$request->id)->first();
	    if($user->is_block==="1"){
	        $user->is_block=(int)"0";
            $user->save();
        }
	    if($user->is_block==="0"){
	        $user->is_block=(int)"1";
            $user->save();
        }
	    $user->save();
    }

    public function deletepromo(Request $request){
	    Promotions::where('id',$request->id)->forceDelete();
	    return response()->json(['success'=>true,'message'=>'Done']);
    }


    public function reportedads(){
        $title = "Reported Posts";
        $data = DB::table('user_posts as up')
//            ->where('up.is_reported',1)
            ->join('reported_posts as rp','up.id','=','rp.post_id')
            ->groupBy('up.id')
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('users as ru','ru.id','=','rp.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->leftJoin('user_post_images as uip','uip.post_id','=','up.id')
            ->orderBy('up.created_at','desc')
            ->select('ru.username as userkaname','rp.reported_message as reported_message','u.image as userimages','rp.created_at','u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
            ->whereMonth('rp.created_at', Carbon::now()->month)
            ->paginate(5);
        return view("admin.reportedads",
            [
                'data' => $data,
                'title' => $title
            ]
        );
    }

    public function reportedadsNew(){
        $title = "Reported Posts";
        $data = DB::table('user_posts as up')
//            ->where('up.is_reported',1)
            ->join('reported_posts as rp','up.id','=','rp.post_id')
            ->groupBy('up.id')
            ->leftJoin('users as u','u.id','=','up.user_id')
            ->leftJoin('users as ru','ru.id','=','rp.user_id')
            ->leftJoin('breeds as b','b.id','=','up.breed_id')
            ->leftJoin('genders as g','g.id','=','up.gender_id')
            ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
            ->leftJoin('user_post_images as uip','uip.post_id','=','up.id')
            ->orderBy('up.created_at','desc')
            ->select('ru.username as userkaname','rp.reported_message as reported_message','u.image as userimages','rp.created_at','u.phone as phone','up.address as address','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username','up.created_date as ad_posted')
            ->whereMonth('rp.created_at', Carbon::now()->month)
            ->paginate(5);
        return view("admin.reportedads-new",
            [
                'data' => $data,
                'title' => $title
            ]
        );
    }

    public function blockUserCnf(Request $request){
	    $user_post = UserPost::where('id',$request->id)->first();
	    User::where('id',$user_post['user_id'])->update(
	        [
                'is_block'=>1
            ]
        );
	    return redirect()->back()->with(['success'=>'User Blocked Successfully']);
    }
    
    public function getcontent(){
        $content = Content::paginate(5);
        return view('admin.contentmanagement',['data'=>$content,'title'=>"Content Management"]);
    }
    
    public function getcontentNew(){
        $content = Content::all();
        // $content = Content::paginate(5);
        return view('admin.contentmanagement-new',['data'=>$content,'title'=>"Content Management"]);
        // return view('admin.contentmanagement',['data'=>$content,'title'=>"Content Management"]);
    }
    
    public function editContent(Request $request){
	    $content = Content::where('type',$request->type)->first();
	    $content->content = $request->text;
	    $content->save();
	    return response()->json(['success'=>true,'message'=>'Content Updated Successfully']);
    }
}