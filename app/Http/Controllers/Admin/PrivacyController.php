<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Data\Models\Setting;
use Redirect;
use Input;
use Validator;
use Illuminate\Validaion\ValidaionException;

class PrivacyController extends Controller
{
	public function index( Request $request )
	{
		$title = 'Privacy and Policy';

		$privacy = Setting::where('id',2)->first();

		return view("admin.pages.privacy.index",
			[
				'title' 	=> $title,
				'privacy' 	=> $privacy,
			]
		);
	}

	public function update_policy( Request $request )
	{
		$input = $request->input();
		
		$policy = Setting::find(2);
		
		$policy->content = $input['description'];
		
		$policy->save();
		
		return redirect('privacy-policy');
	}

}