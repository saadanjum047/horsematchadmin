<?php
namespace App\Http\Controllers\Admin;
use App\Data\Repositories\BlogRepository;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Data\Models\Blog;
use Illuminate\Support\Facades\DB;
use Redirect;
use Input;
use Validator;
use Illuminate\Validaion\ValidaionException;
use Image, Storage;

class BlogController extends Controller {


    const PER_PAGE = 10;
    private $_repository;

    public function __construct(BlogRepository $blogRepository){
        $this->_repository = $blogRepository;
    }

	public function index(Request $request){

		$title = 'Blogs';
		//$blogs = Blog::get_blogs();
        $blogs = DB::table('blogs')
                ->where('deleted_at',null)
            ->paginate(5);
//		$blogs=$this->_repository->findByAll(true,10);
        return view("admin.emailcolletaral",
			[
				'title' => $title,
				'blogs' => $blogs
			]
		);
	}
	public function indexNew(Request $request){

		$title = 'Blogs';

		//$blogs = Blog::get_blogs();
        $blogs = DB::table('blogs')
                ->where('deleted_at',null)
            ->get();
        return view("admin.emailcolletaral-new",
			[
				'title' => $title,
				'blogs' => $blogs
			]
		);
	}

	public function view( Request $request, $id ) {


		$title = 'View Blog';
   $id=Helper::hashid_decode($id);
        $blog=$this->_repository->findById($id);

        //$blog = Blog::find($id);


		if ($blog) {

			return view("admin.pages.blog.view",
				[
					'title' => $title,
					'blog' 	=> $blog,
					'id' 	=> $id
				]
			);

		}
		else {

			return redirect('blogs');

		}
	}

	public function delete( Request $request, $id ) {

		$blog = Blog::find($id);

		$blog->delete();

		return response()->json(['message'=>'Blog Deleted Successfully']);

	}

	public function add_post( Request $request ) {

		$title = 'Add Blog';

		return view("admin.addblog",
			[
				'title' => $title
			]

		);

	}

	public function update_post( Request $request) {

		$input = $request->all();
        $input['image'] = $request->file('image');
		$blog = Blog::find($input['id']);


        if(isset($input['image'])){
            $blog->image= $input['image']->hashName();
           $this->crop($input['image']);
        }



        $blog->title = $input['title'];

		$blog->description = $input['description'];

//		if ( isset(request()->image) && request()->image->getClientOriginalExtension() ) {
//
//			$imageName = time().'.'.request()->image->getClientOriginalExtension();
//
//        	request()->image->move(public_path('blog'), $imageName);
//
//        	$blog->image = $imageName;
//
//		}


		$blog->save();
		$request->session()->put('message','Blog Updated Successfully');
		return redirect('blogs');
	}

	public function insert_post( Request $request ) {

		$input = $request->input();

		$imageName = time().'.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('blog'), $imageName);


		$blog = new Blog;

		$blog->title = $input['title'];

		$blog->description = $input['description'];
        if(isset($input['image'])){
            $blog->image= $imageName;
            $this->crop($input['image']);
        }
        $blog->created_date = Carbon::now()->toDateString();
        $blog->save();
		$request->session()->put('message','Blog Added Successfully');
		return redirect('blogs');
	}


    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.blogs.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.blogs.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.blogs.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.blogs.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }

        }
        return true;
    }
    public function changeBlogStatus(Request $request){
        $blog_id = $request->blogid;
        $blog = Blog::where('id',$blog_id)->first();
        $blog_status = $blog->is_featured===0?1:0;
        Blog::where('id',$blog_id)->update(['is_featured'=>$blog_status]);
        return response()->json(['message'=>'Blog Updated Successfully']);
    }

    public function edit_post($id){
        $blog = Blog::find($id);
        return view('admin.updateblog',
            [
                'data'=>$blog
            ]
        );
    }
}
