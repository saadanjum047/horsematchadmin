<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Data\Models\Setting;
use Redirect;
use Input;
use Validator;
use Illuminate\Validaion\ValidaionException;

class TermsController extends Controller
{
	public function index( Request $request )
	{
		$title = 'Terms & Conditions';

		$terms = Setting::where('id',1)->first();

		return view("admin.pages.terms.index",
			[
				'title' => $title,
				'terms' => $terms,
			]
		);
	}

	public function update_terms( Request $request )
	{
		$input = $request->input();
		
		$policy = Setting::find(1);
		
		$policy->content = $input['description'];
		
		$policy->save();
		
		return redirect('terms-conditions');
	}
}