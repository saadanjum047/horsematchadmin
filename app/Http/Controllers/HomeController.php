<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Psy\Exception\FatalErrorException;
use Psy\Exception\ParseErrorException;
use Symfony\Component\CssSelector\Exception\SyntaxErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Validator, App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;

//models
use App\Data\Models\User;
use App\Data\Models\UserVenue;
use App\Data\Models\UserPayment;
use App\Data\Models\Subscription;
use App\Data\Models\Coupan;
use App\Data\Models\UserEvent;
use App\Data\Models\UserEventMatch;
use App\Data\Models\EventMatchImage;
use App\Data\Models\UserMatch;
use App\Data\Models\UserGroup;
use App\Data\Models\UserSport;
use App\Data\Models\Sport;
use App\Data\Models\Setting;

use Image,Storage;

class HomeController extends Controller{

    const PER_PAGE = 10;

    public $view_data = array();

    //buy_package
    public function buy_package(Request $request) {

        $input = $request->all();

        $user_payment = array();
        $coupan_detail = array();

        try{

            $dateTime = @date("Y-m-d H:i:s");
            $currentDate = @date('Y-m-d');
            $Date =  $dateTime;
            $expirationDate =  date('Y-m-d H:i:s', strtotime($Date. ' + 364 days'));

            $subscription = Subscription::where('id', $input['package_id'])->first();

            $input_sub_package = isset($input['package_id']) ? $input['package_id'] : "0";
            $input_sub_package = (int)$input_sub_package;
            if ($input_sub_package == 1) {
                $sub_access = "basic";
            } else if ($input_sub_package == 2) {
                $sub_access = "premium";
            } else if ($input_sub_package == 3) {
                $sub_access = "premium";
            } else {
                $sub_access = "basic";
            }

            $package_id = $input['package_id'];
            $package_name = isset($subscription->title) ? $subscription->title : "";
            $package_description = isset($subscription->description) ? $subscription->description : "";

            $actual_amount = isset($subscription->price) ? $subscription->price : "0";

            if ($input['coupan_id'] != "0") {

                $coupan_detail = Coupan::where('id', $input['coupan_id'])->first();
                $discount_amount = isset($coupan_detail->price) ? $coupan_detail->price : "0";
                if ($actual_amount > $discount_amount) {
                    $final_amount = $actual_amount - $discount_amount;
                } else {
                    $final_amount = $actual_amount;
                }

            } else {
                $discount_amount = "0";
                $final_amount = $actual_amount;
            }


            $data['stripeToken'] = isset($input['stripeToken']) ? $input['stripeToken'] : "";
            $data['stripePrice'] = $final_amount;
            $data['stripeEmail'] = isset($input['stripeEmail']) ? $input['stripeEmail'] : "";
            $data['description'] = "Payment is made for getting subscription in $package_name ";

            //dd($input, $data);

            $stripe_charge = stripe_charge($data);

           //dd($stripe_charge);

            //payment validation
            if (isset($stripe_charge['success']) && is_array($stripe_charge['success']) && count($stripe_charge['success']) > 0) {
                //dd($stripe_charge['success']);

                $old_subscription = UserPayment::where('user_id', $input['user_id'])->first();
                //subscription exist
                $update_subsc = "";
                if ($old_subscription != NULL) {
                    $sub_payment_array = array();
                    $sub_payment_array['sub_status'] = 'completed';
                    $update_subsc = UserPayment::where('user_id', $input['user_id'])->update($sub_payment_array);
                }//subscription exist

                $user_detail = User::where('id', $input['user_id'])->first();

                $username = isset($user_detail->username) ? $user_detail->username : "N/A";

                $reciept = isset($stripe_charge['success']['receipt_url']) ? $stripe_charge['success']['receipt_url'] : "";
                $reciepturl = "<a href='".$reciept."' target='_blank'> Click Here </a>";

                //email working
                $message = "Welcome $username <br><br> Thanks for subscribing to $package_description . <br> Click on link below to see receipt <br> $reciepturl <br><br>  Regards <br> QualityReferee Support Team";
                $sendEmail = sendEmail($input['stripeEmail'], "Package Subscription", $message);
                //email working ends here

                $user_payment = array();
                $user_payment['user_id'] = $input['user_id'];
                $user_payment['user_type'] = isset($user_detail->user_type) ? $user_detail->user_type : "referee";
                $user_payment['charge_id'] = isset($stripe_charge['success']['id']) ? $stripe_charge['success']['id'] : "";
                $user_payment['transaction_id'] = isset($stripe_charge['success']['balance_transaction']) ? $stripe_charge['success']['balance_transaction'] : "";
                $user_payment['subscription_id'] = $input['package_id'];

                //sububscription access
                $user_payment['sub_access'] = $sub_access;

                $user_payment['coupan_id'] = $input['coupan_id'];
                
                $user_payment['actual_amount'] = $actual_amount;
                $user_payment['discount_amount'] = $discount_amount;
                $user_payment['final_amount'] = $final_amount;

                $user_payment['sub_startdate'] = $currentDate;
                $user_payment['sub_enddate'] = $expirationDate;
                $user_payment['sub_status'] = "progress";
                $user_payment['receipt_url'] = $reciept;
                $user_payment['transaction_detail'] = isset($stripe_charge['success']) ? serialize($stripe_charge['success']) : "";
                $user_payment['module'] = "subscription";
                $user_payment['method'] = "add";
                $user_payment['payment_gateway'] = "stripe";
                $user_payment['created_date'] = $currentDate;
                $user_payment['created_at'] = $dateTime;

                $action = UserPayment::insertGetId($user_payment);

                $cache_clear = Cache::flush();

                $session = Session::put('success',"Payment is made");
                return redirect()->back();
            } else {
                $session = Session::put('error',"Payment could not be made");
                return redirect()->back();
            }//payment validation

        }catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $session = Session::put('error',"Opps something went wrong");
            return redirect()->back();
        }

    }//buy_package method endds here

    //update_subscription
    public function update_subscription(Request $request) {

        $input = $request->all();

        $user_payment = array();
        $coupan_detail = array();

        //dd($input);

        try{

            $dateTime = @date("Y-m-d H:i:s");
            $currentDate = @date('Y-m-d');
            $Date =  $dateTime;
            $expirationDate =  date('Y-m-d H:i:s', strtotime($Date. ' + 364 days'));

            $data['stripeToken'] = isset($input['stripeToken']) ? $input['stripeToken'] : "";
            $data['stripePrice'] = isset($input['package_price']) ? $input['package_price'] : "";
            $data['stripeEmail'] = isset($input['stripeEmail']) ? $input['stripeEmail'] : "";
            $data['description'] = "Payment is made switching subscription (basic to premium) ";

            //dd($input, $data);

            $stripe_charge = stripe_charge($data);

            // dd($stripe_charge);

            //payment validation
            if (isset($stripe_charge['success']) && is_array($stripe_charge['success']) && count($stripe_charge['success']) > 0) {
                //dd($stripe_charge['success']);

                $old_subscription = UserPayment::where('user_id', $input['user_id'])->first();
                //subscription exist
                $update_subsc = "";
                if ($old_subscription != NULL) {
                    $sub_payment_array = array();
                    $sub_payment_array['sub_access'] = 'premium';
                    $sub_payment_array['method'] = 'update';

                    $final_amount = $old_subscription->final_amount + $input['package_price'];

                    $update_subsc = UserPayment::where('user_id', $input['user_id'])->update($sub_payment_array);
                }//subscription exist


                $cache_clear = Cache::flush();

                $session = Session::put('success',"Payment is made.");
                return redirect()->back();
            } else {
                $session = Session::put('error',"Payment could not be made");
                return redirect()->back();
            }//payment validation

        }catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $session = Session::put('error',"Opps something went wrong");
            return redirect()->back();
        }

    }//update_subscription method endds here

    /* ------------------ Organization Web Links ------------------ */

    //organization_home
    public function organization_home(Request $request) {


        try{

            $this->view_data['title'] = "Home";

            $this->view_data['event_text'] = "Create new events and matches for various sports and manage the jobs for the referees with respect to the matches.";
            $this->view_data['venue_text'] = "Add various venues. View and manage different bookings of various matches with respect to different sports.";
            $this->view_data['calender_text'] = "Custom made View and manage the scheduled matches and perform other relevant activities.";
            $this->view_data['referee_text'] = "View and search for the referees and invite them for the matches.";

            //view
            return view("organization.index", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_home method ends here

    //organization_profile
    public function organization_profile(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Profile";
            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_id'] = $session_id;
            $this->view_data['encoded_session_id'] = hashid_encode(Session::get('session_id'));
            $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
            $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
            $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "";
            //view
            return view("organization.profile", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_profile method ends here

    //organization_editprofile
    public function organization_editprofile(Request $request) {

        $cache = Cache::flush();

        try{

            //page title
            $this->view_data['title'] = "Edit Profile";
            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_detail'] = $user_detail;
            $this->view_data['session_id'] = $session_id;
            $this->view_data['encoded_session_id'] = hashid_encode(Session::get('session_id'));
            $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
            $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
            $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "0";
            $this->view_data['city_id'] =  isset($user_detail->city_id) ? $user_detail->city_id : "0";

            //sports
            $query = Sport::orderBy('id', 'desc')->get();
            if (count($query) > 0) {
                foreach($query as $q) {
                    $sports[] = array(
                        "id" => hashid_encode($q->id),
                        "name" => $q->name,
                    );
                }
            } else {
                $sports = array();
            }

            $this->view_data['sports'] = $sports;

            $profile_sports = $user_detail->sports;
            if (count($profile_sports) > 0) {
                foreach($profile_sports as $spt) {
                    $user_sports[] = $spt['id'];
                }
            } else {
                $user_sports = array();
            }
            $this->view_data['user_sports'] = $user_sports;

            //view
            return view("organization.editprofile", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_editprofile method ends here

    //organization_selectplans
    public function organization_selectplans(Request $request) {

        try{

            $is_front = Session::get('is_front');

            if ($is_front == NULL) {
                // return view("organization.login");
                return redirect('/');
            } else {

                //page title
                $this->view_data['title'] = "Select Plan";
                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                $this->view_data['session_id'] = $session_id;
                $this->view_data['session_email'] = Session::get('session_email');
                $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "0";
                //view
                return view("organization.selectplans", $this->view_data);
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_selectplans method ends here

    //organization_forum
    public function organization_forum(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Forum";

            $content = Setting::where('type', 'forum')->first();
            $this->view_data['forum_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['forum_id'] = $content->id;
            $this->view_data['forum_simple_content'] = isset($content->content) ? handle_string($content->content) : "";


            //view
            return view("organization.forum", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_forum method ends here

    //organization_dispute
    public function organization_dispute(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Dispute";
            //view
            return view("organization.dispute", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_dispute method ends here

    //organization_disputedetail
    public function organization_disputedetail(Request $request) {

        $input = $request->all();

        try{

            $decoded = hashid_decode($input['id']);
            $detail =  App::make('UserMatchDisputeRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Dispute Detail";

                $session_id = Session::get('session_id');
                $this->view_data['session_id'] = hashid_encode($session_id);
                $this->view_data['id'] = $input['id'];
                $this->view_data['match_id'] = $detail->match_id;

                //view
                return view("organization.disputedetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_disputedetail method ends here

    //organization_calender
    public function organization_calender(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Calendar";
            $this->view_data['calender_text'] = "Custom made View and manage the scheduled matches and perform other relevant activities.";
            //view
            return view("organization.calender", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_calender method ends here

    //organization_referees
    public function organization_referees(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Referees";

            $this->view_data['referee_text'] = "View and search for the referees and invite them for the matches.";

            //view
            return view("organization.referees", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_referees method ends here

    //organization_aboutus
    public function organization_aboutus(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "About Us";


            $content = Setting::where('type', 'us')->first();
            $this->view_data['us_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['us_id'] = $content->id;
            $this->view_data['us_simple_content'] = isset($content->content) ? handle_string($content->content) : "";

            //view
            return view("organization.aboutus", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_aboutus method ends here

    //organization_contactus
    public function organization_contactus(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Contact Us";
            //session data
            $user_repo = App::make('UserRepository');
            $session_id = Session::get('session_id');
            $this->view_data['session_detail'] = $user_repo->findById($session_id, false, true);
            //view
            return view("organization.contactus", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_contactus method ends here

    //organization_notifications
    public function organization_notifications(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Notification";

            //view
            return view("organization.notifications", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_notifications method ends here

    //organization_payments
    public function organization_payments(Request $request) {

        $cache =  Cache::flush();

        try{

            //page title
            $this->view_data['title'] = "Payments";


            $session_id = Session::get('session_id');
            $user_payments = UserPayment::where('user_id', $session_id)->sum('final_amount');
            $this->view_data['total_spendings'] = isset($user_payments) ? $user_payments : "0";

            $rec_query = UserEventMatch::join('user_venues', 'user_venues.id','=','user_event_matches.venue_id')
                                        ->where('user_venues.user_id', $session_id)
                                        ->where('user_event_matches.booking_status', 'pending')
                                        ->where('user_event_matches.payment_status', 'not_paid')
                                        ->sum('user_event_matches.match_fees');
            $this->view_data['total_receivable'] = isset($rec_query) ? $rec_query : "0";

            
            $withdraw_query = UserEventMatch::join('user_venues', 'user_venues.id','=','user_event_matches.venue_id')
                                             ->where('user_venues.user_id', $session_id)
                                             ->where('user_event_matches.booking_status', 'accepted')
                                             ->where('user_event_matches.payment_status', 'paid')
                                            ->sum('user_event_matches.match_fees');
            $this->view_data['total_withdraw'] = isset($withdraw_query) ? $withdraw_query : "0";

            $this->view_data['total_earning'] = isset($withdraw_query) ? $withdraw_query : "0";

            //view
            return view("organization.payments", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_payments method ends here

    //organization_chatzone
    public function organization_chatzone(Request $request) {

        try{

            $this->view_data['title'] = "Chat Zone";
            //view
            return view("organization.chatzone", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_chatzone method ends here

    //privacy_policy
    public function privacy_policy(Request $request) {

        try{

            $this->data['title'] = "Privacy and Policy";


            $content = Setting::where('type', 'pp')->first();
            $this->data['pp_content'] = isset($content->content) ? $content->content : "";
            $this->data['pp_id'] = $content->id;
            $this->data['pp_simple_content'] = isset($content->content) ? handle_string($content->content) : "";


            //view
            return view("organization.privacy", $this->data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//privacy_policy method ends here

    //terms_and_contions
    public function terms_and_contions(Request $request) {

        try{

            $this->data['title'] = "Terms and Conditions";

            $content = Setting::where('type', 'tc')->first();
            $this->data['tc_content'] = isset($content->content) ? $content->content : "";
            $this->data['tc_id'] = $content->id;
            $this->data['tc_simple_content'] = isset($content->content) ? handle_string($content->content) : "";


            //view
            return view("organization.terms", $this->data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//terms_and_contions method ends here

    //organization_eventadd
    public function organization_eventadd(Request $request) {

        $cache = Cache::flush();

        try {

            //page title
            $this->view_data['title'] = "Add Event";
            //sports data
            $session_id = Session::get('session_id');
            $sports_query = UserSport::select('sports.id','sports.name')->join('sports', 'sports.id', '=', 'user_sports.sport_id')->where('user_sports.user_id', $session_id)->get();
            if (count($sports_query) > 0) {
                $sports = array();
                foreach($sports_query as $spt) {
                    $sports[] = array(
                        "id" => hashid_encode($spt->id),
                        "name" => $spt->name
                    );
                }
            } else {
                $sports = array();
            }
            $this->view_data['sports'] = $sports;
            //view
            return view("organization.eventadd", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_eventadd method ends here

    //organization_events
    public function organization_events(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Events";
            $this->view_data['event_text'] = "Create new events and matches for various sports and manage the jobs for the referees with respect to the matches.";

            //view
            return view("organization.events", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_events method ends here

    //organization_eventdetail
    public function organization_eventdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail =  App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Event Detail";
                $this->view_data['record'] = $detail;
                $this->view_data['input_id'] = $input['id'];
                //dd($detail);

                $event_no_of_matches = isset($detail->no_of_matches) ? $detail->no_of_matches : "0";

                $matches_created = UserEventMatch::where('event_id', $decoded)->count();
                //dd($matches_created);

                if ($event_no_of_matches == $matches_created) {
                    $this->view_data['quota_status'] = "yes";
                } else {
                    $this->view_data['quota_status'] = "no";
                }
                //dd($event_no_of_matches, $matches_created);

                $this->view_data['matches_created'] = $matches_created;

                //view
                return view("organization.eventdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_eventdetail method ends here

    //organization_eventedit
    public function organization_eventedit(Request $request) {

        Cache::flush();

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Event Edit";
                $this->view_data['record'] = $detail;
//                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);

                $this->view_data['session_detail'] = $user_detail;
                $this->view_data['session_id'] = $session_id;
                $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
                $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
                $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "0";

                $sports_query = UserSport::select('sports.id','sports.name')->join('sports', 'sports.id', '=', 'user_sports.sport_id')->where('user_sports.user_id', $session_id)->get();
                if (count($sports_query) > 0) {
                    $sports = array();
                    foreach($sports_query as $spt) {
                        $sports[] = array(
                            "id" => hashid_encode($spt->id),
                            "name" => $spt->name
                        );
                    }
                } else {
                    $sports = array();
                }
                $this->view_data['sports'] = $sports;


                //view
                return view("organization.eventedit", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_eventedit method ends here

    //organization_addmatch
    public function organization_addmatch(Request $request) {

        $input = $request->all();
        $cache = Cache::flush();

        try {

            $decoded = hashid_decode($input['id']);
            //dd($decoded);

            $detail = App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Add Match";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['sequence_no']= UserEventMatch::where('event_id', $decoded)->count() + 1;
                //view
                return view("organization.addmatch", $this->view_data);

            } else {
                $this->data['title'] = "Home";
                //view
                return view("organization.index", $this->data);
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_addmatch method ends here

    //organization_doaddmatch
    public function organization_doaddmatch(Request $request){

        $input = $request->all();
        //dd($input);

        $input['description'] = isset($input['description']) ? $input['description'] : "";

        $input['image'] = $request->file('image');

        //image handling
        if(isset($input['image'])){
            $file_name = $input['image']->store(config('app.files.matches.folder_name'));
            $user_image =$input['image']->hashName();
            $this->crop($input['image']);
        } else {
            $user_image = "";
        }
        $input['image'] = $user_image;



        //match session data
        $event_input['event_id'] = $input['event_id'];
        $event_input['sequence_no'] = $input['sequence_no'];
        $event_input['title'] = $input['title'];
        $event_input['total_referee'] = $input['total_referee'];
        $event_input['match_fees'] = $input['match_fees'];
        $event_input['image'] = $input['image'];
        $event_input['description'] = $input['description'];
        $event_input['created_date'] = date("Y-m-d");
        //match session data ends here

        Session::forget('event_input');
        $session = Session::put('event_input', $event_input);
        //$event_input = Session::get('event_input');
        //dd($event_input);
        //redirect to venue
        return redirect('organization-matchvenues');


    }//organization_doaddmatch

    //organization_mymatches
    public function organization_mymatches(Request $request) {

        $input = $request->all();
        //dd($input);

        try {


            //page title
            $this->view_data['title'] = "Event Matches";
            //$this->view_data['record'] = $detail;
            //$this->view_data['input_id'] = $input['id'];
            //dd($this->view_data);

            //view
            return view("organization.mymatches", $this->view_data);



        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_mymatches method ends here

    //organization_eventmatches
    public function organization_eventmatches(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);
            //dd($decoded);

            $detail = $session_detail = App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Event Matches";
                $this->view_data['record'] = $detail;
                $this->view_data['input_id'] = $input['id'];
                //dd($this->view_data);

                //view
                return view("organization.eventmatches", $this->view_data);

            } else {
                $this->data['title'] = "Home";
                //view
                return view("organization.index", $this->data);
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_eventmatches method ends here

    //organization_matchdetail
    public function organization_matchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Match Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['event_id'] = $detail->event_id;
                $this->view_data['event_status'] = $detail->event_status;

                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                $this->view_data['session_id'] = $session_id;
                $this->view_data['session_email'] = Session::get('session_email');
                $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "";

                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                $this->view_data['session_id'] = $session_id;
                $this->view_data['session_email'] = Session::get('session_email');
                $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "";

                //dd($detail);
                //dd($event_match_detail);

                $referee_match_status_count = UserMatch::where('match_id', $decoded)->where('status', 'accepted')->count();

                //dd($referee_match_status_count);
                if ($detail->total_referee == $referee_match_status_count) {
                    $this->view_data['quota_done'] = "yes";
                } else {
                    $this->view_data['quota_done'] = "no";
                }

                //view
                return view("organization.matchdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_matchdetail method ends here

    //organization_draftmatchdetail
    public function organization_draftmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Match Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['event_id'] = $detail->event_id;
                $this->view_data['event_status'] = $detail->event_status;
                //view
                return view("organization.draftmatchdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_draftmatchdetail method ends here

    //organization_pendingmatchdetail
    public function organization_pendingmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Match Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['event_id'] = $detail->event_id;
                $this->view_data['event_status'] = $detail->event_status;
                //view
                return view("organization.pendingmatchdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_pendingmatchdetail method ends here

    //organization_publishmatchdetail
    public function organization_publishmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {
                    //page title
                    $this->view_data['title'] = "Match Detail";
                    $this->view_data['input_id'] = $input['id'];
                    $this->view_data['event_id'] = $detail->event_id;
                    $this->view_data['event_status'] = $detail->event_status;
                    //session data
                    $session_id = Session::get('session_id');
                    $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                    $this->view_data['session_id'] = $session_id;
                    $this->view_data['session_email'] = Session::get('session_email');
                    $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "";

                    //dd($detail);
                    //dd($event_match_detail);

                    $referee_match_status_count = UserMatch::where('match_id', $decoded)->where('status', 'accepted')->count();

                    //dd($referee_match_status_count);
                    if ($detail->total_referee == $referee_match_status_count) {
                        $this->view_data['quota_done'] = "yes";
                    } else {
                        $this->view_data['quota_done'] = "no";
                    }

                    //dd($this->view_data['quota_done']);


                //view
                return view("organization.publishmatchdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_publishmatchdetail method ends here

    //organization_progressmatchdetail
    public function organization_progressmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Match Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['event_id'] = $detail->event_id;
                $this->view_data['event_status'] = $detail->event_status;
                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                $this->view_data['session_id'] = $session_id;
                $this->view_data['session_email'] = Session::get('session_email');
                $this->view_data['country_id'] =  $user_detail->country_id;
                //view
                return view("organization.progressmatchdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_progressmatchdetail method ends here

    //organization_bookingdetail
    public function organization_bookingdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail =  App::make('UserEventMatchRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Booking Detail";
                $this->view_data['record'] = $detail;
                $this->view_data['encoded_id'] = hashid_encode(Session::get('session_id'));
                //view
                return view("organization.bookingdetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_bookingdetail method ends here

    //organization_venueadd
    public function organization_venueadd(Request $request) {

        $cache = Cache::flush();

        try{
            //page title
            $this->view_data['title'] = "Venue Add";
            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_id'] = $session_id;
            $this->view_data['subscription_title'] = $user_detail->sub_package;
            $this->view_data['subscription_type'] = $user_detail->sub_type;
            $this->view_data['country_id'] =  $user_detail->country_id;
            //sports data
            $sports_query = UserSport::select('sports.id','sports.name')->join('sports', 'sports.id', '=', 'user_sports.sport_id')->where('user_sports.user_id', $session_id)->get();
            if (count($sports_query) > 0) {
                $sports = array();
                foreach($sports_query as $spt) {
                    $sports[] = array(
                        "id" => hashid_encode($spt->id),
                        "name" => $spt->name
                    );
                }
            } else {
                $sports = array();
            }
            $this->view_data['sports'] = $sports;
            //view
            return view("organization.venueadd", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_venueadd method ends here

    //organization_venues
    public function organization_venues(Request $request) {

        $input = $request->all();
        $cache = Cache::flush();

        try{

            //page title
            $this->view_data['title'] = "Venues";

            $this->view_data['venue_text'] = "Add various venues. View and manage different bookings of various matches with respect to different sports.";

            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_id'] = $session_id;
            //subscription detail
            $this->view_data['sub_id'] = isset($user_detail->subscription->id) ? hashid_decode($user_detail->subscription->id) : "";
            $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
            $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
            $this->view_data['sub_access'] = isset($user_detail->sub_access) ? $user_detail->sub_access : "";
            //subscription detail ends here
            $this->view_data['session_email'] = Session::get('session_email');
            $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "";
            //sub detail
            $this->view_data['own_id'] = hashid_encode($session_id);
            $this->view_data['subs_detail'] = Subscription::where('id', '2')->first();

            $this->view_data['rejection_flow'] = "no";
            //view
            return view("organization.venues", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_venues method ends here

    //organization_matchvenues
    public function organization_matchvenues(Request $request) {

        $input = $request->all();
        $cache = Cache::flush();

        try{

            //page title
            $this->view_data['title'] = "Venues";
            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_id'] = $session_id;
            //subscription detail
            $this->view_data['sub_id'] = isset($user_detail->subscription->id) ? hashid_decode($user_detail->subscription->id) : "";
            $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
            $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
            $this->view_data['sub_access'] = isset($user_detail->sub_access) ? $user_detail->sub_access : "";
            //subscription detail ends here
            $this->view_data['session_email'] = Session::get('session_email');
            $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "0";

            $event_input = Session::get('event_input');
            if (isset($event_input) && is_array($event_input) && count($event_input) > 0) {

                $event_id = isset($event_input['event_id']) ? hashid_decode($event_input['event_id']) : 0;
                $event_detail = App::make('UserEventRepository')->findById($event_id, false, true);
                //dd($event_detail->);

                $this->view_data['event_id'] = $event_id;
                $this->view_data['sport_id'] = isset($event_detail->sport->id) ? $event_detail->sport->id : 0;
            } else {
                $this->view_data['event_id'] = "0";
                $this->view_data['sport_id'] = "0";
            }

            $this->view_data['rejection_flow'] = "no";

            //dd($this->view_data);

            //view
            return view("organization.matchvenues", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_matchvenues method ends here

    //organization_venueselection
    public function organization_venueselection(Request $request) {

        $input = $request->all();
        $cache = Cache::flush();

        try{

            $decoded = hashid_decode($input['id']);

            $detail =  App::make('UserEventMatchRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Venues";
                $this->view_data['match_id'] = $input['id'];
                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                $this->view_data['session_id'] = $session_id;
                //subscription detail
                $this->view_data['sub_id'] = isset($user_detail->subscription->id) ? hashid_decode($user_detail->subscription->id) : "";
                $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
                $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
                $this->view_data['sub_access'] = isset($user_detail->sub_access) ? $user_detail->sub_access : "";
                //subscription detail ends here
                $this->view_data['session_email'] = Session::get('session_email');
                $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "0";

                $event_id = hashid_decode($detail->event_id);


                $event_detail = App::make('UserEventRepository')->findById($event_id, false, true);
                //dd($event_detail->);

                $this->view_data['event_id'] = $event_id;
                $this->view_data['sport_id'] = isset($event_detail->sport->id) ? $event_detail->sport->id : 0;

                $this->view_data['rejection_flow'] = "yes";
                $this->view_data['match_id'] = $input['id'];

                //dd($this->view_data);

                //view
                return view("organization.venueselection", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_venueselection method ends here

    //organization_venuedetailselection
    public function organization_venuedetailselection(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = App::make('UserVenueRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Venue Detail";
                $this->view_data['record'] = $detail;
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['is_other'] = $input['is_other'];
                $this->view_data['session_user_id'] = Session::get('session_user_id');
                $this->view_data['session_email'] = Session::get('session_email');
                //current date
                $this->view_data['currentdate'] = @date("Y-m-d");



                $match_detail = App::make('UserEventMatchRepository')->findById(hashid_decode($input['match_id']), false, true);
                $this->view_data['match_detail'] = $match_detail;
                $this->view_data['reject_match_id'] = $input['match_id'];

                //dd( $this->view_data);

                //view
                return view("organization.venuedetailselection", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_venuedetailselection method ends here

    //organization_venuedetail
    public function organization_venuedetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserVenueRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Venue Detail";
                $this->view_data['record'] = $detail;
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['is_other'] = $input['is_other'];
                $event_input = Session::get('event_input');
                if (isset($event_input) && is_array($event_input) && count($event_input)) {
                    $this->view_data['event_input'] = Session::get('event_input');
                } else {
                    $this->view_data['event_input'] = array();
                }
//                $this->view_data['event_input'] = Session::get('event_input');
                //event input data
                $event_input = Session::get('event_input');
                $this->view_data['event_id'] = isset($event_input['event_id']) ? $event_input['event_id'] : 0;
                $this->view_data['sequence_no'] = isset($event_input['sequence_no']) ? $event_input['sequence_no'] : 0;
                $this->view_data['title'] = isset($event_input['title']) ? $event_input['title'] : "";
                $this->view_data['total_referee'] = isset($event_input['total_referee']) ? $event_input['total_referee'] : 0;
                $this->view_data['match_fees'] = isset($event_input['match_fees']) ? $event_input['match_fees'] : 0;
                $this->view_data['description'] = isset($event_input['description']) ? $event_input['description'] : "";
                $this->view_data['created_date'] = isset($event_input['created_date']) ? $event_input['created_date'] : "";
                $this->view_data['image'] = isset($event_input['image']) ? $event_input['image'] : "";
                $this->view_data['session_user_id'] = Session::get('session_user_id');
                $this->view_data['session_email'] = Session::get('session_email');
                
                //current date
                $this->view_data['currentdate'] = @date("Y-m-d");
                //view
                return view("organization.venuedetail", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_venuedetail method ends here

    //organization_venueedit
    public function organization_venueedit(Request $request) {

        Cache::flush();

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserVenueRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Venue Edit";
                $this->view_data['record'] = $detail;
                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                //dd($user_detail);
                $this->view_data['session_detail'] = $user_detail;
                $this->view_data['session_id'] = $session_id;
                $this->view_data['subscription_title'] = isset($user_detail->sub_package) ? $user_detail->sub_package : "";
                $this->view_data['subscription_type'] = isset($user_detail->sub_type) ? $user_detail->sub_type : "";
                $this->view_data['country_id'] =  isset($user_detail->country_id) ? $user_detail->country_id : "";
                //view
                return view("organization.venueedit", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_venueedit method ends here

    //organization_search
    public function organization_search(Request $request) {

        $input = $request->all();

        try {

            $this->view_data['title'] = "Search Results";

            //view
            return view("organization.searchresult", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_search method ends here

    //organization_appliedreferees
    public function organization_appliedreferees(Request $request) {

        $input = $request->all();

        try{

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Applied Referees";
                $this->view_data['session_id'] = Session::get('session_id');
                $this->view_data['organization_id'] = $detail->organization_id;
                $this->view_data['input_id'] = $input['id'];

                $this->view_data['session_user_id'] = Session::get('session_user_id');
                $this->view_data['session_email'] = Session::get('session_email');



                //view
                return view("organization.appliedreferees", $this->view_data);

            } else {
                return redirect('organization-home');
            }


        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_appliedreferees method ends here

    //organization_requestreferees
    public function organization_requestreferees(Request $request) {

        $input = $request->all();

        try{

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);

            //dd($detail);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Referee List";
                //session data
                $this->view_data['session_check'] = Session::get('is_front');
                $this->view_data['session_id'] = Session::get('session_id');
                $this->view_data['match_id'] = $input['id'];
                $this->view_data['sport_id'] = $detail->event_sport_id;

                //view
                return view("organization.requestreferees", $this->view_data);

            } else {
                return redirect('organization-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_requestreferees method ends here

    //organization_setting
    public function organization_setting(Request $request) {


        try{
            //page title
            $this->data['title'] = "Setting";
            $this->data['session_id'] = Session::get('session_id');
            //view
            return view("organization.setting", $this->data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_setting method ends here

    //organization_subscription
    public function organization_subscription(Request $request) {

        Cache::flush();


        try{
            //page title
            $this->view_data['title'] = "Packages";
            $this->view_data['session_id'] = Session::get('session_id');
            $this->view_data['session_email'] = Session::get('session_email');

            $session_id = Session::get('session_id');
            $record = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['record'] = $record;
            $this->view_data['is_subscribe'] = isset($record->is_subscribe) ? $record->is_subscribe : "0";

            //view
            return view("organization.subscription", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//organization_subscription method ends here


    /* ------------------ Referee Web Links ------------------ */

    //referee_home
    public function referee_home(Request $request) {

        try{

            $this->view_data['title'] = "Home";

            $this->view_data['event_text'] = "Search for the events and apply for the matches that suits best with your skill set and experience.";
            $this->view_data['match_text'] = "Stay updated about the status of all the matches that you have applied for or have become the part of.";
            $this->view_data['calender_text'] = "View and manage the scheduled matches and perform other relevant activities.";
            $this->view_data['group_text'] = "Always stay connected and communicate proactively among your fellow mates.";

            //view
            return view("referee.index", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_home method ends here

    //referee_dispute
    public function referee_dispute(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Dispute";
            //view
            return view("referee.dispute", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_dispute method ends here

    //referee_disputedetail
    public function referee_disputedetail(Request $request) {

        $input = $request->all();

        try{

            $decoded = hashid_decode($input['id']);
            $detail =  App::make('UserMatchDisputeRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Dispute Detail";
                $session_id = Session::get('session_id');
                $this->view_data['session_id'] = hashid_encode($session_id);
                $this->view_data['id'] = $input['id'];
                $this->view_data['match_id'] = $detail->match_id;

                //view
                return view("referee.disputedetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_disputedetail method ends here

    //referee_profile
    public function referee_profile(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Profile";
            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_id'] = $session_id;
            $this->view_data['subscription_title'] = $user_detail->sub_package;
            $this->view_data['subscription_type'] = $user_detail->sub_type;
            $this->view_data['country_id'] =  $user_detail->country_id;
            //view
            return view("referee.profile", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_profile method ends here

    //referee_editprofile
    public function referee_editprofile(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Edit Profile";
            //session data
            $session_id = Session::get('session_id');
            $user_detail = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['session_detail'] = $user_detail;
            $this->view_data['session_id'] = $session_id;
            $this->view_data['subscription_title'] = $user_detail->sub_package;
            $this->view_data['subscription_type'] = $user_detail->sub_type;
            $this->view_data['country_id'] =  $user_detail->country_id;
            $this->view_data['city_id'] =  $user_detail->city_id;

            //view
            return view("referee.editprofile", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_editprofile method ends here

    //referee_aboutus
    public function referee_aboutus(Request $request) {

        try{

            $this->view_data['title'] = "About us";

            $content = Setting::where('type', 'us')->first();
            $this->view_data['us_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['us_id'] = $content->id;
            $this->view_data['us_simple_content'] = isset($content->content) ? handle_string($content->content) : "";


            //view
            return view("referee.aboutus", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_aboutus method ends here

    //referee_selectplans
    public function referee_selectplans(Request $request) {

        try{

            $is_front = Session::get('is_front');

            if ($is_front == NULL) {
               // return view("organization.login");
                return redirect('/');
            } else {
                //page title
                $this->view_data['title'] = "Select Plan";
                //session data
                $session_id = Session::get('session_id');
                $user_detail = App::make('UserRepository')->findById($session_id, false, true);
                $this->view_data['session_id'] = Session::get('session_id');
                $this->view_data['session_email'] = Session::get('session_email');
                //view
                return view("referee.selectplans", $this->view_data);
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_selectplans method ends here

    //referee_chatzone
    public function referee_chatzone(Request $request) {

        try{

            $this->view_data['title'] = "Chat Zone";
            //view
            return view("referee.chatzone", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_chatzone method ends here

    //referee_payment
    public function referee_payment(Request $request) {

        $cache =  Cache::flush();


        try{
            //page title
            $this->view_data['title'] = "Payment";

            $session_id = Session::get('session_id');
            $user_payments = UserPayment::where('user_id', $session_id)->sum('final_amount');
            $this->view_data['total_spendings'] = isset($user_payments) ? $user_payments : "0";

            $rec_query = UserMatch::join('user_event_matches', 'user_event_matches.id','=','user_matches.match_id')
                ->where('user_matches.to_id', $session_id)
                ->where('user_matches.milestone_status', 'pending')
                ->sum('user_matches.final_amount');

            $this->view_data['total_receivable'] = isset($rec_query) ? $rec_query : "0";

            $withdraw_query = UserMatch::join('user_event_matches', 'user_event_matches.id','=','user_matches.match_id')
                ->where('user_matches.to_id', $session_id)
                ->where('user_matches.milestone_status', 'approved')
                ->sum('user_matches.final_amount');
            $this->view_data['total_withdraw'] = isset($withdraw_query) ? $withdraw_query : "0";
           

            $this->view_data['total_earning'] = isset($withdraw_query) ? $withdraw_query : "0";

            //view
            return view("referee.payment", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_payment method ends here

    //referee_forum
    public function referee_forum(Request $request) {

        try{

            $this->view_data['title'] = "Forum";

            $content = Setting::where('type', 'forum')->first();
            $this->view_data['forum_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['forum_id'] = $content->id;
            $this->view_data['forum_simple_content'] = isset($content->content) ? handle_string($content->content) : "";

            //view
            return view("referee.forum", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_forum method ends here

    //referee_contactus
    public function referee_contactus(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Contact Us";
            //session data
            $user_repo = App::make('UserRepository');
            $session_id = Session::get('session_id');
            $this->view_data['session_id'] = $session_id;
            $this->view_data['session_detail'] = $user_repo->findById($session_id, false, true);

            //view
            return view("referee.contactus", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_contactus method ends here

    //referee_events
    public function referee_events(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Events";

            $this->view_data['event_text'] = "Search for the events and apply for the matches that suits best with your skill set and experience.";



            //view
            return view("referee.events", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_events method ends here

    //referee_eventdetail
    public function referee_eventdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {



            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Event Detail";
                $this->view_data['record'] = $detail;
                $this->view_data['input_id'] = $input['id'];


                $matches_created = UserEventMatch::where('event_id', $decoded)->count();
                $this->view_data['matches_created'] = $matches_created;

                //view
                return view("referee.eventdetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_eventdetail method ends here

    //referee_eventmatches
    public function referee_eventmatches(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);
            //dd($decoded);

            $detail = $session_detail = App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Event Matches";
                $this->view_data['record'] = $detail;
                $this->view_data['input_id'] = $input['id'];
                //view
                return view("referee.eventmatches", $this->view_data);

            } else {
                $this->data['title'] = "Home";
                //view
                return view("referee.index", $this->data);
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_eventmatches method ends here

    //referee_matchdetail
    public function referee_matchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);
            //dd($detail);


            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Match Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['session_id'] = Session::get('session_id');
                $this->view_data['event_id'] = $detail->event_id;
                $this->view_data['organization_id'] = $detail->organization_id;
                $this->view_data['currentdate'] = @date("Y-m-d");


                $group_exist = UserGroup::where('user_id', $this->view_data['session_id'])->first();

                if ($group_exist != NULL) {
                    $this->view_data['is_group'] = "1";
                    $this->view_data['group_id'] = hashid_encode($group_exist->id);
                } else {
                    $this->view_data['is_group'] = "0";
                    $this->view_data['group_id'] = "0";
                }

                //view
                return view("referee.matchdetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_matchdetail method ends here

    //referee_mymatches
    public function referee_mymatches(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "My Matches";
            $this->view_data['match_text'] = "Stay updated about the status of all the matches that you have applied for or have become the part of.";
            //view
            return view("referee.mymatches", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_mymatches method ends here

    //referee_appliedmatchdetail
    public function referee_appliedmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserMatchRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Applied Match Detail";
                $this->view_data['input_id'] = $input['id'];
                //view
                return view("referee.appliedmatchdetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_appliedmatchdetail method ends here

    //referee_pendingmatchdetail
    public function referee_pendingmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserMatchRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Pending Match Detail";
                $this->view_data['input_id'] = $input['id'];
                //view
                return view("referee.pendingmatchdetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_pendingmatchdetail method ends here

    //referee_upcomingsmatchdetail
    public function referee_upcomingsmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserMatchRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Upcoming Match Detail";
                $this->view_data['input_id'] = $input['id'];
                //view
                return view("referee.upcomingsmatchdetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_upcomingsmatchdetail method ends here

    //referee_progressmatchdetail
    public function referee_progressmatchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserMatchRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Progress Match Detail";
                $this->view_data['input_id'] = $input['id'];
                //view
                return view("referee.progressmatchdetail", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_progressmatchdetail method ends here

    //referee_calender
    public function referee_calender(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Calendar";
            $this->view_data['calender_text'] = "Custom made View and manage the scheduled matches and perform other relevant activities.";


            $this->view_data['dateTime'] = @date("Y-m-d H:i:s");
            //view
            return view("referee.calender", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_calender method ends here

    //referee_addgroup
    public function referee_addgroup(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Add Group";
            $this->view_data['loggedin_id'] = hashid_encode(Session::get('session_id'));
            //view
            return view("referee.addgroup", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_addgroup method ends here

    //referee_mygroups
    public function referee_mygroups(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "My Groups";
            //view
            return view("referee.mygroups", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_mygroups method ends here

    //referee_myinvitations
    public function referee_myinvitations(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Group Invitations";
            //view
            return view("referee.myinvitations", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_myinvitations method ends here

    //referee_groupmembers
    public function referee_groupmembers(Request $request) {

        $input = $request->all();

        try{

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserGroupRepository')->findById($decoded, false, true);
            //dd($detail);

            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Group Members";
                //session data
                $this->view_data['group_id'] = $input['id'];
                $this->view_data['group_title'] = $detail->title;
                $this->view_data['group_image'] = "";
                $this->view_data['group_owner_id'] = $detail->user_id;
                $this->view_data['member_count'] = $detail->member_count;
                $this->view_data['icon'] = $detail->icon;
                $this->view_data['color_code'] = $detail->color_code;
                $this->view_data['session_encoded'] = hashid_encode(Session::get('session_id'));
                //view
                return view("referee.groupmembers", $this->view_data);

            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_groupmembers method ends here

    //referee_addgroupmembers
    public function referee_addgroupmembers(Request $request) {

        $input = $request->all();

        try{

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserGroupRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Add More Members";
                //session data
                $this->view_data['group_id'] = $input['id'];
                $this->view_data['group_title'] = $detail->title;
                $this->view_data['group_image'] = "";
                $this->view_data['icon'] = $detail->icon;
                $this->view_data['color_code'] = $detail->color_code;
                $this->view_data['loggedin_id'] = Session::get('session_id');
                //view
                return view("referee.addgroupmembers", $this->view_data);
            } else {
                return redirect('referee-home');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_addgroupmembers method ends here

    //referee_notifications
    public function referee_notifications(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Notification";

            //view
            return view("referee.notifications", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_notifications method ends here

    //referee_links
    public function referee_links(Request $request) {

        try{
            //page title
            $this->view_data['title'] = "Referee Links";

            //view
            return view("referee.links", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_links method ends here

    //referee_setting
    public function referee_setting(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Setting";
            $this->view_data['session_id'] = Session::get('session_id');

            //view
            return view("referee.setting", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_setting method ends here

    //referee_subscription
    public function referee_subscription(Request $request) {

        Cache::flush();

        try{

            //page title
            $this->view_data['title'] = "Packages";
            $this->view_data['session_id'] = Session::get('session_id');
            $this->view_data['session_email'] = Session::get('session_email');

            $session_id = Session::get('session_id');
            $record = App::make('UserRepository')->findById($session_id, false, true);
            $this->view_data['record'] = $record;
            $this->view_data['is_subscribe'] = isset($record->is_subscribe) ? $record->is_subscribe : "0";

            //view
            return view("referee.subscription", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//referee_subscription method ends here

    /* ------------------ Admin Panel Web Links ------------------ */

    //admin_dashboard
    public function admin_dashboard(Request $request) {

        try{

            $this->view_data['title'] = "Admin Dashboard";
            //stats
            $this->view_data['user_count'] = User::where('user_type', '!=', 'admin')->count();
            $this->view_data['event_count'] = UserEvent::count();
            $this->view_data['venue_count'] = UserVenue::count();
            $this->view_data['pay_count'] = UserPayment::sum('final_amount');
            //view
            return view("admin.dashboard", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_dashboard method ends here

    //admin_user
    public function admin_user(Request $request) {

        try{

            $this->view_data['title'] = "User Management";

            //view
            return view("admin.user", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_user method ends here

    //admin_organizationdetail
    public function admin_organizationdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                //page title
                $this->view_data['title'] = "Organization Detail";
                //input id
                $this->view_data['input_id'] = $input['id'];

                //view
                return view("admin.organizationdetail", $this->view_data);

            } else {
                return redirect('admin-dashboard');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_organizationdetail method ends here

    //admin_refereedetail
    public function admin_refereedetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserRepository')->findById($decoded, false, true);

            if ($detail != NULL) {
                //page title
                $this->view_data['title'] = "Referee Detail";
                //input id
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['ref_access_token'] = $detail->access_token;
                //view
                return view("admin.refereedetail",  $this->view_data);

            } else {
                return redirect('admin-dashboard');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_refereedetail method ends here

    //admin_group
    public function admin_group(Request $request) {

        try{

            //page title
            $this->view_data['title'] = "Group Management";
            //view
            return view("admin.group", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_group method ends here

    //admin_groupdetail
    public function admin_groupdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserGroupRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                $this->view_data['title'] = "Group Detail";
                $this->view_data['input_id'] = $input['id'];

                //view
                return view("admin.groupdetail", $this->view_data);

            } else {
                return redirect('admin-dashboard');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_groupdetail method ends here

    //admin_event
    public function admin_event(Request $request) {

        try{

            $this->view_data['title'] = "Event Management";

            //view
            return view("admin.event", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_event method ends here

    //admin_eventdetail
    public function admin_eventdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserEventRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                $this->view_data['title'] = "Event Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['record'] = $detail;

                //view
                return view("admin.eventdetail", $this->view_data);

            } else {
                return redirect('admin-dashboard');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_eventdetail method ends here

    //admin_matchdetail
    public function admin_matchdetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);
            //dd($decoded);
            $detail = $session_detail = App::make('UserEventMatchRepository')->findById($decoded, false, true);


//
            if ($detail != NULL) {

                $this->view_data['title'] = "Match Detail";
                $this->view_data['input_id'] = $input['id'];
                $this->view_data['event_id'] = $detail->event_id;
                $this->view_data['event_status'] = $detail->event_status;

                //view
                return view("admin.matchdetail", $this->view_data);

            } else {
                return redirect('admin-dashboard');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_matchdetail method ends here

    //admin_venue
    public function admin_venue(Request $request) {

        try{

            $this->view_data['title'] = "Venue Management";

            //view
            return view("admin.venue", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_venue method ends here

    //admin_venuedetail
    public function admin_venuedetail(Request $request) {

        $input = $request->all();
        //dd($input);

        try {

            $decoded = hashid_decode($input['id']);

            $detail = $session_detail = App::make('UserVenueRepository')->findById($decoded, false, true);

            if ($detail != NULL) {

                $this->view_data['title'] = "Venue Detail";
                $this->view_data['input_id'] = $input['id'];

                //view
                return view("admin.venuedetail", $this->view_data);

            } else {
                return redirect('admin-dashboard');
            }

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_venuedetail method ends here

    //admin_dispute
    public function admin_dispute(Request $request) {

        try{

            $this->view_data['title'] = "Dispute Management";

            //view
            return view("admin.dispute", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_dispute method ends here

    //admin_coupan
    public function admin_coupan(Request $request) {


        try{

            $this->view_data['title'] = "Coupon Management";

            //view
            return view("admin.coupan", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_coupan method ends here

    //admin_sports
    public function admin_sports(Request $request) {

        try{

            $this->view_data['title'] = "Sports Management";

            //view
            return view("admin.sports", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_sports method ends here

    //admin_links
    public function admin_links(Request $request) {

        try{

            $this->view_data['title'] = "Referee Links";

            //view
            return view("admin.links", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_links method ends here

    public function admin_qa(Request $request) {

        try{

            $this->view_data['title'] = "Forum Management";

            //view
            return view("admin.qa", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_qa method ends here

    //admin_payment
    public function admin_payment(Request $request) {

        try{

            $this->view_data['title'] = "Payment Management";

            //view
            return view("admin.payment", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_payment method ends here

    //admin_content
    public function admin_content(Request $request) {

        try{

            $this->view_data['title'] = "Content Management";

            $content = Setting::where('type', 'tc')->first();
            $this->view_data['tc_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['tc_id'] = $content->id;
            $this->view_data['tc_simple_content'] = isset($content->content) ? handle_string($content->content) : "";

            $content = Setting::where('type', 'pp')->first();
            $this->view_data['pp_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['pp_id'] = $content->id;
            $this->view_data['pp_simple_content'] = isset($content->content) ? handle_string($content->content) : "";

            $content = Setting::where('type', 'us')->first();
            $this->view_data['us_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['us_id'] = $content->id;
            $this->view_data['us_simple_content'] = isset($content->content) ? handle_string($content->content) : "";

            $content = Setting::where('type', 'forum')->first();
            $this->view_data['forum_content'] = isset($content->content) ? $content->content : "";
            $this->view_data['forum_id'] = $content->id;
            $this->view_data['forum_simple_content'] = isset($content->content) ? handle_string($content->content) : "";

            //dd($this->view_data);

            //view
            return view("admin.content", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_content method ends here

    //admin_contact
    public function admin_contact(Request $request) {

        try{

            $this->view_data['title'] = "Contact Management";

            //view
            return view("admin.contact", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_contact method ends here

    //admin_notifications
    public function admin_notifications(Request $request) {

        try{

            $this->view_data['title'] = "Notifications";

            //view
            return view("admin.notifications", $this->view_data);

        } catch (\Exception $e){
            $custom_error = $e->getFile().' '.$e->getLine().' '.$e->getMessage();
            \Log::debug($custom_error);
            $message = "Opps something went wrong";
            echo "<center>$message</center>";
        }

    }//admin_notifications method ends here

    /* ------------------ Helpers ------------------ */

    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.matches.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.matches.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.matches.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.collage.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }

        }
        return true;
    }


}//SettingController class ends here