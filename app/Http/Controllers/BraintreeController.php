<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Braintree_ClientToken;
use Braintree_PaymentMethodNonce;
use Braintree;

class BraintreeController extends Controller{

    public function token(Request $request){

        try {

            $token = Braintree_ClientToken::generate();
            if ($token != "") {
                $code = 200;
                $message = "Token found";
                $output = ['response' => ['code' => $code, 'messages' => [$message], 'data' => $token]];
            } else {
                $code = 401;
                $message = "Opps something went wrong";
                $output = ['error' => ['code' => $code, 'messages' => [$message]]];
            }


        } catch(\Exception $e){
            $code = 401;
            $message = "Opps something went wrong";
            $output = ['error' => ['code' => $code, 'messages' => [$message]]];
        }

        return response()->json($output, $code);

    }//token method ends here

}//BraintreeController class ends here