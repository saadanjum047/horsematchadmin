<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Blog
class Blog extends Model{

    protected $table = "blogs";
    use SoftDeletes;


    public static function get_blogs() {
    	return Blog::orderBy('id','DESC')->paginate(10);
    }

}//Blog class ends here
