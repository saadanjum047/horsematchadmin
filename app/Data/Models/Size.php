<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Setting
class Size extends Model{

    protected $table = "sizes";
    use SoftDeletes;

}//Setting class ends here
