<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Promotions extends Model
{
    //

    protected $table = 'promotions';
    protected $fillable = [
        'promotion_text',
        'number_of_users'
    ];
}
