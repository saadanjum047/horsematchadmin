<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Color
class Color extends Model{

    protected $table = "colors";
    use SoftDeletes;

}//Color class ends here
