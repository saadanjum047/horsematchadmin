<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Setting
class Temperament extends Model{

    protected $table = "temperaments";
    use SoftDeletes;

}//Setting class ends here
