<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserFavouritePost
class UserFavouritePost extends Model{

    protected $table = "user_favourite_posts";
    use SoftDeletes;

}//UserFavouritePost class ends here
