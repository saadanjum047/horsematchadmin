<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserDevice
class UserDevice extends Model{

    protected $table = "user_devices";
    use SoftDeletes;

}//UserDevice class ends here
