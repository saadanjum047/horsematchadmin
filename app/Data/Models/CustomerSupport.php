<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Blog
class CustomerSupport extends Model{

    protected $table = "contact_us";
    use SoftDeletes;


    public static function get_messages() {
    	return CustomerSupport::orderBy('id','DESC')->paginate(10);
    }

}//Blog class ends here
