<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'subscription_id',
        'package_id',
        'amount'
    ];
    
    public function subscriptions(){
        return $this->belongsTo(Subscription::class,'subscription_id','id');
    }

    public function packages(){
        return $this->belongsTo(Packages::class,'package_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
