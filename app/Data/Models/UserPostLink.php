<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserPostLink
class UserPostLink extends Model{

    protected $table = "user_post_links";
    use SoftDeletes;

}//UserPostLink class ends here
