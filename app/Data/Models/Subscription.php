<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = [
        'name',
        'duration',
        'horses_count',
        'amount'
    ];
}
