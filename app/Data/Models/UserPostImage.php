<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserPostImage
class UserPostImage extends Model{

    protected $table = "user_post_images";
    use SoftDeletes;

}//UserPostImage class ends here
