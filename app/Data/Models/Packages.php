<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $table = 'packages';

    protected $fillable = [
        'name',
        'price',
        'numberofdays'
    ];
}
