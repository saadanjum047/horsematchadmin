<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Breed
class Breed extends Model{

    protected $table = "breeds";
    use SoftDeletes;

}//Breed class ends here
