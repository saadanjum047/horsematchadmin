<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class UserFollower extends Model
{
    protected $table = 'user_followers';

    protected $fillable = [
      'user_id',
      'follower_id'
    ];
}
