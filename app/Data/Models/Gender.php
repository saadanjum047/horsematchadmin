<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Gender
class Gender extends Model{

    protected $table = "genders";
    use SoftDeletes;

}//Gender class ends here
