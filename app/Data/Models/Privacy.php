<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Activity
class Privacy extends Model{

    protected $table = "activities";
    use SoftDeletes;

}//Activity class ends here
