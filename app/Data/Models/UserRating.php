<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserRating
class UserRating extends Model{

    protected $table = "user_ratings";
    use SoftDeletes;

public  function buyer(){

        return $this->hasOne('App\Data\Models\User', 'id','user_id');
}



}//UserRating class ends here
