<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserSocialAccount
class UserSocialAccount extends Model{

    protected $table = "user_social_accounts";
    use SoftDeletes;

}//UserSocialAccount class ends here
