<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserPost
class UserPost extends Model{

    protected $table = "user_posts";
    use SoftDeletes;



    public function breed(){
        return $this->belongsTo(Breed::class);
    }

    public function discipline(){
        return $this->belongsTo(Discipline::class , 'discipline_id');
    }

    public function color(){
        return $this->belongsTo(Color::class);
    }

    public function gender(){
        return $this->belongsTo(Gender::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function images(){
        return $this->belongsTo(UserPostImage::class);
    }


    public function notinterested(){

        return $this->hasMany('App\Data\Models\UserNotinterestPost', 'post_id', 'id');
    }

    public function disciplines(){

        return $this->hasMany('App\Data\Models\UserPostDiscipline', 'post_id', 'id');
    }
    
    public function notsold(){
        return $this->hasOne('App\Data\Models\UserSoldPost','post_id','id');
    }
}//UserPost class ends here
