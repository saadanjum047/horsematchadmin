<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PackageManagement extends Model{

    protected $table = "package_management";
    use SoftDeletes;


    public static function get_packages() {
    	return PackageManagement::orderBy('id','DESC')->paginate(10);
    }

}
