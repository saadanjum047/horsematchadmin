<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Discipline
class Discipline extends Model{

    protected $table = "disciplines";
    use SoftDeletes;

}//Discipline class ends here
