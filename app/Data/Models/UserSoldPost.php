<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserSoldPost
class UserSoldPost extends Model{

    protected $table = "user_sold_posts";
    use SoftDeletes;

}//UserSoldPost class ends here
