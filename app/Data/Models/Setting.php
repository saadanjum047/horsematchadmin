<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Setting
class Setting extends Model{

    protected $table = "settings";
    use SoftDeletes;

}//Setting class ends here
