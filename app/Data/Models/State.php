<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Setting
class State extends Model{

    protected $table = "states";
    use SoftDeletes;

}//Setting class ends here
