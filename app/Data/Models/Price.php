<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//Setting
class Price extends Model{

    protected $table = "prices";
    use SoftDeletes;

}//Setting class ends here
