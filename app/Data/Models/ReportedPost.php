<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ReportedPost extends Model
{
    //

    protected $table = 'reported_posts';
    protected $fillable = [
        'user_id',
        'post_id',
        'reported_message'
    ];
}
