<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//UserPreference
class UserPreference extends Model{

    protected $table = "user_preferences";
    use SoftDeletes;

}//UserPreference class ends here
