<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Model {

    protected $table = "users";
    use SoftDeletes;

//    /* ========= Manage Relationships  ======== */
//
//    public function children(){
//        return $this->hasMany(User::class, 'parent_id');
//    }
//
//    public function parent(){
//        return $this->belongsTo(User::class, 'parent_id');
//    }
//
//    //user has many menus
//    public function menus(){
//        return $this->hasMany(UserMenu::class, 'user_id');
//    }//user has many menus ends here
//
//    //user has many vouchers
//    public function vouchers(){
//        return $this->hasMany(Voucher::class, 'user_id');
//    }//user has many vouchers ends here


    public static function get_users() {
    	return DB::table('users')
    			->where('user_type','!=','admin')
    			->orderBy('id','DESC')
    			->paginate(5);
    }

    public static function get_user( $id ) {
    	return User::find($id);
    }

    public function discipline(){
        return $this->belongsTo(Discipline::class,'discipline','id');
    }

}
