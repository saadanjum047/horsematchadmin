<?php

namespace App\Data\Repositories;

use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Event;
use App\Data\Contracts\RepositoryContract;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Hashids\Hashids;
use JWTAuth;
use Storage, Image;
use \App,DB,StdClass;
use Laravel\Cashier\Cashier;
use Braintree_Transaction;
use checkmobi\CheckMobiRest;
use DateTime;

//models
use App\Data\Models\User;
use App\Data\Models\UserDevice;
use App\Data\Models\Breed;
use App\Data\Models\Discipline;
use App\Data\Models\UserSoldPost;
use App\Data\Models\UserRating;
use App\Data\Models\UserPreference;
use App\Data\Models\UserNotinterestPost;
use App\Helpers\Helper;
use App\Data\Models\Gender;
use App\Data\Models\Color;
class UserRepository extends AbstractRepository implements RepositoryContract{

    public $model;

    protected $_cacheKey = 'user-';
    protected $_cacheTotalKey = 'total-users';

    public function __construct(User $user) {
        $this->builder = $user;
        $this->model = $user;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {


       // parent::setEncodedKeys(array('country_id', 'city_id', 'sport_id'));
        $data = parent::findById($id, $refresh, $details, $encode);

        $currentdate = @date("Y-m-d");

        if ($data) {


            $decoded = Helper::hashid_decode($data->id);

            //rating
            $rating = 0;
            $rAverage = "";
            $rating_query = UserRating::where('buyer_id',$decoded)->get();
            if (count($rating_query) > 0) {
                foreach($rating_query as $rat) {
                    $rating = $rating + $rat->rating;
                }
                $rCount = count($rating_query);
                $rAverage = $rating/$rCount;
                $rAverage = ceil($rAverage);
            } else {
                $rating = 0;
                $rAverage = 0;
            }
            $data->rating = $rAverage;

            if($encode){
                //$data->id = hashid_encode($data->id);
                $data->discipline = Helper::hashid_encode($data->discipline);
                $data->breed = Helper::hashid_encode($data->breed);
            } else {
                $data->discipline = Helper::hashid_encode($data->discipline);
                $data->breed = Helper::hashid_encode($data->breed);

            }
            
            // $data->postsremaining = 2;


            if (isset($data->discipline)) {


                $discipline_query = Discipline::where('id', Helper::hashid_decode($data->discipline))->first();
                //  dd(Helper::hashid_decode($data->discipline));
                $data->discipline_text = isset($discipline_query->title) ? Helper::handle_string($discipline_query->title) : "";
            }
            if (isset($data->breed)) {
                $breed_query = Breed::where('id', Helper::hashid_decode($data->breed))->first();
                $data->breed_text = isset($breed_query->title) ? Helper::handle_string($breed_query->title) : "";

            }



            if(isset($data->image) && $data->image !=NULL){
                list($file, $extension) = @explode('.', $data->image);
                $file = config('app.files.users.folder_name') . "/" . $file;

                $data->image_urls['1x'] = config('app.url') . '/storage/app/' . $file . '.' . $extension;
                $data->image_urls['2x'] = config('app.url') . '/storage/app/' . $file . '@2x.' . $extension;
                $data->image_urls['3x'] = config('app.url') . '/storage/app/' . $file . '@3x.' . $extension;

            } else {
                $data->image_urls['1x'] = url('/public/images/logo.png');
                $data->image_urls['2x'] = url('/public/images/logo.png');
                $data->image_urls['3x'] = url('/public/images/logo.png');
            }

            unset($data->created_at, $data->updated_at, $data->deleted_at);

            //minimum view
            if ($recoverPassword == true) {
                $data->id = hashid_encode($data->id);
                unset($data->parent_id, $data->user_type, $data->signup_via, $data->email, $data->password, $data->org_password, $data->image,  $data->contact_person, $data->dob, $data->country_id, $data->city_id, $data->zipcode, $data->address, $data->latitude, $data->longitude, $data->weburl, $data->experience, $data->firebase_id, $data->news_letter, $data->is_notification, $data->activation_code, $data->login_at, $data->is_block, $data->is_online, $data->device_type, $data->recover_password_key, $data->recover_attempt_at, $data->account_visibility, $data->visibility, $data->created_date, $data->access_token, $data->sports, $data->seperated_sports, $data->country_name, $data->city_name);
            }

        }


        return $data;
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true,$refresh=true){

        //dd($data);

        $users = $this->builder;

        if( isset($data['keyword']) && $data['keyword'] != ""){
            $users = $users->whereRaw('(users.username LIKE "%'.$data['keyword'].'%")');
        }

        if( isset($data['user_id']) && $data['user_id'] != ""){
            $users = $users->where('users.id', '!=', $data['user_id']);
        }

        $users = $users->where('users.user_type', '!=', 'admin')->where('users.verify_status','1');
        $users = $users->orderBy("users.created_at","desc")->select('users.id');
        $this->builder = $users;

        return parent::findByAll($pagination,$perPage,[],$detail,true);


    }

    //register
    public function register(array $data = [], $role_id = 0){

        $current_datetime = @date("Y-m-d H:i:s");

        if(isset($data['password']) && !empty($data['password'])){
            $data['password'] = Hash::make($data['password']);
            // $data['create_password'] =  1 ;
        }else{
            $data['password'] = Hash::make(rand(7,5));
            // $data['create_password'] =  0 ;
        }


        //verify_code
        $code = Helper::random_number(4);
        $exist = User::where('verify_code', $code)->first();
        if ($exist != NULL) {
            $data['verify_code'] = Helper::random_number(4);
        } else {
            $data['verify_code'] = $code;
        }//verify_code ends here

        if ($data) {

            if ($user = parent::create($data, true, false)) {

                //send email
                $message = "Welcome to HorseMatch. <br> Your verification code : <b>$code</b>  ";
                $sendEmail = Helper::sendEmail($data['email'], "Welcome to HorseMatch", $message);
                //send email ends here

                return $this->update([
                    'id' => $user->id,
                    'login_at' => Carbon::now(),
                    'access_token' => JWTAuth::fromUser($user),
                ], true, true);

            }
        } else {
            return false;
        }

    }//register method ends here



    public function login($data) {

        $user_exist = User::where('email', $data['email'])->orWhere('phone',$data['email'])->get()->first();
        //validation
        if (count($user_exist) > 0) {
            $user = $this->findByAttribute('email', $user_exist['email'], false, true, false);
            if ($user != NULL) {
                if (Hash::check($data['password'], $user->password)) {

                    return $this->update([
                        'id' => $user->id,
                        'login_at' => Carbon::now(),
                        'access_token' => JWTAuth::fromUser($user),
                    ], true, true);

                }
            }
            return false;
        } else {
            return false;
        }
    }

    //verify_code
    public function verify_code($data) {

        $user_exist = User::where('verify_code', $data['verify_code'])->count();
        //validation
        if ($user_exist > 0) {
            $user = $this->findByAttribute('verify_code', $data['verify_code'], false, true, false);
            if ($user != NULL) {
                return $this->update([
                    'id' => $user->id,
                    'login_at' => Carbon::now(),
                    'verify_status' => "1",
                    'access_token' => JWTAuth::fromUser($user),
                ], true, true);
            }
            return false;
        } else {
            return false;
        }

    }//verify_code method ends here

    private function saveImage($image, $name) {
        list($name, $ext) = explode('.', $name);
        $width = $image->width();
        $height = $image->height();
        Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
        Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
        Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        return config('app.files.users.folder_name').'/'.$name.'.'.$ext;
    }

    //users_list
    public function users_list($data){

        $array = array();

        $requests = User::where('user_level', $data['id'])->orderBy('id', 'desc')->paginate(150);
        $count = count($requests);

        //validation
        if ($count > 0) {

            foreach ($requests as $req) {
                $user_id = $req['id'];
                $array[] =  $this->findById($user_id);
            }

        } else {
            $array = array();
        }

        $data['data']=$array;
        $data['pagination'] = Helper::pagination($requests);

        return $data;

    }//users_list method ends here

    //manage_device
    public function manage_device($data) {

        $info = array();
        $user_device = array();
        $update = "";
        $add = "";

        $device_exist = UserDevice::where('user_id', $data['user_id'])->where('udid', $data['udid'])->count();
        if ($device_exist > 0) {
            $user_device = array();
            $user_device['token'] = $data['token'];
            $user_device['type'] = $data['type'];
            $update = UserDevice::where('user_id', $data['user_id'])->where('udid', $data['udid'])->update($user_device);
            $info['message'] = "You have successfully updated the device";
        } else {
            $user_device = array();
            $user_device = array();
            $user_device['user_id'] = $data['user_id'];
            $user_device['udid'] = $data['udid'];
            $user_device['token'] = $data['token'];
            $user_device['type'] = $data['type'];
            $add = UserDevice::insertGetId($user_device);
            $info['message'] = "New device is added";
        }

        return $info;

    }//manage_device method ends here

    //remove_device
    public function remove_device($data){

        $info = array();
        $update = "";

        $device_exist = UserDevice::where('user_id', $data['user_id'])->where('udid', $data['udid'])->count();
        if ($device_exist > 0) {

            $update =  UserDevice::where('user_id', $data['user_id'])->where('udid', $data['udid'])->forceDelete();

            $info['status'] = "1";
            $info['message'] = "You are logged out";
        } else {
            $info['status'] = "1";
            $info['message'] = "You are logged out";
        }

        return $info;

    }//remove_device method ends here

    public function changePassword($data){

        $user = $this->model->where('id', $data['user_id'] )->first();

        if($user){
            if($user->signup_via == "email"){

                $check = \Auth::attempt(['email' => $user->email, 'password' => $data['old_password']]);

                if($check){
                    $updatePass = [];
                    $updatePass['id'] = $user->id;
                    $updatePass['password'] = Hash::make($data['new_password']);
                    return parent::update($updatePass, true);
                } else {
                    $result = ['error' => 'old password incorrect.'];
                    return $result;
                }
            } else {
                $result = ['error' => 'You cannot reset the password with social account.'];
                    return $result;
            }
        }

    }

    //manage_preference
    public function manage_preference($input){
        $dateTime = @date("Y-m-d H:i:s");
        $currentdate = @date("Y-m-d");

        if(isset($input['not_interested']) && $input['not_interested']=="reset"){
            UserNotinterestPost::where(['user_id'=>$input['user_id']])->delete();
        }

        $exist = UserPreference::where('user_id', $input['user_id'])->first();
        if ($exist != NULL) {
            $array = array();
            $array['input'] = serialize($input);
            $action = UserPreference::where('id', $exist->id)->update($array);
            $info['status'] = 1;
            $info['message'] = "Your preference is updated";
        } else {
            $array = array();
            $array['user_id'] = $input['user_id'];
            $array['input'] = serialize($input);
            $array['created_date'] = $currentdate;
            $array['created_at'] = $dateTime;
            $action = UserPreference::insertGetId($array);
            $info['status'] = 1;
            $info['message'] = "Your preference is set";
        }



        return $info;
    }//manage_preference

    //get_preference
    public function get_preference($input){
        $query = UserPreference::where('user_id', $input['user_id'])->first();




        if ($query != NULL) {
            $info = unserialize($query->input);

if(isset($info['breeds'])) {

    $breeds = $info['breeds'];
    $info['breeds'] = array();
    $breedList = Breed::orderBy('id', 'asc')->get()->toArray();
    foreach ($breedList as $key => $value) {
        $value['id'] = Helper::hashid_encode($value['id']);
        $value['title'] = Helper::handle_string($value['title']);
        if (in_array($value['id'], $breeds)) {
            $value['isSelected'] = 1;
        } else {
            $value['isSelected'] = 0;
        }
        $info['breeds'][] = $value;

    }
}


            if(isset($info['disciplines'])) {

                $breeds = $info['disciplines'];
                $info['disciplines'] = array();
                $disciplineList = Discipline::orderBy('id', 'asc')->get()->toArray();
                foreach ($disciplineList as $key => $value) {
                    $value['id'] = Helper::hashid_encode($value['id']);
                    $value['title'] = Helper::handle_string($value['title']);
                    if (in_array($value['id'], $breeds)) {
                        $value['isSelected'] = 1;
                    } else {
                        $value['isSelected'] = 0;
                    }
                    $info['disciplines'][] = $value;

                }
            }


            if(isset($info['genders'])) {

                $breeds = $info['genders'];
                $info['genders'] = array();
                $gendersList = Gender::orderBy('id', 'asc')->get()->toArray();
                foreach ($gendersList as $key => $value) {
                    $value['id'] = Helper::hashid_encode($value['id']);
                    $value['title'] = Helper::handle_string($value['title']);
                    if (in_array($value['id'], $breeds)) {
                        $value['isSelected'] = 1;
                    } else {
                        $value['isSelected'] = 0;
                    }
                    $info['genders'][] = $value;

                }
            }


            if(isset($info['colors'])) {

                $breeds = $info['colors'];
                $info['colors'] = array();
                $colorsList = Color::orderBy('id', 'asc')->get()->toArray();
                foreach ($colorsList as $key => $value) {
                    $value['id'] = Helper::hashid_encode($value['id']);
                    $value['title'] = Helper::handle_string($value['title']);
                    if (in_array($value['id'], $breeds)) {
                        $value['isSelected'] = 1;
                    } else {
                        $value['isSelected'] = 0;
                    }
                    $info['colors'][] = $value;

                }
            }



        } else {
            $info = (object) array();
        }
        return $info;
    }//get_preference method ends here

}
