<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\Log;
use JWTAuth;
use App\Data\Contracts\RepositoryContract;
use Hash;
use Storage, Image;
use \App;
use Validator;
use DB;
use App\Data\Models\User;
use App\Data\Models\UserPost;
use App\Data\Models\Breed;
use App\Data\Models\Color;
use App\Data\Models\Gender;
use App\Data\Models\Discipline;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserSoldPost;
use App\Data\Models\UserFavouritePost;
use App\Data\Models\UserPreference;
use App\Data\Models\UserPostDiscipline;



//Helpers
use App\Helpers\Helper;

//UserPostRepository
class UserPostRepository extends AbstractRepository  implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * category data will be stored
     * category Auto incremented Id will be append to it
     *
     * Example: category-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'userpost-';
    protected $_cacheTotalKey = 'total-userposts';

    public function __construct(UserPost $userPost) {
        $this->builder = $userPost;
        $this->model = $userPost;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

        //dd('test');

        $data = parent::findById($id, $refresh, $details, $encode);
        if ($data) {

            //dd($data);

            if ($encode) {
                $data->user_id = Helper::hashid_encode($data->user_id);
//                $data->breed_id = Helper::hashid_encode($data->breed_id);
               // $data->discipline_id = Helper::hashid_encode($data->discipline_id);
                $data->color_id = Helper::hashid_encode($data->color_id);
                $data->gender_id = Helper::hashid_encode($data->gender_id);
            }

            $decoded_id = Helper::hashid_decode($data->id);

            if ($details) {

//                $breeddetail = Breed::where('id',Helper::hashid_decode($data->breed_id))->get()->first();
//if($breeddetail!=null){
//    $data->breed_text = Helper::handle_string($breeddetail['title']);
//
//} else {
//    $data->breed_text = "";
//
//}

          $decoded_ids= explode(',',$data->discipline_id);
          $decoded_breed_ids = explode(',',$data->breed_id);
          foreach ($decoded_breed_ids as $val){
              $decodedbreedids[] = Helper::hashid_decode($val);
          }
          $breeddetail = Breed::whereIn('id',$decodedbreedids)->get()
              ->map(function ($breed){
                 return [
                     "id"=>Helper::hashid_encode($breed->id),
                     "title"=>Helper::handle_string($breed->title)
                 ];
              });

   foreach ($decoded_ids as $val){
       $decoded_idsnew[]=Helper::hashid_decode($val);
    }


          $disciplinedetail = Discipline::whereIn('id',$decoded_idsnew)->get()
              ->map(function ($discipline){
                  return [
                      "id" => Helper::hashid_encode($discipline->id),
                      "title" => Helper::handle_string($discipline->title)
                  ];
              });


          //                $data->discipline_text = Helper::handle_string($disciplinedetail->title);

                $colordetail = Color::where('id',Helper::hashid_decode($data->color_id))->get(['id','title'])->first();
                if($colordetail!=null){
                    $data->color_text = Helper::handle_string($colordetail['title']);
                }else {
                    $data->color_text="";
                }

                $genderdetail = Gender::where('id',Helper::hashid_decode($data->gender_id))->get(['id','title'])->first();
                if($genderdetail!=null){
                    $data->gender_text = Helper::handle_string($genderdetail['title']);

                }else {
                    $data->gender_text;
                }

                $data->disciplines=$disciplinedetail;
                $data->breeds=$breeddetail;
                $input_links = UserPostLink::
                where('post_id', $decoded_id)
                ->orderBy('id', 'asc')
                    ->get()
                    ->map(function ($input_links){
                        return [
                            "id" => Helper::hashid_encode($input_links->id),
                            "link" => $input_links->link
                        ];
                    });
                $data->input_links = $input_links;

                //images handling

                $img_query = UserPostImage::where('post_id', $decoded_id)->get();
                if (count($img_query) > 0) {
                    foreach($img_query as $img) {

                        list($file, $extension) = @explode('.', $img->image);
                        $file = config('app.files.postimages.folder_name') . "/" . $file;

                        $event_images['1x'] = config('app.url') . '/storage/app/' . $file . '.' . $extension;
                        $event_images['2x'] = config('app.url') . '/storage/app/' . $file . '@2x.' . $extension;
                        $event_images['3x'] = config('app.url') . '/storage/app/' . $file . '@3x.' . $extension;

                        $input_images[] = array(
                            "id" => Helper::hashid_encode($img->id),
                            "image" => $event_images
                        );

                    }
                } else {
                    $input_images = array();
                }

                $data->input_images = $input_images;

                $repo = App::make('UserRepository');
                $data->owner = $repo->findById(Helper::hashid_decode($data->user_id));

                //buyer handling
                $sold_detail = UserSoldPost::join('users', 'users.id','=','user_sold_posts.buyer_id')->where('user_sold_posts.post_id',$decoded_id)->select('users.id', 'users.username')->first();
                //dd($sold_detail);
                $buyer_id = isset($sold_detail->id) ? Helper::hashid_encode($sold_detail->id) : "";
                $data->buyer_id = $buyer_id;
                $buyer_id=(Helper::hashid_decode($buyer_id));
                $chat_id=User::where('id',$buyer_id)->first();
                if($chat_id){
                $chat_id=$chat_id->chat_id;
                $data->chat_id=$chat_id;
                }
                $buyer_name = isset($sold_detail->username) ? $sold_detail->username : "";
                $data->buyer_name = $buyer_name;
                //buyer handling ends here

            }

            $token = JWTAuth::getToken();
            $token_user_id = 0;
            if($token) {
                $tokenUser = JWTAuth::toUser($token);
                $token_user_id =  $tokenUser->id;
                $data->is_sold = UserSoldPost::where('user_id', $token_user_id)->where('post_id', $decoded_id)->count();
                $data->is_favourite = UserFavouritePost::where('user_id', $token_user_id)->where('post_id', $decoded_id)->count();
            } else {
                $data->is_sold = 0;
                $data->is_favourite = 0;
            }


        }

        unset($data->updated_at, $data->deleted_at);

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, array $data = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;


        if (isset($input['admin_loggedin']) == true ) {
        } else {
            if (isset($data['user_id']) && $data['user_id'] != "") {
                $records = $records->where('user_posts.user_id', '!=',  $data['user_id']);

            }
        }

        $user_preference = UserPreference::where('user_id', $data['user_id'])->first();

//dd    ($user_preference->input);
        //preference validation
        if ($user_preference != NULL) {

            $input = unserialize($user_preference->input);


            if(isset($input['lat']) && isset($input['long']) && isset($input['miles_end'])){

            $lat = $input['lat'];
            $lng = $input['long'];
            $radius = $input['miles_end'];


            $haversine = "(3959 * acos(cos(radians(" . $lat . "))
                    * cos(radians(`latitude`))
                    * cos(radians(`longitude`)
                    - radians(" . $lng . "))
                    + sin(radians(" . $lat . "))
                    * sin(radians(`latitude`))))";


            $records= $records->whereRaw($haversine.'<='.$radius)->select(DB::raw("*, $haversine AS distance ORDER BY distance asc"));
            //$records = $records->where('user_posts.is_boosted',1)->orWhere('user_posts.is_boosted',0);
            $records = $records->where('user_posts.user_id', '!=',  $data['user_id']);

            $records = $records->whereNOTIn('user_posts.id',function($query){
            $query->select('post_id')->from('user_sold_posts');
            });

            //$records = $records->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_favourite_posts` WHERE `user_id` = '.$input['user_id'].'))');
            $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('user_favourite_posts')->where('user_favourite_posts.user_id', $data['user_id']);
            });

            $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('user_notinterest_posts')->where('user_notinterest_posts.user_id', $data['user_id']);
            });
            $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
                $query->select('post_id')->from('reported_posts')->where('reported_posts.user_id', $data['user_id']);
            });
            $this->builder = $records->select('user_posts.id')->orderBy('user_posts.is_boosted','desc');
            $filteredlocation = parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);
//            \Log::debug($filteredlocation);
            return $filteredlocation;
            // $records = $records->orWhere('user_posts.is_boosted',1);
            // $records= $records->whereRaw($haversine.'<='.$radius)->select(DB::raw("*, $haversine AS distance"));
            }

                $filterrecords = $this->model;
//            $filterrecords = $filterrecords->where('user_posts.is_boosted',0)->orWhere('user_posts.is_boosted',1);
                if(isset($input['is_lease'])&& $input['is_lease']==1){
                 $filterrecords = $filterrecords->where('user_posts.is_lease',1);
            }
//                if(isset($input['breeds']) && $input['breeds'] != "") {
//                $bes = array();
//                $b_es = array();
//                foreach ($input['breeds'] as $b) {
//                    $b_es[] = Helper::hashid_decode($b);
//                }
//                $bes = @implode(",", $b_es);
//                // $records = $records->whereRaw('(user_posts.`breed_id` IN (' . $bes . '))');
//              $filterrecords = $filterrecords->whereRaw('(user_posts.`breed_id` IN (' . $bes . '))');
////                $records = $records->orWhere('user_posts.is_boosted',1);
//            }

            $discipline_count = 0;
            if (isset($input['disciplines']) && $input['disciplines'] != "") {
                foreach ($input['disciplines'] as $d){
                    $discipline_count = $discipline_count+1;
                }
            }
            \Log::debug("discipline count");
            \Log::debug($discipline_count);
            if($discipline_count<=163){
                if (isset($input['disciplines']) && $input['disciplines'] != "") {
                    $displine = array();

                    foreach ($input['disciplines'] as $d) {
                        $filterrecords = $filterrecords->where('discipline_id','LIKE','%'. $d.'%');
                    }


//                $filterrecords=$filterrecords->whereHas('disciplines',function($query) use ($displine){
//                    $query->whereIn('discipline_id',$displine);
//                });
                }
            }



            $breedcount = 0;
            if (isset($input['breeds']) && $input['breeds'] != "") {
                $arrayval = array();
                foreach ($input['breeds'] as $d){
                    $breedcount = $breedcount+1;
                }
            }
            \Log::debug($breedcount);
            if($breedcount<=204) {
                if (isset($input['breeds']) && $input['breeds'] != "") {
                    $breed = array();
                    foreach ($input['breeds'] as $d) {
//                    $breed[]=Helper::hashid_decode($d);
                        $breed[] = ($d);

                        $filterrecords = $filterrecords->where('breed_id','LIKE','%'. $d.'%');
                    }
//                $filterrecords=$filterrecords->whereHas('breeds',function($query) use ($breed){
//                    $query->whereIn('breed_id',$breed);
//                });
//                $filterrecords = $filterrecords->where('breed_id','like', '%' . $input['breeds'] . '%');
                }
            }

            if (isset($input['genders']) && $input['genders'] != "") {
                $ges = array();
                $g_es = array();
                foreach ($input['genders'] as $g) {
                    $g_es[] = Helper::hashid_decode($g);
                }
                $ges = @implode(",", $g_es);
                // $records = $records->whereRaw('(user_posts.`gender_id` IN (' . $ges . '))');
                $filterrecords = $filterrecords->whereRaw('(user_posts.`gender_id` IN (' . $ges . '))');
//                $records = $records->orWhere('user_posts.is_boosted',1);
            }

            if (isset($input['colors']) && $input['colors'] != "") {
                $ces = array();
                $c_es = array();
                foreach ($input['colors'] as $c) {
                    $c_es[] = Helper::hashid_decode($c);
                }
                $ces = @implode(",", $c_es);
                // $records = $records->whereRaw('(user_posts.`color_id` IN (' . $ces . '))');
                $filterrecords = $filterrecords->whereRaw('(user_posts.`color_id` IN (' . $ces . '))');
//                $records = $records->orWhere('user_posts.is_boosted',1);
            }

            if (isset($input['size_start']) && $input['size_start'] != "" && isset($input['size_end']) && $input['size_end'] != "") {
               //$records = $records->whereBetween('size', [$input['size_start'], $input['size_end']]);
                $filterrecords = $filterrecords->whereRaw('user_posts.size >= '.$input['size_start'].'')
                                    ->whereRaw('user_posts.size <= '.$input['size_end'].'');
//                $records = $records->where([
//                    ['user_posts.size', '>=', $input['size_start']],
//                    ['user_posts.size', '<=', $input['size_end']]]);
            }

            if (isset($input['temp_start']) && $input['temp_start'] != "" && isset($input['temp_end']) && $input['temp_end'] != "") {
                //$records = $records->whereBetween('temperament', [$input['temp_start'], $input['temp_end']]);
                $filterrecords = $filterrecords->whereRaw('user_posts.temperament >= '.$input['temp_start'].'')
                                    ->whereRaw('user_posts.temperament <= '.$input['temp_end'].'');
//                $records = $records->where([
//                    ['user_posts.temperament', '<=', $input['temp_start']],
//                    ['user_posts.temperament', '>=', $input['temp_end']]]);
            }


            if (isset($input['price_start']) && $input['price_start'] != "" && isset($input['price_end']) && $input['price_end'] != "") {
                //$records = $records->whereBetween('price', [$input['price_start'], $input['price_end']]);
                $filterrecords = $filterrecords->whereRaw('user_posts.price >= '.$input['price_start'].'')
                                    ->whereRaw('user_posts.price <= '.$input['price_end'].'');
//                $records = $records->where([
//                    ['user_posts.price', '<=', $input['price_start']],
//                    ['user_posts.price', '>=', $input['price_end']]]);
            }


        }

        //Handle Preference Crash Now
        else{
            $records = $records->where('user_posts.user_id', '!=',  $data['user_id']);
            $records = $records->whereNOTIn('user_posts.id',function($query){
                $query->select('post_id')->from('user_sold_posts');
            });

            //$records = $records->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_favourite_posts` WHERE `user_id` = '.$input['user_id'].'))');
            $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
                $query->select('post_id')->from('user_favourite_posts')->where('user_favourite_posts.user_id', $data['user_id']);
            });

            $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
                $query->select('post_id')->from('user_notinterest_posts')->where('user_notinterest_posts.user_id', $data['user_id']);
            });

            $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
                $query->select('post_id')->from('reported_posts')->where('reported_posts.user_id', $data['user_id']);
            });


            $this->builder = $records->select('user_posts.id')->orderBy('user_posts.is_boosted','desc');
            return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);
        }




        //preference validation ends here
        $filterrecords = $filterrecords->where('user_posts.user_id', '!=',  $data['user_id']);
        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query){
            $query->select('post_id')->from('user_sold_posts');
        });

        //$records = $records->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_favourite_posts` WHERE `user_id` = '.$input['user_id'].'))');
        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('user_favourite_posts')->where('user_favourite_posts.user_id', $data['user_id']);
        });

        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('user_notinterest_posts')->where('user_notinterest_posts.user_id', $data['user_id']);
        });
        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('reported_posts')->where('reported_posts.user_id', $data['user_id']);
        });

        $this->builder = $filterrecords->select('user_posts.id')->orderBy('user_posts.is_boosted','desc');
        $filtered = parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);
        if(isset($filteredlocation)){
            $filteredsearches = ($filteredlocation + $filtered);
            return $filteredsearches;
        }
        else{
            return $filtered;
        }
        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query){
            $query->select('post_id')->from('user_sold_posts');
        });

        //$records = $records->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_favourite_posts` WHERE `user_id` = '.$input['user_id'].'))');
        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('user_favourite_posts')->where('user_favourite_posts.user_id', $data['user_id']);
        });

        $filterrecords = $filterrecords->whereNOTIn('user_posts.id',function($query) use ($data){
            $query->select('post_id')->from('user_notinterest_posts')->where('user_notinterest_posts.user_id', $data['user_id']);
        });


//        $records=$records->whereHas('notinterested',function($q) use ($data) {
//            $q->where('user_id','!=',$data['user_id']);
//        });

//        $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
//            $query->select('post_id')->from('user_favourite_posts')->where('user_notinterest_posts.user_id', $data['user_id']);
//        });



        $this->builder = $records->select('user_posts.id')->orderBy('user_posts.is_boosted', 'desc');

        // $this->builder = $records->select('user_posts.id')->orderBy('user_posts.is_boosted', 'desc')->orderBy('distance','asc');
        //dd($this->builder->toSql(), $this->builder->getBindings());

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);


    }

    public function searchByHorseName($pagination = false, $perPage = 10, array $data = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;
        // $records = DB::table('user_sold_posts')
        //     ->pluck('post_id')
        //     ->toArray();
        // return $records;
        // return response()->json(['success'=>true,'data'=>$records]);    

        // //dd($data);

    
        //     $records = $records->join('user_sold_posts', 'user_sold_posts.post_id','=','user_posts.id');
        //     $records= $records->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_sold_posts`))');
           if (isset($data['user_id']) && $data['user_id'] != "") {
                $records = $records->where('user_posts.user_id', '!=',  $data['user_id']);
            }
            $records = $records->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_sold_posts`))');
        // $records = $records->whereNOTIn('user_posts.id',function($query){
        //     $query->select('post_id')->from('user_sold_posts');
        // });

//        $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
//            $query->select('post_id')->from('user_favourite_posts')->where('user_favourite_posts.user_id', $data['user_id']);
//        });

        // $records = $records->whereNOTIn('user_posts.id',function($query) use ($data){
        //     $query->select('post_id')->from('user_notinterest_posts')->where('user_notinterest_posts.user_id', $data['user_id']);
        // });


        // $records = $records->whereNOTIn('user_posts.id',function($query){
        //   $query->select('post_id')->from('user_sold_posts');
        // });
        $records=$records->where('title', 'like', '%' . $data['keyword'] . '%');

        $this->builder = $records->select('user_posts.id')->orderBy('user_posts.id', 'desc');

        //dd($this->builder->toSql(), $this->builder->getBindings());

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);


    }

    //my_all
    public function my_all($input) {


        $input['per_page'] = isset($input['per_page']) ? $input['per_page'] : 20;

        $requests = UserPost::where('user_posts.user_id', $input['user_id']);

        if( isset($input['is_sold']) && $input['is_sold'] == "yes"){
            $requests->join('user_sold_posts', 'user_sold_posts.post_id','=','user_posts.id');
        }

        if( isset($input['is_sold']) && $input['is_sold'] == "no"){
            $requests->whereRaw('(user_posts.`id` NOT IN (SELECT post_id FROM `user_sold_posts`))');
                    $requests = $requests->select('user_posts.id')->orderBy('user_posts.id', 'desc')->paginate($input['per_page']);
        }

        else{
                    $requests = $requests->select('user_posts.id')->orderBy('user_sold_posts.id', 'desc')->paginate($input['per_page']);
        }
        // $requests = $requests->select('user_posts.id')->orderBy('user_posts.id', 'desc')->paginate($input['per_page']);;
        



        //dd($requests->toSql(), $requests->getBindings());

        $count = count($requests);

        $repo = App::make('UserPostRepository');

        //validation
        if ($count > 0) {

            foreach ($requests as $req) {
                $detail = $repo->findById($req->id, true, true, true, true);
                $array[] = $detail;
            }//loop ends here


        } else {
            $array = array();
        }

        $data['data']=$array;
        $data['pagination'] = Helper::pagination($requests);

        return $data;

    }//my_all method ends here

}//UserPostRepository class ends here
