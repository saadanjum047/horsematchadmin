<?php

namespace App\Data\Repositories;

use JWTAuth;
use App\Data\Contracts\RepositoryContract;
use Hash;
use Storage, Image;
use \App;
use Validator;

//models
use App\Data\Models\User;
use App\Data\Models\Setting;
//filter models
use App\Data\Models\Temperament;
use App\Data\Models\Price;
use App\Data\Models\State;
use App\Data\Models\Size;
use App\Data\Models\Breed;
use App\Data\Models\Discipline;
use App\Data\Models\Color;
use App\Data\Models\Gender;

use App\Helpers\Helper;


class SettingRepository extends AbstractRepository  implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * category data will be stored
     * category Auto incremented Id will be append to it
     *
     * Example: category-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'setting-';
    protected $_cacheTotalKey = 'total-settings';

    public function __construct(Setting $setting) {
        $this->builder = $setting;
        $this->model = $setting;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

        $tokenUser = array();
        $tokenuserid = 0;

        $data = parent::findById($id, $refresh, $details, $encode);


        unset( $data->created_at, $data->updated_at, $data->deleted_at);

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, array $input = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;

        if( isset($input['type']) && $input['type'] != ""){
            $records = $records->where('type', $input['type']);
        }
        if( isset($input['keyword']) && $input['keyword'] != ""){
            $records = $records->whereRaw('(title LIKE "%'.$input['keyword'].'%")');
        }

        $this->builder = $records->orderBy('id', 'desc');

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);


    }

    /* ---------- Filters ----------------- */

    //app_filters
    public function app_filters(){

        $info = array();

        //temp handling
        $temperaments = Temperament::
                        orderBy('id', 'asc')
                        ->get()
                        ->map(function ($temperaments){
                            return [
                                "id" => Helper::hashid_encode($temperaments->id),
                                "title" => $temperaments->title
                            ];
                        });
        $info['temperaments'] = $temperaments;

        //price handling
        $prices = Price::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($prices){
                return [
                    "id" => Helper::hashid_encode($prices->id),
                    "title" => $prices->title
                ];
            });
        $info['prices'] = $prices;

        //size handling
        $sizes = Size::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($sizes){
                return [
                    "id" => Helper::hashid_encode($sizes->id),
                    "title" => $sizes->title
                ];
            });
        $info['sizes'] = $sizes;

        //states handling
        $states = State::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($states){
                return [
                    "id" => Helper::hashid_encode($states->id),
                    "symbol" => Helper::handle_string($states->symbol),
                    "title" => Helper::handle_string($states->title)
                ];
            });
        $info['states'] = $states;

        //breeds handling
        $breeds = Breed::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($breeds){
                return [
                    "id" => Helper::hashid_encode($breeds->id),
                    "title" => Helper::handle_string($breeds->title)
                ];
            });
        $info['breeds'] = $breeds;

        //disciplines handling
        $disciplines = Discipline::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($disciplines){
                return [
                    "id" => Helper::hashid_encode($disciplines->id),
                    "title" => Helper::handle_string($disciplines->title)
                ];
            });
        $info['disciplines'] = $disciplines;

        //colors handling
        $colors = Color::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($colors){
                return [
                    "id" => Helper::hashid_encode($colors->id),
                    "title" => Helper::handle_string($colors->title)
                ];
            });
        $info['colors'] = $colors;

        //genders handling
        $genders = Gender::
        orderBy('id', 'asc')
            ->get()
            ->map(function ($genders){
                return [
                    "id" => Helper::hashid_encode($genders->id),
                    "title" => Helper::handle_string($genders->title)
                ];
            });
        $info['genders'] = $genders;

        return $info;

    }//app_filters method ends here

}