<?php

namespace App\Data\Repositories;

use JWTAuth;
use App\Data\Contracts\RepositoryContract;
use Illuminate\Support\Facades\Cache;
use Hash;
use Storage, Image;
use \App;
use Validator;

use App\Data\Models\User;
use App\Data\Models\UserPost;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserSoldPost;


//Helpers
use App\Helpers\Helper;

//UserSoldPostRepository
class UserSoldPostRepository extends AbstractRepository  implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * category data will be stored
     * category Auto incremented Id will be append to it
     *
     * Example: category-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'usersoldpost-';
    protected $_cacheTotalKey = 'total-usersoldposts';

    public function __construct(UserSoldPost $userSoldPost) {
        $this->builder = $userSoldPost;
        $this->model = $userSoldPost;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

        $data = parent::findById($id, $refresh, $details, $encode);
        if ($data) {

            //dd($data);

            if ($encode) {
                $data->user_id = Helper::hashid_encode($data->user_id);
                $data->post_id = Helper::hashid_encode($data->post_id);
                $data->buyer_id = Helper::hashid_encode($data->buyer_id);
            }

            $postrepo = App::make('UserPostRepository');
            $data->post = $postrepo->findById(Helper::hashid_decode($data->post_id));

            $userrepo = App::make('UserRepository');
            $data->buyer = $userrepo->findById(Helper::hashid_decode($data->buyer_id));

            if ($details) {
            }

        }

        unset($data->updated_at, $data->deleted_at);

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, array $input = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;

        if (isset($input['admin_loggedin']) == true ) {

        } else {

            if (isset($input['user_id']) && $input['user_id'] != "") {
                $records = $records->where('user_id',  $input['user_id']);
            }

        }

        $this->builder = $records->orderBy('id', 'desc');

        // dd($this->builder->toSql(), $this->builder->getBindings()    );

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);

    }

    //manage
    public function manage($data) {

        $info = array();
        $action = "";
        $dateTime = @date("Y-m-d H:i:s");
        $currentdate = @date("Y-m-d");
        $postdetail = UserPost::where('id', $data['post_id'])->first();
        $count = UserSoldPost::where('user_id', $data['user_id'])->where('post_id', $data['post_id'])->get()->first();
        $cache_clear = "";

        if ($data['action'] == "add") {
            if (isset($count['buyer_id']) && $count['buyer_id']===0) {
                $sold = array();
                //insert
                $sold['user_id'] = $data['user_id'];
                $sold['post_id'] = $data['post_id'];
                $sold['buyer_id'] = $data['buyer_id'];
                $sold['updated_at'] = $dateTime;
                $action = UserSoldPost::where(['id'=>$count['id']])->update($sold);


                $info['status'] = 0;
                $info['data'] = (object) array();
                $info['message'] = "You have already sold $postdetail->title";
            } else if($count != null){


                $info['status'] = 0;
                $info['data'] = (object) array();
                $info['message'] = "You have already sold $postdetail->title";

            } else {
                $info['status'] = 1;
                $sold = array();
                //insert
                $sold['user_id'] = $data['user_id'];
                $sold['post_id'] = $data['post_id'];
                $sold['buyer_id'] = isset($data['buyer_id'])?$data['buyer_id']:0;
                $sold['created_date'] = $currentdate;
                $sold['created_at'] = $dateTime;
                $action = UserSoldPost::insertGetId($sold);
                $cache_clear =  Cache::flush();
                $detail = $this->findById($action);
                $info['data'] = $detail;
                //insert ends here
                $info['message'] = "You have sold $postdetail->title";
            }
        } else  if ($data['action'] == "remove") {
            if ($count != NULL) {
                $action = UserSoldPost::where('id', $count->id)->forceDelete();
                $cache_clear =  Cache::flush();
                $info['status'] = 1;
                $info['data'] = (object) array();
                $info['message'] = "$postdetail->title is removed from sold";
            } else {
                $info['status'] = 0;
                $info['data'] = (object) array();
                $info['message'] = "$postdetail->title is not sold yet";
            }
        }

        return $info;

    }//manage method ends here

    //all
    public function all($input) {

        $requests = UserSoldPost::where('user_id', $input['user_id']);
        $input['per_page'] = isset($input['per_page']) ? $input['per_page'] : 20;
        $requests = $requests->orderBy('id', 'desc')->paginate($input['per_page']);

        $count = count($requests);

        $repo = App::make('UserPostRepository');
        $userrepo = App::make('UserRepository');

        //validation
        if ($count > 0) {

            foreach ($requests as $req) {
                $user_detail =  $userrepo->findById($req->buyer_id);
                $detail = $repo->findById($req->post_id, true, true, true, true, true);
                $detail->buyer_id = $user_detail->id;
                $detail->buyer_name = $user_detail->username;
                $array[] = $detail;
            }//loop ends here

        } else {
            $array = array();
        }

        $data['data']=$array;
        $data['pagination'] = Helper::pagination($requests);

        return $data;

    }//all method ends here

    public function bought($input) {

        $requests = UserSoldPost::where('buyer_id', $input['user_id']);
        $input['per_page'] = isset($input['per_page']) ? $input['per_page'] : 20;
        $requests = $requests->orderBy('id', 'desc')->paginate($input['per_page']);

        $count = count($requests);

        $repo = App::make('UserPostRepository');
        $userrepo = App::make('UserRepository');

        //validation
        if ($count > 0) {

            foreach ($requests as $req) {
                $user_detail =  $userrepo->findById($req->buyer_id);
                $detail = $repo->findById($req->post_id, true, true, true, true, true);
                $detail->buyer_id = $user_detail->id;
                $detail->buyer_name = $user_detail->username;
                $array[] = $detail;
            }//loop ends here

        } else {
            $array = array();
        }

        $data['data']=$array;
        $data['pagination'] = Helper::pagination($requests);

        return $data;

    }//all method ends here

}//UserSoldPostRepository class ends here
