<?php

namespace App\Data\Repositories;

use JWTAuth;
use App\Data\Contracts\RepositoryContract;
use Illuminate\Support\Facades\Cache;
use Hash;
use Storage, Image;
use \App;
use Validator;

use App\Data\Models\User;
use App\Data\Models\Temperament;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserSoldPost;

//Helpers
use App\Helpers\Helper;

//TemperamentRepository
class TemperamentRepository extends AbstractRepository  implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * category data will be stored
     * category Auto incremented Id will be append to it
     *
     * Example: category-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'temperament-';
    protected $_cacheTotalKey = 'total-temperaments';

    public function __construct(Temperament $temperament) {
        $this->builder = $temperament;
        $this->model = $temperament;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

        $data = parent::findById($id, $refresh, $details, $encode);
        if ($data) {

            if ($encode) {
            }


            if ($details) {
            }

        }

        unset($data->updated_at, $data->deleted_at);

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, array $input = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;

        if (isset($input['admin_loggedin']) == true ) {

        } else {

            if (isset($input['user_id']) && $input['user_id'] != "") {
                $records = $records->where('user_id',  $input['user_id']);
            }

        }


        $this->builder = $records->orderBy('id', 'desc');

        // dd($this->builder->toSql(), $this->builder->getBindings()    );

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);


    }




}//TemperamentRepository class ends here