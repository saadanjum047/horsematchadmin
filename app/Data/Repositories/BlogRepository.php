<?php

namespace App\Data\Repositories;

use JWTAuth;
use App\Data\Contracts\RepositoryContract;
use Illuminate\Support\Facades\Cache;
use Hash;
use Storage, Image;
use \App;
use Validator;

use App\Data\Models\User;
use App\Data\Models\Blog;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserSoldPost;

//Helpers
use App\Helpers\Helper;

//BlogRepository
class BlogRepository extends AbstractRepository  implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * category data will be stored
     * category Auto incremented Id will be append to it
     *
     * Example: category-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'blog-';
    protected $_cacheTotalKey = 'total-blogs';

    public function __construct(Blog $blog) {
        $this->builder = $blog;
        $this->model = $blog;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

        $data = parent::findById($id, $refresh, $details, $encode);
        if ($data) {



            if ($encode) {
//
            }

            $short_description = Helper::shortDescription(($data->description));
            $data->short_description = $short_description;


            if ($details) {
            }


            if(isset($data->image) && $data->image !=NULL){
                list($file, $extension) = @explode('.', $data->image);
                $file = config('app.files.blogs.folder_name') . "/" . $file;

                $data->image_urls['1x'] = config('app.url') . '/storage/app/' . $file . '.' . $extension;
                $data->image_urls['2x'] = config('app.url') . '/storage/app/' . $file . '@2x.' . $extension;
                $data->image_urls['3x'] = config('app.url') . '/storage/app/' . $file . '@3x.' . $extension;

            } else {
                $data->image_urls['1x'] = url('/public/images/logo.png');
                $data->image_urls['2x'] = url('/public/images/logo.png');
                $data->image_urls['3x'] = url('/public/images/logo.png');
            }

        }

        unset($data->updated_at, $data->deleted_at);

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, array $input = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;

        if (isset($input['admin_loggedin']) == true ) {

        } else {

//            if (isset($input['user_id']) && $input['user_id'] != "") {
//                $records = $records->where('user_id',  $input['user_id']);
//            }

        }

        $this->builder = $records->orderBy('id', 'desc');

        // dd($this->builder->toSql(), $this->builder->getBindings()    );

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);

    }

}//BlogRepository class ends here
