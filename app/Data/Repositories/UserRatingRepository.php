<?php

namespace App\Data\Repositories;

use http\Env\Request;
use JWTAuth;
use App\Data\Contracts\RepositoryContract;
use Illuminate\Support\Facades\Cache;
use Hash;
use Storage, Image;
use \App;
use Validator;

use App\Data\Models\User;
use App\Data\Models\UserRating;
use App\Data\Models\UserPostImage;
use App\Data\Models\UserPostLink;
use App\Data\Models\UserFavouritePost;

//Helpers
use App\Helpers\Helper;

//UserRatingRepository
class UserRatingRepository extends AbstractRepository  implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the
     * category data will be stored
     * category Auto incremented Id will be append to it
     *
     * Example: category-1
     *
     * @var string
     * @access protected
     *
     **/

    protected $_cacheKey = 'userrating-';
    protected $_cacheTotalKey = 'total-userratings';

    public $ratingRelationKeys = [];


    public function __construct(UserRating $UserRating) {
        $this->builder = $UserRating;
        $this->model = $UserRating;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

//        if(count($this->ratingRelationKeys) > 0){
//            parent::setDataKeys( $this->ratingRelationKeys );
//        } else {
//            parent::setDataKeys(['buyer']);
//        }

        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {

            //dd($data);

//            if ($encode) {
//                $data->user_id = Helper::hashid_encode($data->user_id);
//                $data->buyer_id = Helper::hashid_encode($data->buyer_id);
//            }

            $repo = App::make('UserRepository');
            $data->buyer = $repo->findById($data->buyer_id);

            if ($details) {
            }


        }

        unset($data->updated_at, $data->deleted_at);

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, array $input = [],$detail = false,$encode = true ,$postView = true, $refresh = false){

        $records = $this->model;


        if (isset($input['admin_loggedin']) == true ) {

        } else {

            if (isset($input['user_id']) && $input['user_id'] != "") {
                $records = $records->where('user_id',  $input['user_id']);
            }

        }

        if( isset($input['buyer_id']) && $input['buyer_id'] != '' ){
           $records = $records->where( 'buyer_id', $input['buyer_id'] );
        }



            $this->builder = $records->orderBy('id', 'desc');

        // dd($this->builder->toSql(), $this->builder->getBindings()    );

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);


    }

    public function create(array $data=[])
    {
        $locationModel = $this->model;
        $record = parent::create($data);

//        $data = UserRating::where('buyer_id', $data['buyer_id'])->with(['buyer'=>function($q){
//            $q->select(['id','username','image']);
//        }])->get();
//
//        $data=$data->map(function($user){
//
//            if(isset($user->buyer->image) && $user->buyer->image !=NULL){
//
//                list($file, $extension) = @explode('.', $user->image);
//                $image_urls['1x'] = url('/uploads/userImages/'.$file.'.'.$extension);
//                $image_urls['2x'] = url('/uploads/userImages/'.$file.'@2x.'.$extension);
//                $image_urls['3x'] = url('/uploads/userImages/'.$file.'@3x.'.$extension);
//            } else {
//
//                $image_urls['1x'] = url('/public/images/logo.png');
//                $image_urls['2x'] = url('/public/images/logo.png');
//                $image_urls['3x'] = url('/public/images/logo.png');
//            }
//
//            $user['buyer']['image_urls']=$image_urls;
//            return $user;
//        })->toArray();


//        $info['status'] = 1;
//        $info['data'] = $record;
//        //insert ends here
//        $info['message'] = "You have rated";

    return $record;
        //
    }
    //manage
    public function manage($data) {


       // dd($data);

        $repo = App::make('UserRepository');

        $info = array();
        $action = "";
        $dateTime = @date("Y-m-d H:i:s");
        $currentdate = @date("Y-m-d");
        $detail = User::where('id', $data['buyer_id'])->first();
        $count = UserRating::where('user_id', $data['user_id'])->where('buyer_id', $data['buyer_id'])->first();
        $cache_clear = "";

        $info['status'] = 1;
        $user_rating = array();
        //insert
        $user_rating['user_id'] = $data['user_id'];
        $user_rating['buyer_id'] = $data['buyer_id'];
        $user_rating['rating'] = $data['rating'];
        $user_rating['created_date'] = $currentdate;
        $user_rating['created_at'] = $dateTime;
        $action = UserRating::insertGetId($user_rating);
        $cache_clear =  Cache::flush();
        $userdetail = $repo->findById($data['buyer_id']);
        $info['data'] = $userdetail;
        //insert ends here
        $info['message'] = "You have rated $detail->username";

        return $info;

    }//manage method ends here

    //all
    public function all($input) {

        $data = UserRating::where('buyer_id', $input['user_id'])->with(['buyer'=>function($q){
            $q->select(['id','username','image']);
        }])->orderBy('id', 'desc')->get();

        $data=$data->map(function($user){

            if(isset($user->buyer->image) && $user->buyer->image !=NULL){

                list($file, $extension) = @explode('.', $user->buyer->image);
                $image_urls['1x'] = config('app.url') . '/storage/app/users/' . $file . '.' . $extension;
                $image_urls['2x'] = config('app.url') . '/storage/app/users/' .$file.'@2x.'.$extension;
                $image_urls['3x'] = config('app.url') . '/storage/app/users/' .$file.'@3x.'.$extension;
            } else {

                $image_urls['1x'] = url('/public/images/logo.png');
                $image_urls['2x'] = url('/public/images/logo.png');
                $image_urls['3x'] = url('/public/images/logo.png');
            }

            $user['buyer']['image_urls']=$image_urls;
            return $user;
        })->toArray();

        $info['status'] = 1;
        $info['data'] = $data;
        //insert ends here
        $info['message'] = "You have rated";

        return $info;


    }//all method ends here

    //other_rating
    public function other_rating($input) {
        $data = UserRating::where('buyer_id', $input['other_id'])->with(['buyer'=>function($q){
            $q->select(['id','username','image']);
        }])->orderBy('id','DESC')->get();
        $data=$data->map(function($user){

            if(isset($user->buyer->image) && $user->buyer->image !=NULL){

                list($file, $extension) = @explode('.', $user->buyer->image);
                $image_urls['1x'] = config('app.url') . '/storage/app/users/' . $file . '.' . $extension;
                $image_urls['2x'] = config('app.url') . '/storage/app/users/' . $file . '@2x.' . $extension;
                $image_urls['3x'] = config('app.url') . '/storage/app/users/' . $file . '@3x.' . $extension;

               // $data->image_urls['1x'] = config('app.url') . '/storage/app/' . $file . '.' . $extension;
              //  $data->image_urls['2x'] = config('app.url') . '/storage/app/' . $file . '@2x.' . $extension;
              //  $data->image_urls['3x'] = config('app.url') . '/storage/app/' . $file . '@3x.' . $extension;
            } else {

                $image_urls['1x'] = url('/public/images/logo.png');
                $image_urls['2x'] = url('/public/images/logo.png');
                $image_urls['3x'] = url('/public/images/logo.png');
            }


            $user['buyer']['image_urls']=$image_urls;
            return $user;
        });

        $info['status'] = 1;
        $info['data'] = $data;
        //insert ends here
        $info['message'] = "You have rated";

        return $info;

    }//other_rating method ends here

    public function ratingRelations( $keys = array() )
    {
        $this->ratingRelationKeys = $keys;
    }



}//UserRatingRepository class ends here
