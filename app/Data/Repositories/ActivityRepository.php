<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\Event;
use App\Data\Contracts\RepositoryContract;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;
use Hashids\Hashids;
use JWTAuth;
use Storage, Image;
use \App,DB,StdClass;
use Laravel\Cashier\Cashier;
use Braintree_Transaction;
use checkmobi\CheckMobiRest;
use Illuminate\Support\Str;

use App\Data\Models\Activity;
use App\Data\Models\User;
use App\Data\Models\UserPost;
use App\Helpers\Helper;
use App\Data\Models\UserPostImage;

class ActivityRepository extends AbstractRepository implements RepositoryContract{

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    public $activityRepo;
    public $hashids = "";

    /**
     *
     * This is the prefix of the cache key to which the
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'activity-';
    protected $_cacheTotalKey = 'total-activities';


    public function __construct(Activity $activity) {
        $this->builder = $activity;
        $this->model = $activity;
        $this->hashids = new Hashids('this is my salt');
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {

        $hashids = new Hashids();

        $data = parent::findById($id, $refresh, $details, $encode);


        if ($data->action=="rating") {
          $user= User::where(['id'=>$data->actor_id])->get(['image'])->first();
            if(isset($user['image']) && $user['image']){

                list($file, $extension) = @explode('.', $user['image']);
                $image = config('app.url') . '/storage/app/users/' . $file . '.' . $extension;
                  } else {
                $image = url('/public/images/logo.png');
                }

          $data->image=$image;
        }

        if ($data->action=="post") {

            $post = UserPostImage::where(['post_id'=>$data->action_id])->get(['id','image'])->first();


            if(isset($post['image']) && $post['image']){
                list($file, $extension) = @explode('.', $post['image']);
                $image = config('app.url') . '/storage/app/postimages/' . $file . '.' . $extension;
            } else {
                $image = $post['image']; //url('/public/images/logo.png');
            }
            $data->image=$image;
        }

        $data->action_id=  Helper::hashid_encode($data->action_id);
        //dd($data);

        return $data;
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true,$refresh=true){

        $records = $this->model;
         $records=$records->where('user_id','=',$data['user_id']);

        $this->builder = $records->orderBy('id', 'DESC');

        // dd($this->builder->toSql(), $this->builder->getBindings()    );

        return parent::findByAll($pagination, $perPage,[],$detail,$encode ,$postView, $refresh = false);

    }

    //admin_activities
    public function admin_activities($input){

        $info = array();

        $query = Activity::where('user_id', $input['user_id'])->where('is_admin', '1')->where('viewed', '0');
        $query = $query->orderBy('id', 'desc')->get();

        if (count($query) > 0) {
            foreach($query as $q) {
                $detail = $this->findById($q->id);
                $info[] =$detail;
            }
        } else {
            $info = array();
        }

        return $info;

    }


}
