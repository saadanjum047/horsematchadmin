<?php

use App\Data\Models\UserPost;
Route::get('/', 'Admin\AdminController@login');
Route::get('/admin', 'Admin\AdminController@login');
Route::post('/dologin', 'Admin\AdminController@doLogin');
Route::post('/changepass','Admin\AdminController@changepasswordadmin');


Route::group( ['middleware' => 'admin'], function () {

    // Dashboard Route
    Route::get('dashboard', ['uses' => 'Admin\AdminController@index']);


    //Content
    Route::get('content',['uses'=>'Admin\UsersController@getcontentNew']);
    Route::get('content-new',['uses'=>'Admin\UsersController@getcontentNew']);
    //Users
    Route::get('users', ['uses' => 'Admin\UsersController@usersone']);
    Route::get('usersAjax', ['uses' => 'Admin\UsersController@usersAjax']);
    Route::post('users', ['uses' => 'Admin\UsersController@index']);
    Route::get('users/details/{id}', ['uses' => 'Admin\UsersController@users_details']);
    Route::post('users/changestatus', ['uses' => 'Admin\UsersController@changeStatus']);

    //Selling Horses
    Route::get('sellinghorses',['uses'=>'Admin\UsersController@sellingHorses']);
    Route::get('sellinghorsesnew',['uses'=>'Admin\UsersController@sellinghorsesnewNew']);
    Route::get('sellinghorsesnew-new',['uses'=>'Admin\UsersController@sellinghorsesnewNew']);

    Route::get('sellingHorsesAjax',['uses'=>'Admin\UsersController@sellinghorsesAjax']);
    Route::get('getHorsesImagesAjax/{id}',['uses'=>'Admin\UsersController@getHorsesImagesAjax']);
    //Sold Horses
    Route::get('soldhorses',['uses'=>'Admin\UsersController@soldHorses']);
    //Reported Ads
    Route::get('reportedads',['uses'=>'Admin\UsersController@reportedadsNew']);
    Route::get('reportedads-new',['uses'=>'Admin\UsersController@reportedadsNew']);



    //Blog Resource
    Route::get('emailcollateral', ['uses' => 'Admin\BlogController@indexNew']);
    Route::get('blog-management', ['uses' => 'Admin\BlogController@indexNew']);
    Route::get('blog/view/{id}', ['uses' => 'Admin\BlogController@view']);
    Route::get('blog/delete/{id}', ['uses' => 'Admin\BlogController@delete']);
    Route::get('blog/add-post/', ['uses' => 'Admin\BlogController@add_post']);
    Route::post('blog/insert-post/', ['uses' => 'Admin\BlogController@insert_post']);
    Route::get('blog/edit-post/{id}',['uses'=>'Admin\BlogController@edit_post']);
    Route::post('blog/update-post', ['uses' => 'Admin\BlogController@update_post']);
    Route::post('blog/changeisfeatured',['uses'=>'Admin\BlogController@changeBlogStatus']);

    //Privacy
    Route::get('privacy-policy', ['uses' => 'Admin\PrivacyController@index']);
    Route::post('privacy-policy/update-policy', ['uses' => 'Admin\PrivacyController@update_policy']);

    //Privacy
    Route::get('terms-conditions', ['uses' => 'Admin\TermsController@index']);
    Route::post('terms-conditions/update-terms', ['uses' => 'Admin\TermsController@update_terms']);

    //About Us
    Route::get('about-us', ['uses' => 'Admin\AboutController@index']);
    Route::post('about-us/update-about', ['uses' => 'Admin\AboutController@update_about']);

    //Customer Support
    Route::get('customer-support', ['uses' => 'Admin\CustomerSupportController@index']);
    Route::get('customer-support/views/{id}', ['uses' => 'Admin\CustomerSupportController@view']);
    Route::get('customer-support/delete/{id}', ['uses' => 'Admin\CustomerSupportController@delete']);
    Route::post('customer-support/send', ['uses' => 'Admin\CustomerSupportController@send']);

    //Package Management
    Route::get('package-management', ['uses' => 'Admin\PackageManagementController@index']);
    Route::get('package-management/delete/{package_id}', ['uses' => 'Admin\PackageManagementController@delete']);
    Route::get('package-management/add-package', ['uses' => 'Admin\PackageManagementController@add_package']);
    Route::get('package-management/view/{id}', ['uses' => 'Admin\PackageManagementController@view_package']);
    Route::post('package-management/update/{id}', ['uses' => 'Admin\PackageManagementController@update_package']);
    Route::post('package-management/store/', ['uses' => 'Admin\PackageManagementController@store_package']);
    Route::post('package-management/check_package/', ['uses' => 'Admin\PackageManagementController@check_package']);

    //Promotions Management
    Route::get('promotions',['uses'=>'Admin\AdminController@promotionsNew']);
    Route::get('promotions-new',['uses'=>'Admin\AdminController@promotionsNew']);
    Route::post('sendpromotions',['uses'=>'Admin\AdminController@sendPromotion']);

    //Packages Management
    Route::get('packages',['uses'=>'Admin\AdminController@getPackagesNew']);
    Route::get('packages-new',['uses'=>'Admin\AdminController@getPackagesNew']);
    Route::get('subscription',['uses'=>'Admin\AdminController@getSubscriptionsNew']);
    Route::get('subscription-new',['uses'=>'Admin\AdminController@getSubscriptionsNew']);

    Route::get('/logout', function(){
        Session::flush();
        return redirect()->back();
    });

    Route::get('/apis', function(){
        echo '{"af":"16.63","al":"11.58","dz":"158.97","ao":"85.81","ag":"1.1","ar":"351.02","am":"8.83","au":"1219.72","at":"366.26","az":"52.17","bs":"7.54","bh":"21.73","bd":"105.4","bb":"3.96","by":"52.89","be":"461.33","bz":"1.43","bj":"6.49","bt":"1.4","bo":"19.18","ba":"16.2","bw":"12.5","br":"2023.53","bn":"11.96","bg":"44.84","bf":"8.67","bi":"1.47","kh":"11.36","cm":"21.88","ca":"1563.66","cv":"1.57","cf":"2.11","td":"7.59","cl":"199.18","cn":"5745.13","co":"283.11","km":"0.56","cd":"12.6","cg":"11.88","cr":"35.02","ci":"22.38","hr":"59.92","cy":"22.75","cz":"195.23","dk":"304.56","dj":"1.14","dm":"0.38","do":"50.87","ec":"61.49","eg":"216.83","sv":"21.8","gq":"14.55","er":"2.25","ee":"19.22","et":"30.94","fj":"3.15","fi":"231.98","fr":"2555.44","ga":"12.56","gm":"1.04","ge":"11.23","de":"3305.9","gh":"18.06","gr":"305.01","gd":"0.65","gt":"40.77","gn":"4.34","gw":"0.83","gy":"2.2","ht":"6.5","hn":"15.34","hk":"226.49","hu":"132.28","is":"12.77","in":"1430.02","id":"695.06","ir":"337.9","iq":"84.14","ie":"204.14","il":"201.25","it":"2036.69","jm":"13.74","jp":"5390.9","jo":"27.13","kz":"129.76","ke":"32.42","ki":"0.15","kr":"986.26","undefined":"5.73","kw":"117.32","kg":"4.44","la":"6.34","lv":"23.39","lb":"39.15","ls":"1.8","lr":"0.98","ly":"77.91","lt":"35.73","lu":"52.43","mk":"9.58","mg":"8.33","mw":"5.04","my":"218.95","mv":"1.43","ml":"9.08","mt":"7.8","mr":"3.49","mu":"9.43","mx":"1004.04","md":"5.36","mn":"5.81","me":"3.88","ma":"91.7","mz":"10.21","mm":"35.65","na":"11.45","np":"15.11","nl":"770.31","nz":"138","ni":"6.38","ne":"5.6","ng":"206.66","no":"413.51","om":"53.78","pk":"174.79","pa":"27.2","pg":"8.81","py":"17.17","pe":"153.55","ph":"189.06","pl":"438.88","pt":"223.7","qa":"126.52","ro":"158.39","ru":"1476.91","rw":"5.69","ws":"0.55","st":"0.19","sa":"434.44","sn":"12.66","rs":"38.92","sc":"0.92","sl":"1.9","sg":"217.38","sk":"86.26","si":"46.44","sb":"0.67","za":"354.41","es":"1374.78","lk":"48.24","kn":"0.56","lc":"1","vc":"0.58","sd":"65.93","sr":"3.3","sz":"3.17","se":"444.59","ch":"522.44","sy":"59.63","tw":"426.98","tj":"5.58","tz":"22.43","th":"312.61","tl":"0.62","tg":"3.07","to":"0.3","tt":"21.2","tn":"43.86","tr":"729.05","tm":0,"ug":"17.12","ua":"136.56","ae":"239.65","gb":"2258.57","us":"14624.18","uy":"40.71","uz":"37.72","vu":"0.72","ve":"285.21","vn":"101.99","ye":"30.02","zm":"15.69","zw":"5.57"}';
    });
    
Route::get('users1', ['uses' => 'Admin\UsersController@usersone']);
// Route::get('emailcollateral', ['uses' => 'Admin\UsersController@emailcollateral']);
Route::get('teammanagement', ['uses' => 'Admin\UsersController@teammanagement']);
Route::post('addpackage',['uses'=>'Admin\AdminController@addNewPackage']);
Route::post('editpackage',['uses'=>'Admin\AdminController@editPackage']);
Route::post('deletepackage',['uses'=>'Admin\AdminController@deletepackage']);

Route::post('deletesubscription',['uses'=>'Admin\AdminController@deletesubscription']);
Route::post('editsubscription',['uses'=>'Admin\AdminController@editsubscription']);
Route::post('addsubscription',['uses'=>'Admin\AdminController@addnewSubscription']);

Route::get('paymenthistory',['uses'=>'Admin\AdminController@getTransactionsNew']);
Route::get('paymenthistory-new',['uses'=>'Admin\AdminController@getTransactionsNew']);
Route::post('deleteuserid',['uses'=>'Admin\AdminController@deleteuseradmin']);

Route::post('deleteblogpost',['uses'=>'Admin\AdminController@deleteblogadmin']);

Route::get('viewhorse/{id}',['uses'=>'Admin\UsersController@sellinghorse']);

Route::get('reportedadview/{id}',['uses'=>'Admin\UsersController@reportedadview']);

Route::post('deleteuserpost',['uses'=>'Admin\UsersController@deleteuserpost']);
Route::post('addblog',['uses'=>'Admin\UsersController@addBlogPost']);
Route::post('editblog',['uses'=>'Admin\UsersController@editBlogPost']);

Route::post('resendpromotion',['uses'=>'Admin\UsersController@resendpromotion']);
Route::post('blockunblock',['uses'=>'Admin\UsersController@blockunblock']);


Route::post('deletepromo',['uses'=>'Admin\UsersController@deletepromo']);

Route::post('blockuser',['uses'=>'Admin\UsersController@blockUserCnf']);
});

Route::post('editcontent',['uses'=>'Admin\UsersController@editContent']);


Route::get('facebook-4/{id}/details',  function($id){

    $horse_first = UserPost::findOrFail($id);

    $horse = DB::table('user_posts as up')
    ->where('up.id','=',$horse_first->id)
    ->leftJoin('users as u','u.id','=','up.user_id')
    ->leftJoin('breeds as b','b.id','=','up.breed_id')
    ->leftJoin('genders as g','g.id','=','up.gender_id')
    ->leftJoin('disciplines as d','d.id','=','up.discipline_id')
    ->leftJoin('colors as cl','cl.id','=','up.color_id')
    ->leftJoin('user_post_links as link','link.post_id','=','up.id')
    ->join('user_post_images as uip','uip.post_id','=','up.id')
    ->select('u.phone as phone','up.address as address','up.is_lease as is_lease','uip.image as image','up.id as id','b.title as breedtitle','up.temperament as temperament','up.title as horsetitle' ,'up.headline as headline' , 'up.created_at as posted_date','up.size as size','up.price as price','g.title as gender','d.title as discipline','u.username as username' , 'link.link as link' , 'u.image as userimage' , 'cl.title as color')
    ->first();

    $horse_images = DB::table('user_post_images as image')->where('image.post_id' , $horse->id)->select('*')->get();


    return view('facebook' , compact('horse' , 'horse_images'));
} )->name('facebook-share') ;