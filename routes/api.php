<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/testing/traits', "RatingController@testingTrait");


Route::get('/hashid/{id}', function($id){
	echo hashid_encode($id);
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/clear-cache',function(){
    Cache::flush();
    echo "Cache is clear";
});

Route::get('/braintree/token', 'BraintreeController@token');

Route::post('/register', ['uses'=>'Api\UserController@register']);
Route::post('/login', ['uses'=>'Api\UserController@login']);
Route::post('/verify/code', ['uses'=>'Api\UserController@verify_code']);
Route::post('/forgotpassword', ['uses'=>'Api\UserController@forgotpassword']);
Route::post('/forgotpasswordnew',['uses'=>'Api\UserController@forgotpasswordnew']);

//content
Route::get('/get/content', ['uses'=>'Api\SettingController@app_content']);
Route::post('/update/content', ['uses'=>'Api\SettingController@update_content']);

//filter
Route::get('/get/filter/listing', ['uses'=>'Api\SettingController@app_filters']);

/*----------------- Admin Services ---------------------- */

Route::get('/admin/users', ['uses'=>'Api\AdminServiceController@admin_users']);

Route::get('/admin/pending/ads', ['uses'=>'Api\AdminServiceController@admin_pending_ads']);
Route::get('/admin/approved/ads', ['uses'=>'Api\AdminServiceController@admin_approved_ads']);
Route::get('/admin/rejected/ads', ['uses'=>'Api\AdminServiceController@admin_rejected_ads']);


Route::get('/admin/breeds', ['uses'=>'Api\AdminServiceController@admin_breeds']);
Route::get('/admin/disciplines', ['uses'=>'Api\AdminServiceController@admin_disciplines']);
Route::get('/admin/temperaments', ['uses'=>'Api\AdminServiceController@admin_temperaments']);
Route::get('/admin/user/horses', ['uses'=>'Api\AdminServiceController@admin_user_horses']);
Route::get('/admin/user/sold', ['uses'=>'Api\AdminServiceController@admin_user_sold']);
Route::get('/admin/user/favourite', ['uses'=>'Api\AdminServiceController@admin_user_favourite']);
Route::get('/admin/recent/posts' , 'Api\AdminServiceController@recent_posts');

//auth middleware
Route::group( ['middleware' => 'access-token'], function () {


    Route::get('/deleteToken', 'Api\DeviceController@deleteToken');
    Route::post('/loginDeviceToken', 'Api\DeviceController@loginDeviceToken');
    Route::get('/getNotification', 'Api\DeviceController@getNotification');
    Route::get('/unReadCount', 'Api\DeviceController@unReadCount');
    Route::get('/readNotifications', 'Api\DeviceController@readNotifications');
    Route::get('/deleteNotification', 'Api\DeviceController@deleteNotification');
    Route::get('/deleteAllNotification', 'Api\DeviceController@deleteAllNotification');
    Route::get('/notificationStatus', 'Api\DeviceController@notificationStatus');
    Route::post('/testNotification', 'Api\DeviceController@testNotification');
    Route::get('/chatNotification', 'Api\UserController@chatNotification');


    Route::get('/profile/view', ['uses'=>'Api\UserController@view']);

    Route::get('/admin/user/view', ['uses'=>'Api\UserController@admin_user_view']);
    Route::post('/admin/user/update', ['uses'=>'Api\UserController@admin_user_update']);
    Route::post('/admin/user/delete', ['uses'=>'Api\UserController@admin_user_delete']);

    Route::post('/profile/update', ['uses'=>'Api\UserController@update']);

    Route::post('/resend/code', ['uses'=>'Api\UserController@resend_code']);

    Route::post('/manage/preference', ['uses'=>'Api\UserController@manage_preference']);
    Route::get('/get/preference', ['uses'=>'Api\UserController@get_preference']);

    Route::get('/user/list', ['uses'=>'Api\UserController@all']);

    //manage profile
    Route::post('/reset-password',['uses' => 'Api\UserController@resetPassword']);

    Route::get('/user/detail', ['uses'=>'Api\UserController@userdetail']);

    Route::post('/organization/profile/update', ['uses'=>'Api\UserController@organization_update']);

    //update setting
    Route::post('/setting/update', ['uses'=>'Api\UserController@setting_update']);

    //contact us
    Route::post('/contact/us', ['uses'=>'Api\UserController@contact_us']);

    /*------------------- Horse/Post Management ----------------------*/

    Route::post('/post/add', ['uses'=>'Api\UserPostController@add']);
    Route::post('/post/edit', ['uses'=>'Api\UserPostController@editPost']);
    Route::get('/post/detail', ['uses'=>'Api\UserPostController@view']);
    Route::get('/my/post/list', ['uses'=>'Api\UserPostController@my_all']);
    Route::get('/post/list', ['uses'=>'Api\UserPostController@all']);
    Route::get('/searchByHorseName', ['uses'=>'Api\UserPostController@searchByHorseName']);
    Route::post('/notInterested', ['uses'=>'Api\UserPostController@notInterested']);
    Route::get('/deletePost', ['uses'=>'Api\UserPostController@deletePost']);


    Route::post('/manage/sold', ['uses'=>'Api\UserSoldPostController@manage']);
    Route::get('/solid/list', ['uses'=>'Api\UserSoldPostController@all']);
    Route::get('/bought/list', ['uses'=>'Api\UserSoldPostController@bought']);


    Route::post('/manage/favourite', ['uses'=>'Api\UserFavouritePostController@manage']);
    Route::get('/favourite/list', ['uses'=>'Api\UserFavouritePostController@all']);


    Route::post('/manage/rating', ['uses'=>'Api\UserRatingController@manage']);
    Route::get('/rating/list', ['uses'=>'Api\UserRatingController@all']);
    Route::get('/other/rating/list', ['uses'=>'Api\UserRatingController@other_rating']);

    Route::post('/admin/post/accept', ['uses'=>'Api\UserPostController@admin_post_accept']);
    Route::post('/admin/post/reject', ['uses'=>'Api\UserPostController@admin_post_reject']);

    /*------------------- Admin Services ----------------------*/

    Route::post('/admin/breed/add', ['uses'=>'Api\BreedController@add']);
    Route::get('/admin/breed/view', ['uses'=>'Api\BreedController@view']);
    Route::post('/admin/breed/update', ['uses'=>'Api\BreedController@update']);
    Route::post('/admin/breed/delete', ['uses'=>'Api\BreedController@delete']);

    Route::post('/admin/discipline/add', ['uses'=>'Api\DisciplineController@add']);
    Route::get('/admin/discipline/view', ['uses'=>'Api\DisciplineController@view']);
    Route::post('/admin/discipline/update', ['uses'=>'Api\DisciplineController@update']);
    Route::post('/admin/discipline/delete', ['uses'=>'Api\DisciplineController@delete']);

    Route::post('/admin/temperament/add', ['uses'=>'Api\TemperamentController@add']);
    Route::get('/admin/temperament/view', ['uses'=>'Api\TemperamentController@view']);
    Route::post('/admin/temperament/update', ['uses'=>'Api\TemperamentController@update']);
    Route::post('/admin/temperament/delete', ['uses'=>'Api\TemperamentController@delete']);

    Route::get('/blog/list', ['uses'=>'Api\BlogController@all']);
    Route::get('/blog/detail', ['uses'=>'Api\BlogController@view']);

    Route::get('/activity/list', ['uses'=>'Api\ActivityController@all']);

    Route::post('/test/push', ['uses'=>'Api\UserController@test_push']);
    Route::post('/charge/payment',['uses'=>'Api\UserController@chargePayment']);
    Route::get('/transaction/history',['uses'=>'Api\UserController@transactions']);

    Route::post('/followuser',['uses'=>'Api\UserController@followUser']);
    Route::post('/reportpost',['uses'=>'Api\UserController@reportpost']);


});//auth middleware ends here


Route::get('/get/packages',['uses'=>'Api\UserController@getPackages']);
Route::get('/get/subscriptions',['uses'=>'Api\UserController@getSubscriptions']);
Route::get('/chargecron',['uses'=>'Api\UserController@boostpostcron']);


Route::get('get/content/{type}',['uses'=>'Api\UserController@getContent']);