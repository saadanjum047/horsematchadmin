/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.47-cll-lve : Database - hmworking
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hmworking` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hmworking`;

/*Table structure for table `activities` */

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `action` enum('confirmation','reviewed','inform','dispatch','processed','issue_certificate','approved','review') COLLATE utf8mb4_unicode_ci DEFAULT 'inform',
  `action_id` bigint(20) unsigned NOT NULL,
  `object` enum('payment','event','feedback','task','payment_request','processed','payment_process','course','certificate') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` bigint(20) unsigned NOT NULL,
  `actor_id` bigint(20) unsigned NOT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci,
  `visibility` int(11) NOT NULL DEFAULT '1',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `batch_view` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activities_action_index` (`action`),
  KEY `activities_object_index` (`object`),
  KEY `activities_actor_id_foreign` (`actor_id`),
  KEY `activities_user_id_foreign` (`user_id`),
  CONSTRAINT `fk_actor_id_act` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id_act` FOREIGN KEY (`actor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `activities` */

/*Table structure for table `blogs` */

DROP TABLE IF EXISTS `blogs`;

CREATE TABLE `blogs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `image` varchar(255) DEFAULT '',
  `description` longtext,
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `blogs` */

insert  into `blogs`(`id`,`title`,`image`,`description`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (1,'test','','<h2>ACCESS NETWORK STATE, INTERNET</h2>\r\n\r\n<p>In order to determine whether the device is connected to the network or not. If not the data will be saved locally until accessibility to the internet is available. Internet is used in order to send the data to the server so the admin can view the inspection reports.</p>\r\n\r\n<h2>READ EXTERNAL STORAGE</h2>\r\n\r\n<p>In case the data which needs to be submitted in the status or any post is saved and has to be submitted in the profile and saved locally.</p>\r\n\r\n<h2>WRITE EXTERNAL STORAGE</h2>\r\n\r\n<p>In case there is a need to status or any post in the external storage. If the local storage is full.</p>\r\n\r\n','2020-06-14','2020-06-14 12:18:18',NULL,NULL),(2,'practi','','<h2>ACCESS NETWORK STATE, INTERNET</h2>\r\n\r\n<p>In order to determine whether the device is connected to the network or not. If not the data will be saved locally until accessibility to the internet is available. Internet is used in order to send the data to the server so the admin can view the inspection reports.</p>\r\n','2020-06-14','2020-06-14 12:23:30',NULL,NULL);

/*Table structure for table `breeds` */

DROP TABLE IF EXISTS `breeds`;

CREATE TABLE `breeds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;

/*Data for the table `breeds` */

insert  into `breeds`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'Any\r\n','2020-05-16 13:42:40',NULL,NULL),(2,'Akhal Teke Horses\r\n','2020-05-16 13:42:46',NULL,NULL),(3,'American Appendix AAHA\r\n','2020-05-16 13:42:54',NULL,NULL),(4,'American Brindle Equine Association\r\n','2020-05-16 13:42:59',NULL,NULL),(5,'American Buckskin ABRA\r\n','2020-05-16 13:43:04',NULL,NULL),(6,'American Cream Drafts\r\n','2020-05-16 13:43:10',NULL,NULL),(7,'American Creme and White Albinos\r\n','2020-05-16 13:43:15',NULL,NULL),(8,'American Dartmoor Ponies\r\n','2020-05-16 13:43:22',NULL,NULL),(9,'American Gaited Endurance Horses\r\n','2020-05-16 13:43:27',NULL,NULL),(10,'American Half Quarter Horses\r\n','2020-05-16 13:43:32',NULL,NULL),(11,'American Indian Horse Registry\r\n','2020-05-16 13:43:36',NULL,NULL),(12,'American Paint Ponies\r\n','2020-05-16 13:43:41',NULL,NULL),(13,'American Polo Horses\r\n','2020-05-16 13:43:45',NULL,NULL),(14,'American Ranch Horse Association\r\n','2020-05-16 13:43:50',NULL,NULL),(15,'American Saddlebreds\r\n','2020-05-16 13:43:55',NULL,NULL),(16,'American Show Ponies\r\n','2020-05-16 13:44:01',NULL,NULL),(17,'American Sport Ponies\r\n','2020-05-16 13:44:07',NULL,NULL),(18,'American Sulphur Horses\r\n','2020-05-16 13:44:11',NULL,NULL),(19,'American Warmblood Registry\r\n','2020-05-16 13:44:15',NULL,NULL),(20,'American Warmblood Society\r\n','2020-05-16 13:44:19',NULL,NULL),(21,'Andalusians\r\n','2020-05-16 13:44:25',NULL,NULL),(22,'Andalusian Crosses\r\n','2020-05-16 13:44:30',NULL,NULL),(23,'APHA Paints\r\n','2020-05-16 13:44:34',NULL,NULL),(24,'APHA Paint Overos\r\n','2020-05-16 13:44:39',NULL,NULL),(25,'APHA Paint Tobianos\r\n','2020-05-16 13:44:46',NULL,NULL),(26,'APHA Paint Toveros\r\n','2020-05-16 13:44:52',NULL,NULL),(27,'Appalachian Singlefoots\r\n','2020-05-16 13:44:56',NULL,NULL),(28,'Appaloosas\r\n','2020-05-16 13:45:03',NULL,NULL),(29,'AQHA Quarter Horses\r\n','2020-05-16 13:45:08',NULL,NULL),(30,'AQHA Quarter Horse Appendix\r\n','2020-05-16 13:45:14',NULL,NULL),(31,'AQHA Quarter Horse Crosses\r\n','2020-05-16 13:45:18',NULL,NULL),(32,'AraAppaloosas\r\n','2020-05-16 13:45:23',NULL,NULL),(33,'Arabian Horses\r\n','2020-05-16 13:45:28',NULL,NULL),(34,'Arabian Crosses\r\n','2020-05-16 13:45:32',NULL,NULL),(35,'Arabian Pintos\r\n','2020-05-16 13:45:37',NULL,NULL),(36,'Arabian Saddlebreds\r\n','2020-05-16 13:45:43',NULL,NULL),(37,'Arabian TB Crosses\r\n','2020-05-16 13:45:48',NULL,NULL),(38,'Anglo Arabians\r\n','2020-05-16 13:45:52',NULL,NULL),(39,'Half Arabian Horses\r\n','2020-05-16 13:45:53',NULL,NULL),(40,'Shagya Arabians\r\n','2020-05-16 14:15:03',NULL,NULL),(41,'Argentine Criollo Horses\r\n','2020-05-16 14:15:07',NULL,NULL),(42,'Australian Stock Horses\r\n','2020-05-16 14:15:12',NULL,NULL),(43,'Australian Warmbloods\r\n','2020-05-16 14:15:16',NULL,NULL),(44,'Aztecas\r\n','2020-05-16 14:15:20',NULL,NULL),(45,'Bashkir Curly Horses\r\n','2020-05-16 14:15:24',NULL,NULL),(46,'Bavarian Warmbloods\r\n','2020-05-16 14:15:29',NULL,NULL),(47,'Belgians\r\n','2020-05-16 14:15:34',NULL,NULL),(48,'Belgian Sporthorses\r\n','2020-05-16 14:15:37',NULL,NULL),(49,'Belgian Warmbloods\r\n','2020-05-16 14:15:41',NULL,NULL),(50,'Black Forest Horses\r\n','2020-05-16 14:15:44',NULL,NULL),(51,'Blazer Horses\r\n','2020-05-16 14:15:48',NULL,NULL),(52,'Boulonnais Horses\r\n','2020-05-16 14:15:52',NULL,NULL),(53,'Brabant Horses\r\n','2020-05-16 14:16:03',NULL,NULL),(54,'Brandenburgers\r\n','2020-05-16 14:16:07',NULL,NULL),(55,'British Riding Ponies\r\n','2020-05-16 14:16:14',NULL,NULL),(56,'Budyonny Horses\r\n','2020-05-16 14:16:18',NULL,NULL),(57,'Canadian Crosses\r\n','2020-05-16 14:16:24',NULL,NULL),(58,'Canadian Horses\r\n','2020-05-16 14:16:28',NULL,NULL),(59,'','2020-05-18 13:09:21','2020-05-18 20:09:21',NULL),(60,'Canadian Warmbloods\r\n','2020-05-16 14:16:35',NULL,NULL),(61,'Caspian Horses\r\n','2020-05-16 14:16:39',NULL,NULL),(62,'Champagne Horse ICHR\r\n','2020-05-16 14:16:43',NULL,NULL),(63,'Chincoteague Ponies\r\n','2020-05-16 14:16:47',NULL,NULL),(64,'Cleveland Bays\r\n','2020-05-16 14:16:51',NULL,NULL),(65,'Clydesdales\r\n','2020-05-16 14:16:55',NULL,NULL),(66,'Colonial Spanish Horses\r\n','2020-05-16 14:16:59',NULL,NULL),(67,'Colorado Rangerbreds\r\n','2020-05-16 14:17:03',NULL,NULL),(68,'Connemaras\r\n','2020-05-16 14:17:07',NULL,NULL),(69,'Costa Rican Pasos\r\n','2020-05-16 14:17:10',NULL,NULL),(70,'Curly Horses\r\n','2020-05-16 14:17:15',NULL,NULL),(71,'Czech Warmbloods\r\n','2020-05-16 14:17:18',NULL,NULL),(72,'Dales Ponies\r\n','2020-05-16 14:17:22',NULL,NULL),(73,'Danish Warmbloods\r\n','2020-05-16 14:17:31',NULL,NULL),(74,'Dartmoor Ponies\r\n','2020-05-16 14:17:40',NULL,NULL),(75,'Draft Horses\r\n','2020-05-16 14:17:44',NULL,NULL),(76,'Draft Horse Crosses\r\n','2020-05-16 14:17:48',NULL,NULL),(77,'Drum Horses\r\n','2020-05-16 14:17:52',NULL,NULL),(78,'Dutch Harness Horses\r\n','2020-05-16 14:17:55',NULL,NULL),(79,'Dutch Warmbloods\r\n','2020-05-16 14:18:00',NULL,NULL),(80,'Fell Ponies\r\n','2020-05-16 14:18:04',NULL,NULL),(81,'Fjords\r\n','2020-05-16 14:18:07',NULL,NULL),(82,'Florida Cracker Horses\r\n','2020-05-16 14:18:11',NULL,NULL),(83,'Foundation Quarter Horse Registry\r\n','2020-05-16 14:18:14',NULL,NULL),(84,'Frederiksborgs\r\n','2020-05-16 14:18:18',NULL,NULL),(85,'Friesians\r\n','2020-05-16 14:18:21',NULL,NULL),(86,'Friesian Crosses\r\n','2020-05-16 14:18:24',NULL,NULL),(87,'Galicenos\r\n','2020-05-16 14:18:31',NULL,NULL),(88,'Georgian Grandes\r\n','2020-05-16 14:18:34',NULL,NULL),(89,'German Riding Ponies\r\n','2020-05-16 14:18:38',NULL,NULL),(90,'German Warmbloods\r\n','2020-05-16 14:18:41',NULL,NULL),(91,'Grade Horses\r\n','2020-05-16 14:18:44',NULL,NULL),(92,'Gypsians\r\n','2020-05-16 14:18:48',NULL,NULL),(93,'Gypsy Horses\r\n','2020-05-16 14:18:52',NULL,NULL),(94,'Gypsy Horse Crosses\r\n','2020-05-16 14:18:55',NULL,NULL),(95,'Hackney Horses\r\n','2020-05-16 14:18:59',NULL,NULL),(96,'Hackney Ponies\r\n','2020-05-16 14:19:02',NULL,NULL),(97,'Haflingers\r\n','2020-05-16 14:19:07',NULL,NULL),(98,'Hanoverians\r\n','2020-05-16 14:19:11',NULL,NULL),(99,'Heck Tarpans\r\n','2020-05-16 14:19:15',NULL,NULL),(100,'Hessens\r\n','2020-05-16 14:19:23',NULL,NULL),(101,'Highland Ponies\r\n','2020-05-16 14:19:27',NULL,NULL),(102,'Hinnies\r\n','2020-05-16 14:19:31',NULL,NULL),(103,'Holsteiners\r\n','2020-05-16 14:19:34',NULL,NULL),(104,'Hungarian Warmbloods\r\n','2020-05-16 14:19:41',NULL,NULL),(105,'Iberian Warmbloods\r\n','2020-05-16 14:19:46',NULL,NULL),(106,'Icelandics\r\n','2020-05-16 14:19:47',NULL,NULL),(107,'International Buckskin IBHA\r\n','2020-05-16 14:30:38',NULL,NULL),(108,'International Sporthorses\r\n','2020-05-16 14:30:42',NULL,NULL),(109,'Irish Cobs\r\n','2020-05-16 14:30:45',NULL,NULL),(110,'Irish Draughts\r\n','2020-05-16 14:30:49',NULL,NULL),(111,'Irish Sport Horses\r\n','2020-05-16 14:30:52',NULL,NULL),(112,'Kentucky Mountain Horses\r\n','2020-05-16 14:30:56',NULL,NULL),(113,'Kerry Bog Ponies\r\n','2020-05-16 14:31:01',NULL,NULL),(114,'Kiger Mustangs\r\n','2020-05-16 14:31:04',NULL,NULL),(115,'Knabstruppers\r\n','2020-05-16 14:31:07',NULL,NULL),(116,'Lipizzans\r\n','2020-05-16 14:31:11',NULL,NULL),(117,'Lusitanos\r\n','2020-05-16 14:31:30',NULL,NULL),(118,'Mangalarga Marchadors\r\n','2020-05-16 14:31:34',NULL,NULL),(119,'Mangalarga Paulistas\r\n','2020-05-16 14:31:37',NULL,NULL),(120,'McCurdy Plantation Horses\r\n','2020-05-16 14:31:40',NULL,NULL),(121,'Mexican Warmbloods\r\n','2020-05-16 14:31:43',NULL,NULL),(122,'Miniature Horses\r\n','2020-05-16 14:31:47',NULL,NULL),(123,'Missouri Fox Trotter Horses\r\n','2020-05-16 14:31:51',NULL,NULL),(124,'Missouri Fox Trotter Ponies\r\n','2020-05-16 14:31:55',NULL,NULL),(125,'Montana Travlers\r\n','2020-05-16 14:31:59',NULL,NULL),(126,'Morabs\r\n','2020-05-16 14:32:02',NULL,NULL),(127,'Morgan Horses\r\n','2020-05-16 14:32:05',NULL,NULL),(128,'Moriesian Horses\r\n','2020-05-16 14:32:09',NULL,NULL),(129,'Morocco Spotted Horses\r\n','2020-05-16 14:32:12',NULL,NULL),(130,'Mountain Pleasure Horses\r\n','2020-05-16 14:32:15',NULL,NULL),(131,'Mustangs\r\n','2020-05-16 14:32:18',NULL,NULL),(132,'Nambian Warmbloods\r\n','2020-05-16 14:32:21',NULL,NULL),(133,'National Foundation Quarter Horse\r\n','2020-05-16 14:32:25',NULL,NULL),(134,'National Quarter Horse Registry\r\n','2020-05-16 14:32:29',NULL,NULL),(135,'National Show Horses\r\n','2020-05-16 14:32:33',NULL,NULL),(136,'New Forest Ponies\r\n','2020-05-16 14:32:39',NULL,NULL),(137,'New Zealand Warmbloods\r\n','2020-05-16 14:32:43',NULL,NULL),(138,'Newfoundland Ponies\r\n','2020-05-16 14:32:47',NULL,NULL),(139,'Nez Perce Horses\r\n','2020-05-16 14:32:50',NULL,NULL),(140,'Nokotas\r\n','2020-05-16 14:32:53',NULL,NULL),(141,'North American Spotted Haflingers\r\n','2020-05-16 14:32:57',NULL,NULL),(142,'Oberlander Draft Horses\r\n','2020-05-16 14:33:00',NULL,NULL),(143,'Oldenburgs\r\n','2020-05-16 14:33:04',NULL,NULL),(144,'Orlov Rostopchin Horses\r\n','2020-05-16 14:33:08',NULL,NULL),(145,'Other\r\n','2020-05-16 14:33:11',NULL,NULL),(146,'Palomino Horse Breed PHBA\r\n','2020-05-16 14:33:17',NULL,NULL),(147,'Paso Finos\r\n','2020-05-16 14:33:20',NULL,NULL),(148,'Percherons\r\n','2020-05-16 14:33:24',NULL,NULL),(149,'Peruvian Pasos\r\n','2020-05-16 14:33:27',NULL,NULL),(150,'Pintabians\r\n','2020-05-16 14:33:31',NULL,NULL),(151,'Pintaloosas\r\n','2020-05-16 14:33:34',NULL,NULL),(152,'Pintos\r\n','2020-05-16 14:34:22',NULL,NULL),(153,'Pinto Pasos\r\n','2020-05-16 14:34:29',NULL,NULL),(154,'Pleasure Saddle Horse Registry\r\n','2020-05-16 14:34:33',NULL,NULL),(155,'Polish Warmblood Wielkopolskis\r\n','2020-05-16 14:34:38',NULL,NULL),(156,'Ponies\r\n','2020-05-16 14:34:41',NULL,NULL),(157,'Pony of Americas\r\n','2020-05-16 14:34:45',NULL,NULL),(158,'PRE Mundials\r\n','2020-05-16 14:34:49',NULL,NULL),(159,'Pura Raza Espanolas\r\n','2020-05-16 14:34:52',NULL,NULL),(160,'Quarabs\r\n','2020-05-16 14:34:57',NULL,NULL),(161,'Quarter Ponies\r\n','2020-05-16 14:35:00',NULL,NULL),(162,'Racking Horses\r\n','2020-05-16 14:35:04',NULL,NULL),(163,'Rheinland Pfalz Saars\r\n','2020-05-16 14:35:07',NULL,NULL),(164,'Rocky Mountain Horses\r\n','2020-05-16 14:35:11',NULL,NULL),(165,'Russian Trotters\r\n','2020-05-16 14:35:19',NULL,NULL),(166,'Russian Warmbloods\r\n','2020-05-16 14:35:23',NULL,NULL),(167,'Selle Francais\r\n','2020-05-16 14:35:28',NULL,NULL),(168,'firsts','2020-05-20 12:28:58','2020-05-20 19:28:58',NULL),(169,'Shetland Ponies\r\n','2020-05-16 14:35:35',NULL,NULL),(170,'Shire Horses\r\n','2020-05-16 14:35:38',NULL,NULL),(171,'Singlefoot Horses\r\n','2020-05-16 14:35:43',NULL,NULL),(172,'Spanish Barbs\r\n','2020-05-16 14:35:47',NULL,NULL),(173,'Spanish Mustangs\r\n','2020-05-16 14:35:52',NULL,NULL),(174,'Spanish Normans\r\n','2020-05-16 14:35:55',NULL,NULL),(175,'Sport Horse of Color\r\n','2020-05-16 14:35:58',NULL,NULL),(176,'Spotted Drafts\r\n','2020-05-16 14:36:02',NULL,NULL),(177,'Spotted Mountain Horses\r\n','2020-05-16 14:36:05',NULL,NULL),(178,'Spotted Saddle Horses\r\n','2020-05-16 14:36:13',NULL,NULL),(179,'Standard Quarter SQHA\r\n','2020-05-16 14:36:17',NULL,NULL),(180,'Standardbreds\r\n','2020-05-16 14:36:20',NULL,NULL),(181,'Suffolks\r\n','2020-05-16 14:36:24',NULL,NULL),(182,'Sugarbush Harlequin Draft Horses\r\n','2020-05-16 14:36:28',NULL,NULL),(183,'Swedish Gotlands\r\n','2020-05-16 14:36:31',NULL,NULL),(184,'Swedish Warmbloods\r\n','2020-05-16 14:36:35',NULL,NULL),(185,'Swiss Warmbloods\r\n','2020-05-16 14:36:38',NULL,NULL),(186,'Tennessee Walking Horses\r\n','2020-05-16 14:36:41',NULL,NULL),(187,'Tennuvians\r\n','2020-05-16 14:36:44',NULL,NULL),(188,'Thoroughbreds\r\n','2020-05-16 14:36:48',NULL,NULL),(189,'Thoroughbred Crosses\r\n','2020-05-16 14:36:51',NULL,NULL),(190,'Thoroughbred Welsh Crosses\r\n','2020-05-16 14:36:56',NULL,NULL),(191,'Tigre Horses\r\n','2020-05-16 14:37:00',NULL,NULL),(192,'Trakehners\r\n','2020-05-16 14:37:04',NULL,NULL),(193,'Trote y Galope Horses\r\n','2020-05-16 14:37:08',NULL,NULL),(194,'United Mountain Horses\r\n','2020-05-16 14:37:11',NULL,NULL),(195,'Virginia Highlanders\r\n','2020-05-16 14:37:15',NULL,NULL),(196,'Walkaloosas\r\n','2020-05-16 14:37:19',NULL,NULL),(197,'Walking Ponies\r\n','2020-05-16 14:37:22',NULL,NULL),(198,'Warlanders\r\n','2020-05-16 14:37:26',NULL,NULL),(199,'Warmblood Crosses\r\n','2020-05-16 14:37:29',NULL,NULL),(200,'Warmbloods of Color\r\n','2020-05-16 14:37:33',NULL,NULL),(201,'Welara Ponies\r\n','2020-05-16 14:37:36',NULL,NULL),(202,'Welsh Cobs\r\n','2020-05-16 14:37:39',NULL,NULL),(203,'Welsh Ponies\r\n','2020-05-16 14:37:43',NULL,NULL),(204,'Westphalians\r\n','2020-05-16 14:37:46',NULL,NULL),(205,'Zangersheides\r\n','2020-05-16 14:37:49',NULL,NULL),(206,'Zweibruckers\r\n','2020-05-16 14:37:50',NULL,NULL);

/*Table structure for table `colors` */

DROP TABLE IF EXISTS `colors`;

CREATE TABLE `colors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `colors` */

insert  into `colors`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'Any\r\n','2020-05-16 13:49:56',NULL,NULL),(2,'Bay \r\n','2020-05-16 13:50:01',NULL,NULL),(3,'Bay Dun \r\n','2020-05-16 13:50:06',NULL,NULL),(4,'Bay Dun Roan \r\n','2020-05-16 13:50:10',NULL,NULL),(5,'Bay Roan \r\n','2020-05-16 13:50:14',NULL,NULL),(6,'Black \r\n','2020-05-16 13:50:18',NULL,NULL),(7,'Black Bay \r\n','2020-05-16 13:50:24',NULL,NULL),(8,'Blue Roan \r\n','2020-05-16 13:50:30',NULL,NULL),(9,'Brindle \r\n','2020-05-16 13:50:35',NULL,NULL),(10,'Brown \r\n','2020-05-16 13:50:39',NULL,NULL),(11,'Buckskin \r\n','2020-05-16 13:50:44',NULL,NULL),(12,'Buckskin Roan \r\n','2020-05-16 13:50:49',NULL,NULL),(13,'Champagne \r\n','2020-05-16 13:50:53',NULL,NULL),(14,'Chestnut \r\n','2020-05-16 13:52:16',NULL,NULL),(15,'Chocolate \r\n','2020-05-16 13:52:23',NULL,NULL),(16,'Cremello \r\n','2020-05-16 13:52:29',NULL,NULL),(17,'Cremello Dun \r\n','2020-05-16 13:52:36',NULL,NULL),(18,'Dappled Grey\r\n','2020-05-16 13:52:50',NULL,NULL),(19,'Dun \r\n','2020-05-16 13:52:55',NULL,NULL),(20,'Dunalino \r\n','2020-05-16 13:52:59',NULL,NULL),(21,'Dunskin \r\n','2020-05-16 13:53:04',NULL,NULL),(22,'Grey \r\n','2020-05-16 13:53:08',NULL,NULL),(23,'Grullo \r\n','2020-05-16 13:53:13',NULL,NULL),(24,'Liver Chestnut \r\n','2020-05-16 13:53:19',NULL,NULL),(25,'Paint\r\n','2020-05-16 13:53:24',NULL,NULL),(26,'Palomino \r\n','2020-05-16 13:53:29',NULL,NULL),(27,'Palomino Roan \r\n','2020-05-16 13:53:33',NULL,NULL),(28,'Pearl \r\n','2020-05-16 13:53:37',NULL,NULL),(29,'Perlino \r\n','2020-05-16 13:53:42',NULL,NULL),(30,'Perlino Dun \r\n','2020-05-16 13:53:47',NULL,NULL),(31,'Piebald \r\n','2020-05-16 13:53:51',NULL,NULL),(32,'Red Chocolate \r\n','2020-05-16 13:53:56',NULL,NULL),(33,'Red Dun \r\n','2020-05-16 13:54:01',NULL,NULL),(34,'Red Dun Roan \r\n','2020-05-16 13:54:05',NULL,NULL),(35,'Red Roan \r\n','2020-05-16 13:54:10',NULL,NULL),(36,'Skewbald\r\n','2020-05-16 13:54:14',NULL,NULL),(37,'Sorrel\r\n','2020-05-16 13:54:17',NULL,NULL);

/*Table structure for table `disciplines` */

DROP TABLE IF EXISTS `disciplines`;

CREATE TABLE `disciplines` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;

/*Data for the table `disciplines` */

insert  into `disciplines`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'4H\r\n','2020-05-08 00:49:55',NULL,NULL),(2,'5-Panel DNA Test N-N\r\n','2020-05-08 00:50:01',NULL,NULL),(3,'Any\r\n','2020-05-16 13:46:53',NULL,NULL),(4,'6-Panel DNA Test N-N\r\n','2020-05-16 13:46:58',NULL,NULL),(5,'Advanced Rider Recommended\r\n','2020-05-16 13:47:03',NULL,NULL),(6,'Agility\r\n','2020-05-16 13:47:08',NULL,NULL),(7,'All Around Show Horse\r\n','2020-05-16 13:47:13',NULL,NULL),(8,'Barefoot Hoofcare\r\n','2020-05-16 13:47:18',NULL,NULL),(9,'Baroque Horse\r\n','2020-05-16 13:47:22',NULL,NULL),(10,'Barrel Racing\r\n','2020-05-16 13:47:26',NULL,NULL),(12,'Beginner-Safe Horse\r\n','2020-05-16 13:47:36',NULL,NULL),(13,'Breakaway Roping\r\n','2020-05-16 13:47:41',NULL,NULL),(14,'Breeders Sweepstakes\r\n','2020-05-16 13:47:46',NULL,NULL),(15,'Breeders Trust\r\n','2020-05-16 13:47:50',NULL,NULL),(16,'Bridle Horse\r\n','2020-05-16 13:47:54',NULL,NULL),(17,'Broodmare\r\n','2020-05-16 13:47:59',NULL,NULL),(18,'Broodmare Only\r\n','2020-05-16 13:48:04',NULL,NULL),(19,'Calf Roping\r\n','2020-05-16 13:48:09',NULL,NULL),(20,'Champion\r\n','2020-05-16 13:48:13',NULL,NULL),(21,'Champion Pedigree\r\n','2020-05-16 13:48:18',NULL,NULL),(22,'Color Tested\r\n','2020-05-16 13:48:22',NULL,NULL),(23,'Companion Only\r\n','2020-05-16 13:48:27',NULL,NULL),(24,'Country English Pleasure\r\n','2020-05-16 13:48:31',NULL,NULL),(25,'Cowboy Dressage\r\n','2020-05-16 13:48:41',NULL,NULL),(26,'Cowboy Mounted Shooting\r\n','2020-05-16 13:48:45',NULL,NULL),(27,'Cowboy Racing\r\n','2020-05-16 13:48:50',NULL,NULL),(28,'Cowhorse\r\n','2020-05-16 13:48:54',NULL,NULL),(29,'Cutting\r\n','2020-05-16 13:48:59',NULL,NULL),(30,'Double Registered\r\n','2020-05-16 13:49:03',NULL,NULL),(31,'Double Registered AQHA-APHA\r\n','2020-05-16 13:49:07',NULL,NULL),(32,'Draft\r\n','2020-05-16 13:49:08',NULL,NULL),(33,'Dressage\r\n','2020-05-16 14:20:15',NULL,NULL),(34,'Drill Team\r\n','2020-05-16 14:20:20',NULL,NULL),(35,'Driving\r\n','2020-05-16 14:20:24',NULL,NULL),(36,'Endurance Riding\r\n','2020-05-16 14:20:28',NULL,NULL),(37,'English Pleasure\r\n','2020-05-16 14:20:32',NULL,NULL),(38,'Eventing\r\n','2020-05-16 14:20:35',NULL,NULL),(39,'Field Trials\r\n','2020-05-16 14:20:39',NULL,NULL),(40,'Foundation Bred\r\n','2020-05-16 14:20:43',NULL,NULL),(41,'Foxhunter\r\n','2020-05-16 14:20:46',NULL,NULL),(42,'Futurity Eligible\r\n','2020-05-16 14:20:49',NULL,NULL),(43,'GBED N-G\r\n','2020-05-16 14:20:52',NULL,NULL),(44,'GBED N-N\r\n','2020-05-16 14:20:56',NULL,NULL),(45,'Gun-Safe Hunting Horse\r\n','2020-05-16 14:20:59',NULL,NULL),(46,'Gymkhana\r\n','2020-05-16 14:21:02',NULL,NULL),(47,'Hackamore Horse\r\n','2020-05-16 14:21:06',NULL,NULL),(48,'Halter\r\n','2020-05-16 14:21:13',NULL,NULL),(49,'Harness\r\n','2020-05-16 14:21:17',NULL,NULL),(50,'Harness Racing\r\n','2020-05-16 14:21:21',NULL,NULL),(51,'Hazing\r\n','2020-05-16 14:21:24',NULL,NULL),(52,'HERDA N-Hrd\r\n','2020-05-16 14:21:27',NULL,NULL),(53,'HERDA N-N\r\n','2020-05-16 14:21:31',NULL,NULL),(54,'Hermaphrodite Horse\r\n','2020-05-16 14:21:34',NULL,NULL),(55,'Heterozygous\r\n','2020-05-16 14:21:38',NULL,NULL),(56,'High-Percentage Color Producer\r\n','2020-05-16 14:21:42',NULL,NULL),(57,'High-Percentage Cropout Producer\r\n','2020-05-16 14:21:46',NULL,NULL),(58,'High-Percentage Dun Factor Producer\r\n','2020-05-16 14:21:50',NULL,NULL),(59,'Homozygous\r\n','2020-05-16 14:21:54',NULL,NULL),(60,'Homozygous Black\r\n','2020-05-16 14:21:57',NULL,NULL),(61,'Homozygous Dun\r\n','2020-05-16 14:22:01',NULL,NULL),(62,'Homozygous Roan\r\n','2020-05-16 14:22:04',NULL,NULL),(63,'Homozygous Tobiano\r\n','2020-05-16 14:22:07',NULL,NULL),(64,'Horse Ball\r\n','2020-05-16 14:22:11',NULL,NULL),(65,'Horsemanship\r\n','2020-05-16 14:22:14',NULL,NULL),(66,'Hunt Seat Equitation\r\n','2020-05-16 14:22:18',NULL,NULL),(67,'Hunter\r\n','2020-05-16 14:22:21',NULL,NULL),(68,'Hunter Pleasure\r\n','2020-05-16 14:22:25',NULL,NULL),(69,'Hunter Under Saddle\r\n','2020-05-16 14:22:28',NULL,NULL),(70,'Husband-Safe Horse\r\n','2020-05-16 14:22:35',NULL,NULL),(71,'HYPP N-H\r\n','2020-05-16 14:22:39',NULL,NULL),(72,'HYPP N-N\r\n','2020-05-16 14:22:42',NULL,NULL),(73,'IMM-MYH1 N-My\r\n','2020-05-16 14:22:46',NULL,NULL),(74,'IMM-MYH1 N-N\r\n','2020-05-16 14:22:50',NULL,NULL),(75,'Incentive Fund\r\n','2020-05-16 14:22:53',NULL,NULL),(76,'Intermediate Rider Recommended\r\n','2020-05-16 14:22:57',NULL,NULL),(77,'Jumping\r\n','2020-05-16 14:23:00',NULL,NULL),(78,'Kentucky Breeders Incentive Program\r\n','2020-05-16 14:23:04',NULL,NULL),(79,'Kid-Friendly Family Horse\r\n','2020-05-16 14:23:07',NULL,NULL),(80,'Lesson Horse\r\n','2020-05-16 14:23:11',NULL,NULL),(81,'Longe Line\r\n','2020-05-16 14:23:17',NULL,NULL),(82,'Money Winner\r\n','2020-05-16 14:23:21',NULL,NULL),(83,'Mounted Games\r\n','2020-05-16 14:23:24',NULL,NULL),(84,'Mounted Patrol\r\n','2020-05-16 14:23:28',NULL,NULL),(85,'Mounted Police\r\n','2020-05-16 14:23:32',NULL,NULL),(86,'Native Costume\r\n','2020-05-16 14:23:35',NULL,NULL),(87,'Natural Horsemanship Training\r\n','2020-05-16 14:23:39',NULL,NULL),(88,'NRBC Eligible\r\n','2020-05-16 14:23:47',NULL,NULL),(89,'NRBC Enrolled\r\n','2020-05-16 14:23:52',NULL,NULL),(90,'NRCHA Subscribed Stallion\r\n','2020-05-16 14:23:55',NULL,NULL),(91,'NRHA Futurity-Derby Nominated\r\n','2020-05-16 14:23:59',NULL,NULL),(92,'NRHA Sire and Dam Program\r\n','2020-05-16 14:24:02',NULL,NULL),(93,'Nurse Mare\r\n','2020-05-16 14:24:06',NULL,NULL),(94,'Orphan Foal\r\n','2020-05-16 14:24:10',NULL,NULL),(95,'Pacing Gait\r\n','2020-05-16 14:24:14',NULL,NULL),(96,'Pack Horse\r\n','2020-05-16 14:24:19',NULL,NULL),(97,'Parades\r\n','2020-05-16 14:24:23',NULL,NULL),(98,'Park\r\n','2020-05-16 14:24:26',NULL,NULL),(99,'Pleasure Driving\r\n','2020-05-16 14:24:30',NULL,NULL),(100,'PMU Foal\r\n','2020-05-16 14:24:31',NULL,NULL),(101,'Point Earner\r\n','2020-05-16 14:25:34',NULL,NULL),(102,'Pole Bending\r\n','2020-05-16 14:25:39',NULL,NULL),(103,'Polo\r\n','2020-05-16 14:25:42',NULL,NULL),(104,'Polocrosse\r\n','2020-05-16 14:25:46',NULL,NULL),(105,'Pony Breed\r\n','2020-05-16 14:25:49',NULL,NULL),(106,'Pony Club\r\n','2020-05-16 14:25:53',NULL,NULL),(107,'PSSM P2 P3 P4 Px n-n\r\n','2020-05-16 14:25:57',NULL,NULL),(108,'Racing\r\n','2020-05-16 14:26:00',NULL,NULL),(109,'Racing, Retired Racehorse\r\n','2020-05-16 14:26:03',NULL,NULL),(110,'Racking Gait\r\n','2020-05-16 14:26:07',NULL,NULL),(111,'Ranch Riding-Ranch Pleasure\r\n','2020-05-16 14:26:13',NULL,NULL),(112,'Ranch Sorting\r\n','2020-05-16 14:26:18',NULL,NULL),(113,'Ranch Versatility\r\n','2020-05-16 14:26:21',NULL,NULL),(114,'Ranch Work\r\n','2020-05-16 14:26:24',NULL,NULL),(115,'Ranching Heritage Breeders\r\n','2020-05-16 14:26:28',NULL,NULL),(116,'Recipient Broodmare\r\n','2020-05-16 14:26:31',NULL,NULL),(117,'Register of Merit\r\n','2020-05-16 14:26:35',NULL,NULL),(118,'Reining\r\n','2020-05-16 14:26:38',NULL,NULL),(119,'Reining-Cowhorse-Cutting\r\n','2020-05-16 14:26:42',NULL,NULL),(120,'Rescue Horse\r\n','2020-05-16 14:26:46',NULL,NULL),(121,'Ridden English\r\n','2020-05-16 14:26:52',NULL,NULL),(122,'Ridden Western\r\n','2020-05-16 14:26:56',NULL,NULL),(123,'Rider Level: Amateur\r\n','2020-05-16 14:26:59',NULL,NULL),(124,'Rider Level: Novice\r\n','2020-05-16 14:27:03',NULL,NULL),(125,'Rider Level: Open\r\n','2020-05-16 14:27:07',NULL,NULL),(126,'Rodeo Bronc\r\n','2020-05-16 14:27:11',NULL,NULL),(127,'Rodeo Pickup Horse\r\n','2020-05-16 14:27:14',NULL,NULL),(128,'Rodeo Queen Horse\r\n','2020-05-16 14:27:20',NULL,NULL),(129,'Roping\r\n','2020-05-16 14:27:24',NULL,NULL),(130,'Roping, All Around\r\n','2020-05-16 14:27:28',NULL,NULL),(131,'Saddle Seat\r\n','2020-05-16 14:27:32',NULL,NULL),(132,'School\r\n','2020-05-16 14:27:36',NULL,NULL),(133,'Schoolmaster\r\n','2020-05-16 14:27:39',NULL,NULL),(134,'Show Experience\r\n','2020-05-16 14:27:43',NULL,NULL),(135,'Show Hack\r\n','2020-05-16 14:27:47',NULL,NULL),(136,'Show Winner\r\n','2020-05-16 14:27:50',NULL,NULL),(137,'Showmanship at Halter\r\n','2020-05-16 14:27:53',NULL,NULL),(138,'Sidesaddle\r\n','2020-05-16 14:27:57',NULL,NULL),(139,'Spayed Mare\r\n','2020-05-16 14:28:04',NULL,NULL),(140,'Sporthorse\r\n','2020-05-16 14:28:08',NULL,NULL),(141,'Stallion, Breeding\r\n','2020-05-16 14:28:11',NULL,NULL),(142,'Started Under Saddle\r\n','2020-05-16 14:28:15',NULL,NULL),(143,'Superior Award\r\n','2020-05-16 14:28:18',NULL,NULL),(144,'Team Driving\r\n','2020-05-16 14:28:21',NULL,NULL),(145,'Team Penning\r\n','2020-05-16 14:28:26',NULL,NULL),(146,'Team Roping\r\n','2020-05-16 14:28:30',NULL,NULL),(147,'Team Roping Head Horse\r\n','2020-05-16 14:28:36',NULL,NULL),(148,'Team Roping Heel Horse\r\n','2020-05-16 14:28:39',NULL,NULL),(149,'Therapeutic Riding\r\n','2020-05-16 14:28:42',NULL,NULL),(150,'Three-In-One Package\r\n','2020-05-16 14:28:46',NULL,NULL),(151,'Trail Class Competition\r\n','2020-05-16 14:28:50',NULL,NULL),(152,'Trail Riding, Competitive\r\n','2020-05-16 14:28:53',NULL,NULL),(153,'Trail Riding, Recreational\r\n','2020-05-16 14:28:56',NULL,NULL),(154,'Trick Horse\r\n','2020-05-16 14:29:00',NULL,NULL),(155,'Triple Registered\r\n','2020-05-16 14:29:05',NULL,NULL),(156,'Turnback Horse\r\n','2020-05-16 14:29:08',NULL,NULL),(157,'Two-In-One Package\r\n','2020-05-16 14:29:11',NULL,NULL),(158,'Two-Rein Horse\r\n','2020-05-16 14:29:15',NULL,NULL),(159,'Vaulting\r\n','2020-05-16 14:29:18',NULL,NULL),(160,'Weanling\r\n','2020-05-16 14:29:22',NULL,NULL),(161,'Western Dressage\r\n','2020-05-16 14:29:25',NULL,NULL),(162,'Western Pleasure\r\n','2020-05-16 14:29:29',NULL,NULL),(163,'Western Riding\r\n','2020-05-16 14:29:32',NULL,NULL),(164,'Wife-Safe Horse\r\n','2020-05-16 14:29:36',NULL,NULL),(165,'Working Equitation\r\n','2020-05-16 14:29:39',NULL,NULL),(166,'Yearling\r\n','2020-05-16 14:29:42',NULL,NULL),(167,'Youth\r\n','2020-05-16 14:29:43',NULL,NULL);

/*Table structure for table `genders` */

DROP TABLE IF EXISTS `genders`;

CREATE TABLE `genders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `genders` */

insert  into `genders`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'Filly\r\n','2020-05-08 00:53:28',NULL,NULL),(2,'Colt\r\n','2020-05-08 00:53:33',NULL,NULL),(3,'Gelding\r\n','2020-05-08 00:53:38',NULL,NULL),(4,'Mare\r\n','2020-05-08 00:53:44',NULL,NULL),(5,'Stallion\r\n','2020-05-08 00:53:47',NULL,NULL);

/*Table structure for table `prices` */

DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `prices` */

insert  into `prices`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'10000','2020-05-02 09:35:14',NULL,NULL),(2,'20000','2020-05-02 09:35:18',NULL,NULL),(3,'30000','2020-05-02 09:35:26',NULL,NULL),(4,'40000','2020-05-02 09:35:31',NULL,NULL),(5,'50000','2020-05-02 09:35:38',NULL,NULL),(6,'60000','2020-05-02 09:35:46',NULL,NULL),(7,'70000','2020-05-02 09:35:50',NULL,NULL),(8,'80000','2020-05-02 09:35:55',NULL,NULL),(9,'90000','2020-05-02 09:35:59',NULL,NULL),(10,'100000','2020-05-02 09:36:07',NULL,NULL),(11,'100000+','2020-05-02 09:36:15',NULL,NULL);

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('pp','tc','us') DEFAULT 'tc',
  `content` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`id`,`type`,`content`,`created_at`,`updated_at`,`deleted_at`) values (1,'tc','<p>tc dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>','2019-05-25 18:41:39','2020-05-29 23:03:24',NULL),(2,'pp','<p>pp&nbsp;ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</p>','2019-10-10 15:54:29','2020-05-29 23:01:57',NULL),(3,'us','<p>us vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>','2019-10-10 15:54:35','2020-05-29 23:05:09',NULL);

/*Table structure for table `sizes` */

DROP TABLE IF EXISTS `sizes`;

CREATE TABLE `sizes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `sizes` */

insert  into `sizes`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (0,'10','2020-05-02 09:46:12',NULL,NULL),(2,'11','2020-05-02 09:45:21',NULL,NULL),(3,'12','2020-05-02 09:45:22',NULL,NULL),(4,'13','2020-05-02 09:45:24',NULL,NULL),(5,'14','2020-05-02 09:45:27',NULL,NULL),(6,'15','2020-05-02 09:45:29',NULL,NULL),(7,'16','2020-05-02 09:46:46',NULL,NULL),(8,'17','2020-05-02 09:46:50',NULL,NULL),(9,'18','2020-05-02 09:46:22',NULL,NULL),(10,'18+','2020-05-02 09:45:44',NULL,NULL);

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(255) DEFAULT '',
  `title` varchar(255) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`symbol`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'AL\r\n','Alabama\r\n','2020-05-03 05:34:58',NULL,NULL),(2,'AK\r\n','Alaska\r\n','2020-05-03 05:35:14',NULL,NULL),(3,'AZ\r\n','Arizona','2020-05-03 05:35:23',NULL,NULL),(4,'AR\r\n','Arkansas\r\n','2020-05-03 05:35:48',NULL,NULL),(5,'CA\r\n','California\r\n','2020-05-03 05:35:59',NULL,NULL),(6,'CO\r\n','Colorado\r\n','2020-05-16 14:00:28',NULL,NULL),(7,'CT\r\n','Connecticut\r\n','2020-05-16 14:01:04',NULL,NULL),(8,'DE\r\n','Delaware\r\n','2020-05-16 14:03:36',NULL,NULL),(9,'FL\r\n','Florida\r\n','2020-05-16 14:03:51',NULL,NULL),(10,'GA\r\n','Georgia','2020-05-16 14:04:11',NULL,NULL),(11,'HI\r\n','Hawaii','2020-05-16 14:04:25',NULL,NULL),(12,'ID\r\n','Idaho\r\n','2020-05-16 14:04:41',NULL,NULL),(13,'IL\r\n','Illinois','2020-05-16 14:04:53',NULL,NULL),(14,'IN\r\n','Indiana','2020-05-16 14:05:28',NULL,NULL),(15,'IA\r\n','Iowa\r\n','2020-05-16 14:06:08',NULL,NULL),(16,'KS','Kansas','2020-05-16 14:05:47',NULL,NULL),(17,'KY\r\n','Kentucky[E]\r\n','2020-05-16 14:06:24',NULL,NULL),(18,'LA\r\n','Louisiana','2020-05-16 14:06:38',NULL,NULL),(19,'ME\r\n','Maine','2020-05-16 14:06:47',NULL,NULL),(20,'MD\r\n','Maryland','2020-05-16 14:07:09',NULL,NULL),(21,'MA\r\n','Massachusetts[E]\r\n','2020-05-16 14:07:56',NULL,NULL),(22,'MI\r\n','Michigan','2020-05-16 14:08:12',NULL,NULL),(23,'MN\r\n','Minnesota\r\n','2020-05-16 14:08:25',NULL,NULL),(24,'MS\r\n','Mississippi\r\n','2020-05-16 14:08:33',NULL,NULL),(25,'MO\r\n','Missouri\r\n','2020-05-16 14:09:21',NULL,NULL),(26,'MT\r\n','Montana\r\n','2020-05-16 14:09:23',NULL,NULL),(27,'NE\r\n','Nebraska\r\n','2020-05-16 14:09:25',NULL,NULL),(28,'NV\r\n','Nevada\r\n','2020-05-16 14:09:28',NULL,NULL),(29,'NH\r\n','New Hampshire\r\n','2020-05-16 14:10:10',NULL,NULL),(30,'NJ\r\n','New Jersey\r\n','2020-05-16 14:10:13',NULL,NULL),(31,'NM\r\n','New Mexico\r\n','2020-05-16 14:10:22',NULL,NULL),(32,'NY\r\n','New York','2020-05-16 14:10:35',NULL,NULL),(33,'NC\r\n','North Carolina\r\n','2020-05-16 14:10:46',NULL,NULL),(34,'ND\r\n','North Dakota','2020-05-16 14:10:58',NULL,NULL),(35,'OH\r\n','Ohio\r\n','2020-05-16 14:11:09',NULL,NULL),(36,'OK\r\n','Oklahoma\r\n','2020-05-16 14:11:21',NULL,NULL),(37,'OR\r\n','Oregon\r\n','2020-05-16 14:11:40',NULL,NULL),(38,'PA\r\n','Pennsylvania[E]\r\n','2020-05-16 14:11:45',NULL,NULL),(39,'RI\r\n','Rhode Island[F]\r\n','2020-05-16 14:12:06',NULL,NULL),(40,'SC\r\n','South Carolina\r\n','2020-05-16 14:12:17',NULL,NULL),(41,'SD\r\n','South Dakota\r\n','2020-05-16 14:12:27',NULL,NULL),(42,'TN\r\n','Tennessee\r\n','2020-05-16 14:12:37',NULL,NULL),(43,'TX\r\n','Texas','2020-05-16 14:12:50',NULL,NULL),(44,'UT\r\n','Utah\r\n','2020-05-16 14:12:59',NULL,NULL),(45,'VT\r\n','Vermont\r\n','2020-05-16 14:13:09',NULL,NULL),(46,'VA\r\n','Virginia[E]','2020-05-16 14:13:21',NULL,NULL),(47,'WA\r\n','Washington\r\n','2020-05-16 14:13:31',NULL,NULL),(48,'WV\r\n','West Virginia','2020-05-16 14:13:42',NULL,NULL),(49,'WI\r\n','Wisconsin\r\n','2020-05-16 14:13:52',NULL,NULL),(50,'WY\r\n','Wyoming\r\n','2020-05-16 14:13:59',NULL,NULL);

/*Table structure for table `temperaments` */

DROP TABLE IF EXISTS `temperaments`;

CREATE TABLE `temperaments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `temperaments` */

insert  into `temperaments`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'1','2020-05-29 08:03:21',NULL,NULL),(2,'2','2020-05-29 08:03:23',NULL,NULL),(3,'3','2020-05-29 08:03:26',NULL,NULL),(4,'4','2020-05-29 08:03:27',NULL,NULL),(5,'5','2020-05-29 08:03:28',NULL,NULL),(6,'6','2020-05-29 08:03:30',NULL,NULL),(7,'7','2020-05-29 08:03:31',NULL,NULL),(8,'8','2020-05-29 08:03:33',NULL,NULL),(9,'9','2020-05-29 08:03:34',NULL,NULL),(10,'10','2020-05-29 08:03:36',NULL,NULL);

/*Table structure for table `user_devices` */

DROP TABLE IF EXISTS `user_devices`;

CREATE TABLE `user_devices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `udid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('ios','android') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ios',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_device_fk` (`user_id`),
  CONSTRAINT `user_device_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_devices` */

/*Table structure for table `user_favourite_posts` */

DROP TABLE IF EXISTS `user_favourite_posts`;

CREATE TABLE `user_favourite_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_fav_p_fk` (`user_id`),
  KEY `p_fav_p_fk` (`post_id`),
  CONSTRAINT `p_fav_p_fk` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `u_fav_p_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `user_favourite_posts` */

insert  into `user_favourite_posts`(`id`,`user_id`,`post_id`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (13,94,50,'2020-06-22','2020-06-22 20:04:58',NULL,NULL),(14,94,43,'2020-06-22','2020-06-22 20:05:01',NULL,NULL),(15,94,19,'2020-06-22','2020-06-22 20:05:05',NULL,NULL),(16,94,17,'2020-06-22','2020-06-22 20:05:06',NULL,NULL),(17,94,51,'2020-06-22','2020-06-22 20:30:09',NULL,NULL),(18,94,44,'2020-06-22','2020-06-22 20:30:10',NULL,NULL),(19,94,20,'2020-06-22','2020-06-22 20:30:13',NULL,NULL),(20,94,18,'2020-06-22','2020-06-22 20:30:15',NULL,NULL),(21,94,16,'2020-06-22','2020-06-22 20:30:19',NULL,NULL),(22,92,51,'2020-06-22','2020-06-22 21:53:21',NULL,NULL),(23,92,44,'2020-06-22','2020-06-22 22:08:40',NULL,NULL),(24,92,43,'2020-06-22','2020-06-22 22:08:41',NULL,NULL),(25,92,20,'2020-06-22','2020-06-22 22:08:41',NULL,NULL),(26,92,19,'2020-06-22','2020-06-22 22:08:42',NULL,NULL),(27,92,18,'2020-06-22','2020-06-22 22:08:44',NULL,NULL),(28,92,17,'2020-06-22','2020-06-22 22:08:45',NULL,NULL);

/*Table structure for table `user_post_images` */

DROP TABLE IF EXISTS `user_post_images`;

CREATE TABLE `user_post_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `image` varchar(255) DEFAULT '',
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_p_image_fk` (`user_id`),
  KEY `u_post_image_fk` (`post_id`),
  CONSTRAINT `u_p_image_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `u_post_image_fk` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;

/*Data for the table `user_post_images` */

insert  into `user_post_images`(`id`,`user_id`,`post_id`,`image`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (27,56,15,'1tR8YVK7ddb5OSWE6ZEJiF0y5DTh18DRzjdXd3qq.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(28,56,15,'c4wYZPmfffGdTC2wQILbp6MSaFkmiKLb2cgmIWtk.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(29,56,15,'ae5fSmj6IloAUSceUVuumr6K97oliqnc2S9jzQ5U.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(30,56,15,'xXlqpqEn2A4bkiucXEQGUy1id2BesethpXeg2YOE.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(31,56,15,'6udtfpQJxFF8TLksAHYtrewgNLn9yPdLIVNj7HA4.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(32,56,15,'V4TBuaqylL8d1LtCcYGzIJfGC4yeEunr0wNIAP9K.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(33,56,15,'y6ESsyQiJhEKthA8olpZR6Xca0Qfqbw5VopUPnCd.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(34,56,15,'5dibCz2fBaG9DzDiVbo6p4uDskXL9JFD93YrAzVE.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(35,56,15,'KBLSAqWmwnptMClLGeKcoO1F9KyK0SDQMATVctS1.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(36,56,15,'7fKiqqFLNwETKZUSAdDvews0KEDZRuUvZjkhZU7p.jpeg','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(37,56,16,'XWJA0lbJ85tZEaK09WZZpXvoMsRQlkyxFDNHaiEA.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(38,56,16,'ZRU5DjsoVTPsCICsR5wFozdtJkPdcjcRoloQ1XWR.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(39,56,16,'SZdCwp5rnOQFS52Ip5cZeSqtZo0H67iR8g2PShqD.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(40,56,16,'I0gDGUz0tAsEO1M01jkdAlNC77Jcm8Pic3A3JUW0.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(41,56,16,'MNn1PVkeokGbWHy8Rw1y1VI3pU0WOZ5jgZWjOur4.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(42,56,16,'IFNgZ2cbn4lPzd6e9byk4J5QJIUyrWJ3P1pWZk2q.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(43,56,16,'z4INYtfKYusL4A9fmANWCSn1WOQHOiRXAXB2mC36.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(44,56,16,'nmVOhFHe3RFGp1vypIiyzCd6N23TSp0XM0cBeVOV.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(45,56,16,'xgexscas7naFx9R7wKaZBh2oT0IT7DkWo6Y4r6IG.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(46,56,16,'cOVXjni6LgXfrDHAGUwMCcRCtfbgOlVIjpnOyzsC.jpeg','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(47,56,17,'WBOHyEFLy3YTq0iOZYzLyRepAmedcb4aJOLAMyHl.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(48,56,17,'DnBxL1l1HOf5xxWeRaBkdzdNZzvDN2fpdgpjn9CF.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(49,56,17,'BDN2E3P4oqfLdkzVQ0nt9dmrvGqREwNahqek9KNU.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(50,56,17,'97g2M5EuINxJVscGQ78W2YHty5PMUXEKcNai7HYt.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(51,56,17,'1CXt3cUs8AnhtiFgw7hE9TtwdV0kXCyxZrdXy7eI.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(52,56,17,'9ilaKldEYOMVPORBbvxXqRI26oMQNpknX9KOzuvm.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(53,56,17,'cihqRQcG707otO2tRxG38Zqxmt8v4yXzkgMB3Nh4.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(54,56,17,'KlZTpL3SZF0SS1p5eaDOtcl4eixAeNdZUdIWVdMj.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(55,56,17,'t8vHWcr4ErhYVcflxHbkH7xIKccIaUrhUjPwNOkj.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(56,56,17,'f78hPfyoMQHxhSQUR4FhUB1TpytXjQeenRou0lQF.jpeg','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(57,56,18,'4HVbLqNvsFxDnhFO7RAte8od19SGLFvqMXRRVoVN.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(58,56,18,'Po38cg9sDkwWCpXgPAkjApakQHPxF2RfwcAAOa6E.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(59,56,18,'MHuEWiGIQM0dc1GadlJLcru3bLtLzZ8Evwq7A7w0.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(60,56,18,'MN4oqUkPEERNQFa9bzhstUqJc51iScPGbl8uvZbu.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(61,56,18,'OdB1DYHnc0pDVwScdaKNYRVnFSuoy53AiFEjXn9v.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(62,56,18,'TeSPJnwV6yMxxdR2s94LhOjHt3XXeLEuAickOdnz.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(63,56,18,'8vFym1d83SNuwD4SZLgF3b6iPEzylYz0X4754gnw.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(64,56,18,'F28MNy9NwuHIyf25Ld6xnDO6xPH7RZxSwY9OauEW.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(65,56,18,'LXeudiRcLQqocc38Mg0aDH9GHBglrmEiRMkWsw09.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(66,56,18,'saDVtJm91q69Wf73wdQhA6pX69WKiIKEOT6pnmBc.jpeg','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(67,56,19,'pptkYL9uzHGW6EnGqkmbnIjvrJiY2S7hTX2AGLbZ.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(68,56,19,'qv3eV2ILvHdvWrPui9iV0vS57k4k6y8qnMB2xbQC.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(69,56,19,'eqdYXjI7i6uezfBnP0G0nD8m1TXQ3nMIIPP7aUIf.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(70,56,19,'R0xEFG80UlVWscZbHKfIn3IpcNycBGGlV2tHCdj7.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(71,56,19,'WEegt2cNaBzAcoODv3ZyQql7eXZsgARESgogACty.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(72,56,19,'7NI097g5DEB9Tm9lKnUXBONqf6PNAhyL74dbrH9T.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(73,56,19,'CgioLN5ufNVJEjVgbDrA55IbxllZEhGde4euUzbK.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(74,56,19,'nHNDEoDuWTHajqxoGVvxYBRbstAbu9HvbWfFbXTj.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(75,56,19,'cJg1eiwzngQwNPgLPbjLotBlHqqdRo5cBhA1y2f2.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(76,56,19,'6gzPrAujfwaA4Wz3E979sDobmLLQhNCk85jcZXEY.jpeg','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(77,56,20,'0k1nBWrSRalF0RD8kuUt5peyyVLUgrfF0jcFdjvp.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(78,56,20,'XflP1wNmdXiaBoLjrxE9XI3b2Dg4V6tIcmBAvjHo.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(79,56,20,'1jk0ke2YUa9kw7cUQ5pveAm8cYAIGdoV2hVnoUmw.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(80,56,20,'0Upa0KYOX6Co9R85awFH3YOOGnY4mNXZHCTrpKwL.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(81,56,20,'aPJntMwN5Cc3OALad3XamXRlPqiBoJSf7vIxonzh.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(82,56,20,'6mnc4SkJGl7N67tFZ45Zj9ZQ9tOHtt3NFjX0LdYb.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(83,56,20,'Pq0cJYt1otWLP92hvMTWFoWsdLs4LkF1lM8rnn1R.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(84,56,20,'O0erY30Zfh5bG9JvzETlCTtdcNNxZOfoIEGmuoH6.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(85,56,20,'RiTPK45A3dO19erxxDDKRrucm8KLXPm8J5U6fHuA.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(86,56,20,'i3aaCRe3c42K6R6v30kgBepNr1KDbxrGFBFYlt05.jpeg','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(111,56,43,'acqaEfDh1aLapkIuk5xzxtJCUToEAXE7llcRUNec.jpeg','2020-05-31','2020-05-31 22:45:54',NULL,NULL),(112,56,43,'qfMFZ0zCvIgKW524YRSl5uxVtp5NOfAtjy4auxcp.jpeg','2020-05-31','2020-05-31 22:45:54',NULL,NULL),(113,56,44,'IugEC6kCWEVDjIVo0y3SWsiKgRMfS5OPssCL5LBT.jpeg','2020-05-31','2020-05-31 22:52:17',NULL,NULL),(114,56,44,'BCADMl10RxrDo8bbDglTno7ftbDw3jhnQqMhry6T.jpeg','2020-05-31','2020-05-31 22:52:17',NULL,NULL),(128,93,50,'BuZjCraBpi8HZKLJ2NGnpH4IQgdDWd7Q094MPhtd.jpeg','2020-06-22','2020-06-22 22:39:01',NULL,NULL),(129,93,50,'mqF9nDjN0Z7YQfj600D060ZbqCYCmAG3Zc5VOgRZ.jpeg','2020-06-22','2020-06-22 22:39:01',NULL,NULL),(130,93,50,'W62ozHIKbeP4JQ5Utu8Qnekb6AWK0U0BIru4isNm.jpeg','2020-06-22','2020-06-22 22:39:01',NULL,NULL),(131,95,51,'i2jBM71Hs3odTteSO7gDg7nk6iQTsSFcvWKKtF8B.jpeg','2020-06-22','2020-06-23 03:27:25',NULL,NULL),(132,95,51,'fRKb08XPRQ8bBq0x4Wmb3l0SBM2gt1l4OdeITOLU.jpeg','2020-06-22','2020-06-23 03:27:25',NULL,NULL),(133,95,51,'RzkuuinD34uSTE8ijLOSmJQgp361oMPZyroJeXcV.jpeg','2020-06-22','2020-06-23 03:27:25',NULL,NULL),(134,95,51,'pFQr3N27A8sTOOI3cjxuN1HJNrL1Eplyy3WVUzZk.jpeg','2020-06-22','2020-06-23 03:27:25',NULL,NULL),(135,95,51,'HrqJfbz2XAX2hFTpmqxY4bVtJccpOPu9JKC2NhfV.jpeg','2020-06-22','2020-06-23 03:27:25',NULL,NULL),(136,95,52,'9E7CPjg4ORfPijhsIacTtT8aktsvA6MaAgRCO0Eb.jpeg','2020-06-22','2020-06-23 03:34:18',NULL,NULL),(137,95,52,'lhjBGPNov8X5pG3t1KJwFoxrFVCQKyFGVoXRk3S4.jpeg','2020-06-22','2020-06-23 03:34:18',NULL,NULL),(138,95,52,'OMlMWT2pMKqxcvZSZSWYCqtoaZVzhLpHbG2OFsBp.jpeg','2020-06-22','2020-06-23 03:34:18',NULL,NULL);

/*Table structure for table `user_post_links` */

DROP TABLE IF EXISTS `user_post_links`;

CREATE TABLE `user_post_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `link` varchar(255) DEFAULT '',
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_p_link_fk` (`user_id`),
  KEY `u_post_link_fk` (`post_id`),
  CONSTRAINT `u_p_link_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `u_post_link_fk` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*Data for the table `user_post_links` */

insert  into `user_post_links`(`id`,`user_id`,`post_id`,`link`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (40,56,15,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(41,56,15,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(42,56,15,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:14:25',NULL,NULL),(43,56,16,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(44,56,16,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(45,56,16,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:17:11',NULL,NULL),(46,56,17,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(47,56,17,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(48,56,17,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:22:48',NULL,NULL),(49,56,18,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(50,56,18,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(51,56,18,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:23:48',NULL,NULL),(52,56,19,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(53,56,19,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(54,56,19,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:25:35',NULL,NULL),(55,56,20,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(56,56,20,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(57,56,20,'https://www.google.com.pk/','2020-05-29','2020-05-30 05:28:56',NULL,NULL),(100,56,43,'https://www.google.com.pk/','2020-05-31','2020-05-31 22:45:54',NULL,NULL),(101,56,43,'https://www.google.com.pk/','2020-05-31','2020-05-31 22:45:54',NULL,NULL),(102,56,43,'https://www.google.com.pk/','2020-05-31','2020-05-31 22:45:54',NULL,NULL),(103,56,44,'https://www.google.com.pk/','2020-05-31','2020-05-31 22:52:17',NULL,NULL),(104,56,44,'https://www.google.com.pk/','2020-05-31','2020-05-31 22:52:17',NULL,NULL),(105,56,44,'https://www.google.com.pk/','2020-05-31','2020-05-31 22:52:17',NULL,NULL),(111,93,50,'https://YouTube.com','2020-06-22','2020-06-22 22:39:01',NULL,NULL),(112,95,51,'https://','2020-06-22','2020-06-23 03:27:25',NULL,NULL),(113,95,52,'https://YouTube.com','2020-06-22','2020-06-23 03:34:18',NULL,NULL);

/*Table structure for table `user_posts` */

DROP TABLE IF EXISTS `user_posts`;

CREATE TABLE `user_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) DEFAULT '',
  `headline` varchar(255) DEFAULT '',
  `breed_id` bigint(20) DEFAULT '0',
  `size` varchar(255) DEFAULT '',
  `temperament` varchar(255) DEFAULT '',
  `discipline_id` bigint(20) DEFAULT '0',
  `color_id` bigint(20) DEFAULT '0',
  `gender_id` bigint(20) DEFAULT '0',
  `address` varchar(255) DEFAULT '',
  `latitude` varchar(255) DEFAULT '',
  `longitude` varchar(255) DEFAULT '',
  `price` varchar(255) DEFAULT '',
  `status` enum('pending','approved','rejected') DEFAULT 'pending',
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_post_fk` (`user_id`),
  CONSTRAINT `u_post_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `user_posts` */

insert  into `user_posts`(`id`,`user_id`,`title`,`headline`,`breed_id`,`size`,`temperament`,`discipline_id`,`color_id`,`gender_id`,`address`,`latitude`,`longitude`,`price`,`status`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (15,56,'test','this is test horse',1,'5','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','approved','2020-05-26','2020-05-26 22:14:25','2020-05-29 22:34:15',NULL),(16,56,'first','this is first horse',1,'12','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','approved','2020-05-28','2020-05-28 22:17:11','2020-05-29 22:17:21',NULL),(17,56,'second','this is second horse',1,'12','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','pending','2020-05-28','2020-05-28 22:22:48','2020-05-29 22:22:48',NULL),(18,56,'third','this is third horse',1,'6','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','pending','2020-05-29','2020-05-29 22:23:48','2020-05-29 22:23:48',NULL),(19,56,'forth','this is forth horse',1,'12','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','pending','2020-05-30','2020-05-30 22:25:35','2020-05-29 22:25:35',NULL),(20,56,'fifth','this is fifth horse',1,'7','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','pending','2020-05-30','2020-05-30 22:28:56','2020-05-29 22:28:56',NULL),(43,56,'practice','this is practice horse',1,'12','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','pending','2020-05-31','2020-05-31 15:45:54','2020-05-31 15:45:54',NULL),(44,56,'new','this is new horse',1,'12','2',3,2,2,'Karachi Pakistan','24.8607','67.0011','12','pending','2020-05-31','2020-05-31 15:52:17','2020-05-31 15:52:17',NULL),(50,93,'Lion','A lion does not concern himself with the opinion of sheep',2,'8.1','5',7,7,2,'12340 Boggy Creek Rd, Orlando, FL 32824, USA','28.3853821','-81.3152741','250000','pending','2020-06-22','2020-06-22 15:39:01','2020-06-22 15:39:01',NULL),(51,95,'Zak','Zak is superb',13,'7.1','1',6,9,5,'New York Times Bldg, 620 8th Ave, New York, NY 10018, USA','40.756105','-73.9901921','100','pending','2020-06-22','2020-06-22 20:27:25','2020-06-22 20:27:25',NULL),(52,95,'White chicks','All available for you',33,'9.3','7',20,15,1,'Houston, TX, USA','29.7604267','-95.3698028','300','pending','2020-06-22','2020-06-22 20:34:18','2020-06-22 20:34:18',NULL);

/*Table structure for table `user_preferences` */

DROP TABLE IF EXISTS `user_preferences`;

CREATE TABLE `user_preferences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `input` longtext,
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pref_user_pk` (`user_id`),
  CONSTRAINT `pref_user_pk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `user_preferences` */

insert  into `user_preferences`(`id`,`user_id`,`input`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (2,59,'a:3:{s:7:\"user_id\";s:2:\"59\";s:11:\"price_start\";s:5:\"10000\";s:9:\"price_end\";s:5:\"40000\";}','2020-05-29','2020-05-29 15:57:33','2020-05-29 16:04:37',NULL),(3,56,'a:3:{s:7:\"user_id\";s:2:\"56\";s:10:\"size_start\";s:2:\"10\";s:8:\"size_end\";s:2:\"14\";}','2020-05-29','2020-05-29 16:04:15','2020-05-29 16:17:06',NULL),(7,92,'a:7:{s:7:\"user_id\";s:2:\"92\";s:10:\"size_start\";s:1:\"6\";s:8:\"size_end\";s:2:\"19\";s:10:\"temp_start\";s:1:\"1\";s:8:\"temp_end\";s:2:\"10\";s:11:\"price_start\";s:1:\"0\";s:9:\"price_end\";s:6:\"250000\";}','2020-06-22','2020-06-22 15:33:19','2020-06-22 21:53:03',NULL),(8,94,'a:7:{s:7:\"user_id\";s:2:\"94\";s:10:\"size_start\";s:1:\"6\";s:8:\"size_end\";s:4:\"18.1\";s:10:\"temp_start\";s:1:\"1\";s:8:\"temp_end\";s:2:\"10\";s:11:\"price_start\";s:1:\"0\";s:9:\"price_end\";s:6:\"250000\";}','2020-06-22','2020-06-22 20:04:24',NULL,NULL),(9,96,'a:7:{s:7:\"user_id\";s:2:\"96\";s:10:\"size_start\";s:2:\"11\";s:8:\"size_end\";s:2:\"19\";s:10:\"temp_start\";s:1:\"5\";s:8:\"temp_end\";s:2:\"10\";s:11:\"price_start\";s:1:\"0\";s:9:\"price_end\";s:6:\"250000\";}','2020-06-22','2020-06-22 21:45:56',NULL,NULL);

/*Table structure for table `user_ratings` */

DROP TABLE IF EXISTS `user_ratings`;

CREATE TABLE `user_ratings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `buyer_id` bigint(20) unsigned NOT NULL,
  `rating` varchar(255) DEFAULT '',
  `review` text,
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rating_u_fk` (`user_id`),
  KEY `rating_b_fk` (`buyer_id`),
  CONSTRAINT `rating_b_fk` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rating_u_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_ratings` */

/*Table structure for table `user_social_accounts` */

DROP TABLE IF EXISTS `user_social_accounts`;

CREATE TABLE `user_social_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `social_network` enum('facebook') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'facebook',
  `social_network_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_social_fk` (`user_id`),
  CONSTRAINT `user_social_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_social_accounts` */

/*Table structure for table `user_sold_posts` */

DROP TABLE IF EXISTS `user_sold_posts`;

CREATE TABLE `user_sold_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `buyer_id` bigint(20) unsigned NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `p_sld_fk` (`post_id`),
  KEY `buyer_sld_fk` (`user_id`),
  KEY `b_sld_fk` (`buyer_id`),
  CONSTRAINT `b_sld_fk` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `p_sld_fk` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `u_sld_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user_sold_posts` */

insert  into `user_sold_posts`(`id`,`user_id`,`post_id`,`buyer_id`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (1,95,52,94,'2020-06-22','2020-06-22 21:09:43',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `user_type` enum('admin','user') DEFAULT 'user',
  `signup_via` enum('email','facebook') DEFAULT 'email',
  `username` varchar(128) DEFAULT '',
  `image` varchar(255) DEFAULT '',
  `email` varchar(128) DEFAULT '',
  `password` varchar(128) DEFAULT '',
  `org_password` varchar(255) DEFAULT '',
  `phone` varchar(255) DEFAULT '',
  `access_token` text,
  `dob` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT '',
  `zipcode` varchar(255) DEFAULT '',
  `address` text,
  `latitude` varchar(255) DEFAULT '',
  `longitude` varchar(255) DEFAULT '',
  `verify_code` varchar(255) DEFAULT '',
  `verify_status` tinyint(1) DEFAULT '0',
  `about_me` longtext,
  `years_riding` varchar(255) DEFAULT '',
  `skill_level` varchar(255) DEFAULT '',
  `discipline` bigint(20) DEFAULT '0',
  `breed` bigint(20) DEFAULT '0',
  `firebase_id` text,
  `news_letter` int(1) DEFAULT '1',
  `is_notification` int(1) DEFAULT '1',
  `login_at` datetime DEFAULT NULL,
  `is_block` int(11) DEFAULT '0',
  `recover_password_key` text,
  `recover_attempt_at` datetime DEFAULT NULL,
  `account_visibility` enum('public','private') DEFAULT 'public',
  `visibility` int(1) DEFAULT '1',
  `created_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`parent_id`,`user_type`,`signup_via`,`username`,`image`,`email`,`password`,`org_password`,`phone`,`access_token`,`dob`,`gender`,`zipcode`,`address`,`latitude`,`longitude`,`verify_code`,`verify_status`,`about_me`,`years_riding`,`skill_level`,`discipline`,`breed`,`firebase_id`,`news_letter`,`is_notification`,`login_at`,`is_block`,`recover_password_key`,`recover_attempt_at`,`account_visibility`,`visibility`,`created_date`,`created_at`,`updated_at`,`deleted_at`) values (1,0,'admin','email','Super Admin','','admin@horsematch.com','$2y$10$KglMy.znNB6767KOPPIY3.Wrs5WNio3XpXJcPeGnszX2Pa55cT8O.','','56545','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvZG9Mb2dpbiIsImlhdCI6MTU5MDg0ODIxOSwiZXhwIjoxNjA2NjE2MjE5LCJuYmYiOjE1OTA4NDgyMTksImp0aSI6IlRoMEI5bzhWcnN5ZEhDRGYifQ.wYgx-cz-Y5F-HFatBY83VP6iYFdTUF6cSF5Mo1K-54Q',NULL,'',NULL,NULL,'','','',0,NULL,'','',0,0,NULL,1,1,'2019-07-10 06:55:26',0,NULL,NULL,'public',1,NULL,'2019-05-28 10:35:59','2020-05-30 14:16:59',NULL),(56,0,'user','email','testworking','','testworking@gmail.com','$2y$10$KwXG.JPCujZlwriSqbmjW.48l3CylFK0ptnbvMgNuwcRjlmsrtf02','Z3FuaWVRMnZyT2luRXByNjBqb0Z6QT09','123456','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1NiIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL2xvZ2luIiwiaWF0IjoxNTg5OTk5NTU1LCJleHAiOjE2MDU3Njc1NTUsIm5iZiI6MTU4OTk5OTU1NSwianRpIjoiazdoUktmMUpMeFppbzk5YyJ9.WEta-IFVQBefPexVal2kDuG1JotzC25q51Sk0s5rJmM',NULL,'','',NULL,'','','3896',0,NULL,'','',0,0,NULL,1,1,'2020-05-20 18:32:35',0,NULL,NULL,'public',1,'2020-05-16','2020-05-16 15:46:51','2020-05-20 18:32:35',NULL),(59,0,'user','email','apptests','','apptest@gmail.com','$2y$10$zoZTJLiubAu5Ica5CZSQUei99OvbpdSl.hYXDTj2dOQRLpTaAXhO6','aWUzWVBUQWZyZlNGdTBJLzk0MThpZz09','123','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1OSIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL2xvZ2luIiwiaWF0IjoxNTkwNDUwNzcyLCJleHAiOjE2MDYyMTg3NzIsIm5iZiI6MTU5MDQ1MDc3MiwianRpIjoiRWZXR3pkUHZLQXFtRm4wQiJ9.KM7hnrMgPfdBBOU1aa1JnVDOS4NAE6rsy9jY7ocPKz0',NULL,'','',NULL,'','','1869',1,NULL,'','',0,0,NULL,1,1,'2020-05-25 23:52:52',0,NULL,NULL,'public',1,'2020-05-18','2020-05-18 00:41:34','2020-05-25 23:52:52',NULL),(91,0,'user','email','personals','','personals@gmail.com','$2y$10$VkObEO7YsEwmrIdP9Wm3ReXyNgy1nFGJEV4F.HQRtlswfHm.HlXj2','OXZDamJEM1F5dEZQRitITStDVVMrZz09','+123456789','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5MSIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL3JlZ2lzdGVyIiwiaWF0IjoxNTkyODI0Nzg1LCJleHAiOjE2MDg1OTI3ODUsIm5iZiI6MTU5MjgyNDc4NSwianRpIjoiS2pDZXU1cmlxRkI2T01QeCJ9.K9NVpBGGtuKBtnXadhlUEM5KfE_rEV2RbXq-8qvg48s',NULL,'','',NULL,'','','7622',0,NULL,'','',0,0,NULL,1,1,'2020-06-22 11:19:45',0,NULL,NULL,'public',1,'2020-06-22','2020-06-22 11:19:45','2020-06-22 11:19:45',NULL),(92,0,'user','email','zaidy','','zaidy@gmail.com','$2y$10$3qUL93PeZVFdeatXl6DJKekjbFLe0Nk5NAbrs1bUVZ711pYTWnWWi','NXB0NkpwS0lCZkhFNU5UQ0ZVVjJBdz09','+923332724504','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5MiIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL2xvZ2luIiwiaWF0IjoxNTkyODYyNzc3LCJleHAiOjE2MDg2MzA3NzcsIm5iZiI6MTU5Mjg2Mjc3NywianRpIjoiWkZ0U0pYUlBYbDcwOHF0MiJ9.zIbQ2dvavrX-JA2n8_x4woaTrRWNnv_TJY1Ec7eNJSY',NULL,'','',NULL,'','','8012',0,NULL,'','',0,0,NULL,1,1,'2020-06-22 21:52:57',0,NULL,NULL,'public',1,'2020-06-22','2020-06-22 15:32:47','2020-06-22 21:52:57',NULL),(93,0,'user','email','zaidy seller','','zaidyseller@gmail.com','$2y$10$ij9aeu6eavu2.kLp2ty68ONmLDE/PJjdfj4U8hMIV702nMFUiD7u6','NXB0NkpwS0lCZkhFNU5UQ0ZVVjJBdz09','+923332724505','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5MyIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL3JlZ2lzdGVyIiwiaWF0IjoxNTkyODQwMjQ2LCJleHAiOjE2MDg2MDgyNDYsIm5iZiI6MTU5Mjg0MDI0NiwianRpIjoiVDdWdE1ERWJlTzFCMDlyNyJ9.BkcXBsHgV_Rq7QbRBn32_WxZ3bFNfhym-D-RM75iw7o',NULL,'','',NULL,'','','1939',0,NULL,'','',0,0,NULL,1,1,'2020-06-22 15:37:26',0,NULL,NULL,'public',1,'2020-06-22','2020-06-22 15:37:26','2020-06-22 15:37:26',NULL),(94,0,'user','email','Faisal','','faisal.akhter12@gmail.com','$2y$10$.bNqS1TqqigEzYh.buEkf.YyZnWE4BcvJu9MiHJccuKicYyzHo7Yy','NXB0NkpwS0lCZkhFNU5UQ0ZVVjJBdz09','+923312607440','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NCIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL3JlZ2lzdGVyIiwiaWF0IjoxNTkyODU2MDczLCJleHAiOjE2MDg2MjQwNzMsIm5iZiI6MTU5Mjg1NjA3MywianRpIjoia2NvdU1JTVl5WWZPR2k0TiJ9.OPc8icROhW2MFhQWyWtAN3M--dDxJsKS0vE2NvwhDdg',NULL,'','',NULL,'','','9995',0,NULL,'','',0,0,NULL,1,1,'2020-06-22 20:01:13',0,NULL,NULL,'public',1,'2020-06-22','2020-06-22 20:01:12','2020-06-22 20:01:13',NULL),(95,0,'user','email','Nicky Jones','','nicky@mailinator.com','$2y$10$PG3dDHF.983uq8C/tpgWKOfvmBx6A1AyokFUcODEFiSKRloZQNFNW','OFlDWTVNNVA3ZWF6Z1ppVE1FMURhdz09','+923343972904','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NSIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL3JlZ2lzdGVyIiwiaWF0IjoxNTkyODU2MzEyLCJleHAiOjE2MDg2MjQzMTIsIm5iZiI6MTU5Mjg1NjMxMiwianRpIjoiWGFINDllVjJnVTN5aHdzUiJ9.NPtP8wDweMOnJ52fLsvbK8swate-mlCnLRNI_kgRHP8',NULL,'','',NULL,'','','4175',0,NULL,'','',0,0,NULL,1,1,'2020-06-22 20:05:12',0,NULL,NULL,'public',1,'2020-06-22','2020-06-22 20:05:11','2020-06-22 20:05:12',NULL),(96,0,'user','email','John wick','','wick@mailinator.com','$2y$10$THSoE489lmQ6Ot.UtkFQK.t3Ek.vs6mqYlsxqlP6eVjlbUDgZa8em','OFlDWTVNNVA3ZWF6Z1ppVE1FMURhdz09','+19998536666666','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NiIsImlzcyI6Imh0dHA6Ly90ZWNoYXJhYnMuY29tL2hvcnNlbWF0Y2gvYXBpL2xvZ2luIiwiaWF0IjoxNTkyODYyMTE1LCJleHAiOjE2MDg2MzAxMTUsIm5iZiI6MTU5Mjg2MjExNSwianRpIjoiS2xLTTRFY29KcUxtcVNOUCJ9.7eYnkPRgP1s-uw3MFHYlgVPw66-JquMk1R2U1jsymJU',NULL,'','',NULL,'','','4794',0,NULL,'','',0,0,NULL,1,1,'2020-06-22 21:41:55',0,NULL,NULL,'public',1,'2020-06-22','2020-06-22 21:35:15','2020-06-22 21:41:55',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
