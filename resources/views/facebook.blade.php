<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> {{$horse->horsetitle}} Details</title>

    <meta property="fb:app_id" content="163357105629746" />
    <meta property="og:url" content="{{url()->current()}}" >
    <meta property="og:type" content="article" >
    <meta property="og:title" content=" " >
    <meta property="og:description" content=" " >
    @if($horse_images)
        <meta property="og:image" content="{{ asset('storage/app/postimages/'.$horse_images[0]->image) }}" >
        <meta property="og:image:width" content="400" >
        <meta property="og:image:height" content="300" >
    @endif
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

    <style>
        .carousel-inner img{
            width: 100%;
            height: 100%;
            object-fit: contain;
        }
        .carousel-inner{
            height: 350px;
        }
        body{
            background-image: url('http://horsematch.com/masterpanel/public/adminassets/images/bg.jpg');
            background-blend-mode: overlay;
            background-color: #00000000;
            font-family: 'poppinsregular';
        }
        .container-fluid{
            padding: 70px;
        }
        .horse-container{
            max-width: 1200px;
            margin-left: auto;
            margin-right: auto;
            background-color: #ffffff;
            {{--  background-color: #ffffffcf;  --}}
            padding: 50px;
            border-radius: 5px;
        }

        .heading{
            font-size: 30px;
        }

        .text-col{
            font-size: 18px;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        .carousel-item{
            border-radius: 10px;
            overflow: hidden;
        }
        .carousel-item img{
            border-radius: 10px;
        }

        .splide .splide__slide img{
            width: auto;
            max-width: 100%;
            /* width: 100%; */
            height: auto;
            max-height: 100%;
            object-fit: cover;
        }
        .splide__slide{
            border-radius: 10px;
        }
        .splide__slide img {
            width : 100% !important;
            height: auto;
        }

        .download-area-heading{
            font-size: 25px;
            font-weight: 700;
            text-align: center;
            margin-bottom: 15px;
            margin-top: 20px;
        }
        .download-area{
            margin-bottom: 15px;
        }
        .download-btns img{
            width: 160px;
            height: auto;
            margin-left: 10px;
            margin-right: 10px;
        }
        .download-btns{
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0 60px;
        }

        .user-image{
            width: 50px;
            border-radius: 200%;
        }
        .user-image-box{
            display: flex;
            justify-content: flex-start;
            align-items: center;
            padding-bottom: 10px;
        }
        .user-name-box{
            margin-left: 15px;
            font-weight: 500;
        }
        .address{
            font-size: 16px;
            color: dimgrey;
            padding-bottom: 10px;

            border-bottom: 1px solid #cecece;

        }
        .posted-date{
            font-size: 16px;
            color: dimgrey;
        }

        .details-heading{
            font-size: 25px;
            color: black;
            font-weight: 500;
            color: #2f75bb;
            margin-bottom: 10px;
        }

        .detail-item .item-name{
            width: 25%;
            color: #707070;
        }
        .headlines{
            margin-top: 15px;
            margin-bottom: 15px;
            text-align: center;
        }
        .detail-item{
            display: flex;
            justify-content: flex-start;
            align-items: center;
            border-bottom: 1px solid #cecece;
            padding: 6px 2px;
        }
        .splide-main-box{
            margin: 0;
            margin-top: 20%;
        }
        .video-option{
            float: right;
            background-color: #2f75bb;
            color: white;
            font-size: 16px;
            padding: 6px;
            border-radius: 200%;
            margin-top: 6px;
        }
        @media screen and (max-width:1215px){
            .download-btns{
                padding: 0;
            }
        }
        @media screen and (max-width:400px){
            .download-btns img{
                width: 130px;
            }    
        }
        @media screen and (max-width:820px){
            .container-fluid{
                padding: 0px;
            }
        }
        @media screen and (max-width:768px){
            .user-image-box{
                margin-top:15px;
            }
            .splide-main-box{
                margin: -15px;
            }
    
            .horse-container{
                padding: 15px;
                border-radius: 0;
            }

            .splide__slide{
                border-radius: 0 0 10px 10px;
            }
            .text-col{
                margin-top: 30px;
            }

            .heading{
                margin-top: 10px;
            }
            .detail-item .item-name{
                width: 30%;
                min-width: 130px;
            }
        }

    </style>
    
</head>
<body>

    @php
        //@dump($horse);
    @endphp
    <div class="container-fluid">
        <div class="horse-container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="splide-main-box">

                <div id="image-slider" class="splide">
                    <div class="splide__track">
                        <ul class="splide__list">

                            @foreach ($horse_images as $k => $image )
                            <li class="splide__slide">
                                <img src="{{ asset('storage/app/postimages/'.$image->image)}}">
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                </div>
                
                

                
            </div>
            
            <div class="col-sm-12 col-md-6 col-lg-6 text-col">



                <div class="user-image-box">
                    <div class="username-area">
                        @if($horse->userimage)
                        <img class="user-image" src="{{ asset('storage/app/postimages/'.$horse->userimage)}}" /> 
                        @else
                        <img class="user-image" src="http://dev-cmolds.com/horsematch/public/adminassets/pngfind.com-avatar-png-icon-3819326.png" /> 
                        @endif
                    </div>
                    <div class="user-name-box">
                        <div class="text-left"> {{ $horse->username }} </div>
                        {{--  <div class="posted-date text-left" > Posted on {{ \Carbon\Carbon::parse($horse->posted_date)->format('D M Y') }} </div>  --}}
                        <div class="posted-date text-left" > Posted on: {{ \Carbon\Carbon::parse($horse->posted_date)->formatLocalized('%d-%b-%Y') }} </div>
                    </div>
                </div>

                <div class="heading">  {{ $horse->horsetitle }} </div>

                <div class="address">  <i class="fas fa-map-marker-alt"></i> {{ $horse->address }} </div>
                
                <div class="headlines">
                    {{$horse->headline}}
                </div>

                <div class="details-heading">
                    Horse Details

                    @if($horse->link)
                        <a target="_blank" href="{{$horse->link}}"><i class="fas fa-video video-option"></i></a>
                    @endif

                </div>
                
                <div class="detail-item">
                    <div class="item-name">
                        Breed:
                    </div>

                    <div class="item-value">
                        {{$horse->breedtitle}}
                    </div>
                </div>
                
                <div class="detail-item">
                    <div class="item-name">
                        Size:
                    </div>

                    <div class="item-value">
                        {{$horse->size}}
                    </div>
                </div>
                
                <div class="detail-item">
                    <div class="item-name">
                        Price:
                    </div>

                    <div class="item-value">
                        ${{$horse->price}}
                    </div>
                </div>
                <div class="detail-item">
                    <div class="item-name">
                        Color:
                    </div>

                    <div class="item-value">
                        {{$horse->color}}
                    </div>
                </div>
                
                <div class="detail-item">
                    <div class="item-name">
                        Gender:
                    </div>

                    <div class="item-value">
                        {{$horse->gender}}
                    </div>
                </div>
                 <div class="detail-item">
                    <div class="item-name">
                        Temperament:
                    </div>

                    <div class="item-value">
                        {{$horse->temperament}}
                    </div>
                </div>
                @if($horse->discipline)
                <div class="detail-item">
                    <div class="item-name">
                        Discipline:
                    </div>

                    <div class="item-value">
                        {{$horse->discipline}}
                    </div>
                </div>
                @endif
                <div class="detail-item">
                    <div class="item-name">
                        Lease:
                    </div>

                    <div class="item-value">
                        @if($horse->is_lease)
                            Yes
                        @else
                            No
                        @endif
                    </div>
                </div>

                {{--  <div class="phone">  Owner Contact : <b> {{ $horse->phone }} </b> </div>
                <div class="breedtitle">  Breed : <b>  {{ $horse->breedtitle }} </b> </div>
                <div class="temperament"> Temperament : <b> {{ $horse->temperament }} </b> </div>
                <div class="price">  Price : <b>  {{ $horse->price }} </b> </div>
                <div class="size">  Size : <b> {{ $horse->size }} </b> </div>
                <div class="gender">  Gender : <b> {{ $horse->gender }} </b> </div>
                <div class="address">  Location:  <b> {{ $horse->address }} </b> </div>  --}}

                <div class="download-area">
                    <div class="download-area-heading">Download Now</div>
                    <div class="download-btns">
                        <a href="https://play.google.com/store/apps/details?id=com.horsematch">
                            <img src="{{asset('public/images/store_google.png')}}" />
                        </a>
                        <a href="https://apps.apple.com/app/horse-match/id1545966083">
                            <img src="{{asset('public/images/store_apple.png')}}" />
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


    
       
    </body>
    

    <script>

            new Splide( '#image-slider' , {
                type       : 'slide',
                heightRatio: 0.5,
                pagination : false,
                arrows     : true,
                cover      : true,
                height      : 410,
                breakpoints : {
                    '450': {
                        height    : 300,
                    },
                    '768': {
                        height    : 350,
                    }
                },

            } ).mount();
                //$('.carousel').carousel();
    </script>
</html>



{{-- <html>
<head>
<title>Your Website Title</title>
<!-- You can use Open Graph tags to customize link previews.
Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
<meta property="og:url"           content="http://site.openbazar.nl/facebook" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Your Website Title" />
<meta property="og:description"   content="Your description" />
<meta property="og:image"         content="https://cdn.pixabay.com/photo/2021/02/05/21/10/church-5985941_960_720.jpg" />
</head>
<body>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your share button code -->
<div class="fb-share-button" 
data-href="http://site.openbazar.nl/facebook" 
data-layout="button">
</div>

</body>
</html>
 --}}