@include('admin.layouts.header')

<!-- ############ Content START-->
<div class="app" id="app">
    <div id="addpromotion" class="modal fade show" data-backdrop="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Send Promotion
                        <a data-dismiss="modal" class="ml-auto pull-right">
                            <span class="text-lg l-h-1x" title="" data-toggle="tooltip" data-original-title="Close">×</span>
                        </a>
                    </h5>

                </div>
                <form id="addblog" method="post" action="{{url('sendpromotions')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body custom_modelview">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="inputmodal">
                                    <label class="col-form-label">Promotion Message</label>
                                    <textarea id="promotiontextlimit" maxlength="160" style="font-family:'Poppins';" required class="form-control changecolor" name="description" placeholder="Enter Promotion Message"></textarea>
                                    <div class="pull-right"><label>
                                            <span id="textlimit">160</span>/160
                                        </label>
                                    </div>
{{--                                    <input name="description" type="text" class="changecolor form-control"--}}
{{--                                           placeholder="Blog Title" required="" min="15">--}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">

                                <div class="modal-footer">
                                    <div id="msg"></div>
                                    <a href="javascript:;" data-dismiss="modal" class="cancelBtn">Cancel</a>
                                    <button type="submit" href="javascript:;" class="confirmBtn">Publish
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- ############ LAYOUT START-->

    <!-- ############ Aside START-->
    <div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
        <div class="sidenav modal-dialog dk">
            <!-- sidenav top -->
            <div class="navbarLogo lt">
                <!-- brand -->
                <a href="dashboard" class="navbar-brand">

                    <img src="{{asset('public/adminassets/images/adminlogo2.png')}}" alt=".">

                </a>
                <!-- / brand -->
            </div>

            <!-- Flex nav content -->
            <div class="flex hide-scroll">
                <div class="scroll">
                    <div class="nav-border" data-nav>
                        <ul class="nav bg">


                            <li>
                                <a href="dashboard">
		                  <span class="nav-icon">
		                    <i class="fa fa-dashboard"></i>
		                  </span>
                                    <span class="nav-text">Dashboard</span>
                                </a>
                            </li>

                            <li>
                                <a href="users">
		                  <span class="nav-icon">
		                    <i class="fa fa-users"></i>
		                  </span>
                                    <span class="nav-text">User Management</span>
                                </a>
                            </li>
                    <!--                                                                <li>-->
                    <!--            <a href="{{url('teammanagement')}}">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fas fa-user-friends"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Team Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->
                            
                                                        <li>
                                <a href="{{url('blog-management')}}">
		                  <span class="nav-icon">
		                    <i class="fas fa-blog"></i>
		                  </span>
                                    <span class="nav-text">Blogs Management</span>
                                </a>
                            </li>
                                                        <li>
                                <a href="{{url('content')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-sticky-note"></i>
		                  </span>
                                    <span class="nav-text">Content Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('sellinghorsesnew')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-horse"></i>
		                  </span>
                                    <span class="nav-text">Selling Horse</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('reportedads')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-lock"></i>
		                  </span>
                                    <span class="nav-text">Reported Ads</span>
                                </a>
                            </li>
                    <!--        <li>-->
                    <!--            <a href="blogs">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fa fa-newspaper-o" aria-hidden="true"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Blogs Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->

                            <li class="active">
                                <a href="promotions">
		                  <span class="nav-icon">
		                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
		                  </span>
                                    <span class="nav-text">Promotions Management</span>
                                </a>
                            </li>

                            <li>
                                <a href="packages">
		                  <span class="nav-icon">
		                    <i class="fa fa-money"></i>
		                  </span>
                                    <span class="nav-text">Packages Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="subscription">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Subscription Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="paymenthistory">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Payment History</span>
                                </a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--		                  <span class="nav-icon">--}}
{{--		                    <i class="fa fa-file-text"></i>--}}
{{--		                  </span>--}}
{{--                                    <span class="nav-text">Packages Management</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <li class="pb-2 hidden-folded"></li>
                        </ul>

                    </div>
                </div>
            </div>
                        <div class='no-shrink lt'>
            <div class='nav-fold'>
                <div class='hidden-folded flex p-2-3 bg'>
                    <div class='d-flex p-1'>
                        <a onclick="logout();" href='javascript:;' class='px-2 logout' data-toggle='tooltip' title='' data-original-title='Logout'>
                            <i class='fa fa-sign-out' aria-hidden='true'></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
          </div>

        </div>
    </div>
    <!-- ############ Aside END-->
    <div id="content" class="app-content box-shadow-4 box-radius-4" role="main">
        <!-- Header -->
        <div class="content-header orangeBg box-shadow-4" id="content-header">
            <div class="navbar navbar-expand-lg">
                <!-- btn to toggle sidenav on small screen -->
                <a class="d-lg-none" data-toggle="modal" data-target="#aside">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg>
                </a>
                <!-- Page title -->

                <div class="navbar-text nav-title flex" id="pageTitle">
                    <div class="headerHeading">
                        <h6>Welcome</h6>
                        <h1>Summer Gentry</h1>
                    </div>
                </div>


                <ul class="nav flex-row order-lg-2">
                    <!-- Notification -->
                    <li class="nav-item dropdown" style="width: 20px;">
                        <a title="Notifications" style="right:30px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" data-toggle="dropdown" aria-expanded="false">
                            {{-- <i class="fa fa-bell" aria-hidden="true"></i> --}}
                            <span class="badge badge-pill up danger"></span>
                        </a>
                        <!-- dropdown -->
                        <!--<div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0 notify">-->
                        <!--    <div class="scrollable hover" style="max-height: 250px">-->
                        <!--        <div class="list">-->
                        <!--            <div class="list-item " data-id="item-11">-->
                        <!--    <span class="w-24 avatar circle blue">-->
                        <!--        <span class="fa fa-code-fork"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Tiger Nixon</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Looking for some client-work-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">16:00</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-2">-->
                        <!--    <span class="w-24 avatar circle light-blue">-->
                        <!--        <span class="fa fa-git"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Kygo</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        What&#x27;s the project progress now-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">08:05</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-12">-->
                        <!--    <span class="w-24 avatar circle green">-->
                        <!--        <span class="fa fa-dot-circle-o"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Ashton Cox</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Looking for some client-work-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">11:30</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-4">-->
                        <!--    <span class="w-24 avatar circle pink">-->
                        <!--        <span class="fa fa-male"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Judith Garcia</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Eddel upload a media-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">12:05</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-14">-->
                        <!--    <span class="w-24 avatar circle brown">-->
                        <!--        <span class="fa fa-bell"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Brielle Williamson</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Looking for some client-work-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">08:00</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-9">-->
                        <!--    <span class="w-24 avatar circle cyan">-->
                        <!--        <span class="fa fa-puzzle-piece"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Pablo Nouvelle</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        It&#x27;s been a Javascript kind of day-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">15:00</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--    <div class="d-flex px-3 py-2 b-t">-->
                        <!--        <div class="flex">-->
                        <!--            <span>6 Notifications</span>-->
                        <!--        </div>-->
                        <!--        <a href="#">See all <i class="fa fa-angle-right text-muted"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <div id="activity-dropdown"
                             class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
                            <!-- <label class=“switch”>
                              <input type='checkbox' class=“primary”>
                              <span class=“slider round”></span>
                            </label> -->
                            <div class="loaderNotif" style="display: none;"></div>
                            <div class="body_notification" style="height: 250px;">
                                <div class="scrollable hover notif-padding" style="max-height: 250px">
                                    <!-- <div class='list'>
                                    </div> -->
                                    <div class="notifi-left">
                                        <p class="notify_para">Notifications</p>
                                        <label class="switch">
                                            <input id="notif_toggle" type="checkbox" checked="">
                                            <span id="notif_slid" title="No" class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="noti-right" style="display: block;">
                                        <a href="javascript:;" id="remove_notif" style="display: none;">Mark all as
                                            read</a>
                                    </div>
                                </div>
                                <!--<div class=“d-flex py-2 b-t infonoti cpadding-noti” style=“background: #ececec91;“>
                                  <div class=“flex”>
                                    <span >Notifications</span>
                                  </div>
                                </div> -->
                                <div class="list" style="display: block;">
                                    <div class="no-havenotification"><img
                                                src="http://werdigi.com/public/assets/images/notifications.gif"
                                                class="img-fluid" alt="">
                                        <h2>No Notifications</h2>                                <h4>You currently have
                                            no notifications</h4></div>
                                </div>
                            </div>
                            <!-- / dropdown -->
                        </div>
                        <!-- / dropdown -->
                    </li>




                    <a title="Change Password" style="right:20px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" onclick="changepasswordmodal();">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="badge badge-pill up danger"></span>
                    </a>

                    <!-- User dropdown menu -->
                    <li class="dropdown d-flex align-items-center">
                        <a title="Logout" class="dropdown-item" onclick="logout()" href="#" style="background: transparent !important;border-bottom:none;color:white !important;padding: 0;margin:0">
                            <i class="fa fa-sign-out" aria-hidden="true" style="color: white !important;">
                            </i>
                        </a>
                    </li>
                    <!-- Navarbar toggle btn -->
                    <li class="d-lg-none d-flex align-items-center">
                        <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
                                <path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z"></path>
                            </svg>
                        </a>
                    </li>
                </ul>
                <!-- Navbar collapse -->

            </div>
        </div>
        <!-- Main -->
        <div class="content-main " id="content-main" style="margin-left: 20px">
            <div class="padding">
                <div class="row no-gutters box">
                    <div class="col-sm-12">
                        <div class="paddingBoxHead">
                            <div class="teamMangementHead">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <h5>Promotions Management</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary send-promotion" data-toggle="modal" data-target="#addpromotion">Send New Promotion</button>
                                    </div>
                                    <!--<div class="col-sm-2">-->
                                    <!-- <a href="javascript:;" onclick="add()" data-toggle="modal" data-target="#addLeadModal">Add Lead</a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box2">
                    <div class="">

                    <form id="" action="" autocomplete="off">
                    <table 
                        id="table"
                        data-toggle="table"
                        data-show-export="true"
                        data-show-columns="true"
                        data-search="true"
                        data-pagination="true"
                        data-page-list="[10, 25, 50, 100, ALL]"        
                        class="responsive">
                    <thead>
                        <tr>
                            <th data-sortable="true">ID</th>
                            <th data-sortable="true">Promotion Text</th>
                            <th data-sortable="true">Users Count</th>
                            <th data-sortable="true">Promotion Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($promotions as $promotion)
                            <tr style="height: 45px;">
                                <td style="">{{$promotion->id}}</td>
                                <td style="">{{$promotion->promotion_text}}</td>
                                <td style="">{{$promotion->sent_users}} 
                                @if($promotion->sent_users == $promotion->number_of_users)
                                <a href="#" data-toggle="tooltip" title="Sent to all users">
                                    <i class="fas fa-check ml-1 text-success"></i> 
                                </a>
                                @endif
                                </td>
                                {{--  <td style="">{{$promotion->number_of_users}}</td>  --}}
                                <td style="">{{\Carbon\Carbon::parse($promotion->created_at)->formatLocalized('%d-%b-%Y')}}</td>
                                <td style="">

                                    <!--<a href="javascript:;" onclick="sendpromotion({{$promotion->id}});" data-id="active" class="viewBtn viewUserBtn" data-toggle="modal" data-target="#confirmationDelModal"><i class="fa fa-redo" title="Resend" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Resend</span></a>-->
                                    <a href="javascript:;" onclick="deletepromotion({{$promotion->id}})" data-id="active" class="deleteBtn confirmDel" data-toggle="modal" data-target="#confirmationdeletepromotion"><i class="fa fa-trash-o" title="Delete" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Delete</span></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                </table>
            </form>
            </div>
        </div>



            </div>
        </div>

        <div id="confirmationDelModal" class="modal fade show" data-backdrop="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation</h5>
                    </div>
                    <input type="hidden" id="brand_id" value="XR02EnLZ5o">
                    <input type="text" id="useriddelete" style="display:none;">
                    <input type="hidden" id="newstatus" value="">
                    <div class="modal-body text-center padding40 custom_modelview">
                        <input type="text" style="display: none" id="confirmid">
                        <p>Are you sure you want to resend this promotion?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" data-dismiss="modal" class="cancelBtn">Cancel</a>
                        <a id="resendpromobtn" href="javascript:;" onclick="resendpromotion();" class="confirmBtn remove">Confirm</a>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>


        <div id="confirmationdeletepromotion" class="modal fade show" data-backdrop="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation</h5>
                    </div>
                    <div class="modal-body text-center padding40 custom_modelview">
                        <input type="text" style="display: none" id="confirmdelid">
                        <p>Are you sure you want to delete this promotion?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" data-dismiss="modal" class="cancelBtn">Cancel</a>
                        <a href="javascript:;" onclick="deletepromo();" class="confirmBtn remove">Confirm</a>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
        <div class="content-footer white " id="content-footer">
            <script>
                $(document).ready(function(){
                    var maxLength = 160;
                    $("#promotiontextlimit").on("keyup",function (){
                        var textlen = maxLength - $(this).val().length;
                        $("#textlimit").text(textlen);
                    });
                    $("#searchusers").on("keyup", function() {
                        var value = $(this).val().toLowerCase();
                        $("tbody tr").filter(function() {
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                });
                function sendpromotion(id){
                    $("#confirmid").val(id);
                }


                function resendpromotion(){
                    $("#resendpromobtn").attr("disabled");
                    iziToast.success({
                        title: 'Success',
                        message: 'Promotion Sent Successfully',
                    });
                    var id = $("#confirmid").val();
                    $.ajax({
                        url: "resendpromotion",
                        type: "post",
                        data: {"id":id} ,
                        success: function (response) {
                            window.location.href = window.location.href;
                            // You will get response from your PHP page (what you echo or print)
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                    });
                }

                function deletepromotion(id){
                    $("#confirmdelid").val(id);
                }
                function deletepromo(){
                    var id = $("#confirmdelid").val();

                    $.ajax({
                        url: "deletepromo",
                        type: "post",
                        data: {"id":id} ,
                        success: function (response) {
                            window.location.href = window.location.href;
                            // You will get response from your PHP page (what you echo or print)
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                    });
                }
            </script>
            <style>
                .send-promotion {
                    background: linear-gradient(45deg, #3178BF 0%, #3178bf8a 100%);
                    display: inline-block;
                    width: 100%;
                    border-radius: 6px;
                    text-align: center;
                    color: #fff;
                    padding: 10px 0;
                    font-size: 14px;
                }
                textarea.form-control.changecolor::placeholder {
                    color: grey !important;
                }
            </style>
@extends('admin.layouts.footer')
            <script src="{{asset('public/adminassets/libs/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{asset('public/adminassets/libs/moment/min/moment-with-locales.min.js')}}"></script>
            <script src="{{asset('public/adminassets/libs/chart.js/dist/Chart.min.js')}}"></script>
            <script src="{{asset('public/adminassets/scripts/plugins/jquery.chart.js')}}"></script>
            <script src="{{asset('public/adminassets/scripts/plugins/chartjs.js')}}"></script>
            <script src="{{asset('public/adminassets/scripts/lazyload.config.js')}}"></script>
            <script src="{{asset('public/adminassets/scripts/lazyload.js')}}"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">

@if(Session::has('message'))
                    <script>
                        $(document).ready(function (){
                            console.log('fired');
                            iziToast.success({
                                title: 'Success',
                                message: '{{ Session::get('message') }}',
                            });
                        });
                    </script>
@php(Session::forget('message'))

@endif
            <script>
                function changepasswordmodal(){
                    $("#changepasswordmodal").modal('show');
                }
                function changepassword(){
                    var password = $("#password").val();
                    var confirmpass = $("#confirmpassword").val();
                    if(password===confirmpass){
                        $.ajax({
                            url: "{{url('changepass')}}",
                            type: "post",
                            data: {"password":password},
                            success: function (response) {
                                console.log(response);
                                iziToast.success({
                                    title: 'Success',
                                    message:  response.message ,
                                });
                                setTimeout(function(){ location.reload() }, 4000);
                                // You will get response from your PHP page (what you echo or print)
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                        });
                    }
                    else{
                        iziToast.error({
                            title: 'Error',
                            message:  "Password Did Not Matched" ,
                        });
                    }
                }
            </script>