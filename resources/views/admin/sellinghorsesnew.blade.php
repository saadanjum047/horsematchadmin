@include('admin.layouts.header')
<!-- Confirmation modal -->
<div id="confirmationModal" class="modal fade" data-backdrop="true" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
            </div>
            <div class="modal-body text-center padding40">
                <p>Are you sure you want to block <br> this user?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" data-dismiss="modal" class="cancelBtn">Cancel</a>
                <a href="javascript:;" class="confirmBtn" onclick="changeUserStatus();">Confirm</a>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>

<!-- ############ Content START-->
<div class="app" id="app">

    <!-- ############ LAYOUT START-->

    <!-- ############ Aside START-->
    <div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
        <div class="sidenav modal-dialog dk">
            <!-- sidenav top -->
            <div class="navbarLogo lt">
                <!-- brand -->
                <a href="dashboard" class="navbar-brand">

                    <img src="{{asset('public/adminassets/images/adminlogo2.png')}}" alt=".">

                </a>
                <!-- / brand -->
            </div>

            <!-- Flex nav content -->
            <div class="flex hide-scroll">
                <div class="scroll">
                    <div class="nav-border" data-nav>
                        <ul class="nav bg">


                            <li>
                                <a href="dashboard">
		                  <span class="nav-icon">
		                    <i class="fa fa-dashboard"></i>
		                  </span>
                                    <span class="nav-text">Dashboard</span>
                                </a>
                            </li>

                            <li>
                                <a href="users">
		                  <span class="nav-icon">
		                    <i class="fa fa-users"></i>
		                  </span>
                                    <span class="nav-text">User Management</span>
                                </a>
                            </li>
                    <!--                                                                <li>-->
                    <!--            <a href="{{url('teammanagement')}}">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fas fa-user-friends"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Team Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->
                            
                                                        <li>
                                <a href="{{url('blog-management')}}">
		                  <span class="nav-icon">
		                    <i class="fas fa-blog"></i>
		                  </span>
                                    <span class="nav-text">Blogs Management</span>
                                </a>
                            </li>
                                                        <li>
                                <a href="{{url('content')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-sticky-note"></i>
		                  </span>
                                    <span class="nav-text">Content Management</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="{{url('sellinghorsesnew')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-horse"></i>
		                  </span>
                                    <span class="nav-text">Selling Horse</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('reportedads')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-lock"></i>
		                  </span>
                                    <span class="nav-text">Reported Ads</span>
                                </a>
                            </li>
                    <!--        <li>-->
                    <!--            <a href="blogs">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fa fa-newspaper-o" aria-hidden="true"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Blogs Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->

                            <li>
                                <a href="promotions">
		                  <span class="nav-icon">
		                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
		                  </span>
                                    <span class="nav-text">Promotions Management</span>
                                </a>
                            </li>

                            <li>
                                <a href="packages">
		                  <span class="nav-icon">
		                    <i class="fa fa-money"></i>
		                  </span>
                                    <span class="nav-text">Packages Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="subscription">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Subscription Management</span>
                                </a>
                            </li>

                            <li>
                                <a href="paymenthistory">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Payment History</span>
                                </a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--		                  <span class="nav-icon">--}}
{{--		                    <i class="fa fa-file-text"></i>--}}
{{--		                  </span>--}}
{{--                                    <span class="nav-text">Packages Management</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <li class="pb-2 hidden-folded"></li>
                        </ul>

                    </div>
                </div>
            </div>
                        <div class='no-shrink lt'>
            <div class='nav-fold'>
                <div class='hidden-folded flex p-2-3 bg'>
                    <div class='d-flex p-1'>
                        <a onclick="logout();" href='javascript:;' class='px-2 logout' data-toggle='tooltip' title='' data-original-title='Logout'>
                            <i class='fa fa-sign-out' aria-hidden='true'></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- ############ Aside END-->
    <div id="content" class="app-content box-shadow-4 box-radius-4" role="main">
        <!-- Header -->
        <div class="content-header orangeBg box-shadow-4" id="content-header">
            <div class="navbar navbar-expand-lg">
                <!-- btn to toggle sidenav on small screen -->
                <a class="d-lg-none" data-toggle="modal" data-target="#aside">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg>
                </a>
                <!-- Page title -->

                <div class="navbar-text nav-title flex" id="pageTitle">
                    <div class="headerHeading">
                        <h6>Welcome</h6>
                        <h1>Summer Gentry</h1>
                    </div>
                </div>


                <ul class="nav flex-row order-lg-2">
                    <!-- Notification -->
                    <li class="nav-item dropdown" style="width: 20px;">
                        <a title="Notifications" style="right:30px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                            <span class="badge badge-pill up danger"></span>
                        </a>
                        <!-- dropdown -->
                        <!--<div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0 notify">-->
                        <!--    <div class="scrollable hover" style="max-height: 250px">-->
                        <!--        <div class="list">-->
                        <!--            <div class="list-item " data-id="item-11">-->
                        <!--    <span class="w-24 avatar circle blue">-->
                        <!--        <span class="fa fa-code-fork"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Tiger Nixon</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Looking for some client-work-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">16:00</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-2">-->
                        <!--    <span class="w-24 avatar circle light-blue">-->
                        <!--        <span class="fa fa-git"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Kygo</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        What&#x27;s the project progress now-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">08:05</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-12">-->
                        <!--    <span class="w-24 avatar circle green">-->
                        <!--        <span class="fa fa-dot-circle-o"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Ashton Cox</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Looking for some client-work-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">11:30</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-4">-->
                        <!--    <span class="w-24 avatar circle pink">-->
                        <!--        <span class="fa fa-male"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Judith Garcia</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Eddel upload a media-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">12:05</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-14">-->
                        <!--    <span class="w-24 avatar circle brown">-->
                        <!--        <span class="fa fa-bell"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Brielle Williamson</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        Looking for some client-work-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">08:00</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="list-item " data-id="item-9">-->
                        <!--    <span class="w-24 avatar circle cyan">-->
                        <!--        <span class="fa fa-puzzle-piece"></span>-->
                        <!--    </span>-->
                        <!--                <div class="list-body">-->
                        <!--                    <a href="" class="item-title _500">Pablo Nouvelle</a>-->

                        <!--                    <div class="item-except text-sm text-muted h-1x">-->
                        <!--                        It&#x27;s been a Javascript kind of day-->
                        <!--                    </div>-->

                        <!--                    <div class="item-tag tag hide">-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div>-->
                        <!--                    <span class="item-date text-xs text-muted">15:00</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--    <div class="d-flex px-3 py-2 b-t">-->
                        <!--        <div class="flex">-->
                        <!--            <span>6 Notifications</span>-->
                        <!--        </div>-->
                        <!--        <a href="#">See all <i class="fa fa-angle-right text-muted"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <div id="activity-dropdown"
                             class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
                            <!-- <label class=“switch”>
                              <input type='checkbox” class=“primary”>
                              <span class=“slider round”></span>
                            </label> -->
                            <div class="loaderNotif" style="display: none;"></div>
                            <div class="body_notification" style="height: 250px;">
                                <div class="scrollable hover notif-padding" style="max-height: 250px">
                                    <!-- <div class='list'>
                                    </div> -->
                                    <div class="notifi-left">
                                        <p class="notify_para">Notifications</p>
                                        <label class="switch">
                                            <input id="notif_toggle" type="checkbox" checked="">
                                            <span id="notif_slid" title="No" class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="noti-right" style="display: block;">
                                        <a href="javascript:;" id="remove_notif" style="display: none;">Mark all as
                                            read</a>
                                    </div>
                                </div>
                                <!--<div class=“d-flex py-2 b-t infonoti cpadding-noti” style=“background: #ececec91;“>
                                  <div class=“flex”>
                                    <span >Notifications</span>
                                  </div>
                                </div> -->
                                <div class="list" style="display: block;">
                                    <div class="no-havenotification"><img
                                                src="http://werdigi.com/public/assets/images/notifications.gif"
                                                class="img-fluid" alt="">
                                        <h2>No Notifications</h2>                                <h4>You currently have
                                            no notifications</h4></div>
                                </div>
                            </div>
                            <!-- / dropdown -->
                        </div>
                        <!-- / dropdown -->
                    </li>




                    <a title="Change Password" style="right:20px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" onclick="changepasswordmodal();">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="badge badge-pill up danger"></span>
                    </a>

                    <!-- User dropdown menu -->
                    <li class="dropdown d-flex align-items-center">
                        <a title="Logout" class="dropdown-item" onclick="logout()" href="#" style="background: transparent !important;border-bottom:none;color:white !important;padding: 0;margin:0">
                            <i class="fa fa-sign-out" aria-hidden="true" style="color: white !important;">
                            </i>
                        </a>
                    </li>
                    <!-- Navarbar toggle btn -->
                    <li class="d-lg-none d-flex align-items-center">
                        <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
                                <path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z"></path>
                            </svg>
                        </a>
                    </li>
                </ul>
                <!-- Navbar collapse -->

            </div>
        </div>
        <!-- Main -->
        <div class="content-main " id="content-main">

            <!-- ############ Main START-->
            <div class="padding">
                
                
                <div class="row no-gutters box">
          <div class="col-sm-12">
            <div class="paddingBoxHead">
              <div class="teamMangementHead">
                <div class="row">
                  <div class="col-sm-12">
                    <h5>Selling Horses</h5>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
 <div class="box2">
         
         <div class="table-responsive">
   <div class="bootstrap-table">
      <div class="fixed-table-toolbar">
{{--         <div class="columns columns-right btn-group pull-right">--}}
{{--            <div class="keep-open btn-group" title="Columns">--}}
{{--               <button type="button" aria-label="columns" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th icon-th"></i> <span class="caret"></span></button>--}}
{{--               <ul class="dropdown-menu" role="menu">--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="0" value="0" checked="checked"> Logo</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="1" value="1" checked="checked"> Brand</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="2" value="2" checked="checked"> Full Name</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="3" value="3" checked="checked"> Email</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="4" value="4" checked="checked"> WhatsApp No.</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="5" value="5" checked="checked"> Total<br>Request</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="6" value="6" checked="checked"> Monthly<br>Request</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="7" value="7" checked="checked"> Contract<br>Sent</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="8" value="8" checked="checked"> Apps<br>Sold</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="9" value="9" checked="checked"> Status</label></li>--}}
{{--                  <li role="menuitem"><label><input type="checkbox" data-field="10" value="10" checked="checked"> Action</label></li>--}}
{{--               </ul>--}}
{{--            </div>--}}
{{--            <div class="export btn-group">--}}
{{--               <button class="btn btn-default dropdown-toggle" aria-label="export type" title="Export data" data-toggle="dropdown" type="button"><i class="glyphicon glyphicon-export icon-share"></i> <span class="caret"></span></button>--}}
{{--               <ul class="dropdown-menu" role="menu">--}}
{{--                  <li role="menuitem" data-type="json"><a href="javascript:void(0)">JSON</a></li>--}}
{{--                  <li role="menuitem" data-type="xml"><a href="javascript:void(0)">XML</a></li>--}}
{{--                  <li role="menuitem" data-type="csv"><a href="javascript:void(0)">CSV</a></li>--}}
{{--                  <li role="menuitem" data-type="txt"><a href="javascript:void(0)">TXT</a></li>--}}
{{--                  <li role="menuitem" data-type="sql"><a href="javascript:void(0)">SQL</a></li>--}}
{{--                  <li role="menuitem" data-type="excel"><a href="javascript:void(0)">MS-Excel</a></li>--}}
{{--               </ul>--}}
{{--            </div>--}}
{{--         </div>--}}
         <div class="pull-right search"><input id="searchusers" class="form-control" type="text" placeholder="Search"></div>
      </div>
      <div class="fixed-table-container" style="padding-bottom: 0px;">
         <div class="fixed-table-header" style="display: none;">
            <table></table>
         </div>
         <div class="fixed-table-body">
            <div class="fixed-table-loading" style="top: 66px;">Loading, please wait...</div>
            <table class="table table-hover" data-plugin="bootstrapTable" id="brandtable" data-search="true" data-pagination="true" data-show-columns="true" data-show-export="true" data-detail-view="false">
               <thead>
                  <tr>
                     <th style="" data-field="0">
                        <div class="th-inner">Profile Image</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="1">
                        <div class="th-inner">Owner Name</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="2">
                        <div class="th-inner">Owner Contact</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="3">
                        <div class="th-inner">Horse Name</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="4">
                        <div class="th-inner">Breed</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="5">
                        <div class="th-inner">Size (hands)</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="6">
                        <div class="th-inner">Price ($)</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="7">
                        <div class="th-inner">Gender</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="8">
                        <div class="th-inner">Temperament</div>
                        <div class="fht-cell"></div>
                     </th>
{{--                     <th style="" data-field="9">--}}
{{--                        <div class="th-inner">Discipline</div>--}}
{{--                        <div class="fht-cell"></div>--}}
{{--                     </th>--}}
{{--                     <th style="" data-field="10">--}}
{{--                        <div class="th-inner">Location</div>--}}
{{--                        <div class="fht-cell"></div>--}}
{{--                     </th>--}}
                     <th style="" data-field="11">
                        <div class="th-inner">Date Posted</div>
                        <div class="fht-cell"></div>
                     </th>
                     <th style="" data-field="12">
                        <div class="th-inner">Action</div>
                        <div class="fht-cell"></div>
                     </th>
                     
                    
                  </tr>
               </thead>
               <tbody>
               @foreach($data as $sellinghorses)
                  <tr data-index="0" style="height: 45px">
                     <td style="">
                        <span class="avatar w-40">
                            @if($sellinghorses->userimages)
                        <img src="storage/app/users/{{$sellinghorses->userimages}}" alt="...">
                            @else
                            <img src="http://dev-cmolds.com/horsematch/public/adminassets/pngfind.com-avatar-png-icon-3819326.png">
                                @endif

                        </span>
                     </td>
                     <td style="">{{$sellinghorses->username}}</td>
                     <td style=""> {{$sellinghorses->phone}}</td>
                     <td style="">{{($sellinghorses->horsetitle=="")?"N/A":$sellinghorses->horsetitle}}</td>
                     <td style=""> {{($sellinghorses->breedtitle=="")?"N/A":$sellinghorses->breedtitle}}</td>
                     <td style=""> {{($sellinghorses->size=="")?"N/A":$sellinghorses->size}}</td>
{{--                     <td style=""> {{($sellinghorses->price=="")?"N/A":$sellinghorses->price}}</td>--}}
                     <td style=""> {{($sellinghorses->price=="")?"N/A":number_format($sellinghorses->price, 2, '.', ',')}}</td>
                     <td style=""> {{($sellinghorses->gender=="")?"N/A":$sellinghorses->gender}}</td>
                     <td style=""> {{($sellinghorses->temperament=="")?"N/A":$sellinghorses->temperament}}</td>
{{--                     <td style=""> {{($sellinghorses->discipline=="")?"N/A":$sellinghorses->discipline}}</td>--}}
                      <td style="">{{\Carbon\Carbon::parse($sellinghorses->created_at)->formatLocalized('%d-%b-%Y')}}</td>
{{--                     <td style=""> {{($sellinghorses->address=="")?"N/A":$sellinghorses->address}}</td>--}}
{{--                     <td style=""><a href="javascript:;" onclick="" data-id="active" data-val="active" data-toggle="modal" data-target="" class="postedBtn">Posted</a></td>--}}
                     <td style="">
                        <a href="javascript:;" onclick="viewsellinghorses({{$sellinghorses->id}})" data-id="active" class="viewBtn viewDetailsBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">View</span></a>
                        <a href="javascript:;" onclick="deletehorse({{$sellinghorses->id}})" data-id="active" class="deleteBtn confirmDel" data-toggle="modal" ><i class="fa fa-trash-o" title="delete" data-toggle="tooltip" aria-hidden="true"></i><span class="sr-only">Delete</span></a>
                     </td>
                  </tr>
               @endforeach
               </tbody>
            </table>
         </div>
         <div class="fixed-table-footer" style="display: none;">
            <table>
               <tbody>
                  <tr></tr>
               </tbody>
            </table>
         </div>
         <div class="fixed-table-pagination">
            <div class="pull-left pagination-detail">
               <span class="pagination-info">{{$data->links()}}</span>
               <span class="page-list" style="display: none;">
                  <span class="btn-group dropup">
                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="page-size">10</span> <span class="caret"></span></button>
                     <ul class="dropdown-menu" role="menu">
                        <li role="menuitem" class="active"><a href="#">10</a></li>
                     </ul>
                  </span>
                  rows per page
               </span>
            </div>
            <div class="pull-right pagination" style="display: none;">
               <ul class="pagination">
                  <li class="page-pre"><a href="#">‹</a></li>
                  <li class="page-number active"><a></a><a href="#">1</a></li>
                  <li class="page-next"><a href="#">›</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
</div>

        </div>
            </div>

            <!-- ############ Main END-->

        </div>
        
        <!-- View Modal-->
<div id="viewDetailsModal" class="modal fade show" data-backdrop="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Details
          <a data-dismiss="modal" class="ml-auto pull-right">
            <span class="text-lg l-h-1x" title="Close" data-toggle="tooltip">×</span>
          </a>
        </h5>

      </div>
      <div class="modal-body custom_modelview">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="brandImg">
              <img class="viewhorseimg" src="http://werdigi.com/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Owner Name</label>
              <p class="read-only" id="name">Name</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <div class="form-group">
                <label class="col-form-label">Owner Contact</label>
                <p class="read-only" id="contact">0213456789</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Horse Name</label>
              <p class="read-only" id="horsename"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Breed</label>
              <p class="read-only" id="breed">N/A</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Size</label>
              <p class="read-only" id="size">23-Oct-2020 | 8:10 PM</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Price</label>
              <p class="read-only" id="price">23-Oct-2020 | 8:10 PM</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Gender</label>
              <p class="read-only" id="gender">23-Oct-2020 | 8:10 PM</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Temperament</label>
              <p class="read-only" id="temperamant">23-Oct-2020 | 8:10 PM</p>
            </div>
          </div>
{{--          <div class="col-md-6 col-sm-12">--}}
{{--            <div class="inputmodal">--}}
{{--              <label class="col-form-label">Discipline</label>--}}
{{--              <p class="read-only" id="discipline">23-Oct-2020 | 8:10 PM</p>--}}
{{--            </div>--}}
{{--          </div>--}}
          <div class="col-md-12 col-sm-12">
            <div class="inputmodal">
              <label class="col-form-label">Location</label>
              <p class="read-only" id="location">23-Oct-2020 | 8:10 PM</p>
            </div>
          </div>
           </div>
           
          
         



        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div><!-- /.modal-content -->
  </div>
</div>

<!-- View Modal End-->
<!-- Del Modal-->
<div id="confirmationDelModal" class="modal fade show" data-backdrop="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Confirmation</h5>
    </div>
    <input type="hidden" id="brand_id" value="XR02EnLZ5o">
    <input type="hidden" id="newstatus" value="">
    <input type="text" id="postid" style="display: none;">
    <div class="modal-body text-center padding40 custom_modelview">
      <p>Are you sure you want to delete this?</p>
    </div>
    <div class="modal-footer">
      <a href="javascript:;"  data-dismiss="modal" class="cancelBtn">Cancel</a>
      <a href="javascript:;" onclick="deleteuserpost()" class="confirmBtn remove">Confirm</a>
    </div>
    </div><!-- /.modal-content -->
  </div>
</div>
<!---->
<script>
    $(document).ready(function(){
        $("#searchusers").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
    function deletehorse(id){
        $("#postid").val(id);
    }

    function deleteuserpost(){
        var id = $("#postid").val();
        $.ajax({
            url: "deleteuserpost",
            type: "post",
            data:{"id":id},
            success: function (response){
                window.location.href = window.location.href;
            }
        });
    }
    function viewsellinghorses(id){
        $.ajax({
            url: "viewhorse/"+id,
            type: "get",
            success: function (response) {
                $(".viewhorseimg").attr("src","storage/app/postimages/"+response.data.image);
                $("#name").text(response.data.username);
                $("#contact").text(response.data.phone);
                $("#horsename").text(response.data.horsetitle);
                $("#breed").text(response.data.breedtitle);
                $("#size").text(response.data.size);
                $("#price").text(response.data.price);
                $("#gender").text(response.data.gender);
                $("#temperamant").text(response.data.temperament);
                $("#discipline").text(response.data.discipline);

                $("#location").text(response.data.address);
                // window.location.href = window.location.href;
                // You will get response from your PHP page (what you echo or print)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
    $(document).ready(function () {
    $(".viewDetailsBtn").click(function(){
         // show Modal
         $('#viewDetailsModal').modal('show');
    });
        $(".confirmDel").click(function(){
         // show Modal
         $('#confirmationDelModal').modal('show');
    });
});
</script>
        <!-- Footer -->
        <div class="content-footer white " id="content-footer">

        </div>
    </div>
    <!-- ############ Content END-->
    @extends('admin.layouts.footer')
    <script src="{{asset('public/adminassets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('public/adminassets/libs/moment/min/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('public/adminassets/libs/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/plugins/jquery.chart.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/plugins/chartjs.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/lazyload.config.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/lazyload.js')}}"></script>
<script>
    function changepasswordmodal(){
        $("#changepasswordmodal").modal('show');
    }
    function changepassword(){
        var password = $("#password").val();
        var confirmpass = $("#confirmpassword").val();
        if(password===confirmpass){
            $.ajax({
                url: "{{url('changepass')}}",
                type: "post",
                data: {"password":password},
                success: function (response) {
                    console.log(response);
                    iziToast.success({
                        title: 'Success',
                        message:  response.message ,
                    });
                    setTimeout(function(){ location.reload() }, 4000);
                    // You will get response from your PHP page (what you echo or print)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
        else{
            iziToast.error({
                title: 'Error',
                message:  "Password Did Not Matched" ,
            });
        }
    }
</script>