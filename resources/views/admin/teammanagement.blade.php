@include('admin.layouts.header')
<!-- ############ Content START-->
<div class="app" id="app">

    <!-- ############ LAYOUT START-->

    <!-- ############ Aside START-->
    <div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
        <div class="sidenav modal-dialog dk">
            <!-- sidenav top -->
            <div class="navbarLogo lt">
                <!-- brand -->
                <a href="dashboard" class="navbar-brand">

                    <img src="{{asset('public/adminassets/images/adminlogo2.png')}}" alt=".">

                </a>
                <!-- / brand -->
            </div>

            <!-- Flex nav content -->
            <div class="flex hide-scroll">
                <div class="scroll">
                    <div class="nav-border" data-nav>
                        <ul class="nav bg">


                            <li>
                                <a href="dashboard">
		                  <span class="nav-icon">
		                    <i class="fa fa-dashboard"></i>
		                  </span>
                                    <span class="nav-text">Dashboard</span>
                                </a>
                            </li>

                            <li>
                                <a href="users">
		                  <span class="nav-icon">
		                    <i class="fa fa-users"></i>
		                  </span>
                                    <span class="nav-text">User Management</span>
                                </a>
                            </li>
                    <!--                                                                <li  class="active">-->
                    <!--            <a href="{{url('teammanagement')}}">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fas fa-user-friends"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Team Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->
                            
                                                        <li>
                                <a href="{{url('blog-management')}}">
		                  <span class="nav-icon">
		                    <i class="fas fa-blog"></i>
		                  </span>
                                    <span class="nav-text">Blogs Management</span>
                                </a>
                            </li>
                                                        <li>
                                <a href="{{url('content')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-sticky-note"></i>
		                  </span>
                                    <span class="nav-text">Content Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('sellinghorsesnew')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-horse"></i>
		                  </span>
                                    <span class="nav-text">Selling Horse</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('reportedads')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-lock"></i>
		                  </span>
                                    <span class="nav-text">Reported Ads</span>
                                </a>
                            </li>
                    <!--        <li>-->
                    <!--            <a href="blogs">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fa fa-newspaper-o" aria-hidden="true"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Blogs Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->

                            <li>
                                <a href="promotions">
		                  <span class="nav-icon">
		                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
		                  </span>
                                    <span class="nav-text">Promotions Management</span>
                                </a>
                            </li>

                            <li>
                                <a href="packages">
		                  <span class="nav-icon">
		                    <i class="fa fa-money"></i>
		                  </span>
                                    <span class="nav-text">Packages Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="subscription">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Subscription Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="paymenthistory">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Payment History</span>
                                </a>
                            </li>
                            {{--                            <li>--}}
                            {{--                                <a href="#">--}}
                            {{--		                  <span class="nav-icon">--}}
                            {{--		                    <i class="fa fa-file-text"></i>--}}
                            {{--		                  </span>--}}
                            {{--                                    <span class="nav-text">Packages Management</span>--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            <li class="pb-2 hidden-folded"></li>
                        </ul>

                    </div>
                </div>
            </div>
                        <div class='no-shrink lt'>
            <div class='nav-fold'>
                <div class='hidden-folded flex p-2-3 bg'>
                    <div class='d-flex p-1'>
                        <a onclick="logout();" href='javascript:;' class='px-2 logout' data-toggle='tooltip' title='' data-original-title='Logout'>
                            <i class='fa fa-sign-out' aria-hidden='true'></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- ############ Aside END-->
    <div id="content" class="app-content box-shadow-4 box-radius-4" role="main">
        <!-- Header -->
        <div class="content-header orangeBg box-shadow-4" id="content-header">
        <div class="navbar navbar-expand-lg">
            <!-- btn to toggle sidenav on small screen -->
            <a class="d-lg-none" data-toggle="modal" data-target="#aside">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"></path></svg>
            </a>
            <!-- Page title -->

            <div class="navbar-text nav-title flex" id="pageTitle">
                <div class="headerHeading">
                    <h6>Welcome</h6>
                    <h1>Summer Gentry</h1>
                </div>
            </div>


            <ul class="nav flex-row order-lg-2">
                <!-- Notification -->
                <li class="nav-item dropdown" style="width: 20px;">
                    <a title="Notifications" style="right:30px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell" aria-hidden="true"></i>
                        <span class="badge badge-pill up danger"></span>
                    </a>
                    <!-- dropdown -->
                    <!--<div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0 notify">-->
                    <!--    <div class="scrollable hover" style="max-height: 250px">-->
                    <!--        <div class="list">-->
                    <!--            <div class="list-item " data-id="item-11">-->
                    <!--    <span class="w-24 avatar circle blue">-->
                    <!--        <span class="fa fa-code-fork"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Tiger Nixon</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Looking for some client-work-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">16:00</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-2">-->
                    <!--    <span class="w-24 avatar circle light-blue">-->
                    <!--        <span class="fa fa-git"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Kygo</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        What&#x27;s the project progress now-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">08:05</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-12">-->
                    <!--    <span class="w-24 avatar circle green">-->
                    <!--        <span class="fa fa-dot-circle-o"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Ashton Cox</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Looking for some client-work-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">11:30</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-4">-->
                    <!--    <span class="w-24 avatar circle pink">-->
                    <!--        <span class="fa fa-male"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Judith Garcia</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Eddel upload a media-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">12:05</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-14">-->
                    <!--    <span class="w-24 avatar circle brown">-->
                    <!--        <span class="fa fa-bell"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Brielle Williamson</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Looking for some client-work-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">08:00</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-9">-->
                    <!--    <span class="w-24 avatar circle cyan">-->
                    <!--        <span class="fa fa-puzzle-piece"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Pablo Nouvelle</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        It&#x27;s been a Javascript kind of day-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">15:00</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="d-flex px-3 py-2 b-t">-->
                    <!--        <div class="flex">-->
                    <!--            <span>6 Notifications</span>-->
                    <!--        </div>-->
                    <!--        <a href="#">See all <i class="fa fa-angle-right text-muted"></i></a>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div id="activity-dropdown"
                         class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
                        <!-- <label class=“switch”>
                          <input type='checkbox” class=“primary”>
                          <span class=“slider round”></span>
                        </label> -->
                        <div class="loaderNotif" style="display: none;"></div>
                        <div class="body_notification" style="height: 250px;">
                            <div class="scrollable hover notif-padding" style="max-height: 250px">
                                <!-- <div class='list'>
                                </div> -->
                                <div class="notifi-left">
                                    <p class="notify_para">Notifications</p>
                                    <label class="switch">
                                        <input id="notif_toggle" type="checkbox" checked="">
                                        <span id="notif_slid" title="No" class="slider round"></span>
                                    </label>
                                </div>
                                <div class="noti-right" style="display: block;">
                                    <a href="javascript:;" id="remove_notif" style="display: none;">Mark all as
                                        read</a>
                                </div>
                            </div>
                            <!--<div class=“d-flex py-2 b-t infonoti cpadding-noti” style=“background: #ececec91;“>
                              <div class=“flex”>
                                <span >Notifications</span>
                              </div>
                            </div> -->
                            <div class="list" style="display: block;">
                                <div class="no-havenotification"><img
                                            src="http://werdigi.com/public/assets/images/notifications.gif"
                                            class="img-fluid" alt="">
                                    <h2>No Notifications</h2>                                <h4>You currently have
                                        no notifications</h4></div>
                            </div>
                        </div>
                        <!-- / dropdown -->
                    </div>
                    <!-- / dropdown -->
                </li>




                <a title="Change Password" style="right:20px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" onclick="changepasswordmodal();">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span class="badge badge-pill up danger"></span>
                </a>

                <!-- User dropdown menu -->
                <li class="dropdown d-flex align-items-center">
                    <a title="Logout" class="dropdown-item" onclick="logout()" href="#" style="background: transparent !important;border-bottom:none;color:white !important;padding: 0;margin:0">
                        <i class="fa fa-sign-out" aria-hidden="true" style="color: white !important;">
                        </i>
                    </a>
                </li>
                <!-- Navarbar toggle btn -->
                <li class="d-lg-none d-flex align-items-center">
                    <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
                            <path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z"></path>
                        </svg>
                    </a>
                </li>
            </ul>
            <!-- Navbar collapse -->

        </div>
    </div>
        <!-- Main -->
        <div class="content-main " id="content-main" style="margin-left: 20px">
           <div class="padding">
      
            
            

    <div class="row no-gutters box">
          <div class="col-sm-12">
            <div class="paddingBoxHead">
              <div class="teamMangementHead">
                <div class="row">
                  <div class="col-sm-10">
                    <h5>Team Management</h5>
                  </div>
                  <div class="col-sm-2">
                   <a href="javascript:;" onclick="add()" data-toggle="modal" data-target="#addTeamModal">Add Team</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <div class="">
          <div class="b-b nav-active-primary">
              <ul class="nav nav-tabs formargin-btm">
              <li class="nav-item">
                <a class="nav-link active show" href="#" data-toggle="tab" data-target="#tab4">Unit Head</a>
              </li>              
              <li class="nav-item">
                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab5">Sales</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tab" data-target="#tab6">Presale</a>
              </li>

              </ul>
            </div>
            <div class="box2">
            <div class="tab-content">
              <div class="tab-pane animate fadeIn text-muted active show" id="tab4">             
                  <div class="table-responsive">
            <div class="bootstrap-table"><div class="fixed-table-toolbar"><div class="columns columns-right btn-group pull-right"><div class="keep-open btn-group" title="Columns"><button type="button" aria-label="columns" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th icon-th"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem"><label><input type="checkbox" data-field="0" value="0" checked="checked"> Logo</label></li><li role="menuitem"><label><input type="checkbox" data-field="1" value="1" checked="checked"> Brand</label></li><li role="menuitem"><label><input type="checkbox" data-field="2" value="2" checked="checked"> Full Name</label></li><li role="menuitem"><label><input type="checkbox" data-field="3" value="3" checked="checked"> Email</label></li><li role="menuitem"><label><input type="checkbox" data-field="4" value="4" checked="checked"> WhatsApp No.</label></li><li role="menuitem"><label><input type="checkbox" data-field="5" value="5" checked="checked"> Total<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="6" value="6" checked="checked"> Monthly<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="7" value="7" checked="checked"> Contract<br>Sent</label></li><li role="menuitem"><label><input type="checkbox" data-field="8" value="8" checked="checked"> Apps<br>Sold</label></li><li role="menuitem"><label><input type="checkbox" data-field="9" value="9" checked="checked"> Status</label></li><li role="menuitem"><label><input type="checkbox" data-field="10" value="10" checked="checked"> Action</label></li></ul></div><div class="export btn-group"><button class="btn btn-default dropdown-toggle" aria-label="export type" title="Export data" data-toggle="dropdown" type="button"><i class="glyphicon glyphicon-export icon-share"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" data-type="json"><a href="javascript:void(0)">JSON</a></li><li role="menuitem" data-type="xml"><a href="javascript:void(0)">XML</a></li><li role="menuitem" data-type="csv"><a href="javascript:void(0)">CSV</a></li><li role="menuitem" data-type="txt"><a href="javascript:void(0)">TXT</a></li><li role="menuitem" data-type="sql"><a href="javascript:void(0)">SQL</a></li><li role="menuitem" data-type="excel"><a href="javascript:void(0)">MS-Excel</a></li></ul></div></div><div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div></div><div class="fixed-table-container" style="padding-bottom: 0px;"><div class="fixed-table-header" style="display: none;"><table></table></div><div class="fixed-table-body"><div class="fixed-table-loading" style="top: 66px;">Loading, please wait...</div><table class="table table-hover" data-plugin="bootstrapTable" id="brandtable" data-search="true" data-pagination="true" data-show-columns="true" data-show-export="true" data-detail-view="false">
              <thead><tr><th style="" data-field="0"><div class="th-inner ">Logo</div><div class="fht-cell"></div></th><th style="" data-field="1"><div class="th-inner sortable both">Brand</div><div class="fht-cell"></div></th><th style="" data-field="2"><div class="th-inner sortable both">Full Name</div><div class="fht-cell"></div></th><th style="" data-field="3"><div class="th-inner sortable both">Email</div><div class="fht-cell"></div></th><th style="" data-field="4"><div class="th-inner sortable both">WhatsApp No.</div><div class="fht-cell"></div></th><th style="" data-field="5"><div class="th-inner sortable both">Total<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="6"><div class="th-inner sortable both">Monthly<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="7"><div class="th-inner sortable both">Contract<br>Sent</div><div class="fht-cell"></div></th><th style="" data-field="8"><div class="th-inner sortable both">Apps<br>Sold</div><div class="fht-cell"></div></th><th style="" data-field="9"><div class="th-inner sortable both">Status</div><div class="fht-cell"></div></th><th style="" data-field="10"><div class="th-inner ">Action</div><div class="fht-cell"></div></th></tr></thead>
              <tbody><tr data-index="0"> <td style="">
                  <span class="avatar w-40">
                    <img src="http://werdigi.com/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
                  </span>
                </td> <td style="">Maaz</td> <td style="">Maaz Ali</td> <td style="">khawaja.maaz@digitonics.com</td> <td style="">(+92)341 021 7541</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('za4LA7xGZX','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('za4LA7xGZX')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('za4LA7xGZX')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="1"> <td style="">
                  <span class="avatar w-40">
                    <img src="http://werdigi.com/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
                  </span>
                </td> <td style="">CMOLDS</td> <td style="">Ahsan Mirza</td> <td style="">ahsun.m@cmolds.com</td> <td style="">(+92)335 655 5996</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('3n7LXp4L6W','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('3n7LXp4L6W')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('3n7LXp4L6W')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="2"> <td style="">
                  <span class="avatar w-40">
                    <img src="http://werdigi.com/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
                  </span>
                </td> <td style="">DU - Sales</td> <td style="">Ayub Ahmed</td> <td style="">ayub.ahmed@digitonics.com</td> <td style="">(+92)332 246 3008</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('MEnGeQ9G6A','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('MEnGeQ9G6A')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('MEnGeQ9G6A')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="3"> <td style="">
                  <span class="avatar w-40">
                    <img src="http://werdigi.com/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
                  </span>
                </td> <td style="">CMOLDS</td> <td style="">Ayub Ahmed</td> <td style="">ayub.ahmed@cmolds.com</td> <td style="">(+92)332 246 3008</td> <td style="">3</td> <td style="">0</td> <td style="">1</td> <td style="">1</td> <td style=""><a href="javascript:;" onclick="setId('PJEbYd6LXa','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('PJEbYd6LXa')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('PJEbYd6LXa')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr></tbody>
            </table></div><div class="fixed-table-footer" style="display: none;"><table><tbody><tr></tr></tbody></table></div><div class="fixed-table-pagination"><div class="pull-left pagination-detail"><span class="pagination-info">Showing 1 to 4 of 4 rows</span><span class="page-list" style="display: none;"><span class="btn-group dropup"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="page-size">10</span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" class="active"><a href="#">10</a></li></ul></span> rows per page</span></div><div class="pull-right pagination" style="display: none;"><ul class="pagination"><li class="page-pre"><a href="#">‹</a></li><li class="page-number active"><a href="#">1</a></li><li class="page-next"><a href="#">›</a></li></ul></div></div></div></div><div class="clearfix"></div>


          </div>
              </div>
              <div class="tab-pane animate fadeIn text-muted " id="tab5">
               
                  <div class="table-responsive">
            <div class="bootstrap-table"><div class="fixed-table-toolbar"><div class="columns columns-right btn-group pull-right"><div class="keep-open btn-group" title="Columns"><button type="button" aria-label="columns" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th icon-th"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem"><label><input type="checkbox" data-field="0" value="0" checked="checked"> Logo</label></li><li role="menuitem"><label><input type="checkbox" data-field="1" value="1" checked="checked"> Brand</label></li><li role="menuitem"><label><input type="checkbox" data-field="2" value="2" checked="checked"> Full Name</label></li><li role="menuitem"><label><input type="checkbox" data-field="3" value="3" checked="checked"> Email</label></li><li role="menuitem"><label><input type="checkbox" data-field="4" value="4" checked="checked"> WhatsApp No.</label></li><li role="menuitem"><label><input type="checkbox" data-field="5" value="5" checked="checked"> Total<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="6" value="6" checked="checked"> Monthly<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="7" value="7" checked="checked"> Contract<br>Sent</label></li><li role="menuitem"><label><input type="checkbox" data-field="8" value="8" checked="checked"> Apps<br>Sold</label></li><li role="menuitem"><label><input type="checkbox" data-field="9" value="9" checked="checked"> Status</label></li><li role="menuitem"><label><input type="checkbox" data-field="10" value="10" checked="checked"> Action</label></li></ul></div><div class="export btn-group"><button class="btn btn-default dropdown-toggle" aria-label="export type" title="Export data" data-toggle="dropdown" type="button"><i class="glyphicon glyphicon-export icon-share"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" data-type="json"><a href="javascript:void(0)">JSON</a></li><li role="menuitem" data-type="xml"><a href="javascript:void(0)">XML</a></li><li role="menuitem" data-type="csv"><a href="javascript:void(0)">CSV</a></li><li role="menuitem" data-type="txt"><a href="javascript:void(0)">TXT</a></li><li role="menuitem" data-type="sql"><a href="javascript:void(0)">SQL</a></li><li role="menuitem" data-type="excel"><a href="javascript:void(0)">MS-Excel</a></li></ul></div></div><div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div></div><div class="fixed-table-container" style="padding-bottom: 0px;"><div class="fixed-table-header" style="display: none;"><table></table></div><div class="fixed-table-body"><div class="fixed-table-loading" style="top: 1px;">Loading, please wait...</div><table class="table table-hover" data-plugin="bootstrapTable" id="brandtable" data-search="true" data-pagination="true" data-show-columns="true" data-show-export="true" data-detail-view="false">
              <thead style=""><tr><th style="" data-field="0"><div class="th-inner ">Logo</div><div class="fht-cell"></div></th><th style="" data-field="1"><div class="th-inner sortable both">Brand</div><div class="fht-cell"></div></th><th style="" data-field="2"><div class="th-inner sortable both">Full Name</div><div class="fht-cell"></div></th><th style="" data-field="3"><div class="th-inner sortable both">Email</div><div class="fht-cell"></div></th><th style="" data-field="4"><div class="th-inner sortable both">WhatsApp No.</div><div class="fht-cell"></div></th><th style="" data-field="5"><div class="th-inner sortable both">Total<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="6"><div class="th-inner sortable both">Monthly<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="7"><div class="th-inner sortable both">Contract<br>Sent</div><div class="fht-cell"></div></th><th style="" data-field="8"><div class="th-inner sortable both">Apps<br>Sold</div><div class="fht-cell"></div></th><th style="" data-field="9"><div class="th-inner sortable both">Status</div><div class="fht-cell"></div></th><th style="" data-field="10"><div class="th-inner ">Action</div><div class="fht-cell"></div></th></tr></thead>
              <tbody><tr class="no-records-found"><td colspan="11">No matching records found</td></tr></tbody>
            </table></div><div class="fixed-table-footer" style="display: none;"><table><tbody><tr></tr></tbody></table></div><div class="fixed-table-pagination" style="display: none;"><div class="pull-left pagination-detail"><span class="pagination-info">Showing 1 to 0 of 0 rows</span><span class="page-list" style="display: none;"><span class="btn-group dropup"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="page-size">10</span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" class="active"><a href="#">10</a></li></ul></span> rows per page</span></div><div class="pull-right pagination" style="display: none;"><ul class="pagination"><li class="page-pre"><a href="#">‹</a></li><li class="page-next"><a href="#">›</a></li></ul></div></div></div></div><div class="clearfix"></div>


          </div>
          </div>
          <div class="tab-pane animate fadeIn text-muted " id="tab6">
               
                  <div class="table-responsive">
            <div class="bootstrap-table"><div class="fixed-table-toolbar"><div class="columns columns-right btn-group pull-right"><div class="keep-open btn-group" title="Columns"><button type="button" aria-label="columns" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th icon-th"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem"><label><input type="checkbox" data-field="0" value="0" checked="checked"> Logo</label></li><li role="menuitem"><label><input type="checkbox" data-field="1" value="1" checked="checked"> Brand</label></li><li role="menuitem"><label><input type="checkbox" data-field="2" value="2" checked="checked"> Full Name</label></li><li role="menuitem"><label><input type="checkbox" data-field="3" value="3" checked="checked"> Email</label></li><li role="menuitem"><label><input type="checkbox" data-field="4" value="4" checked="checked"> WhatsApp No.</label></li><li role="menuitem"><label><input type="checkbox" data-field="5" value="5" checked="checked"> Total<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="6" value="6" checked="checked"> Monthly<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="7" value="7" checked="checked"> Contract<br>Sent</label></li><li role="menuitem"><label><input type="checkbox" data-field="8" value="8" checked="checked"> Apps<br>Sold</label></li><li role="menuitem"><label><input type="checkbox" data-field="9" value="9" checked="checked"> Status</label></li><li role="menuitem"><label><input type="checkbox" data-field="10" value="10" checked="checked"> Action</label></li></ul></div><div class="export btn-group"><button class="btn btn-default dropdown-toggle" aria-label="export type" title="Export data" data-toggle="dropdown" type="button"><i class="glyphicon glyphicon-export icon-share"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" data-type="json"><a href="javascript:void(0)">JSON</a></li><li role="menuitem" data-type="xml"><a href="javascript:void(0)">XML</a></li><li role="menuitem" data-type="csv"><a href="javascript:void(0)">CSV</a></li><li role="menuitem" data-type="txt"><a href="javascript:void(0)">TXT</a></li><li role="menuitem" data-type="sql"><a href="javascript:void(0)">SQL</a></li><li role="menuitem" data-type="excel"><a href="javascript:void(0)">MS-Excel</a></li></ul></div></div><div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div></div><div class="fixed-table-container" style="padding-bottom: 0px;"><div class="fixed-table-header" style="display: none;"><table></table></div><div class="fixed-table-body"><div class="fixed-table-loading" style="top: 1px;">Loading, please wait...</div><table class="table table-hover" data-plugin="bootstrapTable" id="brandtable" data-search="true" data-pagination="true" data-show-columns="true" data-show-export="true" data-detail-view="false">
              <thead style=""><tr><th style="" data-field="0"><div class="th-inner ">Logo</div><div class="fht-cell"></div></th><th style="" data-field="1"><div class="th-inner sortable both">Brand</div><div class="fht-cell"></div></th><th style="" data-field="2"><div class="th-inner sortable both">Full Name</div><div class="fht-cell"></div></th><th style="" data-field="3"><div class="th-inner sortable both">Email</div><div class="fht-cell"></div></th><th style="" data-field="4"><div class="th-inner sortable both">WhatsApp No.</div><div class="fht-cell"></div></th><th style="" data-field="5"><div class="th-inner sortable both">Total<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="6"><div class="th-inner sortable both">Monthly<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="7"><div class="th-inner sortable both">Contract<br>Sent</div><div class="fht-cell"></div></th><th style="" data-field="8"><div class="th-inner sortable both">Apps<br>Sold</div><div class="fht-cell"></div></th><th style="" data-field="9"><div class="th-inner sortable both">Status</div><div class="fht-cell"></div></th><th style="" data-field="10"><div class="th-inner ">Action</div><div class="fht-cell"></div></th></tr></thead>
              <tbody><tr data-index="0"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/dRqnwx7oLb1fgwNrctSXyJRZ6KJlxzGRPqijr985@3x.jpeg" alt="...">
                  </span>
                </td> <td style="">DU - Sales</td> <td style="">Bilal Ali</td> <td style="">bilal.khan@digitonics.com</td> <td style="">(+92)345 602 7021</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('aDQbBRvbBM','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('aDQbBRvbBM')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('aDQbBRvbBM')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="1"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/n3Ei5c9FSJuS9V272FFWMBGwWngwwsdoIkn9NlT9@3x.jpeg" alt="...">
                  </span>
                </td> <td style="">DigiCreations</td> <td style="">Danial Khan</td> <td style="">danial.khan@mailinator.com</td> <td style="">(+92)340 837 8757</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('lyM2QnNL1w','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('lyM2QnNL1w')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('lyM2QnNL1w')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr></tbody>
            </table></div><div class="fixed-table-footer" style="display: none;"><table><tbody><tr></tr></tbody></table></div><div class="fixed-table-pagination"><div class="pull-left pagination-detail"><span class="pagination-info">Showing 1 to 2 of 2 rows</span><span class="page-list" style="display: none;"><span class="btn-group dropup"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="page-size">10</span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" class="active"><a href="#">10</a></li></ul></span> rows per page</span></div><div class="pull-right pagination" style="display: none;"><ul class="pagination"><li class="page-pre"><a href="#">‹</a></li><li class="page-number active"><a href="#">1</a></li><li class="page-next"><a href="#">›</a></li></ul></div></div></div></div><div class="clearfix"></div>


          </div>
          </div>

          <div class="tab-pane animate fadeIn text-muted " id="tab6">
               
                  <div class="table-responsive">
            <div class="bootstrap-table"><div class="fixed-table-toolbar"><div class="columns columns-right btn-group pull-right"><div class="keep-open btn-group" title="Columns"><button type="button" aria-label="columns" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th icon-th"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem"><label><input type="checkbox" data-field="0" value="0" checked="checked"> Logo</label></li><li role="menuitem"><label><input type="checkbox" data-field="1" value="1" checked="checked"> Brand</label></li><li role="menuitem"><label><input type="checkbox" data-field="2" value="2" checked="checked"> Full Name</label></li><li role="menuitem"><label><input type="checkbox" data-field="3" value="3" checked="checked"> Email</label></li><li role="menuitem"><label><input type="checkbox" data-field="4" value="4" checked="checked"> WhatsApp No.</label></li><li role="menuitem"><label><input type="checkbox" data-field="5" value="5" checked="checked"> Total<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="6" value="6" checked="checked"> Monthly<br>Request</label></li><li role="menuitem"><label><input type="checkbox" data-field="7" value="7" checked="checked"> Contract<br>Sent</label></li><li role="menuitem"><label><input type="checkbox" data-field="8" value="8" checked="checked"> Apps<br>Sold</label></li><li role="menuitem"><label><input type="checkbox" data-field="9" value="9" checked="checked"> Status</label></li><li role="menuitem"><label><input type="checkbox" data-field="10" value="10" checked="checked"> Action</label></li></ul></div><div class="export btn-group"><button class="btn btn-default dropdown-toggle" aria-label="export type" title="Export data" data-toggle="dropdown" type="button"><i class="glyphicon glyphicon-export icon-share"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" data-type="json"><a href="javascript:void(0)">JSON</a></li><li role="menuitem" data-type="xml"><a href="javascript:void(0)">XML</a></li><li role="menuitem" data-type="csv"><a href="javascript:void(0)">CSV</a></li><li role="menuitem" data-type="txt"><a href="javascript:void(0)">TXT</a></li><li role="menuitem" data-type="sql"><a href="javascript:void(0)">SQL</a></li><li role="menuitem" data-type="excel"><a href="javascript:void(0)">MS-Excel</a></li></ul></div></div><div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div></div><div class="fixed-table-container" style="padding-bottom: 0px;"><div class="fixed-table-header" style="display: none;"><table></table></div><div class="fixed-table-body"><div class="fixed-table-loading" style="top: 1px;">Loading, please wait...</div><table class="table table-hover" data-plugin="bootstrapTable" id="brandtable" data-search="true" data-pagination="true" data-show-columns="true" data-show-export="true" data-detail-view="false">
              <thead style=""><tr><th style="" data-field="0"><div class="th-inner ">Logo</div><div class="fht-cell"></div></th><th style="" data-field="1"><div class="th-inner sortable both">Brand</div><div class="fht-cell"></div></th><th style="" data-field="2"><div class="th-inner sortable both">Full Name</div><div class="fht-cell"></div></th><th style="" data-field="3"><div class="th-inner sortable both">Email</div><div class="fht-cell"></div></th><th style="" data-field="4"><div class="th-inner sortable both">WhatsApp No.</div><div class="fht-cell"></div></th><th style="" data-field="5"><div class="th-inner sortable both">Total<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="6"><div class="th-inner sortable both">Monthly<br>Request</div><div class="fht-cell"></div></th><th style="" data-field="7"><div class="th-inner sortable both">Contract<br>Sent</div><div class="fht-cell"></div></th><th style="" data-field="8"><div class="th-inner sortable both">Apps<br>Sold</div><div class="fht-cell"></div></th><th style="" data-field="9"><div class="th-inner sortable both">Status</div><div class="fht-cell"></div></th><th style="" data-field="10"><div class="th-inner ">Action</div><div class="fht-cell"></div></th></tr></thead>
              <tbody><tr data-index="0"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/431T2H8x0qLyWLpIAWTn1s404pOHcatIil0lwtrQ@3x.png" alt="...">
                  </span>
                </td> <td style="">Maaz</td> <td style="">Maaz Ali</td> <td style="">khawaja.maaz@digitonics.com</td> <td style="">(+92)341 021 7541</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('za4LA7xGZX','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('za4LA7xGZX')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('za4LA7xGZX')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="1"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
                  </span>
                </td> <td style="">CMOLDS</td> <td style="">Ahsan Mirza</td> <td style="">ahsun.m@cmolds.com</td> <td style="">(+92)335 655 5996</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('3n7LXp4L6W','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('3n7LXp4L6W')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('3n7LXp4L6W')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="2"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/dRqnwx7oLb1fgwNrctSXyJRZ6KJlxzGRPqijr985@3x.jpeg" alt="...">
                  </span>
                </td> <td style="">DU - Sales</td> <td style="">Bilal Ali</td> <td style="">bilal.khan@digitonics.com</td> <td style="">(+92)345 602 7021</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('aDQbBRvbBM','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('aDQbBRvbBM')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('aDQbBRvbBM')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="3"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/n3Ei5c9FSJuS9V272FFWMBGwWngwwsdoIkn9NlT9@3x.jpeg" alt="...">
                  </span>
                </td> <td style="">DigiCreations</td> <td style="">Danial Khan</td> <td style="">danial.khan@mailinator.com</td> <td style="">(+92)340 837 8757</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('lyM2QnNL1w','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('lyM2QnNL1w')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('lyM2QnNL1w')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="4"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/dRqnwx7oLb1fgwNrctSXyJRZ6KJlxzGRPqijr985@3x.jpeg" alt="...">
                  </span>
                </td> <td style="">DU - Sales</td> <td style="">Ayub Ahmed</td> <td style="">ayub.ahmed@digitonics.com</td> <td style="">(+92)332 246 3008</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style="">0</td> <td style=""><a href="javascript:;" onclick="setId('MEnGeQ9G6A','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('MEnGeQ9G6A')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('MEnGeQ9G6A')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr><tr data-index="5"> <td style="">
                  <span class="avatar w-40">
                    <img src="/storage/app/brands/0nyTx1gj2dFkupt9Sj1txOVu0kmulnBxoaZsHrRI@3x.png" alt="...">
                  </span>
                </td> <td style="">CMOLDS</td> <td style="">Ayub Ahmed</td> <td style="">ayub.ahmed@cmolds.com</td> <td style="">(+92)332 246 3008</td> <td style="">3</td> <td style="">0</td> <td style="">1</td> <td style="">1</td> <td style=""><a href="javascript:;" onclick="setId('PJEbYd6LXa','active')" data-id="active" data-val="active" data-toggle="modal" data-target="#confirmationModal" class="activeBtn">active</a></td> <td style="">
                  <a href="javascript:;" onclick="prj.UserList.view('PJEbYd6LXa')" data-id="active" class="viewBtn" data-toggle="modal" data-target="#viewTeamModal"><i class="fa fa-eye" title="View" data-toggle="tooltip"></i></a>
                  <a href="javascript:;" onclick="prj.UserList.edit('PJEbYd6LXa')" data-id="active" class="editBtn" data-toggle="modal" data-target="#addTeamModal"><i class="fa fa-edit" title="Edit" data-toggle="tooltip"></i></a>

                </td> </tr></tbody>
            </table></div><div class="fixed-table-footer" style="display: none;"><table><tbody><tr></tr></tbody></table></div><div class="fixed-table-pagination"><div class="pull-left pagination-detail"><span class="pagination-info">Showing 1 to 6 of 6 rows</span><span class="page-list" style="display: none;"><span class="btn-group dropup"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="page-size">10</span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li role="menuitem" class="active"><a href="#">10</a></li></ul></span> rows per page</span></div><div class="pull-right pagination" style="display: none;"><ul class="pagination"><li class="page-pre"><a href="#">‹</a></li><li class="page-number active"><a href="#">1</a></li><li class="page-next"><a href="#">›</a></li></ul></div></div></div></div><div class="clearfix"></div>


          </div>
          </div>
            </div>

        </div>



        </div>

            </div>
        </div>
        
        <!--Modal-->
<div id="addTeamModal" class="modal fade show" data-backdrop="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span id="tit">Add team Member</span>
          <a data-dismiss="modal" class="ml-auto pull-right">
            <span class="text-lg l-h-1x" title="" data-toggle="tooltip" data-original-title="Close">×</span>
          </a>
        </h5>
      </div>
      <form id="addTeamMember" action="javascript:;"> 
        <div class="modal-body custom_modelview">
          <div class="row">
           
            <div class="col-md-6 col-sm-12">
              <div class="inputmodal">
                <label class="col-form-label">First Name</label>
                <input type="text" name="first_name" id="first_name" class="changecolor form-control" placeholder="Enter First Name" required="" pattern="^[a-zA-Z]+[a-zA-Z0-9_ ]*$" title="" data-toggle="tooltip" data-original-title="Firstname should be letters. e.g. john">
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="inputmodal">
                <label class="col-form-label">Last Name</label>
                <input type="text" name="last_name" id="last_name" class="changecolor form-control" placeholder="Enter Last name" required="" pattern="^[a-zA-Z]+[a-zA-Z0-9_ ]*$" title="" data-toggle="tooltip" data-original-title="Lastname should be letters. e.g. john">
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="inputmodal">
                <label class="col-form-label">Brand</label>
                <select class=" select22 form-control getdomain select2-hidden-accessible" name="assign_brand_id" id="select2-allow-clear" data-plugin="select2" data-option="{}" data-allow-clear="false" required="" tabindex="-1" aria-hidden="true">
                  <optgroup>
                                            <option domain-name="abc.com" selected="selected" value="XR02EnLZ5o">Maaz</option>
                                              <option domain-name="mailinator.com" selected="selected" value="RMg2Jj28QZ">DigiCreations</option>
                                              <option domain-name="cmolds.com" selected="selected" value="DKVLp5bX4v">CMOLDS</option>
                                              <option domain-name="digitonics.com" selected="selected" value="B3Db6mL1yY">DU - Sales</option>
                                         
                  </optgroup>
                </select>
                <!--<span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;">-->
                <!--    <span class="selection">-->
                <!--    <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select2-allow-clear-container">-->
                <!--        <span class="select2-selection__rendered" id="select2-select2-allow-clear-container" title="DU - Sales">DU - Sales</span>-->
                <!--        <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b>-->
                <!--            <span></span>-->
                <!--        </span>-->
                <!--        <span class="dropdown-wrapper" aria-hidden="true"></span>-->
                <!--    </span>-->
                <!--    </span>-->
                <!--</span>-->
              </div>
          </div>
          <div class="col-md-6 col-sm-12">
              <div class="inputmodal">
                <label class="col-form-label">Role</label>
                <select name="type" class=" select22 form-control select2-hidden-accessible" id="type" data-plugin="select2" data-option="{}" data-allow-clear="false" required="" tabindex="-1" aria-hidden="true">
                  <optgroup>                    
                    <option value="admin">Unit Head</option> 
                    <option value="presale">Presale</option>                          
                  </optgroup>
                </select>
                <!--<span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-type-container"><span class="select2-selection__rendered" id="select2-type-container" title="Unit Head">Unit Head</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>-->
              </div>
          </div>
            <div class="col-md-6 col-sm-12">
                <div class="inputmodal">
                  <label class="col-form-label">Email</label>
                  <input type="text" name="email" id="email" class="changecolor form-control" placeholder="abc@example.com" required="">
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="inputmodal">
                  <label class="col-form-label">Whatsapp Number</label>
                  <input name="phone_no" id="phone_no" type="text" class="changecolor form-control" placeholder="(9999) 999 9999" required="" maxlength="17">
                </div>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <div id="msg"></div>
          <input type="hidden" name="profile_id" id="profile_id">
          <button data-dismiss="modal" class="cancelBtn">Cancel</button>
          <button type="submit" class="confirmBtn">Confirm</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div>
</div>
        <!-- Modal End-->
        <script type="text/javascript">

//  function setId($id,$status){  

//     $('div#confirmationModal input#user_id').val($id);
//     if($status == "active"){
//       $('div#activeMsg p').html("Are you sure you want to deactivate this user?");
//       $('div#confirmationModal input#newstatus').val("inactive");
//     }
//     if($status == "inactive"){
//       $('div#activeMsg p').html("Are you sure you want to activate this user?");
//       $('div#confirmationModal input#newstatus').val("active");
//     }
//   }
//   function setDelId($id,$status){    
//     $('div#confirmationDelModal input#user_id').val($id);
//   }
  function edit(id){
  //$('div#addTeamModal h5.modal-title span').html("Edit team Member");
  $('form#addTeamMember input#profile_id').val(id);
  prj.UserList.edit(id);
  }
  function add(){
    $('div#addTeamModal h5.modal-title span#tit').html("Add team Member")
  } 
  
//   $(document).ready(function() {   

      
//      $("form#addTeamMember").submit(function(event){
//         event.preventDefault();
//         var formData = new FormData(this);
//         if($("form#addTeamMember input#profile_id").val() != "")
//             prj.UserList.update(formData);
//         else  
//             prj.UserList.add(formData);
     
//      });
//       $('a.viewBtn').click(function(){        
//         prj.UserList.view($(this).attr('data-id'));
//       });
//       $('a.update').click(function(){
//       prj.UserList.changestatus($('div#confirmationModal input#user_id').val(),
//         $('div#confirmationModal input#newstatus').val());
//       });
    
//  } );
</script>
        <!-- Footer -->
        <div class="content-footer white " id="content-footer">
            <!-- <div class="d-flex p-3">
                <span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
                <div class="text-sm text-muted">Version 1.1.1</div>
            </div> -->
        </div>
    </div>
    <!-- ############ Content END-->

@extends('admin.layouts.footer')

    <script>
        function changepasswordmodal(){
            $("#changepasswordmodal").modal('show');
        }
        function changepassword(){
            var password = $("#password").val();
            var confirmpass = $("#confirmpassword").val();
            if(password===confirmpass){
                $.ajax({
                    url: "{{url('changepass')}}",
                    type: "post",
                    data: {"password":password},
                    success: function (response) {
                        console.log(response);
                        iziToast.success({
                            title: 'Success',
                            message:  response.message ,
                        });
                        setTimeout(function(){ location.reload() }, 4000);
                        // You will get response from your PHP page (what you echo or print)
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
            else{
                iziToast.error({
                    title: 'Error',
                    message:  "Password Did Not Matched" ,
                });
            }
        }
    </script>