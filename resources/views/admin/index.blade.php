@include('admin.layouts.header')
<!-- ############ Content START-->
<div class="app" id="app">

    @php
        //@dd($amount_arr)
        //@dd($sum)
    @endphp

    <!-- ############ LAYOUT START-->

    <!-- ############ Aside START-->
    <div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
        <div class="sidenav modal-dialog dk">
            <!-- sidenav top -->
            <div class="navbarLogo lt">
                <!-- brand -->
                <a href="dashboard" class="navbar-brand">

                    <img src="{{asset('public/adminassets/images/adminlogo2.png')}}" alt=".">

                </a>
                <!-- / brand -->
            </div>

            <!-- Flex nav content -->
            <div class="flex hide-scroll">
                <div class="scroll">
                    <div class="nav-border" data-nav>
                        <ul class="nav bg">


                            <li class="active">
                                <a href="dashboard">
		                  <span class="nav-icon">
		                    <i class="fa fa-dashboard"></i>
		                  </span>
                                    <span class="nav-text">Dashboard</span>
                                </a>
                            </li>

                            <li>
                                <a href="users">
		                  <span class="nav-icon">
		                    <i class="fa fa-users"></i>
		                  </span>
                                    <span class="nav-text">User Management</span>
                                </a>
                            </li>
                    <!--                                                                <li>-->
                    <!--            <a href="{{url('teammanagement')}}">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fas fa-user-friends"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Team Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->
                            
                                                        <li>
                                <a href="{{url('blog-management')}}">
		                  <span class="nav-icon">
		                    <i class="fas fa-blog"></i>
		                  </span>
                                    <span class="nav-text">Blogs Management</span>
                                </a>
                            </li>
                                                        <li>
                                <a href="{{url('content')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-sticky-note"></i>
		                  </span>
                                    <span class="nav-text">Content Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('sellinghorsesnew')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-horse"></i>
		                  </span>
                                    <span class="nav-text">Selling Horse</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('reportedads')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-lock"></i>
		                  </span>
                                    <span class="nav-text">Reported Ads</span>
                                </a>
                            </li>
                    <!--        <li>-->
                    <!--            <a href="blogs">-->
		                  <!--<span class="nav-icon">-->
		                  <!--  <i class="fa fa-newspaper-o" aria-hidden="true"></i>-->
		                  <!--</span>-->
                    <!--                <span class="nav-text">Blogs Management</span>-->
                    <!--            </a>-->
                    <!--        </li>-->

                            <li>
                                <a href="promotions">
		                  <span class="nav-icon">
		                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
		                  </span>
                                    <span class="nav-text">Promotions Management</span>
                                </a>
                            </li>

                            <li>
                                <a href="packages">
		                  <span class="nav-icon">
		                    <i class="fa fa-money"></i>
		                  </span>
                                    <span class="nav-text">Packages Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="subscription">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Subscription Management</span>
                                </a>
                            </li>
                            <li>
                                <a href="paymenthistory">
		                  <span class="nav-icon">
		                    <i class="fa fa-credit-card"></i>
		                  </span>
                                    <span class="nav-text">Payment History</span>
                                </a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--		                  <span class="nav-icon">--}}
{{--		                    <i class="fa fa-file-text"></i>--}}
{{--		                  </span>--}}
{{--                                    <span class="nav-text">Packages Management</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <li class="pb-2 hidden-folded"></li>
                        </ul>

                    </div>
                </div>
            </div>
                        <div class='no-shrink lt'>
            <div class='nav-fold'>
                <div class='hidden-folded flex p-2-3 bg'>
                    <div class='d-flex p-1'>
                        <a onclick="logout();" href='javascript:;' class='px-2 logout' data-toggle='tooltip' title='' data-original-title='Logout'>
                            <i class='fa fa-sign-out' aria-hidden='true'></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- ############ Aside END-->
<div id="content" class="app-content box-shadow-4 box-radius-4" role="main">
    <!-- Header -->
    <div class="content-header orangeBg box-shadow-4" id="content-header">
        <div class="navbar navbar-expand-lg">
            <!-- btn to toggle sidenav on small screen -->
            <a class="d-lg-none" data-toggle="modal" data-target="#aside">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg>
            </a>
            <!-- Page title -->

            <div class="navbar-text nav-title flex" id="pageTitle">
                <div class="headerHeading">
                    <h6>Welcome</h6>
                    <h1>Summer Gentry</h1>
                </div>
            </div>


            <ul class="nav flex-row order-lg-2">
                <!-- Notification -->
                <li class="nav-item dropdown" style="width: 20px;">
                    <a title="Notifications" style="right:30px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" data-toggle="dropdown" aria-expanded="false">
                        {{-- <i class="fa fa-bell" aria-hidden="true"></i> --}}
                        <span class="badge badge-pill up danger"></span>
                    </a>
                    <!-- dropdown -->
                    <!--<div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0 notify">-->
                    <!--    <div class="scrollable hover" style="max-height: 250px">-->
                    <!--        <div class="list">-->
                    <!--            <div class="list-item " data-id="item-11">-->
                    <!--    <span class="w-24 avatar circle blue">-->
                    <!--        <span class="fa fa-code-fork"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Tiger Nixon</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Looking for some client-work-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">16:00</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-2">-->
                    <!--    <span class="w-24 avatar circle light-blue">-->
                    <!--        <span class="fa fa-git"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Kygo</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        What&#x27;s the project progress now-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">08:05</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-12">-->
                    <!--    <span class="w-24 avatar circle green">-->
                    <!--        <span class="fa fa-dot-circle-o"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Ashton Cox</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Looking for some client-work-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">11:30</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-4">-->
                    <!--    <span class="w-24 avatar circle pink">-->
                    <!--        <span class="fa fa-male"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Judith Garcia</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Eddel upload a media-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">12:05</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-14">-->
                    <!--    <span class="w-24 avatar circle brown">-->
                    <!--        <span class="fa fa-bell"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Brielle Williamson</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        Looking for some client-work-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">08:00</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--            <div class="list-item " data-id="item-9">-->
                    <!--    <span class="w-24 avatar circle cyan">-->
                    <!--        <span class="fa fa-puzzle-piece"></span>-->
                    <!--    </span>-->
                    <!--                <div class="list-body">-->
                    <!--                    <a href="" class="item-title _500">Pablo Nouvelle</a>-->

                    <!--                    <div class="item-except text-sm text-muted h-1x">-->
                    <!--                        It&#x27;s been a Javascript kind of day-->
                    <!--                    </div>-->

                    <!--                    <div class="item-tag tag hide">-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div>-->
                    <!--                    <span class="item-date text-xs text-muted">15:00</span>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="d-flex px-3 py-2 b-t">-->
                    <!--        <div class="flex">-->
                    <!--            <span>6 Notifications</span>-->
                    <!--        </div>-->
                    <!--        <a href="#">See all <i class="fa fa-angle-right text-muted"></i></a>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div id="activity-dropdown"
                         class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
                        <!-- <label class=“switch”>
                          <input type='checkbox' class=“primary”>
                          <span class=“slider round”></span>
                        </label> -->
                        <div class="loaderNotif" style="display: none;"></div>
                        <div class="body_notification" style="height: 250px;">
                            <div class="scrollable hover notif-padding" style="max-height: 250px">
                                <!-- <div class='list'>
                                </div> -->
                                <div class="notifi-left">
                                    <p class="notify_para">Notifications</p>
                                    <label class="switch">
                                        <input id="notif_toggle" type="checkbox" checked="">
                                        <span id="notif_slid" title="No" class="slider round"></span>
                                    </label>
                                </div>
                                <div class="noti-right" style="display: block;">
                                    <a href="javascript:;" id="remove_notif" style="display: none;">Mark all as
                                        read</a>
                                </div>
                            </div>
                            <!--<div class=“d-flex py-2 b-t infonoti cpadding-noti” style=“background: #ececec91;“>
                              <div class=“flex”>
                                <span >Notifications</span>
                              </div>
                            </div> -->
                            <div class="list" style="display: block;">
                                <div class="no-havenotification"><img
                                            src="http://werdigi.com/public/assets/images/notifications.gif"
                                            class="img-fluid" alt="">
                                    <h2>No Notifications</h2>                                <h4>You currently have
                                        no notifications</h4></div>
                            </div>
                        </div>
                        <!-- / dropdown -->
                    </div>
                    <!-- / dropdown -->
                </li>




                <a title="Change Password" style="right:20px;width: 20px;margin: 0 auto !important;" class="nav-link px-3" onclick="changepasswordmodal();">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span class="badge badge-pill up danger"></span>
                </a>

                <!-- User dropdown menu -->
                <li class="dropdown d-flex align-items-center">
                    <a title="Logout" class="dropdown-item" onclick="logout()" href="#" style="background: transparent !important;border-bottom:none;color:white !important;padding: 0;margin:0">
                        <i class="fa fa-sign-out" aria-hidden="true" style="color: white !important;">
                        </i>
                    </a>
                </li>
                <!-- Navarbar toggle btn -->
                <li class="d-lg-none d-flex align-items-center">
                    <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
                            <path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z"></path>
                        </svg>
                    </a>
                </li>
            </ul>
            <!-- Navbar collapse -->

        </div>
    </div>
    <!-- Main -->
    <div class="content-main " id="content-main" style="margin-left: 20px">
        
        <div class="padding ">
        <div class="paddingBoxDetail box no-padding-top">
            
        <div class="row" style="">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 paddingcustom">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="graph" checked="" class="check-input" value="1">
                        <div class="admin-content analysis-progrebar-ctn stat-1">
                            <a href="https://horsematch.com/masterpanel/users"><i class="fa fa-users dashBrdIcon" aria-hidden="true"></i></a>
                            <h4 class="text-left text-uppercase">Total Users</h4>
                            <div class="vertical-center-box vertical-center-box-tablet">

                                <div class="col-xs-12 cus-gh-hd-pro">
                                    <h2 class="text-left no-margin">{{$recent_members}}</h2>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 paddingcustom">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="graph" class="check-input" value="2">
                        <div class="admin-content analysis-progrebar-ctn stat-3">
                            <a href="https://horsematch.com/masterpanel/paymenthistory"><i class="fa fa-dollar-sign dashBrdIcon" aria-hidden="true"></i></a>
                            <h4 class="text-left text-uppercase">Total Revenue</h4>
                            <div class="vertical-center-box vertical-center-box-tablet">

                                <div class=" cus-gh-hd-pro">
                                    <h2 class="text-left no-margin">{{ number_format($sum, 2, '.', ',') }}</h2>
{{--                                    <h2 class="text-left no-margin">$ {{$sum}}</h2>--}}
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 paddingcustom">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="graph" class="check-input" value="3">
                        <div class="admin-content analysis-progrebar-ctn stat-2">
                            <a href="https://horsematch.com/masterpanel/sellinghorsesnew"><i class="fa fa-horse-head dashBrdIcon" aria-hidden="true"></i></a>
                            <h4 class="text-left text-uppercase">Selling Horses</h4>
                            <div class="vertical-center-box vertical-center-box-tablet">

                                <div class=" cus-gh-hd-pro">
                                    <h2 class="text-left no-margin">{{$soldhorsescount}}</h2>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 paddingcustom">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="graph" class="check-input" value="4">
                        <div class="admin-content analysis-progrebar-ctn stat-4">
                            <i class="fa fa-horse dashBrdIcon" aria-hidden="true"></i>
                            <h4 class="text-left text-uppercase">Sold Horses</h4>
                            <div class="vertical-center-box vertical-center-box-tablet">

                                <div class=" cus-gh-hd-pro">
                                    <h2 class="text-left no-margin">{{$sellinghorsescount}}</h2>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
   </div>

        <div class="paddingBoxHead box graphcanvas">
            <div class="grphImg ">
            <!--<img class="main-logo" src="{{url('public/assets/images//graphimg.png')}}" alt=""> -->
                <canvas id="total-request" data-plugin="" height="335" width="834" class="chartjs-render-monitor" style="display: block; height: 370px; width: 927px;">
                </canvas>
                <canvas id="ontime-request" data-plugin="" height="335" width="834" class="chartjs-render-monitor" style="display: none; height: 370px; width: 927px;">
                </canvas>
                <canvas id="contract-doc" data-plugin="" height="335" width="834" class="chartjs-render-monitor" style="display: none; height: 370px; width: 927px;">
                </canvas>
                <canvas id="app-sold" data-plugin="" height="335" width="834" class="chartjs-render-monitor" style="display: none; height: 370px; width: 927px;">
                </canvas>
                <canvas id="rev-stats" data-plugin="" height="335" width="834" class="chartjs-render-monitor" style="display: none; height: 370px; width: 927px;">
                </canvas>
                <canvas id="mob-adv" data-plugin="" height="335" width="834" class="chartjs-render-monitor" style="display: none; height: 370px; width: 927px;">
                </canvas>

            </div>
        </div>
        
        </div>
        </div>
        
    </div>
    <!-- Footer -->
    <div class="content-footer white " id="content-footer">
        <!-- <div class="d-flex p-3">
            <span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
            <div class="text-sm text-muted">Version 1.1.1</div>
        </div> -->
    </div>
</div>
<style>
    .dashBrdIcon{
        width:10% !important;
    }
</style>
<!-- ############ Content END-->

@extends('admin.layouts.footer')
<!-- jQuery -->
    <script src="{{asset('public/adminassets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('public/adminassets/libs/moment/min/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('public/adminassets/libs/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/plugins/jquery.chart.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/plugins/chartjs.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/lazyload.config.js')}}"></script>
    <script src="{{asset('public/adminassets/scripts/lazyload.js')}}"></script>
    <script>
        $(document).ready(function() {

            // $('select#select2-allow-clear').change(function(){
            //     var url = $('base').attr('href')+"/dashboard?ajax=true";
            //     getcontent(url+"&post_brand_id="+$(this).children("option:selected").val())
            // })
            // $('select#member_id').change(function(){
            //     var url = $('base').attr('href')+"/unit/dashboard?ajax=true";
            //     getcontent(url+"&post_member_id="+$(this).children("option:selected").val())
            // })

            {{--var label_str = "Number Of Users";--}}
            {{--var dataset ={!! json_encode($user_arr) !!} ;--}}
            {{--var label_name = "Requests";--}}
            {{--var bg_color = "#6f42c1";--}}
            {{--var line_color = "#000";--}}
            {{--label = label_str.split(",");--}}
            {{--var canvas = document.getElementById('total-request');--}}
            {{--var ctx = document.getElementById('total-request').getContext('2d');--}}

            {{--var myChart = new Chart(ctx,--}}
            {{--    {--}}
            {{--        type: 'line',--}}
            {{--        data: {--}}
            {{--            labels:label,--}}
            {{--            datasets: [--}}
            {{--                {--}}
            {{--                    label: label_name,--}}
            {{--                    data: dataset,--}}
            {{--                    fill: true,--}}
            {{--                    lineTension: 0.4,--}}
            {{--                    backgroundColor: hexToRGB(bg_color, 0.2),--}}
            {{--                    borderColor: bg_color,--}}
            {{--                    borderWidth: 2,--}}
            {{--                    borderCapStyle: 'butt',--}}
            {{--                    borderDash: [],--}}
            {{--                    borderDashOffset: 0.0,--}}
            {{--                    borderJoinStyle: 'miter',--}}
            {{--                    pointBorderColor: bg_color,--}}
            {{--                    pointBackgroundColor: '#fff',--}}
            {{--                    pointBorderWidth: 2,--}}
            {{--                    pointHoverRadius: 4,--}}
            {{--                    pointHoverBackgroundColor: bg_color,--}}
            {{--                    pointHoverBorderColor: '#fff',--}}
            {{--                    pointHoverBorderWidth: 2,--}}
            {{--                    pointRadius: 4,--}}
            {{--                    pointHitRadius: 10,--}}
            {{--                    spanGaps: false--}}
            {{--                }--}}
            {{--            ]--}}
            {{--        },--}}
            {{--        options: {--}}
            {{--        }--}}
            {{--    }--}}
            {{--);--}}

            {{--//$('#checkit').change();--}}
            {{--$('.check-input').change(function(){--}}

            {{--    switch ($(this).val()) {--}}

            {{--        case "1":--}}
            {{--            var label_str = "Number Of Users";--}}
            {{--            var dataset ={!! json_encode($user_arr) !!} ;--}}
            {{--            var label_name = "Number Of Users";--}}
            {{--            var bg_color = "#6f42c1";--}}
            {{--            var line_color = "#000";--}}
            {{--            label = label_str.split(",");--}}
            {{--            var canvas = document.getElementById('total-request');--}}
            {{--            break;--}}
            {{--        case "2":--}}
            {{--            var label_str = "Total Revenue";--}}
            {{--            --}}{{--var dataset =[{{$UserPost}}] ;--}}
            {{--            var label_name = "Total Revenue";--}}
            {{--            var bg_color = "#e83e8c";--}}
            {{--            var line_color = "#000";--}}
            {{--            label = label_str.split(",");--}}
            {{--            var canvas = document.getElementById('ontime-request');--}}
            {{--            break;--}}
            {{--        case "3":--}}
            {{--            var label_str = "{{"Total Ads"}}";--}}
            {{--            --}}{{--var dataset =[{{$sum}}] ;--}}
            {{--            var label_name = "Total Ads";--}}
            {{--            var bg_color = "#6610f2";--}}
            {{--            var line_color = "#000";--}}
            {{--            label = label_str.split(",");--}}
            {{--            var canvas = document.getElementById('contract-doc');--}}
            {{--            break;--}}
            {{--        default:--}}

            {{--            break;--}}

            {{--    }--}}
            {{--    //document.getElementById('total-request').getContext('2d');--}}
            {{--    //const context = document.getElementById('total-request').getContext('2d');--}}
            {{--    $('canvas').hide();--}}
            {{--    canvas.style.display = "block";--}}
            {{--    //console.log(canvas);--}}
            {{--    //canvas.css("display:none");--}}
            {{--    const ctx = canvas.getContext('2d');--}}

            {{--    var myChart = new Chart(ctx,--}}
            {{--        {--}}
            {{--            type: 'line',--}}
            {{--            data: {--}}
            {{--                labels:label,--}}
            {{--                datasets: [--}}
            {{--                    {--}}
            {{--                        label: label_name,--}}
            {{--                        data: dataset,--}}
            {{--                        fill: true,--}}
            {{--                        lineTension: 0.4,--}}
            {{--                        backgroundColor: hexToRGB(bg_color, 0.2),--}}
            {{--                        borderColor: bg_color,--}}
            {{--                        borderWidth: 2,--}}
            {{--                        borderCapStyle: 'butt',--}}
            {{--                        borderDash: [],--}}
            {{--                        borderDashOffset: 0.0,--}}
            {{--                        borderJoinStyle: 'miter',--}}
            {{--                        pointBorderColor: bg_color,--}}
            {{--                        pointBackgroundColor: '#fff',--}}
            {{--                        pointBorderWidth: 2,--}}
            {{--                        pointHoverRadius: 4,--}}
            {{--                        pointHoverBackgroundColor: bg_color,--}}
            {{--                        pointHoverBorderColor: '#fff',--}}
            {{--                        pointHoverBorderWidth: 2,--}}
            {{--                        pointRadius: 4,--}}
            {{--                        pointHitRadius: 10,--}}
            {{--                        spanGaps: false--}}
            {{--                    }--}}
            {{--                ]--}}
            {{--            },--}}
            {{--            options: {--}}
            {{--            }--}}
            {{--        }--}}
            {{--    );--}}


            {{--});--}}

            var label_str = "Jan,Feb,Mar,Apr,May,June,July,Aug,Sep,Oct,Nov,Dec";
            var dataset =[0,0,0,0,0,0,0,0,0,0,5,2] ;
            var label_name = "Total Users";
            var bg_color = "#6f42c1";
            var line_color = "#000";
            label = label_str.split(",");
            var canvas = document.getElementById('total-request');
            var ctx = document.getElementById('total-request').getContext('2d');

            var myChart = new Chart(ctx,
                {
                    type: 'line',
                    data: {
                        labels:label,
                        datasets: [
                            {
                                label: label_name,
                                data: {!! json_encode($user_arr) !!},
                                fill: true,
                                lineTension: 0.4,
                                backgroundColor: hexToRGB(bg_color, 0.2),
                                borderColor: bg_color,
                                borderWidth: 3,
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: bg_color,
                                pointBackgroundColor: '#fff',
                                pointBorderWidth: 2,
                                pointHoverRadius: 4,
                                pointHoverBackgroundColor: bg_color,
                                pointHoverBorderColor: '#fff',
                                pointHoverBorderWidth: 2,
                                pointRadius: 4,
                                pointHitRadius: 10,
                                spanGaps: false
                            }
                        ]
                    },
                    options: {
                    }
                }
            );
            //$('#checkit').change();
            $('.check-input').change(function(){
                var dashboard ;
                switch ($(this).val()) {
                    case "1":
                        var label_str =  "Jan,Feb,Mar,Apr,May,June,July,Aug,Sep,Oct,Nov,Dec";
                        var dataset =[0,0,0,0,0,0,0,0,0,0,0,0] ;
                        var label_name = " Users";
                        var bg_color = "#6f42c1";
                        var line_color = "#000";
                        label = label_str.split(",");
                        var canvas = document.getElementById('total-request');
                        dashboard = {!! json_encode($user_arr) !!} ;
                        break;
                    case "2":
                        var label_str =  "Jan,Feb,Mar,Apr,May,June,July,Aug,Sep,Oct,Nov,Dec";
                        var dataset =[0,0,0,0,0,0,0,0,0,0,0,0] ;
                        var label_name = " Revenue";
                        var bg_color = "#e83e8c";
                        var line_color = "#000";
                        label = label_str.split(",");
                        var canvas = document.getElementById('ontime-request');
                        dashboard = {!! json_encode($transactions_arr) !!} ;
                        {{--  dashboard = {!! json_encode($amount_arr) !!} ;  --}}
                        break;
                    case "3":
                        var label_str = "Jan,Feb,Mar,Apr,May,June,July,Aug,Sep,Oct,Nov,Dec";
                        var dataset =[0,0,0,0,0,0,0,0,0,0,0,0] ;
                        var label_name = "Sold Horses";
                        var bg_color = "#FC9F8C";
                        var line_color = "#000";
                        label = label_str.split(",");
                        var canvas = document.getElementById('contract-doc');

                        dashboard = {!! json_encode($sellinghorses) !!} ;
                        break;
                    case "4":
                        var label_str =  "Jan,Feb,Mar,Apr,May,June,July,Aug,Sep,Oct,Nov,Dec";
                        var dataset =[0,0,0,0,0,0,0,0,0,0,0,0] ;
                        var label_name = "Selling Horses";
                        var bg_color = "#17a2b8";
                        var line_color = "#000";
                        label = label_str.split(",");
                        var canvas = document.getElementById('app-sold');
                        dashboard = {!! json_encode($soldhorses) !!} ;
                        break;
                    default:
                        break;
                }
                //document.getElementById('total-request').getContext('2d');
                //const context = document.getElementById('total-request').getContext('2d');
                $('canvas').hide();
                canvas.style.display = "block";
                //console.log(canvas);
                //canvas.css("display:none");
                const ctx = canvas.getContext('2d');
                var myChart = new Chart(ctx,
                    {
                        type: 'line',
                        data: {
                            labels:label,
                            datasets: [
                                {
                                    label: label_name,
                                    data: dashboard,
                                    fill: true,
                                    lineTension: 0.4,
                                    backgroundColor: hexToRGB(bg_color, 0.2),
                                    borderColor: bg_color,
                                    borderWidth: 2,
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: bg_color,
                                    pointBackgroundColor: '#fff',
                                    pointBorderWidth: 2,
                                    pointHoverRadius: 4,
                                    pointHoverBackgroundColor: bg_color,
                                    pointHoverBorderColor: '#fff',
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 4,
                                    pointHitRadius: 10,
                                    spanGaps: false
                                }
                            ]
                        },
                        options: {
  scales: {
    xAxes: [{
      ticks: {
        precision: 0
      }
    }],
  }
                        }
                    }
                );
            });
        });

        function changepasswordmodal(){
            $("#changepasswordmodal").modal('show');
        }

    </script>
    <script>
        function changepassword(){
            var password = $("#password").val();
            var confirmpass = $("#confirmpassword").val();
            if(password===''){
                iziToast.error({
                    title: 'Error',
                    message:  "Please Enter A Password First" ,
                });
            }
            if(password===confirmpass){
                $.ajax({
                    url: "{{url('changepass')}}",
                    type: "post",
                    data: {"password":password},
                    success: function (response) {
                        console.log(response);
                        iziToast.success({
                            title: 'Success',
                            message:  response.message ,
                        });
                        setTimeout(function(){ location.reload() }, 4000);
                        // You will get response from your PHP page (what you echo or print)
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
            else{
                iziToast.error({
                    title: 'Error',
                    message:  "Password Did Not Matched" ,
                });
            }
        }

    </script>

