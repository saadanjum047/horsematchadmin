<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="https://kit.fontawesome.com/e24b5cf32d.js" crossorigin="anonymous"></script>
    <title>HorseMatch</title>
    <meta name="description" content="Responsive, Bootstrap, BS4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{asset('public/adminassets/images/favicon.png')}}">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" type="image/png" href="{{asset('adminassets/images/favicon.png')}}">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('public/adminassets/images/favicon.png')}}">

    <!-- style -->

    <link rel="stylesheet" href="{{asset('public/adminassets/libs/font-awesome/css/font-awesome.min.css')}}" type="text/css" />

    <!-- build:css ../assets/css/app.min.css -->
    <link rel="stylesheet" href="{{asset('public/adminassets//libs/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('public/adminassets/css/app.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('public/adminassets/css/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('public/adminassets/css/izitoast.min.css')}}">
    <link href="{{asset('public/adminassets/css/quill.snow.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/adminassets/libs/bootstrap-table/dist/bootstrap-table.min.css')}}"/>
    <script src="{{asset('public/adminassets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="https://cdn.rawgit.com/fechanique/material-loading/master/material-loading.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css'/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css"/>
    <script>
        // $(document).ready(function () {
        //     materialLoading(true);
        //     setTimeout(function () {
        //         materialLoading(false)
        //     }, 1000);
        // });
    </script>
    <!-- endbuild -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css">

    <!-- fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    <style>


        .profile-image-dt{
            width: 35px;
            border-radius: 200%;
        }
        .app{
            overflow: hidden;
        }
        .export .btn{
            margin-left: 3px !important;
        }
        .columns-right .btn i{
            display: none;
        }

        .fixed-table-container{
            max-height: 500px !important;
        }
        .fixed-table-body{
            min-height: 400px;
            max-height: 500px !important;
        }
        .fixed-table-toolbar .form-control{
            padding: 0 10px !important;
            height: 30px !important;
            margin-top: 4px;
            outline: none;
            box-shadow: none;
        }
        .fixed-table-toolbar .dropdown-item{
            padding: 5px 10px !important;
            margin: 0 !important;
        }
        .page-list .dropdown-item{
            padding: 5px 10px !important;
            margin: 0 !important;
        }
        .btn-secondary:not([disabled]):not(.disabled).active, .btn-secondary:not([disabled]):not(.disabled):active, .show>.btn-secondary.dropdown-toggle{
            border-color: #53a6fa;
        }

        .btn-secondary:not([disabled]):not(.disabled).active:focus, .btn-secondary:not([disabled]):not(.disabled):active:focus, .show>.btn-secondary.dropdown-toggle:focus{
            box-shadow: none;
        }
        .page-list .btn{
            background-color: #53a6fa ;
        }
        .columns-right .btn{
            background-color: #53a6fa ;
        }
        .btn-secondary:not([disabled]):not(.disabled).active, .btn-secondary:not([disabled]):not(.disabled):active, .show>.btn-secondary.dropdown-toggle{
            background-color: #3f8cdb;
        }
        .bootstrap-table .fixed-table-container .table{
            margin-bottom: -10px !important;
        }

        
        .fixed-table-body::-webkit-scrollbar-track{
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #ffffff;
        }

        .fixed-table-body::-webkit-scrollbar{
            width: 5px;
            height: 5px;
            background-color: #F5F5F5;
        }


        .fixed-table-body::-webkit-scrollbar-thumb{
            border-radius: 10px;
            background-color: #696969;
            border: 2px solid #696969;
        }







        
        input.form-control.no-border.no-bg.no-shadow{
            color:white;
        }
        input.form-control::placeholder{
            color:slategrey;
        }
        #content-header{
            height: 6rem;
        }
        .nav-text{
        font-family: 'Poppins', sans-serif;
        }
        input.changecolor.form-control::placeholder {
            color: #999ea5 !important;
        }
        .inputmodal input {
            color:black !important;
        }

        .editBtn{
            color: #2F75BB;
        }
        span.nav-text {
            font-family: "TrajanPro-Bold" !important;
            font-weight: 600;
        }
        .teamMangementHead>div>div>h5{
            font-family: "TrajanPro" !important;
        }
        .th-inner{
            font-family: "TrajanPro" !important;
        }
        div.headerHeading>h6{
            font-family: "TrajanPro" !important;
        }
        div.headerHeading>h1 {
            font-weight: bolder;
            font-family: "TrajanPro-Bold" !important;
        }
        .paddingBoxHead {
            padding: 0.3rem 2rem !important;
        }
        .teamMangementHead a{
            margin-top: 5px !important;
            padding: 5px 0 !important;
        }
        .send-promotion{
            padding: 5px 0 !important;
            width: 115% !important;
            margin-top: 5px !important;
        }
        .brandImg{
            margin: 0 auto !important;
        }

.navbarLogo {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 84px;
    background: #3178C0;
    margin-bottom: 22px;
}
.table-responsive {
    border: none;
}
.box-header {
    border: none;
    margin-bottom: 0;
}
.paddingBoxHead {
    padding: 2.5rem 2rem;
}
.box-header div div.input-group a {
    margin-left: 7.3rem !important;
}
.notify:before {
    width: 0;
    height: 0;
    display: inline-block;
    border: 9px solid transparent;
    border-bottom-color: white;
    content: '';
    top: -18px;
    right: 51px;
    position:absolute;
}
.profile:before {
    width: 0;
    height: 0;
    display: inline-block;
    border: 9px solid transparent;
    border-bottom-color: white;
    content: '';
    top: -18px;
    right: 13px;
    position:absolute;
}
.navbar .dropdown-menu-right.profile {
    right: -4px;
    top: 46px;
}
.navbar .dropdown-menu-right.notify {
    right: 12px;
    top: 36px;
}
form.search-form {
    margin: 20px 20px 10px 0;
}
a.editBtn, a.viewBtn, a.exchangeBtn, a.doneBtn,a.logout {
    transition: all 0.3s ease-in-out;
}
a.px-2.logout {
    color: #408EDC;
}
a.px-2.logout:hover {
    color: #408EDC;
}
.check-input {top: 24%;right: 25px;}
.analysis-progrebar-ctn {padding: 22px 20px;}
.analysis-progrebar-ctn h4, .tranffic-als-inner h3{font-size:15px}
.sidenav .nav-border .active > a, .sidenav .nav-active-text .active > a {
    color: #000000;
}
.sidenav .nav li.active > a {
    background-color: transparent;
    background: linear-gradient(45deg, #3177BE 0%, #4798EB 100%);
    background: linear-gradient(45deg, #3f8cdb63 0%, #f0512300 100%);
    border-left: 8px solid #3F8CDB !important;
}
.sidenav .nav li.active {
    background: linear-gradient(45deg, #3f8cdb63 0%, #f0512300 100%);
}
.sidenav .nav li > a:hover, .sidenav .nav li > a:focus {
    color: #000000;
    background-color: transparent;
    background: linear-gradient(45deg, #3177BE 0%, #4798EB 100%);
    background: linear-gradient(45deg, #3f8cdb63 0%, #f0512300 100%);
    border-left: 4px solid #3F8CDB !important;
}
.nav-border .nav > li.active:after {
    border-color: #3F8CDB;
}
/* Lead Management */
.nav-active-primary .nav-link, .nav-active-primary .nav>li>a{
    font-weight:600;
}
.paddingBoxHead {
    padding: 1rem 2rem;
}
.teamMangementHead h5 {
    font-weight: 600;
}
.pull-right.search input {
    height: 29px !important;
    box-shadow: none;
}
.teamMangementHead a {
    background: linear-gradient(45deg, #3178BF 0%, #3178bf8a 100%);
}
.table-responsive, .table {
    border: 1px solid #e6e6e699;
}
#activity-dropdown:before {
    content: ‘\f0d8’;
    font-family: FontAwesome;
    position: absolute;
    top: -18px;
    right: 18px;
    color: #fff;
    font-size: 38px;
}
.profileinfo:before {
    content: ‘\f0d8’;
    font-family: FontAwesome;
    position: absolute;
    top: -16px;
    right: 16px;
    color: #fff;
    font-size: 38px;
}
.nav-text {
    line-height: 20px;
    padding: 14px 0;
    font-size: 15px;
}
.sidenav .nav li > a:hover, .sidenav .nav li > a:focus {
    transition: all 0.15s ease-in-out;
}
.nav-link {
    font-family: 'Poppins', sans-serif !important;
}
.teamMangementHead h5 {
    font-family: 'poppins' !important;
}
.th-inner.sortable.both {
    font-weight: 600 !important;
    font-size: 15px;
    padding: 10px;
}
.nav-active-primary .nav-link.active, .nav-active-primary .nav > li.active > a {
    color: #FFFFFF !important;
    background-color: #3178C0 !important;
}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th {
    vertical-align: middle;
    color: #999B9F;
}
@media only screen and (max-width: 1056px){
    .analysis-progrebar-ctn h4, .tranffic-als-inner h3 {
        font-size: 14px;
    }
    .cus-gh-hd-pro h2 {
        font-size: 22px;
    }
    .admin-content.analysis-progrebar-ctn{
        padding: 15px;
    }
}
@media only screen and (max-width: 398px){
    .navbarLogo {
    height: 100px;
    background: #2F75BB;
}
.app-aside.show > div {
    width: 100vw;}
.analysis-progrebar-ctn h4, .tranffic-als-inner h3 {
    font-size: 12px;
    padding: 0;
    line-height: 14px;
    }
.cus-gh-hd-pro h2 {
    font-size: 13px;
    margin:0;
    padding:0;
}
.dashBrdIcon {
   margin-bottom: 0px;
    padding-top: 0;
    font-size: 15px;
    height: 26px;
}
}

li.d-lg-none.d-flex.align-items-center {
    display: none !important;
}
@media only screen and (max-width: 299px){
.analysis-progrebar-ctn{margin:2px}
}
.navbar-brand>img {
    height: 100%;
    padding-bottom: 6px;
}
.orangeBg {
    background: #2F75BB;
}
.sidenav .nav li > a {
    border-left: 4px solid #fff;
}
.sidenav .nav li.active:before{
    content: '';
    background-color: #3F8CDB;
    height: 100%;
    width: 8px;
    position: absolute;
    left: 0px;
    z-index: 9999 !important;
}
.sidenav .nav li > a:hover, .sidenav .nav li > a:focus {
    transition: none;
}
.sidenav .nav li.active > a{border-left:none !important}
.navbar-brand>img {
    padding-bottom: 0;
}
.paddingBoxHead.box.graphcanvas {
    margin-bottom: 0;
}
.modal-footer .confirmBtn {
    border: 1px solid #FFFFFF;
}
  .headerHeading {
    color:white !important;
  }
  .teamMangementHead a:hover {
    box-shadow: 0px 1px 10px 3px rgb(129 172 216);
}
.paddingBoxDetail.box.no-padding-top {
    padding-top: 0;
}
.deleteBtn {
    color: #EF4747;
    font-size: 16px;
    margin: 0 5px;
    display: inline-block;
}
a.postedBtn ,a.notPostedBtn{
padding: 3px 13px !important;
 width:97px;
    background: #2fbb4063;
display: inline-block;
    border-radius: 50px;
    padding: 6px 8px;
    color: #000;
    font-size: 13px;
}
a.notPostedBtn{
    background:#ff04047a
}
input.promotionsBtn {
    background: linear-gradient(45deg, #3178BF 0%, #3178bf8a 100%);
    display: inline-block;
    width: 200px;
    border-radius: 6px;
    text-align: center;
    color: #fff;
    padding: 10px 0;
    font-size: 14px;
    border: none;
}
div#addBlogModal .modal-dialog .modal-content form .modal-body{padding-bottom: 0 !important;}
div#addBlogModal .modal-dialog .modal-content .modal-header, .modal-content {width: 850px;}
.mtb-10{margin-top:10px !important ;margin-bottom:10px !important}
input.promotionsBtn {
    background: linear-gradient(45deg, #3178BF 0%, #3178bf8a 100%); !important;
    display: inline-block;
    width: 200px;
    border-radius: 6px;
    text-align: center;
    color: #fff;
    padding: 10px 0;
    font-size: 14px;
    border: none;
}
input.promotionsBtn:hover {
    box-shadow: 0px 1px 10px 3px rgb(129 172 216); !important}
    
    i.fa.fa-trash-o {
    font-size: 13px;
}
.inputmodal label {
    color: #495057;
}
.inputmodal p {
    box-shadow:none;
    font-family: 'poppins';
    color: #999EA5;
}
div#viewDetailsModal .modal-dialog {
    display: block;
}
/*#viewDetailsModal .modal-body {*/
/*    max-height: 560px;*/
/*    overflow-y: scroll;*/
/*}*/
.read-only {
    background: #E9ECEF;
}
.form-control:focus {
    color: #1D2733;
    box-shadow: 0 0 0 1px rgb(47 117 187) !important;
}
        i.fa.fa-sign-out{
            color: #FE5050;
        }
        i.fa.fa-sign-out:hover {
            color: #FE5050;
        }
        i.fa.fa-sign-out {
            transition: all 0.35s ease-in-out;
            font-size: 20px;
            margin-top:5px;
        }
        #activity-dropdown{
            right:10px !important;
        }
        .nav-link.px-3{
            margin-right: 60px !important;
        }
    </style>
</head>
<body>


