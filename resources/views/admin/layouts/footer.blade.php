<!-- ############ LAYOUT END-->
</div>
<div id="changepasswordmodal" class="modal fade show" data-backdrop="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm Password Change</h5>
            </div>

            <div class="modal-body text-center padding40 custom_modelview">
                <div class="form-group">
{{--                <label>New Password</label>--}}
                <input id="password" type="password" class="form-control" placeholder="Enter New Password">
                </div>
                <br>
                <div class="form-group">
{{--                    <label>Confirm Password</label>--}}
                    <input id="confirmpassword" type="password" class="form-control" placeholder="Confirm New Password">
                </div>
            </div>

            <div class="modal-footer">
                <a href="javascript:;"  data-dismiss="modal" class="cancelBtn">Cancel</a>
                <a href="#" onclick="changepassworddata();" class="confirmBtn remove">Confirm</a>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>


<div id="confirmationLogoutModal" class="modal fade show" data-backdrop="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
            </div>
            <input type="hidden" id="brand_id" value="XR02EnLZ5o">
            <input type="hidden" id="newstatus" value="">
            <input type="text" id="postid" style="display: none;">
            <div class="modal-body text-center padding40 custom_modelview">
                <p>Are you sure you want to Logout?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:;"  data-dismiss="modal" class="cancelBtn">Cancel</a>
                <a href="/masterpanel/logout"  class="confirmBtn remove">Confirm</a>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>
<script>
    function changepassworddata(){
        var password = $("#password").val();
        var confirmpass = $("#confirmpassword").val();
        if(password===''){
            iziToast.error({
                title: 'Error',
                message:  "Please Enter A Password First" ,
            });
        }
        if(password===confirmpass){
            $.ajax({
                url: "{{url('changepass')}}",
                type: "post",
                data: {"password":password},
                success: function (response) {

                    iziToast.success({
                        title: 'Success',
                        message:  "Password Updated Successfully" ,
                    });
                    setTimeout(function(){ location.reload() }, 4000);
                    // You will get response from your PHP page (what you echo or print)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
        else{
            iziToast.error({
                title: 'Error',
                message:  "Password Did Not Matched" ,
            });
        }
    }
</script>
<!-- build:js scripts/app.min.js -->
<!-- jQuery -->

<!-- Bootstrap -->
<script src="{{asset('public/adminassets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('public/adminassets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- core -->
<script src="{{asset('public/adminassets/libs/pace-progress/pace.min.js')}}"></script>
<script src="{{asset('public/adminassets/libs/pjax/pjax.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/scrollto.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/toggleclass.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/izitoast.min.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/quill.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{asset('public/adminassets/scripts/theme.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/lazyload.config.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/lazyload.js')}}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>


{{-- <script src="{{asset('public/adminassets/scripts/plugins/bootstrap-table.js')}}"></script> --}}

<script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/export/bootstrap-table-export.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<script>

    $('body').tooltip({
        selector: '.tool-tip'
    });
    

    function setUserId(id){
        localStorage.setItem("userid",id);
    }
    function setBlogId(id){
        localStorage.setItem("blogid",id);
    }
    function changeUserStatus(){
        var user_id = localStorage.getItem('userid');
        $.ajax({
            url: "{{url('/users/changestatus')}}",
            type: "post",
            data: {"userid":user_id} ,
            success: function (response) {
                console.log(response);
                iziToast.success({
                    title: 'Success',
                    message:  response.update ,
                });
                setTimeout(function(){ location.reload() }, 4000);
                // You will get response from your PHP page (what you echo or print)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function changeBlogFeatured(){
        var blog_id = localStorage.getItem('blogid');
        $.ajax({
            url: "{{url('/blog/changeisfeatured')}}",
            type: "post",
            data: {"blogid":blog_id} ,
            success: function (response) {
                console.log(response);
                iziToast.success({
                    title: 'Success',
                    message:  response.message ,
                });
                setTimeout(function(){ location.reload() }, 4000);
                // You will get response from your PHP page (what you echo or print)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }


    function changeBlogDelete(){
        var id = localStorage.getItem('blogid');
        $.ajax({
            url: "{{url('/blog/delete/')}}/"+id,
            type: "get",
            success: function (response) {
                console.log(response);
                iziToast.success({
                    title: 'Success',
                    message:  response.message ,
                });
                setTimeout(function(){ location.reload() }, 4000);
                // You will get response from your PHP page (what you echo or print)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }





    function deleteBlog(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "Are You Sure You Want To Delete This Blog?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "{{url('/blog/delete/')}}/"+id,
                    type: "get",
                    success: function (response) {
                        console.log(response);
                        iziToast.success({
                            title: 'Success',
                            message:  response.message ,
                        });
                        setTimeout(function(){ location.reload() }, 4000);
                        // You will get response from your PHP page (what you echo or print)
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        })
    }



    $(document).ready(function (){
        $('.select22').select2({
            placeholder: 'Select an option'
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    //TextArea Script

    /*var x = $(".pagination li.active").html();
    $(".pagination li.active").html("<a>"+x+"</a>");
    var y = $(".pagination li.disabled").html();
    $(".pagination li.disabled").html("<a>"+y+"</a>");
    */
    }); 
    
    function logout(){
        $("#confirmationLogoutModal").modal('show');
        // Swal.fire({
        //     title: 'Are you sure?',
        //     text: "Are You Sure You Want To Logout",
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Yes'
        // }).then((result) => {
        //     if (result.isConfirmed) {
        //         window.location.href='http://dev-cmolds.com/horsematch/logout';
        //     }
        // })
    }
</script>
<!-- Initialize Quill editor -->
<script>

</script>
<!-- endbuild -->
</body>
</html>
