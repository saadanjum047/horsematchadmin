<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <base href="{{url('/')}}" />

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/adminassets/images/favicon.png')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />

    <link rel="shortcut icon" type="image/png" href="{{asset('public/adminassets/images/favicon.png')}}">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700" rel="stylesheet">


    <style type="text/css">


        .fa-fw {
            float: right;
        }
        .form-group-invalid{
            margin-bottom: 1rem ;
            box-shadow: 0px 1px 10px 3px rgba(253, 80, 80, 0.11);;
            padding: 3px 12px;
            border-radius: 6px;
            border: 1px solid #2F75BBc7;
        }
    </style>
    <style>
        body{
            font-family: 'Poppins', sans-serif;
        }
        .login-block{
            background:url('{{asset('public/adminassets/images/bg.png')}}') no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            height:100vh;
            display: flex;
            align-items: center;
            padding: 20px 0;
        }
        .banner-sec{ padding:0;background: #00000052;}
        .container{
            background: #fff;
            max-width: 1170px;
            padding-left: 0;
        }
        .carousel-inner{}
        .carousel-caption{text-align:left; left:5%;}
        .login-sec{padding: 120px 40px 40px 40px;position:relative;background: #fff;}
        .login-sec .copy-text{font-size: 14px;
            font-weight: 500;position:absolute; width:80%; bottom:20px; text-align:center;}
        .login-sec .copy-text i{color:#FEB58A;}
        .login-sec .copy-text a{color:#E36262;}
        .login-sec h2{
            margin: 10px 0;
            font-weight: 600;
            font-size: 21px;
            color: #2d2d2f;
            text-align: center;
            padding:0;
        }
        .login-sec h2 span{
            color: #2f75bb;
        }
        .login-sec p{
            text-align: center;
            color: #999B9F;
            font-size: 13px;
            margin: 0 !important;
        }
        .btn-login{background: #DE6262; color:#fff; font-weight:600;}
        .banner-text{
            width: 100%;
            /*position: absolute;*/
            /*bottom: 40px;*/
            /*padding-left: 30px;*/
            /*top: 40px;*/
            padding-top:20px;
            display:flex;
            flex-direction:column;
        }
        .banner-text img{
            margin-bottom: 100px;
            width:  70%;
        }
        .banner-text h5, h2 {
    padding-left: 45px;
}
        .banner-text h2{
            color: #fff;
            font-size: 18px;
            text-transform: uppercase;
            margin-bottom: 60px;
        }
        .banner-text h5{
                padding-top: 60px;
            margin-bottom:15px;
            font-style: italic;
            color: #fff;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: normal;
        }
        /* .banner-text h2:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;} */
        .banner-text p{
            color: #fff;
            font-size: 12px;}
        .label {
            display: inline-block;
            width: 100%;
            color: #999B9F;
            font-size: 14px;
            margin-bottom: 0px;
            font-weight: 400;
        }
        .inputForm {
            display: inline-block;
            width: 100%;
            height: 28px;
            border: none;
            color: #2d2d2f;
            outline: none;
        }
        .invalid
        {
            display: inline-block;
            width: 100%;
            height: 28px;
            /* border: none; */
            color: #2727bd;
            outline: none;
            border-color:red;
        }
        .inputForm:focus{
            outline: none;
        }
        
        


        .submitbtn{
            display: block;
            width: 100%;
            margin: 0 auto;
            text-align: center;
        }
        .submitbtn a,button{
            display: inline-block;
            margin: 0 10px;
            padding: 7px 52px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            color: #fff;
            background: #2F75BB;
            border-radius: 6px;
            text-transform: capitalize;
            font-size: 16px;
            margin: 23px 0px 0 0;
            -webkit-box-shadow: 0px 10px 20px 0px rgba(29,57,166,0.3);
            -moz-box-shadow: 0px 10px 20px 0px rgba(29,57,166,0.3);
            box-shadow: 0px 10px 20px 0px rgba(29,57,166,0.3);
            /* width: 100%; */
            outline: none;
            border: none;
            text-align: center;
        }
        .submitbtn a:hover, .submitbtn a:focus{
            color: #fff !important;
            color: #2d2d2f;
            text-decoration: none;
        }
        .login-form{
            margin-top: 20px;
        }
        .carousel-indicators .active{
            background-color:  #26abe3;
        }
        .form-check a{
            /*float: right;*/
            color: #2d2d2e;
            font-size: 13px;
        }
        .login-sec .login-credential p{
            font-size: 14px;
            font-weight: 500;
            display: inline-block;
            width: 100%;
            bottom: 20px;
            text-align: center;
            margin: 10px 0;
        }
        .login-credential a{
            color: #26abe3;
        }
        a:hover{
            text-decoration: none;
            list-style: none;
        }
        .form-group{
            margin-bottom: 1rem;
            box-shadow: 0px 1px 10px 3px rgba(0, 0, 0, 0.11);
            padding: 3px 12px;
            border-radius: 6px;
        }
        .d-block {
            display: block!important;
            height: 100%;
        }

        .banner-text a {
            border: 1px solid #fff;
            border-radius: 6px;
            padding: 6px 20px;
            color: #fff;
            margin: 10px 0;
            display: inline-block;
            font-size: 15px;
        }

        input[type=checkbox]:before {
            font-family: "FontAwesome";
            content: "\f00c";
            font-size: 11px;
            color: transparent !important;
            background: #ffffff;
            display: block;
            width: 15px;
            height: 15px;
            border: 1px solid #2F75BB;
            margin-right: 7px;
            border-radius: 3px;
            text-align: center;}

        input[type=checkbox]:checked:before {

            color: black !important;
        }
        .form-check small{
            font-size: 13px;
        }
        input[type=checkbox]:before {
    font-family: "Font Awesome 5 Free" !important;
    background: transparent !important;
    width: 13px !important;
    height: 13px !important;
}
        @media only screen and (min-width: 300px) and (max-width: 479px)
        {
            .form-check {
                position: relative;
                display: inline-block;
                padding-left: 0;
                text-align: center;
                width: 100%;
            }
            .form-check a{

                display: inline-block;
                padding-left: 0;
                text-align: center;
                width: 100%;
            }
            .banner-text{
                /*width: 70%;*/
                position: absolute;
                bottom: 40px;
                padding-left: 16px;
            }
            .hero {
                height: 100% !important;
            }
            .login-block{
                display: inline-block;
            }
            .banner-text img {
                margin-bottom: 40px;
            }
            .d-block{
                height: 400px;
            }
        }
        @media only screen and (min-width: 480px) and (max-width: 639px)
        {
            .form-check {
                position: relative;
                display: inline-block;
                padding-left: 0;
                text-align: center;
                width: 100%;
            }
            .form-check a{

                display: inline-block;
                padding-left: 0;
                text-align: center;
                width: 100%;
            }
            .banner-text{
                width: 100%;
                position: absolute;
                bottom: 40px;
                padding-left: 16px;
                top: auto;
            }
            .hero {
                height: 100% !important;
            }
            .login-block{
                display: inline-block;
            }
            .banner-text img {
                margin-bottom: 80px;
            }
            .d-block{
                height: 400px;
            }
        }
        @media only screen and (min-width: 640px) and (max-width: 767px)
        {

            .hero {
                height: 100% !important;
            }
            .login-block{
                display: inline-block;
            }
            .banner-text img {
                margin-bottom: 80px;
            }
            .banner-text{
                padding-left: 30px;
                padding-right: 30px;
            }
            .d-block{
                height: auto !important;
            }
        }

        @media only screen and (min-width: 768px) and (max-width: 966px)
        {
            .container {
                max-width: 720px ;
            }
        }
        @media only screen and (min-width: 967px) and (max-width: 1024px)
        {
            .banner-text{
                padding-left: 30px;
                padding-right: 30px;
            }
            .container {
                max-width: 960px;
            }
        }

        .video-hero, .video-hero .container, .video-hero .row, .video-hero .col-md-12 {
            height: 100%;
        }
        ._overlay {
            position: absolute;
            background-color: white;
            opacity: 0.5;
            height: 100%;
            width: 100%;
            z-index: 1;
            top: 0px;
        }
        video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            background-size: cover;
            -webkit-transition: 1s opacity;
            transition: 1s opacity;
        }
        .d_t {
            height: 100%;
            display: table;
            margin: 0px auto;
            position: relative;
            z-index: 9;
            margin-top: 140px;
        }
        .d_c {
            vertical-align: middle;
            display: table-cell;
        }
        .hero .hero-content {
            padding-top: 26%;
            position: relative;
            z-index: 2;
        }
        .hero h1 {
            color: #fff;
            margin-bottom: 0px;
            font-size: 66px;
            text-indent: 19px;
        }
        .hero p.intro {
            color: #aaa9a7;
            line-height: 28px;
            font-size: 16px;
            text-rendering: optimizeSpeed;
            margin-bottom: 0px;
            padding-bottom: 13px;
        }
        .hero{
            height: 100vh;
            overflow: hidden;
            background-size: cover;
            position: relative;
        }
        .bgWhite{
            background: #fff;
        }



        .img-h{
            height: 447px;
        }
        .login-sec {
            padding: 55px 22px 20px 25px !important;
            width: 302px !important;
        }

        @media only screen and (max-width: 600px) {
            .login-sec{
                width: 100% !important;
            }
        }
        }

        .form-group-invalid{
            margin-bottom: 1rem ;
            box-shadow: 0px 1px 10px 3px red !important;;
            padding: 3px 12px;
            border-radius: 6px;
            border-color: red !important;
        }
    </style>
    <style>
        /* big landscape tablets, laptops, and desktops */
        @media (max-width:1024px) {
            .analysis-progrebar-ctn {
                padding: 10px 0 1px 5px;
                height: 110px;
            }

            .analysis-progrebar-ctn h4, .tranffic-als-inner h3 {
                font-size: 12px;
                line-height: 14px;
                height: 28px;
            }
            .cus-gh-hd-pro h2 {
                font-size: 20px;
            }
            .paddingBoxHead.box.graphcanvas canvas {
                height: auto !important;
            }
            .banner-text h2 {
                color: #fff;
                font-size: 15px;
                text-transform: uppercase;
                margin-bottom: 12px;
            }}
        /* tablet, landscape iPad, lo-res laptops ands desktops */
        @media (max-width:800px)  {}

        @media (max-width:768px) {
            .logoformobile{
                display: block !important;
                text-align: center;
                margin: 0 auto;
                margin-bottom: 16px;
            }
            .banner-sec{
                display:none;
            }
            .login-sec {
                padding: 35px 22px 44px 25px !important;
                width: 201px !important;
                margin-bottom: 60px;
            }
            li.d-lg-none.d-flex.align-items-center {
                display: none !important;
            }
            div#aside {
                width: 100% !important;
            }
        }

        /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */
        @media (max-width:600px)  {}

        /* smartphones, Android phones, landscape iPhone */
        @media (max-width:480px)  {
            .login-sec {
                padding: 35px 22px 44px 25px !important;
                width: 100% !important;
                margin-bottom: 60px;
                margin: 0 30px 60px 30px;
            }
            .headerHeading {
                font-size: 12px;
            }
            .headerHeading h1 {
                font-size: 12px !important;
            }
            .headerHeading h6 {
                font-size: 12px !important;
            }
            .headerHeading {
                text-transform: capitalize;
                padding: 0 6px !important;
            }
            a.d-flex.notifyflex.nav-link.px-3 {
                margin: 15px 19px !important;;
            }
            .navbar .avatar img {
                border-radius: 528px;
                width: 37px !important;
                height: 37px !important;
            }
            .paddingBoxHead.box.graphcanvas canvas {
                height: 136px !important;
                padding: 0 8px;
            }

            /* smartphones, portrait iPhone, portrait 480x320 phones (Android) */
            @media (max-width:320px)  {}




    </style>

</head>
<body style="background-image: url('{{asset('public/adminassets/images/bg.jpg')}}')">


<?php  use App\Data\Models\User;

$user_email = "";
$pwd = "";
$rem="";
$readonly = "";
if(isset($registration_key) && $registration_key != ""){
    $user =  User::where('registration_key',$registration_key)->first();
    $user_email = ($user != NULL)?$user->email : "admin-presale@mailinator.com" ;
    $readonly = "readonly";
}else{
    if(isset($_COOKIE["email"]) && $_COOKIE["email"] != ""){
        $user_email = $_COOKIE["email"];
        $rem="checked";
    }
    if(isset($_COOKIE["pwd"]) && $_COOKIE["pwd"] != "")
        $pwd = $_COOKIE["pwd"];


}
?>
<section class="hero">


    <div class="video-hero">
        <div class="">
            <div class="">
                <div class="col-md-12">
                    <div class="d_t">
                        <div class="d_c">

                            <div class="row">
                                <div class="logoformobile" style="display:none;">
                                    <div class="col-md-12">
                                        <img src="{{asset('public/adminassets/images/adminlogo1.png')}}">
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-12 col-xs-12 banner-sec">
                                    <div class="" data-ride="carousel">
                                        <div class="" role="listbox">
                                            <div class="">
                                                <!--<img class="d-block img-fluid img-h" src="{{asset('adminassets/images/inputbgImg.png')}}" alt="First slide">-->
                                                <div class="">
                                                    <div class="banner-text"><img src="{{asset('public/adminassets/images/adminlogo1.png')}}">
                                                        <h5>Horsematch</h5>
                                                        <h2>Find Your Best Western & English Horse Match</h2>
                                                        <!--<a href="javascript:;">Learn More</a> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12 login-sec">

                                    <h2>Sign In to <span>Horsematch</span></h2>
                                    <p>Enter your details below</p>
                                    <form class="login-form" id="login_form" action="{{url('/dologin')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
{{--                                            <label for="exampleInputEmail1" class="label">Email</label>--}}
                                            <input type="text" id="email" class="inputForm" required pattern=[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$ name="email" placeholder="Enter Email">

                                        </div>
                                        <div class="form-group">
{{--                                            <label for="exampleInputPassword1" class="label">Password<span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span></label>--}}
{{--                                            <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>--}}
                                            <input required type="password" name="password" id="password" class="inputForm" placeholder="Enter Password">

                                        </div>

                                        <?php if(isset($user) && $user != NULL){?>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1" class="label">Confirm Password <span toggle="#cnfpassword" class="fa fa-fw fa-eye field-icon toggle-cnfpassword"></span></label>
                                            <input type="hidden" name="registration_key" id="registration_key" required value="{{$registration_key}}">
                                            <input type="password" id="cnfpassword" class="inputForm" placeholder="" required value="">
                                        </div>
                                        <?php } ?>

                                        <div>
                                            <p id="offer_messages"></p>
                                        </div>

{{--                                        <div class="form-check">--}}
{{--                                            <label class="form-check-label" style="cursor:pointer">--}}
{{--                                               <input type="checkbox" id="remember_me" name="remember_me" value="1" class="form-check-input">--}}
{{--                                               <small>Remember Me</small>--}}
{{--                                               </label>--}}
{{--                                            --}}
{{--                                            <a style="line-height: 1.55rem; padding-left: 0.925rem; float:right;" href="javascript:;" id="ForgotSubmit">Forgot your Password?</a>--}}
{{--                                        </div>--}}
                                        <div class="submitbtn">
                                            <!--<a href="javascript:;" id="login">Sign In</a>  -->
                                            <button type="submit"> Sign In </button>
                                        </div>

                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="_overlay"></div>

{{--            <video poster="{{asset('public/adminassets/images/loginBg.png')}}" id="bgvid" playsinline="" autoplay="" muted="" loop="">--}}
{{--                <source src="{{asset('public/public/assets/videos/open-plan-working.webm')}}" type="video/webm">--}}
{{--                <source src="https://www.digitonics.com/assets/videos/open-plan-working.mp4" type="video/mp4">--}}
{{--            </video>--}}

        </div>
    </div>



</section>



<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
<script src="{{asset('public/adminassets/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('public/adminassets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('public/adminassets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- core -->
<script src="{{asset('public/adminassets/libs/pace-progress/pace.min.js')}}"></script>
<script src="{{asset('public/adminassets/libs/pjax/pjax.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/scrollto.js')}}"></script>
<script src="{{asset('public/adminassets/scripts/toggleclass.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function (){
       $(".field-icon.toggle-password").click(function (){
        var type = $("#password").attr("type");
        if(type==="password") {
            $("#password").attr("type", "text");
        }
        else{
            $("#password").attr("type", "password");
        }
       });
    });
</script>
@if(Session::has('message'))
    <script>
        $(document).ready(function (){
            console.log('fired');
            iziToast.error({
                title: 'Error',
                message: '{{ Session::get('message') }}',
            });
        });
    </script>
    @php(Session::forget('message'))
@endif
<style>
    #password::placeholder{
        color: grey !important;
    }
    #email::placeholder{
        color: grey !important;
    }

    ::placeholder {
        font-size: 14px;
        color: #9c9ca8 !important;
        opacity: 1; /* Firefox */
    }
    ::-ms-input-placeholder {
        font-size: 14px;
        /* Internet Explorer 10-11 */
        color: #9c9ca8 !important;
    }
    ::-ms-input-placeholder {
        font-size: 14px;
        /* Microsoft Edge */
        color: #9c9ca8 !important;
    }

</style>
</body>
</html>