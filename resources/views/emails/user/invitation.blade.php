<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tree</title>
<link rel="shortcut icon" type="image/png" href="images/logo-tree.png"/>
<style type="text/css">

</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #fff; color:#1a1a1a;margin:0;padding:0" bgcolor="#e4e4e4">
<table class="body-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px;   margin: 0 auto;width:100%;max-width:700px;padding-bottom:25px;background:#f2f2f2">
     <tr>
        <td><img class="logo-img" src="<?php echo url('images/logo.png'); ?>" align="center" style="max-width: 75px;                    display: block;height: auto; margin:0 auto;margin-top: 19px;margin-bottom: 9px;" /></td>    
    </tr>
     <tr>
        <td style="padding:0;"><img style="width: 100%;" src="<?php echo url('images/tree-banner.png'); ?>" alt="" /></td>
    </tr> 
    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
        <td class="container" width="100%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top"><div class="content" style=" font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding:10px 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope itemtype="http://schema.org/ConfirmAction" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 12px; background-color: #fff; margin: 0;" bgcolor="#fff">
                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top"><meta itemprop="name" content="Confirm Email" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" />
                            <table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 15px; margin: 0;">
                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; margin: 0;">
                                    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"> Hello, </td>
                                </tr>
                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; margin: 0;">
                                    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">I use TREE, the world's first full screen social media application, and would like to join in as well.</td>
                                </tr>
                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; margin: 0;">
                                    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">With TREE, you will experience a powerful platform like never before. Simple, easy, and intuitive. Post and share high quality photos and videos with your friends, engage in conversations by commenting on their posts, and explore your interests on the go. It's time you have had a social application that does not compromise on quality.</td>
                                </tr>
                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; margin: 0;">
                                    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">You can click on the following link and download the application available on the store.</td>
                                </tr>
                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; margin: 0;">
                                    <td class="content-block" itemprop="handler" itemscope itemtype="http://schema.org/HttpActionHandler" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                        <a class="download-app-btn pull-left" href="javascript:;"><img src="<?php echo url('images/apple-btn.png'); ?>"></a> 
                                        <a class="download-app-btn pull-right" href="javascript:;"><img src="<?php echo url('images/google-btn.png'); ?>"></a>
                                
                                        </td>
                                </tr>
                                
                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; margin: 0;">
                                    <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"> Thanks, <br>
                                        Tree Team </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <table  style="display:block; width:175px !important; height:auto !important; margin:1.2em auto 0.5em auto !important; text-align:center !important;" align="center">
                        </table>
                    </tr>
                </table>
            </div></td>
        <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
    </tr>
</table>
</body>
</html>
