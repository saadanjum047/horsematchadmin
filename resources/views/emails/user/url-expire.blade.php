<!DOCTYPE html>
<html>
    <head>
        <title>URL Expire</title>
    </head>
    <body>
        <div class="container">
            <div class="content">
                The URL has already been used. If you have forgotten your account credentials, please contact support.
            </div>
        </div>
    </body>
</html>