

function deleteRecord(id){

    $("#removeModal").modal("show")
    $("#confirm_btn").data("id",id)



    $("#confirm_btn").bind("click",function(){

        $(".loaderImage").show()

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);

        var request = $.ajax({
            url: $("base").attr("href")+'/api/admin/user/delete?id='+$(this).data('id'),
            type: 'POST',
            dataType:'json',
            contentType: false,
            processData: false,
            headers: {"Authorization":"Bearer "+access_token},
        });

        request.done(function(data){

            setTimeout(function(){
                $(".loaderImage").hide()
            }, 1000);

            if(data.response.code == 200) {
                $("#removeModal").modal("hide")

                $(".row_".id).parent().parent().parent().remove()
                $('#users').bootstrapTable('refresh', {silent: true})

            }
        });

        request.fail(function(jqXHR, textStatus){

            setTimeout(function(){
                $(".loaderImage").hide()
            }, 1000);
            //Cakeaway.App.displaySpinner("#reset_loading","hide");

            var jsonResponse = $.parseJSON(jqXHR.responseText);
            var html = '';

            for(var i in jsonResponse.error.messages) {
                html += jsonResponse.error.messages[i];
            }

            $("#removeModal").modal("hide")


        });
    })

}






