
$('#add_modal').click(function(){

    $('.errorMessage').html('');
    $('.errorMessage').hide();

    document.getElementById('add_record').reset();
    $("#addrecord").modal("show")

});


$("#add_btn").unbind()

$("#add_record").submit(function(e){
    e.preventDefault()

    $(".loaderImage").show()

    setTimeout(function(){
        $(".loaderImage").hide()
    }, 1000);
    //alert('test');

    var formdata = new FormData( document.getElementById("add_record") )
    // console.log( formdata.get('first_name') )

    var request = $.ajax({
        url: $("base").attr("href")+'/api/admin/temperament/add',
        data: formdata,
        type: 'POST',
        dataType:'json',
        contentType: false,
        processData: false,
        headers: {"Authorization":"Bearer "+access_token},
    });

    request.done(function(res){

        $('.errorMessage').html('');
        $('.errorMessage').hide();

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);

        if(res.response.code == 200) {
            console.log(res)
            //debugger
            $("#addrecord").modal("hide")
            $('#temperaments').bootstrapTable('refresh', {silent: true})
        }
    });

    request.fail(function(jqXHR, textStatus){

        //console.log(jqXHR);

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);


        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for(var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

        $('.errorMessage').html(html);
        $('.errorMessage').show();


    });

})

function editRecord(id){

    $('.errorMessage').html('');
    $('.errorMessage').hide();

    //debugger
    $("#editModal").modal("show")
    // $("#update_btn").data("id",id)

    $(".loaderImage").show()

    setTimeout(function(){
        $(".loaderImage").hide()
    }, 1000);

    var request = $.ajax({
        url: $("base").attr("href")+'/api/admin/temperament/view?id='+id,
        type: 'GET',
        dataType:'json',
        headers: {"Authorization":"Bearer "+access_token},
    });

    request.done(function(res){

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);

        if(res.response.code == 200) {

            var record = res.response.data;
            $("input[name=title]").val(record.title)
            $("input[name=id]").val(record.id)

        }
    });

    request.fail(function(jqXHR, textStatus){

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);


        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for(var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

        $("#editModal").modal("hide")

    });

}

$("#update_btn").unbind()

$("#user_update").submit(function(e){
    e.preventDefault()

    $(".loaderImage").show()

    setTimeout(function(){
        $(".loaderImage").hide()
    }, 1000);

    var formdata = new FormData( document.getElementById("user_update") )
    // console.log( formdata.get('first_name') )

    var request = $.ajax({
        url: $("base").attr("href")+'/api/admin/temperament/update',
        data: formdata,
        type: 'POST',
        dataType:'json',
        contentType: false,
        processData: false,
        headers: {"Authorization":"Bearer "+access_token},
    });

    request.done(function(res){

        $('.errorMessage').html('');
        $('.errorMessage').hide();

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);

        if(res.response.code == 200) {
            console.log(res)
            //debugger
            $("#editModal").modal("hide")
            $('#temperaments').bootstrapTable('refresh', {silent: true})
        }
    });

    request.fail(function(jqXHR, textStatus){

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);


        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for(var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

        $('.errorMessage').html(html);
        $('.errorMessage').show();

    });

})

function deleteRecord(id){

    $("#removeModal").modal("show")
    $("#confirm_btn").data("id",id)

    $("#confirm_btn").bind("click",function(){

        $(".loaderImage").show()

        setTimeout(function(){
            $(".loaderImage").hide()
        }, 1000);

        var request = $.ajax({
            url: $("base").attr("href")+'/api/admin/temperament/delete?id='+$(this).data('id'),
            type: 'POST',
            dataType:'json',
            contentType: false,
            processData: false,
            headers: {"Authorization":"Bearer "+access_token},
        });

        request.done(function(data){

            setTimeout(function(){
                $(".loaderImage").hide()
            }, 1000);

            if(data.response.code == 200) {
                $("#removeModal").modal("hide")

                $(".row_".id).parent().parent().parent().remove()
                $('#temperaments').bootstrapTable('refresh', {silent: true})

            }
        });

        request.fail(function(jqXHR, textStatus){

            setTimeout(function(){
                $(".loaderImage").hide()
            }, 1000);
            //Cakeaway.App.displaySpinner("#reset_loading","hide");

            var jsonResponse = $.parseJSON(jqXHR.responseText);
            var html = '';

            for(var i in jsonResponse.error.messages) {
                html += jsonResponse.error.messages[i];
            }

            $("#removeModal").modal("hide")


        });
    })

}




