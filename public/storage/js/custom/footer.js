$(document).ready(function() {

    $(".loaderImage").show();

    setTimeout(function(){
        $(".loaderImage").hide()
    }, 1000);

});

$("#dashboardactive").click(function () {
    setTimeout(function(){
        window.location = window.location.href

    },1000)

})

$("#ppactive").click(function () {
    setTimeout(function(){
        window.location = window.location.href

    },1000)

})

$("#tcactive").click(function () {
    setTimeout(function(){
        window.location = window.location.href

    },1000)

})

$("#usactive").click(function () {
    setTimeout(function(){
        window.location = window.location.href

    },1000)

})

$(".audio_uploading").change(function () {
    var fileExtension = ['mp3', '3gp', 'wav', 'wma', 'mpga'];
    var filesize = this.files[0].size;
    //alert(filesize);
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only formats are allowed : "+fileExtension.join(', '));
        $("button[type=submit]").attr("disabled", "disabled");
    } else if (filesize > 50524114) {
        alert("Invalid audio size (at least 50mb)");
        $("button[type=submit]").attr("disabled", "disabled");
    } else {
        $("button[type=submit]").removeAttr("disabled");
    }

});

$(".video_uploading").change(function () {
    var fileExtension = ['mp4', '3gp', 'webam', 'flv', 'mkv', 'avi', 'mov'];
    var filesize = this.files[0].size;
    //alert(filesize);
    //alert(filesize);
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only formats are allowed : "+fileExtension.join(', '));
        $("button[type=submit]").attr("disabled", "disabled");
    } else if (filesize > 100524114) {
        alert("Invalid video size (at least 100mb)");
        $("button[type=submit]").attr("disabled", "disabled");
    } else {
        $("button[type=submit]").removeAttr("disabled");
    }

});

$('.customdatetime').datetimepicker({
    // minDate: '0',
    step: 1
    //disabledTimeIntervals : true,
    //pickDate: false,
    //pickTime: true,
    // useSeconds: false,
    // format: 'hh:mm',
    // stepping: 15 //will change increments to 15m, default is 1m

    //"stepMinute" : 5

    //"minuteInterval": 15
    // dateFormat: 'dd-mm-yy',
    //format:'YYYY-MM-DD HH:mm:ss'

});

var dtToday = new Date();

var month = dtToday.getMonth() + 1;
var day = dtToday.getDate();
var year = dtToday.getFullYear();
if(month < 10)
    month = '0' + month.toString();
if(day < 10)
    day = '0' + day.toString();

var maxDate = year + '-' + month + '-' + day;
//alert(maxDate);
$('.datefield').attr('min', maxDate);

var activeTab = "";
$(document).ready(function(){
    var url = document.location.toString(); // select current url shown in browser.
    $(".nav li a").on('click', function(e) {
        var clicked_url = $(this).attr('href');
        localStorage.setItem('clickedurl', $(this).attr('href'));
        activeTab = localStorage.getItem('clickedurl');
        //alert(activeTab);
        if (url.match('#')) {
            $(".nav li a").addClass("activeTab");
        }
        // if(activeTab){
        //   $(this).addClass("activeTab");
        // }
    });
});
