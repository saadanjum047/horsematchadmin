var baseURL = $('base').attr('href')

function formatDate(date) {

		if (date != null) {
			date = new Date(date);

			var monthNames = [
			    "January", "February", "March",
			    "April", "May", "June", "July",
			    "August", "September", "October",
			    "November", "December"
			  ];

			var day = date.getDate();
			var monthIndex = date.getMonth();
			var year = date.getFullYear();

			return day + ' ' + monthNames[monthIndex] + ' ' + year;	
		}
		return null;
}

function userView(id){
		var jsonData = {
				id:id
			}

		$.ajax({

			url: getAppUrl()+'/users/view',
			type: 'GET',
			dataType:'json',
			data: jsonData,
			success:function(response){
				if(response.response.code === 200){
					$('#user_info').empty()
					$('#Licensetab5').empty()
					$('#Vehicletab').empty()
					$('#Insurancetab').empty()

					$('#profile_pic').attr('src',"{{asset('storage/images/default.png')}}")
					
					var user = response.response.data
					$('#user_name').text(user.first_name)
					$('#user_type').text(user.user_type)
					if(user.image != null ){
						$('#profile_pic').attr('src',user.image['1x'])
					}

					$('#user_info').append('<ul class="nav flex-column">\
			            <li class="nav-item">\
			              	<a class="nav-link d-flex text-left">\
			                	<span class="text-sm">\
			                		<i class="fa fa-fw fa-map-marker"></i>\
			                	</span>&emsp;\
			                	<span class="flex">'+user.address+'</span>\
			              	</a>\
			            </li>\
			            <li class="nav-item">\
			              	<a class="nav-link d-flex text-left">\
			                	<span class="text-sm">\
			                		<i class="fa fa-fw fa-phone"></i>\
			                	</span>&emsp;\
			                	<span class="flex">'+user.phone_no+'</span>\
			              	</a>\
			            </li>\
			            <li class="nav-item">\
			              	<a class="nav-link d-flex text-left">\
			                	<span class="text-sm">\
			                		<i class="fa fa-fw fa-sign-in"></i>\
			                	</span>&emsp;\
			                	<span class="flex">'+formatDate(user.join_date)+'</span>\
			              	</a>\
			            </li>\
			            <li class="nav-item">\
			              	<a class="nav-link d-flex text-left">\
			                	<span class="text-sm">\
			                		<i class="fa fa-fw fa-envelope"></i>\
			                	</span>&emsp;\
			                	<span class="flex">'+user.email+'</span>\
			              	</a>\
			            </li>\
			        </ul>')

			        $('#Licensetab5').append('\
			        	<ul class="nav flex-column">\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Security no : \
				                	</span>&emsp;\
				                	<span class="flex">'+user.proof.security_no+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-id-card-o"></i>\
				                		Driving License no : \
				                	</span>&emsp;\
				                	<span class="flex">'+user.proof.driver_licence+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-id-card"></i>\
				                		Expiry date : \
				                	</span>&emsp;\
				                	<span class="flex">'+user.proof.licence_expired_on+'</span>\
				              	</a>\
				            </li>\
				        </ul>\
				        <img src="'+$('base').attr('href')+'/storage/user/'+user.proof.licence_proof+'" style="width:100%;">')

			        var vehicle = user.proof.vehicle_proof
			        $('#Vehicletab').append('\
			        	<ul class="nav flex-column">\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Color : \
				                	</span>&emsp;\
				                	<span class="flex">'+vehicle.vehicle_color+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Model no : \
				                	</span>&emsp;\
				                	<span class="flex">'+vehicle.vehicle_model_no+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Permit no : \
				                	</span>&emsp;\
				                	<span class="flex">'+vehicle.vehicle_permit_no+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Registration no : \
				                	</span>&emsp;\
				                	<span class="flex">'+vehicle.vehicle_registration_no+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Permit issue date  : \
				                	</span>&emsp;\
				                	<span class="flex">'+formatDate(vehicle.vehicle_permit_issue_date)+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Permit last date : \
				                	</span>&emsp;\
				                	<span class="flex">'+formatDate(vehicle.vehicle_permit_last_date)+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<a href="'+baseURL+'/storage/user/'+vehicle.vehicle_permit_proof+'" target="_blank"><b>Click here</b> to view Permit</a><br>\
				                	<a href="'+baseURL+'/storage/user/'+vehicle.vehicle_registration_proof+'" target="_blank"><b>Click here</b> to view Vehicle Registration</a>\
				              	</a>\
				            </li>\
				        </ul>')

			        var proof = (vehicle.vehicle_insurance_proof == null) ? "123":baseURL+"/storage/user/"+vehicle.vehicle_insurance_proof;

			        $('#Insurancetab').append('\
			        	<ul class="nav flex-column">\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Security no : \
				                	</span>&emsp;\
				                	<span class="flex">'+vehicle.vehicle_color+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-hashtag"></i>\
				                		Security no : \
				                	</span>&emsp;\
				                	<span class="flex">'+vehicle.vehicle_insurance_no+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-id-card-o"></i>\
				                		Driving License no : \
				                	</span>&emsp;\
				                	<span class="flex">'+formatDate(vehicle.vehicle_insurance_issue_date)+'</span>\
				              	</a>\
				            </li>\
				            <li class="nav-item">\
				              	<a class="nav-link d-flex text-left">\
				                	<span class="text-sm">\
				                		<i class="fa fa-fw fa-id-card"></i>\
				                		Expiry date : \
				                	</span>&emsp;\
				                	<span class="flex">'+formatDate(vehicle.vehicle_insurance_last_date)+'</span>\
				              	</a>\
				            </li>\
				        </ul>\
				        <img src="'+proof+'" style="width:100%;">')
				}
			} 

		});
}