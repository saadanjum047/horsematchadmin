(function ($) {
	"use strict";

    function detailFormatter(index, row) {
        var html = [];
        $.each(row, function (key, value) {
            html.push('<p><b>' + key + ':</b> ' + value + '</p>');
        });
        return html.join('');
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="remove" href="javascript:void(0)" title="Remove">',
            '<i class="fa fa-trash"></i>',
            '</a>'
        ].join('');
    }

    function textFormatter(data) {
        return '<strong>'+data+'</strong>';
    }

    function getIdSelections($table) {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.name
        });
    }

    var init = function(){

        //users
        var $table = $('#table'),
        $remove = $('#remove'),
        selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'fullname',
                    title: 'Name',
                    sortable: true
                },
                {
                    field: 'mobile_no',
                    title: 'Phone',
                    sortable: true
                },
                {
                    field: 'dob',
                    title: 'Date of Birth',
                    sortable: true
                },
                {
                    field: 'age',
                    title: 'Age',
                    sortable: true
                },
                {
                    field: 'delete',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        //users
        var $table = $('#users'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'username',
                    title: 'Username',
                    sortable: true
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: true
                },
                {
                    field: 'phone',
                    title: 'Phone',
                    sortable: true
                },
                {
                    field: 'total_rating',
                    title: 'Rating',
                    sortable: true
                },
                {
                    field: 'total_ads',
                    title: 'Ads Posted',
                    sortable: true
                },
                {
                    field: 'total_sold',
                    title: 'Ads Sold',
                    sortable: true
                },

                {
                    field: 'total_reported',
                    title: 'Ads Rejected',
                    sortable: true
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        //breeds
        var $table = $('#breeds'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        //disciplines
        var $table = $('#disciplines'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        //temperaments
        var $table = $('#temperaments'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });


        //userhorses
        var $table = $('#userhorses'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'headline',
                    title: 'Headline',
                    sortable: false
                }
            ]
        });

        //usersold
        var $table = $('#usersold'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'headline',
                    title: 'Headline',
                    sortable: false
                }
            ]
        });

        //userfavourite
        var $table = $('#userfavourite'),
            $remove = $('#remove'),
            selections = [];

        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'headline',
                    title: 'Headline',
                    sortable: false
                }
            ]
        });

        //pending_ads
        var $table = $('#pending_ads'),
            $remove = $('#remove'),
            selections = [];

        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'headline',
                    title: 'Headline',
                    sortable: false
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        //approved_ads
        var $table = $('#approved_ads'),
            $remove = $('#remove'),
            selections = [];

        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'headline',
                    title: 'Headline',
                    sortable: false
                },
                {
                    field: 'total_sold',
                    title: 'Sold Count',
                    sortable: false
                },
                {
                    field: 'total_favourite',
                    title: 'Favourite Count',
                    sortable: false
                },

                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        //rejected_ads
        var $table = $('#rejected_ads'),
            $remove = $('#remove'),
            selections = [];

        $table.bootstrapTable({
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    sortable: true
                },
                {
                    field: 'headline',
                    title: 'Headline',
                    sortable: false
                },
                {
                    field: 'total_sold',
                    title: 'Sold Count',
                    sortable: false
                },
                {
                    field: 'total_favourite',
                    title: 'Favourite Count',
                    sortable: false
                },

                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                }
            ]
        });



        //eventmembers
        var $table = $('#eventmembers'),
            $remove = $('#remove'),
            selections = [];


        $table.bootstrapTable({
            columns: [
                {
                    field: 'fullname',
                    title: 'Name',
                    sortable: true
                },
                {
                    field: 'mobile_no',
                    title: 'Phone',
                    sortable: true
                },
                {
                    field: 'dob',
                    title: 'Date of Birth',
                    sortable: true
                },
                {
                    field: 'age',
                    title: 'Age',
                    sortable: true
                },
                {
                    field: 'delete',
                    title: 'Action',
                    sortable: false
                }
            ]
        });

        $table.on('check.bs.table uncheck.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
            // save your data, here just save the current page
            selections = getIdSelections($table);
            // push or splice the selections if you want to save all data selections
        });

        $table.on('expand-row.bs.table', function (e, index, row, $detail) {
            if (index % 2 == 1) {
                $detail.html('Loading from ajax request...');
                $.get('LICENSE', function (res) {
                    $detail.html(res.replace(/\n/g, '<br>'));
                });
            }
        });

        $table.on('all.bs.table', function (e, name, args) {
            //console.log(name, args);
        });

        $remove.click(function () {
            var ids = getIdSelections($table);
            $table.bootstrapTable('remove', {
                field: 'name',
                values: ids
            });
            $remove.prop('disabled', true);
        });

  }

  // for ajax to init again
  $.fn.bootstrapTable.init = init;

})(jQuery);
