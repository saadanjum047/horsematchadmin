
Qreferee.App.Venue = (function(){
	
	var globalPage = '';
	var globalDirection = '';
	var globalkeyword = '';
	var globalUserType = '';


	var config = Qreferee.Config;
	var apiUrl = config.getApiUrl();
	
	var list = function (page, direction,keyword, user_type) {

		var created_by = $('#hidden_user_id').val();
		var sortBy = {};

		if( $('#sort-by-key').val() != "" ){
			sortBy.key = $('#sort-by-key').val();
		}

		if( $('#sort-by-order').val() != "" ){
			sortBy.order = $('#sort-by-order').val();
		}

        page = page || 1;
        direction = direction || '';
        keyword = keyword || '';
        pagination = true;

        globalPage = page;
        globalDirection = direction;
		globalkeyword = keyword;
		globalUserType = user_type;
       
        var data = {};
        data.pagination = pagination;
        data.keyword = keyword;
        data.created_by = created_by;
        data.sort_by = sortBy;
        data.is_panel = 1;

        var request = $.ajax({

            url: apiUrl+'/venue/list?page='+page,
            data:data,
            type: 'GET',
            dataType:'json',
            // headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
        });

		var VenueObj = [];


        request.done(function(response){

        	$('.fa').css('pointer-events', 'auto');
        	var html = '';	
        	if ((typeof response.response.data != "undefined") && response.response.data.length > 0) {

                var index = response.response.pagination.to;
	        	
	        	$.each(response.response.data, function(key, data){

	        		var username = (data.username != null) ? data.username : "Not Available";
	        		var contactNo = ( data.phone_number ) ? data.phone_number : 'Not Available';

	        		var Title = data.title == null ? 'Not Available' : data.title;
	        		var Desc = data.description == null ? 'Not Available' : data.description;
	        		var Address = data.address == null ? 'Not Available' : data.address;

	        		html += '<tr>'+
	        					'<td>'+Title+'</td>\
                                <td>'+Desc+'</td>\
                                <td>'+Address+'</td>\
                            	<td>'+data.start_time+'</td>\
                                <td>'+data.end_time+'</td>\
                                <td><button class="fa fa-eye btn btn-primary btn-sm view-venue" data-key="'+key+'"></button></td>\
                            </tr>';
                    index++;	

                    VenueObj.push(data)
	        	});

				$('#user-list').html(html);

            } else {
                html = '<tr>\
                            <td colspan="6" style="text-align: center;">No record available.</td>\
                         </tr>';

                $('#user-list').html(html);
            }
            
            // events 
          //   $('.remove-row').unbind('click').bind('click', function(){
          //   	$('#block-modal').modal('show')
          //   	$('#block-btn').data('id', $(this).data('user'))
          //   	$('#block-btn').data('block', $(this).data('block'))
          //   	$('#block-btn').data('btn', $(this))

          //   	if( $(this).data('block') == 0 ){
	         //       	$('#block-unblock-text').text('Block');
	         //       	$('#block-btn').removeClass('btn-primary').addClass('btn-danger');
		        // } else {
		        // 	$('#block-unblock-text').text('Unblock');
	         //       	$('#block-btn').removeClass('btn-danger').addClass('btn-primary');
		        // }
          //   })

            $('.block-btn').unbind('click').bind('click', function(){

		        var rowBtn = $($(this).data('btn')).children('i')

		        if( $(this).data('block') == 0 ){
		        	Qreferee.App.User.block($(this).data('id') , 1)
		           rowBtn.removeClass('fa-lock').addClass('fa-unlock')
		        } else {
		        	Qreferee.App.User.block($(this).data('id') , 0)
		           rowBtn.removeClass('fa-unlock').addClass('fa-lock')
		        }
		    });


            // events end

			if (response.response.pagination && response.response.pagination.total > 0) {

				var pageLinks = '';
				if(response.response.pagination.previous == 0 || response.response.pagination.previous == response.response.pagination.current ){
					pageLinks += '<li class="footable-page-arrow disabled"><a data-page="prev" href="#prev">‹</a></li>'
				} else {
					pageLinks += '<li class="footable-page-arrow"><a data-page="'+response.response.pagination.previous+'" href="#prev">‹</a></li>'
				}


				if(response.response.pagination.pages){
					$.each(response.response.pagination.pages, function(k, pageNo){
						pageLinks += '<li class="footable-page active"><a data-page="'+pageNo+'" href="#">'+pageNo+'</a></li>';
					});
				}

				if(response.response.pagination.next == 0 || response.response.pagination.next == response.response.pagination.last ){
					pageLinks += '<li class="footable-page-arrow disabled"><a data-page="next" href="#next">›</a></li>'
				} else {
					pageLinks += '<li class="footable-page-arrow"><a data-page="'+response.response.pagination.next+'" href="#next">›</a></li>'
				}

				$('#user-pagnation').html(pageLinks);

			}


			// View Venue
			$(".view-venue").click(function(){
				$("#venue-modal").modal("show")

				var data = VenueObj[ $(this).data('key') ]

				var Title = data.title == null ? 'Not Available' : data.title;
        		var Desc = data.description == null ? 'Not Available' : data.description;
        		var Address = data.address == null ? 'Not Available' : data.address;

				$(".venue-modal-body").html("<div class='col-sm-6'><b>Title</b></div>\
	      		<div class='col-sm-6'>"+Title+"</div><div class='col-sm-6'><b>Description</b></div>\
	      		<div class='col-sm-6'>"+Desc+"</div><div class='col-sm-6'><b>Address</b></div>\
	      		<div class='col-sm-6'>"+Address+"</div><div class='col-sm-6'><b>Start Time</b></div>\
	      		<div class='col-sm-6'>"+data.start_time+"</div><div class='col-sm-6'><b>End Time</b></div>\
	      		<div class='col-sm-6'>"+data.end_time+"</div><div class='col-sm-6'><b>Latitude</b></div>\
	      		<div class='col-sm-6'>"+data.latitude+"</div><div class='col-sm-6'><b>Longitude</b></div>\
	      		<div class='col-sm-6'>"+data.longitude+"</div>")


			})

		});
	};

	var block = function (id, is_block){
		
		var jsonData = {
			is_block : is_block,
			id : id,
		}
		
		var request = $.ajax({

			url: apiUrl+'/user/block',
			data: jsonData,
			type: 'POST',
			dataType:'json',
			headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
		});

		request.done(function(data){

			if( typeof data.response != "undefined" && data.response.code == 200 ) {
				var messagesHtml = '';

            	$('#block-modal').modal('hide')
		       	Qreferee.App.User.list()
			}

		});

		request.fail(function(jqXHR) {
			
			$('#user-update-btn-text').show();
			$('.icon-spinner').hide();
			
            var responseError = JSON.parse(jqXHR.responseText);
            var errorHtml = "";
            $.each(responseError.error.messages, function(i, val){
                errorHtml += "<span>"+val+"</span>";
            });
            $('#user-update-msg').html(errorHtml).addClass('alert-danger').removeClass('alert-success').show();
        })
	};

	var remove = function(id){

		$('#row-confirm-text').hide();
		$('.icon-confirm-spinner').show();
		$('#row-alert-message').hide();


		var request = $.ajax({
			url: userApiUrl+'/delete',
			data: {id:id},
			type: 'DELETE',
			dataType:'json',
			headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
		});

		request.done(function(data){
			
			$('#row-confirm-text').show();
			$('.icon-confirm-spinner').hide();
			
			if(data.success) {
				$('#row-alert-message').html(data.success).removeClass('alert-danger').addClass('alert-success').show().delay(3000).fadeOut(function(){
		 			$(this).html('');
		 			$(this).removeClass('alert-success');
		 			 $('.rowModal').modal('hide');
		    	});
			}


			if(data.error){
				$('#row-alert-message').html(data.error).removeClass('alert-success').addClass('alert-danger').show().delay(2000).fadeOut(function(){
		 			$(this).html('');
		 			$(this).removeClass('alert-danger');
		    	});
			}

			Qreferee.App.User.list(globalPage,globalDirection,globalkeyword,globalUserType);

		});
	};

	var view = function(id){
		
		$('#update_first_name').val('');
		$('#update_last_name').val('');
		$('#update_password').val('');
		$('#update_verify_password').val('');
		$('#update_email').val('');
		$('#update_phone_number').val('');
		$('#update_role').val('');
		/////$('input[name="status"]').val('on');

		
		var request = $.ajax({
			url: userApiUrl+'/view',
			data: {id:id},
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			/*console.log(data);
			if(data.error){
				window.location.href = config.getAppUrl()+'401';
			}*/

			if( data && (typeof data != "undefined") ){

				$('#user_update_first_name').val(data.first_name);
				$('#user_update_last_name').val(data.last_name);
				$('#user_update_email').val(data.email);
				$('#user_update_phone_number').val(data.contact_number);
				$('#user_update_address').val(data.address);
				$('#user_update_zip_code').val(data.zip_code);
				$('#user_update_country').val(data.country_id);
				$('#user_update_role').val(data.code);
				$('input[name="update-status"][value="off"]').prop('checked', true);
				
				if (data.status == 'on') {
					$('input[name="update-status"][value="on"]').prop('checked', true);
				} 

			}

			$('#user-update-popup').modal('show');

	        $('.update_user').unbind('click').bind('click',function(e){
	            event.preventDefault();
	            Qreferee.App.User.update(data.id);
	        });

		});
	};

	var forgotPassword = function() {

	    $('#forgot-pass-btn-text').hide();
	    $('#forgot-pass-up-spin').show();
	    $('#forgot-pass-msg').hide();

	    var jsonData = {
	        email: $('#forgot-password-email').val()
	    };

	    var request = $.ajax({

	        url: config.getApiUrl() + '/forgot-password',
	        data: jsonData,
	        type: 'PUT',
	        dataType: 'json',
	    });

	    request.done(function(data) {
	    	
	    	$('#forgot-pass-btn-text').show();
	    	$('#forgot-pass-up-spin').hide();

	        if (data.response.code === 200) {
	            $('#forgot-pass-msg').html("An email has been sent with new password.").removeClass('alert-danger').addClass('alert-success').show().delay(2000).fadeOut(function() {
	                $(this).html('');
	                $(this).removeClass('alert-success');
	                $('#myModal').modal('hide');
	                window.location.href = config.getAppUrl() + '/login';
	            });
	        }

	    });

	    request.fail(function(jqXHR) {

	        $('#forgot-pass-btn-text').show();
	    	$('#forgot-pass-up-spin').hide();

	        var responseError = JSON.parse(jqXHR.responseText);
	        var errorHtml = "";
	        $.each(responseError.error.messages, function(i, val) {
	            errorHtml += "<p>" + val + "</p>";
	        });
	        $('#forgot-pass-msg').html(errorHtml).addClass('alert-danger').removeClass('alert-success').show();
	    })
	};

    var resetPassword = function(){

        $('#reset-msg').hide();

        // input
        var token = $('#token').val();
        var email = $('#hidden-email').val();
        var password = $('#password').val();
        var confirm_password = $('#password-confirm').val();

        /*if(password.length < 6 && confirm_password.length< 6){
            $('#reset-msg').html("Password min 6 digits").removeClass('alert-success').addClass('alert-danger').show();
		}*/
		errors = {};

        if ($.trim(password) == '')
            Qreferee.App.inputError('password', "Please enter password.");
        if ($.trim(confirm_password) == '')
            Qreferee.App.inputError('password-confirm', "Please enter confirm password.");

        if( $.trim(password) != $.trim(confirm_password) ) {
        	Qreferee.App.inputError('password-confirm', "Passwords Don't Match");
            //$('#reset-msg').html("Passwords Don't Match").removeClass('alert-success').addClass('alert-danger').show();
        }

        if (Object.keys(errors).length < 1) {

            $('#reset-pass-btn-text').hide();
            $('.icon-spinner').show();

            var jsonData = {
                token:token,
                password:password,
                email:email
            };

            var request = $.ajax({

                url: config.getApiUrl()+'/password/reset',
                data: jsonData,
                type: 'POST',
                dataType:'json'
            });

            request.done(function(data){

                $('#reset-pass-btn-text').show();
                $('.icon-spinner').hide();
                
                if( typeof data.response != "undefined" && data.response.code == 200 ) {
					var messagesHtml = '';
					
					$.each(data.response.messages, function(i,messgae){
						messagesHtml += '<span>'+messgae+'</span>';
					});
					
					$('#reset-msg').html(messagesHtml).removeClass('alert-danger').addClass('alert-success').show().delay(3000).fadeOut(function(){
			 			$(this).html('');
			 			$(this).removeClass('alert-success');
			 			localStorage.setItem('token', data.response.data.access_token );
			 			window.location.href = config.getAppUrl();
			    	});
				}

				if( typeof data.error != "undefined" ){
					var messagesHtml = '';
					$.each(data.error.messages, function(i,messgae){
						messagesHtml += '<span>'+messgae+'</span>';
					});
					$('#reset-msg').html(messagesHtml).removeClass('alert-success').addClass('alert-danger').show();
				}

                /*if(data.success) {

                    $('#reset-msg').html(data.success).removeClass('alert-danger').addClass('alert-success').show().delay(3000).fadeOut(function(){
                        $(this).html('');
                        $(this).removeClass('alert-success');
                        window.location.href = config.getAppUrl();
                    });
                }

                if(data.error){

                    $('#reset-msg').html(data.error).removeClass('alert-success').addClass('alert-danger').show();
                }*/
            });

        } else {
            $('#reset-pass-btn-text').show();
            $('.icon-spinner').hide();

        }
    };

    var changePassword = function (formData){
    	$('#change-pass-msg').hide();
    	$('#CenterModal').modal('hide');
		$('#change-pass-spin').show();
	    $('#change-pass-text').hide();

	    var routeURL = config.getApiUrl()+'/change-password';

		var request = $.ajax({
			url: routeURL,
			data: formData,
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
		});

		request.done(function(data){

			$('#change-pass-spin').hide();
	        $('#change-pass-text').show();

			if( typeof data.response != "undefined" && data.response.code == 200 ) {
				var messagesHtml = '';

				if( typeof dataID != "undefined" && dataID != "" )
					 $('#save-event').data('id', '');
				
				$.each(data.response.messages, function(i,messgae){
					messagesHtml += '<span>'+messgae+'</span>';
				});
				
				$('#change-pass-msg').html(messagesHtml).removeClass('alert-danger').addClass('alert-success').show().delay(3000).fadeOut(function(){
		 			$(this).html('');
		 			$(this).removeClass('alert-success');
		 			Qreferee.App.User.list();
		 			$("#event-modal").modal('hide')
		 			$("#event-form").trigger('reset');
		 			window.location = config.getAppUrl();
		    	});
			}

			if( typeof data.error != "undefined" ){
				var messagesHtml = '';
				$.each(data.error.messages, function(i,messgae){
					console.log(messgae)

					if( $.isArray(messgae) ){
						$.each(messgae, function(i,messgae2){
							if( messgae2 != 'validation.unique_first_and_last_name')
								messagesHtml += '<span>'+messgae2+'</span><br/>';
						});

					} else {

						if( messgae != 'validation.unique_first_and_last_name')
							messagesHtml += '<span>'+messgae+'</span><br/>';
					}

            	    
				});
				if( $('#old_password').val() == "" || $('#new_password').val() == "" || $('#confirm_new_password').val() == "" ){
					$('#change-pass-msg').html(messagesHtml).removeClass('alert-success').addClass('alert-danger').show();
				} else {
					$('#change-pass-msg').html(messagesHtml).removeClass('alert-success').addClass('alert-danger').show();
				}
				
			}

		});

		request.fail(function(jqXHR) {
	        $('#change-pass-spin').hide();
	        $('#change-pass-text').show();
	        var responseError = JSON.parse(jqXHR.responseText);

	        var errorHtml = "";
	        $.each(responseError.error.messages, function(i, val) {
	            errorHtml += "" + val + "<br>";
	        });

	        if( $('#old_password').val() == "" || $('#new_password').val() == "" || $('#confirm_new_password').val() == "" ){
				$('#change-pass-msg').html(errorHtml).removeClass('alert-success').addClass('alert-danger').show();
			} else {
				$('#change-pass-msg').html(errorHtml).removeClass('alert-success').addClass('alert-danger').show();
			}
	    })
	};

	var logout = function(){

	    var request = $.ajax({

	        url: config.getApiUrl() + '/logout',
	        data: {},
	        type: 'POST',
	        dataType: 'json',
            headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
	    });

	    request.done(function(data) {

	        if (data.response.code === 200) {

                localStorage.removeItem('token')
                window.location.href = config.getAppUrl() + '/session/logout';
	        }
	    });

	    request.fail(function(jqXHR) {
	    	console.log(jqXHR);
	        $('#login-sign-up-spin').hide();
	        $('#login-sign-up-text').show();
	        var responseError = JSON.parse(jqXHR.responseText);
	        var errorHtml = "";
	        $.each(responseError.error.messages, function(i, val) {
	            errorHtml += "<p>" + val + "</p>";
	        });
	        $('#login-msg').html(errorHtml).addClass('alert-danger').removeClass('alert-success').show();
	    })
	};

    return {
		list:list,
		remove:remove, 
		view:view,
		forgotPassword:forgotPassword,
        resetPassword:resetPassword,
        changePassword:changePassword,
        logout:logout,
        block:block
	}
}());
