var apppath = $('base').attr('href');
//console.log(appurl)

$( document ).ready(function() {

    //enable button
    $('#login_btn').attr("disabled", false);

    setTimeout(function(){
        $('#loginForm')[0].reset();
    }, 1000);

    loader(0);

});

//loader
function loader(status){
    if (status == 1) {
        //alert('test')
        $('.loaderImage').show()
    } else {
        //alert('demo')
        $('.loaderImage').hide()
    }
}//loader method ends here

$("#login_btn").unbind()

$("#loginForm").submit(function(e){

    e.preventDefault();

    //loader(1)

    $('.errorMessage').html('');
    $('.errorMessage').hide();

    $('.successMessage').html('');
    $('.successMessage').hide();

    $('#loginbutton-spin').show();
    $('#loginbutton-text').hide();

    var formdata = new FormData(document.getElementById("loginForm"))

    var request = $.ajax({
        //url: $("base").attr("href")+'/api/add-user',
        url: apppath + '/api/login',
        data: formdata,
        type: 'POST',
        contentType: false,
        processData: false,
    });

    request.done(function (res) {



        $('.errorMessage').html('');
        $('.errorMessage').hide();

        // $('.successMessage').html('You are logged in...!');
        // $('.successMessage').show();

        if (res.response.code == 200) {

            //disable login button
            $("#login_btn").attr("disabled", true);

            var record = res.response.data;

            //local storage items
            localStorage.setItem("local_userid", record.id);
            localStorage.setItem("local_accesstoken", record.access_token);
            localStorage.setItem("local_email", record.email);
            localStorage.setItem("udid", new Date().getTime() );
            //local storage items ends here

            var is_subscribe = record.is_subscribe

            $('#loginForm')[0].reset();

            setTimeout(function () {

                $('.successMessage').html('');
                $('.successMessage').hide();

                $('#loginbutton-spin').hide();
                $('#loginbutton-text').show();

                if (record.user_type == "admin") {
                    window.location.href = apppath + '/admin-dashboard';
                } else {

                    //subscription validation
                    if (is_subscribe == 0) {
                        // alert('testing');
                        // //user type
                        if (record.user_type == "organization") {
                            window.location.href = apppath + '/organization-selectplans'
                        } else {
                            window.location.href = apppath + '/referee-selectplans'
                            //window.location.href = apppath + '/referee-selectplans'
                        }
                    } else {
                        // alert('yahan mat aao')
                        // //user type
                        if (record.user_type == "organization") {
                            window.location.href = apppath + '/organization-home';
                        } else {
                            window.location.href = apppath + '/referee-home';
                        }
                    }//validation ends here

                }//admin handling

                //loader(0);

            }, 500);

        }
    });

    request.fail(function (jqXHR, textStatus) {

        // setTimeout(function () {
        //     loader(0);
        // }, 500);

        $('.successMessage').html('');
        $('.successMessage').hide();

        $('#loginbutton-spin').hide();
        $('#loginbutton-text').show();

        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for (var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

        $('.errorMessage').text(html);
        $('.errorMessage').show();

    });

})

$("#update_btn").unbind()

$("#updateRecord").submit(function(e){

    e.preventDefault();

    $('.formerrorMessage').html('');
    $('.formerrorMessage').hide();

    $('.formsuccessMessage').html('');
    $('.formsuccessMessage').hide();


    $('#upadtebutton-spin').show();
    $('#upadtebutton-text').hide();

    //loader(1);

    //alert('test')

    var formdata = new FormData(document.getElementById("updateRecord"))

    //console.log(formdata)

    var request = $.ajax({
        url: appurl + '/api/forgotpassword',
        data: formdata,
        type: 'POST',
        contentType: false,
        processData: false,
        //headers: {"Authorization":"Bearer "+access_token},
    });

    request.done(function (res) {

        $('.formerrorMessage').html('');
        $('.formerrorMessage').hide();

        $('.formsuccessMessage').html('Email is sent...!');
        $('.formsuccessMessage').show();

        // setTimeout(function(){
        //     loader(0);
        // }, 1000);

        if (res.response.code == 200) {
            console.log(res)

            //disable forgot button
            $("#update_btn").attr("disabled", true);

            $('#updateRecord')[0].reset();

            //console.log(is_subscribe);

            setTimeout(function () {

                $('#forgotModal').modal('hide');

                $('.formsuccessMessage').html('');
                $('.formsuccessMessage').hide();

                $('#upadtebutton-spin').hide();
                $('#upadtebutton-text').show();

                // //reload profile
                // showProfile();

            }, 1000);

        }
    });

    request.fail(function (jqXHR, textStatus) {

        console.log('error')

        // setTimeout(function(){
        //     loader(0);
        // }, 1000);

        $('.formsuccessMessage').html('');
        $('.formsuccessMessage').hide();

        $('#upadtebutton-spin').hide();
        $('#upadtebutton-text').show();

        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for (var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

        $('.formerrorMessage').text(html);
        $('.formerrorMessage').show();

    });

})