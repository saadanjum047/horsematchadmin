var appurl = $('base').attr('href');

//isEmail
function isEmail(email) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
}//isEmail method ends here

$(".sub-menu").click(function(e) {
    e.stopPropagation(); });

$(".audio_uploading").change(function () {
    var fileExtension = ['mp3', '3gp', 'wav', 'wma', 'mpga'];
    var filesize = this.files[0].size;
    //alert(filesize);
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only formats are allowed : "+fileExtension.join(', '));
        $("button[type=submit]").attr("disabled", "disabled");
    } else if (filesize > 50524114) {
        alert("Invalid audio size (at least 50mb)");
        $("button[type=submit]").attr("disabled", "disabled");
    } else {
        $("button[type=submit]").removeAttr("disabled");
    }

});

$(".video_uploading").change(function () {
    var fileExtension = ['mp4', '3gp', 'webam', 'flv', 'mkv', 'avi', 'mov'];
    var filesize = this.files[0].size;
    //alert(filesize);
    //alert(filesize);
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only formats are allowed : "+fileExtension.join(', '));
        $("button[type=submit]").attr("disabled", "disabled");
    } else if (filesize > 100524114) {
        alert("Invalid video size (at least 100mb)");
        $("button[type=submit]").attr("disabled", "disabled");
    } else {
        $("button[type=submit]").removeAttr("disabled");
    }

});

$(".file_uploading").change(function (e) {
    //alert( e.target.value.length)
    var input_value = $(this).val();
    var action_length = e.target.value.length;
    //alert(input_value);

    //alert('selected');
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
    if (action_length > 0) {
        if ($.inArray(input_value.split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : " + fileExtension.join(', '));
            $("input[type=submit]").attr("disabled", "disabled");
        } else {
            $("input[type=submit]").removeAttr("disabled");
        }
    }

});