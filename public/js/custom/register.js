var apppath = $('base').attr('href');

$( document ).ready(function() {

    setTimeout(function(){
        $('#regForm')[0].reset();
    }, 1000);

    $('.chzn-select').each(function () {
        if ($(this).html().trim().length > 0) {
            pimpSelect($(this));
        }
    });

});

function pimpSelect(select, options) {
    var prepend = '';
    if (select.attr('data-placeholder')) {
        prepend = '<option></option>';
    }
    if (options) {
        options = prepend + options;
        select.empty().html(options);
    } else {
        select.prepend(prepend);
    }
    if (select.hasClass('chzn-select')) {
        var _width = select.css('width');
        select.chosen({
            width: _width
        });
    }
}

function show1(){
    document.getElementById('div1').style.display ='block';
    document.getElementById('div2').style.display ='none';
}
function show2(){
    document.getElementById('div1').style.display = 'none';
    document.getElementById('div2').style.display ='block';
}

//manageCountries
manageCountries();

//sports_list
sports_list();

//loader
function loader(status){
    if (status == 1) {
        //alert('test')
        $('.loaderImage').show()
    } else {
        //alert('demo')
        $('.loaderImage').hide()
    }
}//loader method ends here

//manageCountries
function manageCountries() {

    loader(1)


    var request = $.ajax({
        url: apppath + '/api/get/countries',
        type: 'GET',
        contentType: false,
        processData: false,
    });

    request.done(function (res) {

        setTimeout(function(){
            loader(0);
        }, 300);

        //console.log(res)

        var record = res.response.data
        var html = '';
        var last_id = '';

        console.log(record)

        if (record.length > 0) {

            var num = 1;
            for (var i = 0; i < record.length; i++) {
                last_id = record[i].code
                html +='<option value="'+record[i].code+'">'+record[i].name+'</option>'
            }

            $('.country_id_list').html(html);

            $('.country_id_list').data("placeholder","Select").trigger('chosen:updated');

            //last id handling

            var country_value = "AF";
            manage_citylist(country_value)

            //ajax method

        } else {
            html = "<option value='0'>No country found</option>";
            $('.country_id_list').html(html);

        }


    });

    request.fail(function (jqXHR, textStatus) {

        setTimeout(function(){
            loader(0);
        }, 300);

        console.log('error');

        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for (var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

    });

}//manageCountries method ends here

$('.country_id_list').on('change', function () {
    var country_id = $(this).val();

    $('.city_id_list').html('')
    
    //change city with country
    manage_citylist(country_id)

    loader(1)

    
})//country on change handling ends here

function manage_citylist(country_id) {

    loader(0)

    var request = $.ajax({
        url: apppath + '/api/get/cities?code='+country_id,
        type: 'GET',
        contentType: false,
        processData: false,
    });

    request.done(function (res) {

        var cityrecord = res.response.data
        var cityhtml = '';

        //conditional work
        if (cityrecord.length > 0) {
            var num = 1;
            for (var i = 0; i < cityrecord.length; i++) {
                //last_id = record[i].id
                cityhtml +='<option value="'+cityrecord[i].id+'">'+cityrecord[i].name+'</option>'
            }

            $('.city_id_list').html(cityhtml);
        } else {
            cityhtml = "<option value='0'>No city found</option>";
            $('.city_id_list').html(cityhtml);
        }//conditional work ends here



    });

    request.fail(function (jqXHR, textStatus) {

        //loader(0)

        console.log('error');

        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for (var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

    });

    //ajax method ends here


}

//sports_list
function sports_list() {

    var request = $.ajax({
        url: apppath + '/api/get/sports',
        type: 'GET',
        contentType: false,
        processData: false,
    });

    request.done(function (res) {

        //console.log(res)

        var record = res.response.data
        var html = '';
        var last_id = '';

        //console.log(record)

        if (record.length > 0) {

            var num = 1;
            for (var i = 0; i < record.length; i++) {
                last_id = record[i].id
                html +='<option value="'+last_id+'">'+record[i].name+'</option>'
            }

            //console.log(html)

            $('#org_sports').html(html);
            $('#org_sports').data("placeholder","Select").trigger('chosen:updated');

        } else {
            html = "<option value='0'>No sport found</option>";
            $('#org_sports').html(html);
            $('#org_sports').data("placeholder","Select").trigger('chosen:updated');
        }

        $(".chzn-select").html(html)

        $('.chzn-select').each(function () {
            if ($(this).html().trim().length > 0) {
                pimpSelect($(this));
            }
        });

    });

    request.fail(function (jqXHR, textStatus) {



        console.log('error');

        var jsonResponse = $.parseJSON(jqXHR.responseText);
        var html = '';

        for (var i in jsonResponse.error.messages) {
            html += jsonResponse.error.messages[i];
        }

    });

}//sports_list method ends here

//isEmail
function isEmail(email) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
};//isEmail method ends here

/* Organization */

$("#reg_btn").unbind()

$("#regForm").submit(function(e){

    e.preventDefault();

    //alert('test');

    //input
    var username = $('#username').val();
    var org_name = $('#org_name').val();
    var contact_person = $('#contact_person').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var dob = $('#dob').val();
    var country_id = $('#country_id').val();
    var city_id = $('#city_id').val();
    var zipcode = $('#zipcode').val();
    var phone = $('#phone').val();

    //input validation
    if (username == "" || username == null) {

        $('html, body').animate({
            scrollTop: $("#username").offset().top - 100

        }, 500);

        $( "#username").css('border', '1px solid red');

    } else if (org_name == "" || org_name == null) {

        $( "#username").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#org_name").offset().top - 100

        }, 500);

        $( "#org_name").css('border', '1px solid red');

    } else if ( contact_person == "" || contact_person == null) {

        $( "#org_name").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#contact_person").offset().top - 100

        }, 500);

        $( "#contact_person").css('border', '1px solid red');

    } else if (!isEmail(email) || email == "" || email == null) {

        $( "#contact_person").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#email").offset().top - 100

        }, 500);

        $( "#email").css('border', '1px solid red');

    } else if ( password == "" || password == null) {

        $( "#email").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#password").offset().top - 100

        }, 500);

        $( "#password").css('border', '1px solid red');

    } else if (dob == "" || dob == null) {

        $( "#password").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#dob").offset().top - 100

        }, 500);

        $( "#dob").css('border', '1px solid red');

    } else if (country_id == 0) {

        $("#dob").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#country_id").offset().top - 100
        }, 500);

        $( "#country_id").css('border', '1px solid red');

    } else if (city_id == 0) {

        $( "#country_id").css('border', '1px solid green');
        $("#dob").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#city_id").offset().top - 100
        }, 500);

        $( "#city_id").css('border', '1px solid red');

    }  else if ( zipcode == "" || zipcode == null) {

        $( "#city_id").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#zipcode").offset().top - 100

        }, 500);

        $( "#zipcode").css('border', '1px solid red');

    } else if ( phone == "" || phone == null) {

        $( "#zipcode").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#phone").offset().top - 100

        }, 500);

        $( "#phone").css('border', '1px solid red');

    }  else {

        $( "#org_name").css('border', '1px solid green');
        $( "#contact_person").css('border', '1px solid green');
        $( "#email").css('border', '1px solid green');
        $( "#username").css('border', '1px solid green');
        $( "#password").css('border', '1px solid green');
        $( "#dob").css('border', '1px solid green');
        $( "#country_id").css('border', '1px solid green');
        $( "#city_id").css('border', '1px solid green');
        $( "#phone").css('border', '1px solid green');
        $( "#zipcode").css('border', '1px solid green');

        $('.errorMessage').html('');
        $('.errorMessage').hide();

        $('.successMessage').html('');
        $('.successMessage').hide();

        $('#addbutton-spin').show();
        $('#addbutton-text').hide();

        var formdata = new FormData(document.getElementById("regForm"))

        var request = $.ajax({
            //url: $("base").attr("href")+'/api/add-user',
            url: apppath + '/api/register',
            data: formdata,
            type: 'POST',
            contentType: false,
            processData: false,
        });

        request.done(function (res) {

            $('.errorMessage').html('');
            $('.errorMessage').hide();

            $('.successMessage').html('You are registered successfully!');
            $('.successMessage').show();

            if (res.response.code == 200) {
                console.log(res)

                var record = res.response.data;
                console.log(record)

                //local storage items
                localStorage.setItem("local_userid", record.id);
                localStorage.setItem("local_accesstoken", record.access_token);
                localStorage.setItem("local_email", record.email);
                //local storage items ends here

                $('#regForm')[0].reset();

                setTimeout(function () {

                    $('.successMessage').html('');
                    $('.successMessage').hide();

                    $('#addbutton-spin').hide();
                    $('#addbutton-text').show();

                    window.location.href = apppath + '/organization-selectplans'

                    //location.reload();

                }, 500);

            }
        });


        request.fail(function (jqXHR, textStatus) {

            console.log('error')

            $('.successMessage').html('');
            $('.successMessage').hide();

            $('#addbutton-spin').hide();
            $('#addbutton-text').show();

            var jsonResponse = $.parseJSON(jqXHR.responseText);
            var html = '';

            for (var i in jsonResponse.error.messages) {
                html += jsonResponse.error.messages[i];
            }

            $('.errorMessage').text(html);
            $('.errorMessage').show();

        });

    }//input validation ends here

})

/* Referee */

$("#ref_btn").unbind()

$("#refForm").submit(function(e){

    e.preventDefault();

    //alert('test');

    //input
    var username = $('#referee_username').val();
    var email = $('#referee_email').val();
    var password = $('#referee_password').val();
    var country_id = $('#refree_country_id').val();
    var city_id = $('#referee_city_id').val();
    var zipcode = $('#referee_zipcode').val();

    //input validation
    if (username == "" || username == null) {

        $('html, body').animate({
            scrollTop: $("#referee_username").offset().top - 100

        }, 500);

        $( "#referee_username").css('border', '1px solid red');

    } else if (!isEmail(email) || email == "" || email == null) {

        $( "#referee_username").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#referee_email").offset().top - 100

        }, 500);

        $( "#referee_email").css('border', '1px solid red');

    } else if ( password == "" || password == null) {

        $( "#referee_email").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#referee_password").offset().top - 100

        }, 500);

        $( "#referee_password").css('border', '1px solid red');

    } else if (country_id == 0) {

        $("#referee_password").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#refree_country_id").offset().top - 100
        }, 500);

        $( "#refree_country_id").css('border', '1px solid red');

    } else if (city_id == 0) {

        $( "#refree_country_id").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#referee_city_id").offset().top - 100
        }, 500);

        $( "#referee_city_id").css('border', '1px solid red');

    }  else if ( zipcode == "" || zipcode == null) {

        $( "#referee_city_id").css('border', '1px solid green');
        $( "#refree_country_id").css('border', '1px solid green');

        $('html, body').animate({
            scrollTop: $("#referee_zipcode").offset().top - 100

        }, 500);

        $( "#referee_zipcode").css('border', '1px solid red');

    }  else {

        $( "#referee_username").css('border', '1px solid green');
        $( "#referee_email").css('border', '1px solid green');
        $( "#referee_password").css('border', '1px solid green');
        $( "#refree_country_id").css('border', '1px solid green');
        $( "#referee_city_id").css('border', '1px solid green');
        $( "#referee_zipcode").css('border', '1px solid green');

        $('.referrorMessage').html('');
        $('.referrorMessage').hide();

        $('.refsuccessMessage').html('');
        $('.refsuccessMessage').hide();

        $('#refaddbutton-spin').show();
        $('#refaddbutton-text').hide();

        var formdata = new FormData(document.getElementById("refForm"))

        var request = $.ajax({
            //url: $("base").attr("href")+'/api/add-user',
            url: apppath + '/api/register',
            data: formdata,
            type: 'POST',
            contentType: false,
            processData: false,
        });

        request.done(function (res) {

            $('.referrorMessage').html('');
            $('.referrorMessage').hide();

            $('.refsuccessMessage').html('You are registered successfully!');
            $('.refsuccessMessage').show();

            if (res.response.code == 200) {
                
                var record = res.response.data;
                console.log(record)

                //local storage items
                localStorage.setItem("local_userid", record.id);
                localStorage.setItem("local_accesstoken", record.access_token);
                localStorage.setItem("local_email", record.email);
                //local storage items ends here


                $('#refForm')[0].reset();

                setTimeout(function () {

                    $('.refsuccessMessage').html('');
                    $('.refsuccessMessage').hide();

                    $('#refaddbutton-spin').hide();
                    $('#refaddbutton-text').show();

                    window.location.href = apppath + '/referee-selectplans'

                    //location.reload();

                }, 500);

            }
        });


        request.fail(function (jqXHR, textStatus) {

            console.log('error')

            $('.refsuccessMessage').html('');
            $('.refsuccessMessage').hide();

            $('#refaddbutton-spin').hide();
            $('#refaddbutton-text').show();

            var jsonResponse = $.parseJSON(jqXHR.responseText);
            var html = '';

            for (var i in jsonResponse.error.messages) {
                html += jsonResponse.error.messages[i];
            }

            $('.referrorMessage').text(html);
            $('.referrorMessage').show();

        });

    }//input validation ends here

})