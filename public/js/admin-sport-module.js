
Qreferee.App.Sport = (function(){
	
	var globalPage = '';
	var globalDirection = '';
	var globalkeyword = '';
	var globalUserType = '';

	var config = Qreferee.Config;
	var apiUrl = config.getApiUrl();

	
	var list = function (page, direction,keyword, user_type) {
		var sports = [];
		var created_by = $('#hidden_user_id').val();
		var sortBy = {};

		if( $('#sort-by-key').val() != "" ){
			sortBy.key = $('#sort-by-key').val();
		}

		if( $('#sort-by-order').val() != "" ){
			sortBy.order = $('#sort-by-order').val();
		}

        page = page || 1;
        direction = direction || '';
        keyword = keyword || '';
        pagination = true;

        globalPage = page;
        globalDirection = direction;
		globalkeyword = keyword;
		globalUserType = user_type;
       
        var data = {};
        data.pagination = pagination;
        data.keyword = keyword;
        data.created_by = created_by;
        data.sort_by = sortBy;
        data.is_panel = 1;

        var request = $.ajax({

            url: apiUrl+'/sports/list?page='+page,
            data:data,
            type: 'GET',
            dataType:'json',
            // headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
        });


        request.done(function(response){

        	$('.fa').css('pointer-events', 'auto');
        	var html = '';	
        	if ((typeof response.response.data != "undefined") && response.response.data.length > 0) {

                var index = response.response.pagination.to;
	        	
	        	$.each(response.response.data, function(key, data){

	        		html += '<tr>'+
	        					'<td>'+(key+1)+'</td>\
                                <td>'+data.name+'</td>\
                                <td class="text-right"><button class="btn btn-primary fa fa-edit edit-sport" data-key="'+key+'"></button>\
                                <button class="btn btn-danger fa fa-trash remove-sport" data-id="'+data.id+'"></button></td>\
                            </tr>';
                    index++;	

                    sports.push(data);
	        	});

				$('#sport-list').html(html);

            } else {
                html = '<tr>\
                            <td colspan="3" style="text-align: center;">No record available.</td>\
                         </tr>';

                $('#sport-list').html(html);
            }

            if (response.response.pagination && response.response.pagination.total > 0) {

				var pageLinks = '';
				if(response.response.pagination.previous == 0 || response.response.pagination.previous == response.response.pagination.current ){
					pageLinks += '<li class="footable-page-arrow disabled"><a data-page="prev" href="#prev">‹</a></li>'
				} else {
					pageLinks += '<li class="footable-page-arrow"><a data-page="'+response.response.pagination.previous+'" href="#prev">‹</a></li>'
				}


				if(response.response.pagination.pages){
					$.each(response.response.pagination.pages, function(k, pageNo){
						pageLinks += '<li class="footable-page active"><a data-page="'+pageNo+'" href="#">'+pageNo+'</a></li>';
					});
				}

				if(response.response.pagination.next == 0 || response.response.pagination.next == response.response.pagination.last ){
					pageLinks += '<li class="footable-page-arrow disabled"><a data-page="next" href="#next">›</a></li>'
				} else {
					pageLinks += '<li class="footable-page-arrow"><a data-page="'+response.response.pagination.next+'" href="#next">›</a></li>'
				}

				$('#sport-pagnation').html(pageLinks);
			}
			
			$('.edit-sport').click(function(){
				$("#add-sport-modal").modal("show")
				var data = sports[ $(this).data('key') ]
				$("input[name=name]").val(data.name)
				$("#sport-save-btn").text("Update").data("id",data.id)
			});

			$('.remove-sport').click(function(){
				$("#remove-modal").modal("show")
				$("#remove-btn").data("id", $(this).data('id') )
			});

			$("#remove-btn").unbind('click')
			$("#remove-btn").click(function(){
				Qreferee.App.Sport.remove($(this).data('id'))
			})
		});
	};

	var create = function (formData){
		
		var request = $.ajax({
			url: apiUrl+'/sport/add',
			data: formData,
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
		});

		request.done(function(data){
			if( typeof data.response != "undefined" && data.response.code == 200 ) {
				var messagesHtml = '';
				$("#add-sport-modal").modal("hide")
		       	Qreferee.App.Sport.list()
			}
		});

		request.fail(function(jqXHR) {
			
			$('#user-update-btn-text').show();
			$('.icon-spinner').hide();
			
            var responseError = JSON.parse(jqXHR.responseText);
            var errorHtml = "";
            $.each(responseError.error.messages, function(i, val){
                errorHtml += "<span>"+val+"</span>";
            });
            $('#user-update-msg').html(errorHtml).addClass('alert-danger').removeClass('alert-success').show();
        })
	};

	var update = function (formData){
		
		var request = $.ajax({
			url: apiUrl+'/sport/update',
			data: formData,
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
		});

		request.done(function(data){
			if( typeof data.response != "undefined" && data.response.code == 200 ) {
				var messagesHtml = '';
				$("#add-sport-modal").modal("hide")
		       	Qreferee.App.Sport.list()
			}
		});

		request.fail(function(jqXHR) {
			
			$('#user-update-btn-text').show();
			$('.icon-spinner').hide();
			
            var responseError = JSON.parse(jqXHR.responseText);
            var errorHtml = "";
            $.each(responseError.error.messages, function(i, val){
                errorHtml += "<span>"+val+"</span>";
            });
            $('#user-update-msg').html(errorHtml).addClass('alert-danger').removeClass('alert-success').show();
        })
	};

	var remove = function(id){

		var request = $.ajax({
			url: apiUrl+'/sport/delete',
			data: {id:id},
			type: 'POST',
			dataType:'json',
			headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
		});

		request.done(function(data){
			$("#remove-modal").modal("hide")
			if(data.response) {
				Qreferee.App.Sport.list()
			}

			if(data.error){
				$('#row-alert-message').html(data.error).removeClass('alert-success').addClass('alert-danger').show().delay(2000).fadeOut(function(){
		 			$(this).html('');
		 			$(this).removeClass('alert-danger');
		    	});
			}

			Qreferee.App.Sport.list(globalPage,globalDirection,globalkeyword,globalUserType);

		});
	};

	var view = function(id){
		
		$('#update_first_name').val('');
		$('#update_last_name').val('');
		$('#update_password').val('');
		$('#update_verify_password').val('');
		$('#update_email').val('');
		$('#update_phone_number').val('');
		$('#update_role').val('');
		/////$('input[name="status"]').val('on');

		
		var request = $.ajax({
			url: userApiUrl+'/view',
			data: {id:id},
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			/*console.log(data);
			if(data.error){
				window.location.href = config.getAppUrl()+'401';
			}*/

			if( data && (typeof data != "undefined") ){

				$('#user_update_first_name').val(data.first_name);
				$('#user_update_last_name').val(data.last_name);
				$('#user_update_email').val(data.email);
				$('#user_update_phone_number').val(data.contact_number);
				$('#user_update_address').val(data.address);
				$('#user_update_zip_code').val(data.zip_code);
				$('#user_update_country').val(data.country_id);
				$('#user_update_role').val(data.code);
				$('input[name="update-status"][value="off"]').prop('checked', true);
				
				if (data.status == 'on') {
					$('input[name="update-status"][value="on"]').prop('checked', true);
				} 

			}

			$('#user-update-popup').modal('show');

	        $('.update_user').unbind('click').bind('click',function(e){
	            event.preventDefault();
	            Qreferee.App.User.update(data.id);
	        });

		});
	};

    return {
		list:list,
		remove:remove, 
		view:view,
        create:create,
        update:update
	}
}());
