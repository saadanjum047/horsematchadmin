
var Qreferee = Qreferee || {}; // "If Qreferee is not defined, make it equal to an empty object"
Qreferee.App = Qreferee.App || {};
Qreferee.Config = Qreferee.Config || {};
var errors = {};
window.localStorage;

Qreferee.Config = (function(){

	if( window.location.host == 'dev-cmolds.com' || window.location.host == 'localhost' || window.location.host == $.trim("localhost:8080") ){
		var apiUrl = $('.serverurl').val()+'/public/api';
        console.log(apiUrl, 'apiUrl');
		var appUrl = $('.serverurl').val();
		var imageUrl = $('.serverurl').val()+'/public';
		var assetUrl = $('.serverurl').val()+'/public';
	} else {
		var apiUrl = $('.serverurl').val()+'/public/api';
		var appUrl = $('.serverurl').val();
		var imageUrl = $('.serverurl').val()+'/';
		var assetUrl = $('.serverurl').val()+'/';
	}

	var getApiUrl = function(){
		return apiUrl;
	};
	var getAppUrl = function(){
		return appUrl;
	};
	var getImageUrl = function(){
		return imageUrl;
	};
	var getAssetUrl = function(){
		return assetUrl;
	};

	return {
		getApiUrl:getApiUrl,
		getAppUrl:getAppUrl,
		getImageUrl:getImageUrl,
		getAssetUrl:getAssetUrl
	}
}());

Qreferee.App = (function () {
	var config = Qreferee.Config;

	var init = function () {
		if (!window.console) window.console = {log: function(obj) {}};
		console.log('Application has been initialized...');
	};

	var minLimit = function(min,value){
    	if(value.length < min){
      		return false;
     	}else{
      		return true;
     	}
    };

    function htmlEntities(str) {
	    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}

    var getWords = function( word, limit ){
        return word.split(/\s+/).slice(0,limit).join(" ");
    };

    var logout = function(){

        console.log(Qreferee.Config.getApiUrl()+'/user/logout');

        var request = $.ajax({
            url: Qreferee.Config.getApiUrl()+'/user/logout',
            type: 'POST',
            dataType:'json',
            headers: {"Authorization": "Bearer "+localStorage.getItem('token')}
        });

        request.done(function(data){
            console.log(data);
            if( typeof data.response != "undefined" && data.response.code == 200 ) {
                localStorage.removeItem('token');
                window.location.href = config.getAppUrl();
            }
        });

    }

	return {
			init:init,
			minLimit:minLimit,
			htmlEntities:htmlEntities,
            getWords:getWords,
            logout:logout
	};
}());
