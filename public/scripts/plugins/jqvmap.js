(function ($) {
	"use strict";

  var init = function(){
    
    // world map
    

    // usa map
    $('#jqvmap-usa').each(function(){
      $(this).vectorMap(
        {
          map: 'usa_en',
          backgroundColor: 'transparent',
          borderColor: '#ffffff',
          borderWidth: 0.25,
          borderOpacity: 0.25,
          color: hexToRGB(app.color.primary, 0.3),
          colors: {
              ca: app.color.primary,
              fl: app.color.accent,
              ny: app.color.warn,
              mo: hexToRGB(app.color.primary, 0.5),
              or: hexToRGB(app.color.warn, 0.5)
          }
        }
      );
    });

  
  }

  // for ajax to init again
  $.fn.vectorMap.init = init;

})(jQuery);
